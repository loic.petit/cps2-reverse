EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 17
Title "Top Level View"
Date "2019-10-17"
Rev ""
Comp ""
Comment1 ""
Comment2 "Licence: CC-BY"
Comment3 "Author: Loic *WydD* Petit"
Comment4 "Project: CPS2 Reverse Engineering: 93646B"
$EndDescr
Connection ~ 14200 6800
Connection ~ 12300 5600
Connection ~ 12100 5250
Connection ~ 12000 6550
Connection ~ 12200 6900
Connection ~ 12000 5100
Connection ~ 10200 7000
Connection ~ 10100 8200
Connection ~ 3100 2150
Connection ~ 750  4250
Connection ~ 850  4100
Connection ~ 950  3850
Connection ~ 1050 3750
Connection ~ 1800 9850
Connection ~ 1800 8850
Connection ~ 2450 9350
Connection ~ 1800 9350
Connection ~ 4650 5050
Connection ~ 4750 5250
Connection ~ 10300 6550
Connection ~ 4500 9700
Connection ~ 4950 9700
Connection ~ 5400 9700
Connection ~ 5850 9700
Connection ~ 6300 9700
Connection ~ 6750 9700
Connection ~ 7200 9700
Connection ~ 7650 9700
Connection ~ 8100 9700
Connection ~ 8550 9700
Connection ~ 9000 9700
Connection ~ 9450 9700
Connection ~ 9900 9700
Connection ~ 4500 10000
Connection ~ 4950 10000
Connection ~ 5400 10000
Connection ~ 5850 10000
Connection ~ 6300 10000
Connection ~ 6750 10000
Connection ~ 7200 10000
Connection ~ 7650 10000
Connection ~ 8100 10000
Connection ~ 8550 10000
Connection ~ 9000 10000
Connection ~ 9450 10000
Connection ~ 9900 10000
Connection ~ 2100 8850
Connection ~ 1800 10150
Entry Wire Line
	3300 1850 3400 1950
Entry Wire Line
	3300 1950 3400 2050
Entry Wire Line
	3300 2050 3400 2150
Entry Wire Line
	3300 2150 3400 2250
Entry Wire Line
	4850 3650 4750 3750
Entry Bus Bus
	3400 2250 3500 2350
Wire Wire Line
	750  2400 750  4250
Wire Wire Line
	750  4250 750  5800
Wire Wire Line
	950  2600 950  3850
Wire Wire Line
	950  3850 950  5500
Wire Wire Line
	1050 2700 1050 3750
Wire Wire Line
	1050 3750 1050 5400
Wire Wire Line
	1050 5400 1300 5400
Wire Wire Line
	1150 2800 1150 3650
Wire Wire Line
	1150 3650 1300 3650
Wire Wire Line
	1300 3750 1050 3750
Wire Wire Line
	1300 3850 950  3850
Wire Wire Line
	1300 4250 750  4250
Wire Wire Line
	1300 5500 950  5500
Wire Wire Line
	1300 5800 750  5800
Wire Wire Line
	1500 8850 1800 8850
Wire Wire Line
	1500 9150 1500 8850
Wire Wire Line
	1500 9550 1500 9850
Wire Wire Line
	1500 9850 1800 9850
Wire Wire Line
	1500 10150 1800 10150
Wire Wire Line
	1800 8850 2100 8850
Wire Wire Line
	1800 9850 2100 9850
Wire Wire Line
	2100 8850 2100 9150
Wire Wire Line
	2100 9850 2100 9550
Wire Wire Line
	2450 8850 2100 8850
Wire Wire Line
	2450 9350 1800 9350
Wire Wire Line
	2450 9550 2450 9350
Wire Wire Line
	2750 9350 2450 9350
Wire Wire Line
	2800 1850 3300 1850
Wire Wire Line
	2800 1950 3300 1950
Wire Wire Line
	2800 2050 3300 2050
Wire Wire Line
	2800 2150 3100 2150
Wire Wire Line
	2800 3200 2900 3200
Wire Wire Line
	2800 3300 2900 3300
Wire Wire Line
	2800 3650 2900 3650
Wire Wire Line
	2800 6200 5000 6200
Wire Wire Line
	2900 3300 2900 3650
Wire Wire Line
	2950 1750 2800 1750
Wire Wire Line
	2950 2250 2800 2250
Wire Wire Line
	3100 2150 3100 2400
Wire Wire Line
	3100 2150 3300 2150
Wire Wire Line
	3100 2400 750  2400
Wire Wire Line
	4050 10000 4500 10000
Wire Wire Line
	4500 5450 5150 5450
Wire Wire Line
	4500 5700 5150 5700
Wire Wire Line
	4500 9700 4050 9700
Wire Wire Line
	4500 10000 4950 10000
Wire Wire Line
	4700 6050 5150 6050
Wire Wire Line
	4850 3650 5150 3650
Wire Wire Line
	4950 1900 5150 1900
Wire Wire Line
	4950 2100 5150 2100
Wire Wire Line
	4950 9700 4500 9700
Wire Wire Line
	4950 10000 5400 10000
Wire Wire Line
	5000 6200 5000 8600
Wire Wire Line
	5000 8600 6900 8600
Wire Wire Line
	5150 1800 4950 1800
Wire Wire Line
	5150 2000 4950 2000
Wire Wire Line
	5150 2600 950  2600
Wire Wire Line
	5150 2700 1050 2700
Wire Wire Line
	5150 2800 1150 2800
Wire Wire Line
	5150 5550 4500 5550
Wire Wire Line
	5150 5800 4500 5800
Wire Wire Line
	5400 9700 4950 9700
Wire Wire Line
	5400 10000 5850 10000
Wire Wire Line
	5850 9700 5400 9700
Wire Wire Line
	5850 10000 6300 10000
Wire Wire Line
	6300 9700 5850 9700
Wire Wire Line
	6300 10000 6750 10000
Wire Wire Line
	6750 9700 6300 9700
Wire Wire Line
	6750 10000 7200 10000
Wire Wire Line
	6800 4600 7000 4600
Wire Wire Line
	6800 6450 7100 6450
Wire Wire Line
	6800 7450 7100 7450
Wire Wire Line
	6800 8100 7200 8100
Wire Wire Line
	6800 8300 6900 8300
Wire Wire Line
	6900 8600 6900 8300
Wire Wire Line
	7000 3400 6800 3400
Wire Wire Line
	7100 6450 7100 7450
Wire Wire Line
	7200 9700 6750 9700
Wire Wire Line
	7200 10000 7650 10000
Wire Wire Line
	7200 6350 7200 8100
Wire Wire Line
	7400 6150 7400 7000
Wire Wire Line
	7650 9700 7200 9700
Wire Wire Line
	7650 10000 8100 10000
Wire Wire Line
	8100 9700 7650 9700
Wire Wire Line
	8100 10000 8550 10000
Wire Wire Line
	8550 9700 8100 9700
Wire Wire Line
	8550 10000 9000 10000
Wire Wire Line
	9000 9700 8550 9700
Wire Wire Line
	9000 10000 9450 10000
Wire Wire Line
	9300 2250 6800 2250
Wire Wire Line
	9300 2250 9300 5250
Wire Wire Line
	9450 9700 9000 9700
Wire Wire Line
	9450 10000 9900 10000
Wire Wire Line
	9900 9700 9450 9700
Wire Wire Line
	9900 10000 10350 10000
Wire Wire Line
	10100 7100 10100 8200
Wire Wire Line
	10100 8200 12200 8200
Wire Wire Line
	10200 5400 10200 7000
Wire Wire Line
	10200 5400 10400 5400
Wire Wire Line
	10200 7000 10400 7000
Wire Wire Line
	10400 6400 10000 6400
Wire Wire Line
	10400 7100 10100 7100
Wire Wire Line
	10350 9700 9900 9700
Wire Wire Line
	12200 7200 12200 8200
Wire Wire Line
	12200 7200 12500 7200
Wire Wire Line
	12300 3800 10000 3800
Wire Wire Line
	12300 7350 12300 8300
Wire Wire Line
	12500 7350 12300 7350
Wire Wire Line
	13900 3450 14200 3450
Wire Wire Line
	13900 3800 15850 3800
Wire Wire Line
	13950 7050 14450 7050
Wire Wire Line
	13950 7350 15850 7350
Wire Wire Line
	14200 3550 13900 3550
Wire Wire Line
	14450 6950 13950 6950
Wire Wire Line
	15500 6950 15750 6950
Wire Wire Line
	15750 4050 13900 4050
Wire Wire Line
	15750 6950 15750 4050
Wire Wire Line
	15850 3800 15850 7350
Wire Bus Line
	650  6200 650  1200
Wire Bus Line
	850  2500 850  4100
Wire Bus Line
	850  4100 850  5650
Wire Bus Line
	850  5650 1300 5650
Wire Bus Line
	1150 4400 1150 5250
Wire Bus Line
	1150 5250 1300 5250
Wire Bus Line
	1300 4100 850  4100
Wire Bus Line
	1300 4400 1150 4400
Wire Bus Line
	1300 6200 650  6200
Wire Bus Line
	2800 4000 2900 4000
Wire Bus Line
	3500 2350 5150 2350
Wire Bus Line
	4500 3800 4750 3800
Wire Bus Line
	4500 3950 4650 3950
Wire Bus Line
	4500 5050 4650 5050
Wire Bus Line
	4500 5250 4750 5250
Wire Bus Line
	4650 5050 5150 5050
Wire Bus Line
	4750 3750 4750 3800
Wire Bus Line
	4750 5250 5150 5250
Wire Bus Line
	4950 1650 5150 1650
Wire Bus Line
	5100 7450 5750 7450
Wire Bus Line
	5150 1500 4950 1500
Wire Bus Line
	5150 2500 850  2500
Wire Bus Line
	6800 1800 9600 1800
Wire Bus Line
	6800 1950 9500 1950
Wire Bus Line
	6800 2100 9400 2100
Wire Bus Line
	6800 2900 7300 2900
Wire Bus Line
	6800 3550 6900 3550
Wire Bus Line
	6800 5550 7300 5550
Wire Bus Line
	6800 7700 7500 7700
Wire Bus Line
	6900 4450 6800 4450
Wire Bus Line
	7300 2900 7300 5550
Wire Bus Line
	9400 2100 9400 5400
Wire Bus Line
	9500 1950 9500 5550
Wire Bus Line
	9600 1800 9600 5700
Wire Bus Line
	9700 1650 6800 1650
Wire Bus Line
	9700 1650 9700 5850
Wire Bus Line
	7600 7250 7600 7850
Wire Bus Line
	10300 5600 10300 6550
Wire Bus Line
	10400 5600 10300 5600
Wire Bus Line
	10400 6550 10300 6550
Wire Bus Line
	11900 5600 12300 5600
Wire Bus Line
	11900 6900 12200 6900
Wire Bus Line
	12000 3950 12000 5100
Wire Bus Line
	12000 3950 12300 3950
Wire Bus Line
	12000 6550 11900 6550
Wire Bus Line
	12000 6550 12000 5100
Wire Bus Line
	12050 1200 12050 3150
Wire Bus Line
	12050 3150 12300 3150
Wire Bus Line
	12100 4100 12100 5250
Wire Bus Line
	12100 4100 12300 4100
Wire Bus Line
	12100 5250 11900 5250
Wire Bus Line
	12100 6700 12100 5250
Wire Bus Line
	12200 6900 12200 5450
Wire Bus Line
	12300 5600 12300 7050
Wire Bus Line
	12300 5600 12500 5600
Wire Bus Line
	12500 5100 12000 5100
Wire Bus Line
	12500 5250 12100 5250
Wire Bus Line
	12500 5450 12200 5450
Wire Bus Line
	12500 6550 12000 6550
Wire Bus Line
	12500 6700 12100 6700
Wire Bus Line
	12500 6900 12200 6900
Wire Bus Line
	12500 7050 12300 7050
Wire Bus Line
	13900 3150 14200 3150
Wire Bus Line
	13900 4250 15650 4250
Wire Bus Line
	13900 4400 14200 4400
Wire Bus Line
	13950 6800 14200 6800
Wire Bus Line
	14200 3300 13900 3300
Wire Bus Line
	14200 4400 14200 6800
Wire Bus Line
	14200 6800 14450 6800
Wire Bus Line
	15650 4250 15650 6800
Wire Bus Line
	15650 6800 15500 6800
Wire Notes Line
	1300 8350 1300 10400
Wire Notes Line
	1300 10400 3000 10400
Wire Notes Line
	3000 8350 1300 8350
Wire Notes Line
	3000 8350 3000 10400
Wire Notes Line
	3850 9450 3850 10400
Wire Notes Line
	3850 10400 10750 10400
Wire Notes Line
	10750 9450 3850 9450
Wire Notes Line
	10750 10400 10750 9450
Text Notes 1350 8550 0    50   ~ 0
Dual diode D5 can be replaced by\nstandard diodes in D2-D3-D4 slots
Text Notes 2250 10350 0    50   ~ 0
VDD Power Supply
Text Notes 6150 10300 0    50   ~ 0
Capacitors are spread across the board to deliver VCC
Text Label 2850 1850 0    50   ~ 0
SECURITY2
Text Label 2850 1950 0    50   ~ 0
SECURITY3
Text Label 2850 2050 0    50   ~ 0
SECURITY4
Text Label 2850 2150 0    50   ~ 0
SECURITY5
Text Label 3600 2350 0    50   ~ 0
SECURITY[2..5]
Text Label 4700 6050 0    50   ~ 0
SECURITY5
Text Label 4750 4050 0    50   ~ 0
BUS-DATA[0..15]
Text Label 5100 3000 2    50   ~ 0
~VBLANK
Text Label 5100 3200 2    50   ~ 0
CLK4M
Text Label 5100 7450 0    50   ~ 0
SECURITY[2..5]
Text Label 5150 3650 2    50   ~ 0
BUS-DATA0
Text Label 5650 8100 2    50   ~ 0
~VBLANK
Text Label 5400 8300 0    50   ~ 0
CLK4M
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5E23EE9C
P 1500 10150
AR Path="/6492EFBF/5E23EE9C" Ref="#FLG?"  Part="1" 
AR Path="/5E23EE9C" Ref="#FLG0102"  Part="1" 
F 0 "#FLG0102" H 1500 10225 50  0001 C CNN
F 1 "PWR_FLAG" H 1500 10324 50  0001 C CNN
F 2 "" H 1500 10150 50  0001 C CNN
F 3 "~" H 1500 10150 50  0001 C CNN
	1    1500 10150
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5E23EE96
P 2450 8850
AR Path="/6492EFBF/5E23EE96" Ref="#FLG?"  Part="1" 
AR Path="/5E23EE96" Ref="#FLG0101"  Part="1" 
F 0 "#FLG0101" H 2450 8925 50  0001 C CNN
F 1 "PWR_FLAG" H 2450 9024 50  0001 C CNN
F 2 "" H 2450 8850 50  0001 C CNN
F 3 "~" H 2450 8850 50  0001 C CNN
	1    2450 8850
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5E2B3D15
P 2750 9350
AR Path="/6492EFBF/5E2B3D15" Ref="#FLG?"  Part="1" 
AR Path="/5E2B3D15" Ref="#FLG0103"  Part="1" 
F 0 "#FLG0103" H 2750 9425 50  0001 C CNN
F 1 "PWR_FLAG" H 2750 9524 50  0001 C CNN
F 2 "" H 2750 9350 50  0001 C CNN
F 3 "~" H 2750 9350 50  0001 C CNN
	1    2750 9350
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR01
U 1 1 5E6EC31C
P 1800 8850
F 0 "#PWR01" H 1800 8700 50  0001 C CNN
F 1 "VCC" H 1817 9023 50  0000 C CNN
F 2 "" H 1800 8850 50  0001 C CNN
F 3 "" H 1800 8850 50  0001 C CNN
	1    1800 8850
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR?
U 1 1 5E6C1C93
P 2450 9350
AR Path="/5E35B989/5E6C1C93" Ref="#PWR?"  Part="1" 
AR Path="/5E6C1C93" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 2450 9200 50  0001 C CNN
F 1 "VDD" H 2467 9523 50  0000 C CNN
F 2 "" H 2450 9350 50  0001 C CNN
F 3 "" H 2450 9350 50  0001 C CNN
	1    2450 9350
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 5DB29546
P 2900 3200
AR Path="/6492EFBF/5DB29546" Ref="#PWR?"  Part="1" 
AR Path="/5DB29546" Ref="#PWR0105"  Part="1" 
F 0 "#PWR0105" H 2900 3050 50  0001 C CNN
F 1 "VCC" H 2917 3373 50  0000 C CNN
F 2 "" H 2900 3200 50  0001 C CNN
F 3 "" H 2900 3200 50  0001 C CNN
	1    2900 3200
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 651E57F3
P 2950 1750
AR Path="/6492EFBF/651E57F3" Ref="#PWR?"  Part="1" 
AR Path="/651E57F3" Ref="#PWR0102"  Part="1" 
AR Path="/5DB29FF0/651E57F3" Ref="#PWR?"  Part="1" 
F 0 "#PWR0102" H 2950 1600 50  0001 C CNN
F 1 "VCC" H 2967 1923 50  0000 C TNN
F 2 "" H 2950 1750 50  0001 C CNN
F 3 "" H 2950 1750 50  0001 C CNN
	1    2950 1750
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR05
U 1 1 5E17B5BD
P 7200 9700
F 0 "#PWR05" H 7200 9550 50  0001 C CNN
F 1 "VCC" H 7217 9873 50  0000 C CNN
F 2 "" H 7200 9700 50  0001 C CNN
F 3 "" H 7200 9700 50  0001 C CNN
	1    7200 9700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E6C1C8D
P 1800 10150
AR Path="/5E35B989/5E6C1C8D" Ref="#PWR?"  Part="1" 
AR Path="/5E6C1C8D" Ref="#PWR02"  Part="1" 
F 0 "#PWR02" H 1800 9900 50  0001 C CNN
F 1 "GND" H 1805 9977 50  0001 C CNN
F 2 "" H 1800 10150 50  0001 C CNN
F 3 "" H 1800 10150 50  0001 C CNN
	1    1800 10150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E7F8FB8
P 2450 9850
AR Path="/5E35B989/5E7F8FB8" Ref="#PWR?"  Part="1" 
AR Path="/5E7F8FB8" Ref="#PWR04"  Part="1" 
F 0 "#PWR04" H 2450 9600 50  0001 C CNN
F 1 "GND" H 2455 9677 50  0001 C CNN
F 2 "" H 2450 9850 50  0001 C CNN
F 3 "" H 2450 9850 50  0001 C CNN
	1    2450 9850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 651E57EC
P 2950 2250
AR Path="/6492EFBF/651E57EC" Ref="#PWR?"  Part="1" 
AR Path="/651E57EC" Ref="#PWR0101"  Part="1" 
AR Path="/5DB29FF0/651E57EC" Ref="#PWR?"  Part="1" 
F 0 "#PWR0101" H 2950 2000 50  0001 C CNN
F 1 "GND" H 2955 2077 50  0001 C CNN
F 2 "" H 2950 2250 50  0001 C CNN
F 3 "" H 2950 2250 50  0001 C CNN
	1    2950 2250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E19D9A4
P 7200 10000
AR Path="/5E35B989/5E19D9A4" Ref="#PWR?"  Part="1" 
AR Path="/5E19D9A4" Ref="#PWR06"  Part="1" 
F 0 "#PWR06" H 7200 9750 50  0001 C CNN
F 1 "GND" H 7205 9827 50  0001 C CNN
F 2 "" H 7200 10000 50  0001 C CNN
F 3 "" H 7200 10000 50  0001 C CNN
	1    7200 10000
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male CN8
U 1 1 5DB289C6
P 2600 3300
F 0 "CN8" H 2550 3250 50  0000 R CNN
F 1 "Conn_01x02_Male" H 2572 3271 50  0001 R CNN
F 2 "" H 2600 3300 50  0001 C CNN
F 3 "~" H 2600 3300 50  0001 C CNN
	1    2600 3300
	1    0    0    1   
$EndComp
$Comp
L Device:Battery_Cell BATT1
U 1 1 5E6EC39D
P 1800 10050
F 0 "BATT1" H 1918 10146 50  0000 L CNN
F 1 "3.2V" H 1918 10055 50  0000 L CNN
F 2 "" V 1800 10110 50  0001 C CNN
F 3 "~" V 1800 10110 50  0001 C CNN
	1    1800 10050
	1    0    0    -1  
$EndComp
$Comp
L Device:CP CC1
U 1 1 5E7F8F88
P 2450 9700
F 0 "CC1" H 2568 9746 50  0000 L CNN
F 1 "10u" H 2568 9655 50  0000 L CNN
F 2 "" H 2488 9550 50  0001 C CNN
F 3 "~" H 2450 9700 50  0001 C CNN
	1    2450 9700
	1    0    0    -1  
$EndComp
$Comp
L Device:CP CCX1
U 1 1 5E12F01E
P 4050 9850
F 0 "CCX1" H 4168 9896 50  0000 L CNN
F 1 "10u" H 4168 9805 50  0000 L CNN
F 2 "" H 4088 9700 50  0001 C CNN
F 3 "~" H 4050 9850 50  0001 C CNN
	1    4050 9850
	1    0    0    -1  
$EndComp
$Comp
L Device:CP CCX2
U 1 1 5E12F12C
P 4500 9850
F 0 "CCX2" H 4618 9896 50  0000 L CNN
F 1 "10u" H 4618 9805 50  0000 L CNN
F 2 "" H 4538 9700 50  0001 C CNN
F 3 "~" H 4500 9850 50  0001 C CNN
	1    4500 9850
	1    0    0    -1  
$EndComp
$Comp
L Device:CP CCX3
U 1 1 5E139EC1
P 4950 9850
F 0 "CCX3" H 5068 9896 50  0000 L CNN
F 1 "10u" H 5068 9805 50  0000 L CNN
F 2 "" H 4988 9700 50  0001 C CNN
F 3 "~" H 4950 9850 50  0001 C CNN
	1    4950 9850
	1    0    0    -1  
$EndComp
$Comp
L Device:CP CCX4
U 1 1 5E139EC8
P 5400 9850
F 0 "CCX4" H 5518 9896 50  0000 L CNN
F 1 "10u" H 5518 9805 50  0000 L CNN
F 2 "" H 5438 9700 50  0001 C CNN
F 3 "~" H 5400 9850 50  0001 C CNN
	1    5400 9850
	1    0    0    -1  
$EndComp
$Comp
L Device:CP CCX5
U 1 1 5E144CBF
P 5850 9850
F 0 "CCX5" H 5968 9896 50  0000 L CNN
F 1 "10u" H 5968 9805 50  0000 L CNN
F 2 "" H 5888 9700 50  0001 C CNN
F 3 "~" H 5850 9850 50  0001 C CNN
	1    5850 9850
	1    0    0    -1  
$EndComp
$Comp
L Device:CP CCX6
U 1 1 5E144CC6
P 6300 9850
F 0 "CCX6" H 6418 9896 50  0000 L CNN
F 1 "10u" H 6418 9805 50  0000 L CNN
F 2 "" H 6338 9700 50  0001 C CNN
F 3 "~" H 6300 9850 50  0001 C CNN
	1    6300 9850
	1    0    0    -1  
$EndComp
$Comp
L Device:CP CCX7
U 1 1 5E144CCD
P 6750 9850
F 0 "CCX7" H 6868 9896 50  0000 L CNN
F 1 "10u" H 6868 9805 50  0000 L CNN
F 2 "" H 6788 9700 50  0001 C CNN
F 3 "~" H 6750 9850 50  0001 C CNN
	1    6750 9850
	1    0    0    -1  
$EndComp
$Comp
L Device:CP CCX8
U 1 1 5E144CD4
P 7200 9850
F 0 "CCX8" H 7318 9896 50  0000 L CNN
F 1 "10u" H 7318 9805 50  0000 L CNN
F 2 "" H 7238 9700 50  0001 C CNN
F 3 "~" H 7200 9850 50  0001 C CNN
	1    7200 9850
	1    0    0    -1  
$EndComp
$Comp
L Device:CP CCX9
U 1 1 5E15A961
P 7650 9850
F 0 "CCX9" H 7768 9896 50  0000 L CNN
F 1 "10u" H 7768 9805 50  0000 L CNN
F 2 "" H 7688 9700 50  0001 C CNN
F 3 "~" H 7650 9850 50  0001 C CNN
	1    7650 9850
	1    0    0    -1  
$EndComp
$Comp
L Device:CP CCX10
U 1 1 5E15A968
P 8100 9850
F 0 "CCX10" H 8218 9896 50  0000 L CNN
F 1 "10u" H 8218 9805 50  0000 L CNN
F 2 "" H 8138 9700 50  0001 C CNN
F 3 "~" H 8100 9850 50  0001 C CNN
	1    8100 9850
	1    0    0    -1  
$EndComp
$Comp
L Device:CP CCX11
U 1 1 5E15A96F
P 8550 9850
F 0 "CCX11" H 8668 9896 50  0000 L CNN
F 1 "10u" H 8668 9805 50  0000 L CNN
F 2 "" H 8588 9700 50  0001 C CNN
F 3 "~" H 8550 9850 50  0001 C CNN
	1    8550 9850
	1    0    0    -1  
$EndComp
$Comp
L Device:CP CCX12
U 1 1 5E15A976
P 9000 9850
F 0 "CCX12" H 9118 9896 50  0000 L CNN
F 1 "10u" H 9118 9805 50  0000 L CNN
F 2 "" H 9038 9700 50  0001 C CNN
F 3 "~" H 9000 9850 50  0001 C CNN
	1    9000 9850
	1    0    0    -1  
$EndComp
$Comp
L Device:CP CCX13
U 1 1 5E15A97D
P 9450 9850
F 0 "CCX13" H 9568 9896 50  0000 L CNN
F 1 "10u" H 9568 9805 50  0000 L CNN
F 2 "" H 9488 9700 50  0001 C CNN
F 3 "~" H 9450 9850 50  0001 C CNN
	1    9450 9850
	1    0    0    -1  
$EndComp
$Comp
L Device:CP CCX14
U 1 1 5E15A984
P 9900 9850
F 0 "CCX14" H 10018 9896 50  0000 L CNN
F 1 "10u" H 10018 9805 50  0000 L CNN
F 2 "" H 9938 9700 50  0001 C CNN
F 3 "~" H 9900 9850 50  0001 C CNN
	1    9900 9850
	1    0    0    -1  
$EndComp
$Comp
L Device:CP CCX15
U 1 1 5E15A98B
P 10350 9850
F 0 "CCX15" H 10468 9896 50  0000 L CNN
F 1 "10u" H 10468 9805 50  0000 L CNN
F 2 "" H 10388 9700 50  0001 C CNN
F 3 "~" H 10350 9850 50  0001 C CNN
	1    10350 9850
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x06_Male CN9
U 1 1 651E23DB
P 2600 1950
F 0 "CN9" H 2500 1900 50  0000 C CNN
F 1 "Conn_01x06_Male" H 2706 2237 50  0001 C CNN
F 2 "" H 2600 1950 50  0001 C CNN
F 3 "~" H 2600 1950 50  0001 C CNN
	1    2600 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:D_x2_KCom_AAK D5
U 1 1 5E780652
P 1800 9150
F 0 "D5" H 1800 9274 50  0000 C CNN
F 1 "D_x2_KCom_AAK" H 1800 9365 50  0001 C CNN
F 2 "" H 1800 9150 50  0001 C CNN
F 3 "~" H 1800 9150 50  0001 C CNN
	1    1800 9150
	1    0    0    -1  
$EndComp
$Comp
L Device:D_x2_KCom_AAK D1
U 1 1 5E702042
P 1800 9550
F 0 "D1" H 1800 9674 50  0000 C CNN
F 1 "D_x2_KCom_AAK" H 1800 9765 50  0001 C CNN
F 2 "" H 1800 9550 50  0001 C CNN
F 3 "~" H 1800 9550 50  0001 C CNN
	1    1800 9550
	1    0    0    1   
$EndComp
$Sheet
S 1300 3500 1500 1050
U 6492EFBF
F0 "CN1" 50
F1 "CN1.sch" 50
F2 "CODE-DATA[0..15]" B R 2800 4000 50 
F3 "C[17..25]" B L 1300 4400 50 
F4 "A5" O R 2800 3650 50 
F5 "CONTROL-OUT[1..6]" I L 1300 4100 50 
F6 "CODE-ADDR[1..23]" B R 2800 3850 50 
F7 "SECURITY5" O L 1300 4250 50 
F8 "~CPSB-OBJUP" O L 1300 3850 50 
F9 "~CPSA-BR" O L 1300 3650 50 
F10 "~Z80-BUS" O L 1300 3750 50 
$EndSheet
$Sheet
S 12300 3000 1600 1600
U 6466CFCE
F0 "CN3" 50
F1 "CN3.sch" 50
F2 "~GFX-OE" I R 13900 3800 50 
F3 "GFXA-DATA[0..31]" I L 12300 4100 50 
F4 "ENABLE3" O L 12300 3800 50 
F5 "QSOUND-DATA[0..7]" I R 13900 4250 50 
F6 "QSOUND-ADDR[0..22]" O R 13900 4400 50 
F7 "QSOUND-ENABLE" O R 13900 4050 50 
F8 "EEPROM[2..5]" B L 12300 3150 50 
F9 "SOUND-DATA[0..7]" I R 13900 3300 50 
F10 "SOUND-ADDR[0..16]" O R 13900 3150 50 
F11 "~SOUND-E2" O R 13900 3550 50 
F12 "~SOUND-E1" O R 13900 3450 50 
F13 "GFXA-ADDR[2..23]" B L 12300 3950 50 
F14 "~A-BOARD-CONNECTED" O L 12300 3700 50 
$EndSheet
$Sheet
S 12500 4950 900  800 
U 644BB5C1
F0 "CN4" 50
F1 "CN4.sch" 50
F2 "GFXB-DATA[0..31]" I L 12500 5600 50 
F3 "GFXA-DATA[0..31]" I L 12500 5250 50 
F4 "GFXB-ADDR[2..23]" B L 12500 5450 50 
F5 "GFXA-ADDR[2..23]" B L 12500 5100 50 
$EndSheet
$Sheet
S 1300 5050 1500 1300
U 5DB29FF0
F0 "CN7" 50
F1 "CN7.sch" 50
F2 "EEPROM[1..5]" B L 1300 6200 50 
F3 "CODE-DATA[0..7]" B R 2800 5250 50 
F4 "CN1-C[17..25]" B L 1300 5250 50 
F5 "CONTROL-OUT[2..3]" I L 1300 5650 50 
F6 "CODE-ADDR[1..16]" B R 2800 5450 50 
F7 "CLK8M" I R 2800 6200 50 
F8 "~CPSB-OBJUP" I L 1300 5500 50 
F9 "~Z80-BUS" I L 1300 5400 50 
F10 "SECURITY5" I L 1300 5800 50 
$EndSheet
$Sheet
S 4350 1400 600  800 
U 5E37B0FC
F0 "SRAM" 50
F1 "SRAM.sch" 50
F2 "ADDR[0..12]" I R 4950 1500 50 
F3 "DATA[0..15]" B R 4950 1650 50 
F4 "~SRAM-E" I R 4950 2100 50 
F5 "~SRAM-OE" I R 4950 2000 50 
F6 "~SRAM-WL" I R 4950 1900 50 
F7 "~SRAM-WH" I R 4950 1800 50 
$EndSheet
$Sheet
S 3650 3350 850  700 
U 5D957A6E
F0 "code-rom" 50
F1 "code-rom.sch" 50
F2 "DATA[0..15]" O R 4500 3800 50 
F3 "~OE" I R 4500 3550 50 
F4 "~E" I R 4500 3450 50 
F5 "ADDR[1..23]" I R 4500 3950 50 
$EndSheet
$Sheet
S 12500 6400 1450 1100
U 5E05893C
F0 "gfx-rom" 50
F1 "gfx-rom.sch" 50
F2 "A-DATA[0..31]" O L 12500 6700 50 
F3 "B-DATA[0..31]" O L 12500 7050 50 
F4 "~OE" O R 13950 7350 50 
F5 "~QROM-E11" O R 13950 6950 50 
F6 "~QROM-E12" O R 13950 7050 50 
F7 "A-ADDR[2..23]" I L 12500 6550 50 
F8 "B-ADDR[2..23]" I L 12500 6900 50 
F9 "CLK8M" I L 12500 7350 50 
F10 "CLK2M" I L 12500 7200 50 
F11 "QS-ADDR[20..22]" I R 13950 6800 50 
$EndSheet
$Sheet
S 14450 6650 1050 550 
U 62E08D9B
F0 "qsound-rom" 50
F1 "qsound-rom.sch" 50
F2 "ADDR[0..21]" I L 14450 6800 50 
F3 "OUT[0..7]" O R 15500 6800 50 
F4 "ENABLE" I R 15500 6950 50 
F5 "~E12" I L 14450 7050 50 
F6 "~E11" I L 14450 6950 50 
$EndSheet
$Sheet
S 3150 4900 1350 1050
U 635BEEB0
F0 "rom-bus" 50
F1 "rom-bus.sch" 50
F2 "B-DATA[0..15]" B L 3150 5250 50 
F3 "A-DATA[0..15]" B R 4500 5250 50 
F4 "ADDR-DIR" I R 4500 5450 50 
F5 "~ADDR-E" I R 4500 5550 50 
F6 "~DATA-E" I R 4500 5800 50 
F7 "DATA-DIR" I R 4500 5700 50 
F8 "A-ADDR[1..23]" I R 4500 5050 50 
F9 "B-ADDR[1..23]" O L 3150 5050 50 
$EndSheet
$Sheet
S 14200 3050 750  600 
U 63F411DC
F0 "sound-rom" 50
F1 "sound-rom.sch" 50
F2 "A[0..16]" I L 14200 3150 50 
F3 "D[0..7]" O L 14200 3300 50 
F4 "~E2" I L 14200 3550 50 
F5 "~E1" I L 14200 3450 50 
$EndSheet
$Sheet
S 7600 5150 1600 1700
U 5D9F70E5
F0 "processor" 50
F1 "processor.sch" 50
F2 "CPU-DATA[0..15]" I L 7600 5700 50 
F3 "GFX-OUT[0..15]" O L 7600 6550 50 
F4 "GFX-BUS[0..31]" B R 9200 6550 50 
F5 "GFX-DATA-E" O R 9200 6400 50 
F6 "CLK" I R 9200 5250 50 
F7 "ENABLE" I L 7600 6150 50 
F8 "SECURITY[2..5]" I R 9200 5550 50 
F9 "CPU-ADDR[1..23]" O L 7600 5550 50 
F10 "OBJRAM-D[0..15]" B R 9200 5850 50 
F11 "OBJRAM-A[0..12]" O R 9200 6000 50 
F12 "~VBLANK" I L 7600 5250 50 
F13 "OBJRAM-C[0..3]" O R 9200 5700 50 
F14 "CONTROL-I[0..8]" I L 7600 5400 50 
F15 "CONTROL-O[0..7]" O R 9200 5400 50 
F16 "GFX-ADDR-W" O R 9200 6700 50 
F17 "~RESETI" I L 7600 6050 50 
F18 "CLK2M" I L 7600 5850 50 
F19 "CLK16M" I L 7600 5950 50 
$EndSheet
Wire Bus Line
	6800 7850 7600 7850
Wire Wire Line
	6900 8300 12300 8300
Connection ~ 6900 8300
Wire Bus Line
	650  1200 12050 1200
Wire Bus Line
	9800 1500 6800 1500
Wire Bus Line
	7400 5400 7400 2600
Wire Wire Line
	6800 4950 7600 4950
Wire Bus Line
	9800 1500 9800 6000
Wire Wire Line
	7500 2450 7500 5250
$Sheet
S 10400 4900 1500 850 
U 62D0139C
F0 "gfx-data-bus" 50
F1 "gfx-data-bus.sch" 50
F2 "GFX-A[0..31]" I R 11900 5250 50 
F3 "GFX-B[0..31]" I R 11900 5600 50 
F4 "ENABLE" I L 10400 5400 50 
F5 "GFX-BUS[0..31]" B L 10400 5600 50 
F6 "A~B" I L 10400 5050 50 
F7 "~G2" I L 10400 5300 50 
F8 "G1" I L 10400 5200 50 
$EndSheet
Wire Wire Line
	10000 5200 10400 5200
Wire Wire Line
	9900 3700 12300 3700
Connection ~ 10000 5200
Wire Wire Line
	10000 5200 10000 6400
$Sheet
S 10400 6250 1500 1150
U 62C44E7C
F0 "gfx-addr-bus" 50
F1 "gfx-addr-bus.sch" 50
F2 "GFX-A[2..23]" O R 11900 6550 50 
F3 "GFX-B[2..23]" O R 11900 6900 50 
F4 "GFX-ADDR-W" I L 10400 6700 50 
F5 "GFX-BUS[0..21]" B L 10400 6550 50 
F6 "ENABLE-SELECT" I L 10400 6400 50 
F7 "ENABLE" I L 10400 7000 50 
F8 "BUS-A~B" I L 10400 7100 50 
F9 "A01-ADDR[0..19]" I L 10400 7250 50 
$EndSheet
Connection ~ 4750 3800
Wire Bus Line
	4750 3800 4750 5250
Wire Wire Line
	4500 3450 5150 3450
Wire Wire Line
	4500 3550 5150 3550
Wire Bus Line
	4650 3950 4650 5050
Wire Wire Line
	4550 3000 5150 3000
Wire Wire Line
	4550 3100 5150 3100
Wire Wire Line
	4550 3200 5150 3200
Text Label 5650 8200 2    50   ~ 0
~REFRESH
Wire Wire Line
	5150 8100 5750 8100
Wire Wire Line
	5150 8200 5750 8200
Wire Wire Line
	5150 8300 5750 8300
Wire Wire Line
	7600 4950 7600 2350
Wire Wire Line
	10000 3800 10000 4950
Wire Wire Line
	9900 3700 9900 4850
Wire Wire Line
	9900 4850 6800 4850
Connection ~ 10000 4950
Wire Wire Line
	10000 4950 10000 5200
Wire Wire Line
	6800 6350 7200 6350
Wire Wire Line
	6800 3100 7200 3100
Wire Wire Line
	6800 6150 7100 6150
Wire Wire Line
	6800 3250 7100 3250
Wire Wire Line
	7200 3100 7200 6350
Wire Wire Line
	7100 3250 7100 6150
Connection ~ 7100 6150
Wire Wire Line
	7100 6150 7400 6150
Wire Wire Line
	7000 3400 7000 4600
Connection ~ 7200 6350
Wire Bus Line
	7400 2600 6800 2600
Wire Wire Line
	7500 2450 6800 2450
Wire Wire Line
	7600 2350 6800 2350
Connection ~ 7600 4950
Wire Wire Line
	7600 4950 10000 4950
$Sheet
S 5150 4250 1650 2300
U 5DC1E7F0
F0 "code-rom-control" 50
F1 "code-rom-control.sch" 50
F2 "CPU-DATA[0..15]" O R 6800 5700 50 
F3 "CBUS-DATA-DIR" O L 5150 5700 50 
F4 "~CBUS-ADDR-ENABLE" O L 5150 5550 50 
F5 "CBUS-ADDR-DIR" O L 5150 5450 50 
F6 "~CBUS-DATA-ENABLE" O L 5150 5800 50 
F7 "CPU-ADDR[1..23]" I R 6800 5550 50 
F8 "CLK16M" I R 6800 6350 50 
F9 "CLK2M" I R 6800 6250 50 
F10 "ENABLE" O R 6800 6150 50 
F11 "BUS-ADDR[1..23]" O L 5150 5050 50 
F12 "BUS-DATA[0..15]" B L 5150 5250 50 
F13 "~RESETI" I R 6800 6450 50 
F14 "~OBJRAM-BANK-W" O R 6800 4600 50 
F15 "BUS-CONTROL[1..7]" I R 6800 4450 50 
F16 "~RESETI-OUT" O R 6800 6050 50 
F17 "~ENABLE-REGISTERS" I L 5150 6050 50 
F18 "ENABLE-ADDR-BUS" I R 6800 4950 50 
F19 "CLK16M-OUT" O R 6800 5950 50 
F20 "CLK2M-OUT" O R 6800 5850 50 
F21 "~A-BOARD-CONNECTED" I R 6800 4850 50 
$EndSheet
Wire Bus Line
	6900 3550 6900 4450
$Sheet
S 5150 1350 1650 2400
U 5E35B989
F0 "CIF" 50
F1 "CIF.sch" 50
F2 "~ROM-E" O L 5150 3450 50 
F3 "~ROM-OE" O L 5150 3550 50 
F4 "CONTROL-OUT[1..6]" O L 5150 2500 50 
F5 "SECURITY-IN[2..5]" I L 5150 2350 50 
F6 "SECURITY-OUT[2..5]" I R 6800 1950 50 
F7 "CLK-OUT" O R 6800 2250 50 
F8 "SRAM-A[0..12]" O L 5150 1500 50 
F9 "SRAM-D[0..15]" B L 5150 1650 50 
F10 "~SRAM-WH" O L 5150 1800 50 
F11 "~SRAM-WL" O L 5150 1900 50 
F12 "~SRAM-OE" O L 5150 2000 50 
F13 "~SRAM-E" O L 5150 2100 50 
F14 "~VBLANK" I L 5150 3000 50 
F15 "CLK16M" I R 6800 3100 50 
F16 "CLK4M" I L 5150 3200 50 
F17 "CODE-ADDR[16..23]" I R 6800 2900 50 
F18 "ENABLE" I R 6800 3250 50 
F19 "OBJRAM-D[0..15]" B R 6800 1650 50 
F20 "OBJRAM-A[0..12]" I R 6800 1500 50 
F21 "CPU-CONTROL-I[0..7]" I R 6800 2100 50 
F22 "CPU-CONTROL-O[0..8]" O R 6800 2600 50 
F23 "OBJRAM-CONTROL[0..3]" O R 6800 1800 50 
F24 "~VBLANK-OUT" O R 6800 2450 50 
F25 "DEBUG-RANGE" I R 6800 2350 50 
F26 "OBJRAM-BANK" I L 5150 3650 50 
F27 "OBJRAM-BANK-CLK" I R 6800 3400 50 
F28 "~CPSB-OBJUP" I L 5150 2600 50 
F29 "~Z80-BUS" I L 5150 2700 50 
F30 "~CPSA-BR" I L 5150 2800 50 
F31 "BUS-CONTROL[1..7]" O R 6800 3550 50 
F32 "~REFRESH" I L 5150 3100 50 
$EndSheet
Wire Wire Line
	6800 8200 7300 8200
Wire Wire Line
	6800 6250 7300 6250
Wire Wire Line
	7300 6250 7300 8200
Connection ~ 7300 8200
Wire Wire Line
	7300 8200 10100 8200
Wire Wire Line
	7400 7000 10200 7000
Wire Wire Line
	7500 5250 7600 5250
Wire Bus Line
	7600 5400 7400 5400
Wire Bus Line
	7300 5550 7600 5550
Connection ~ 7300 5550
Wire Bus Line
	7600 5700 6800 5700
Wire Wire Line
	6800 5850 7600 5850
Wire Wire Line
	6800 5950 7600 5950
Wire Wire Line
	6800 6050 7600 6050
Wire Wire Line
	7600 6150 7400 6150
Connection ~ 7400 6150
Wire Bus Line
	7500 6550 7600 6550
Wire Wire Line
	9200 5250 9300 5250
Wire Bus Line
	9200 5400 9400 5400
Wire Bus Line
	9200 5550 9500 5550
Wire Bus Line
	9200 5700 9600 5700
Wire Bus Line
	9200 5850 9700 5850
Wire Bus Line
	9200 6000 9800 6000
Wire Wire Line
	9200 6400 9900 6400
Wire Bus Line
	9200 6550 10300 6550
Wire Wire Line
	9200 6700 10400 6700
Wire Bus Line
	2800 5250 2900 5250
Wire Bus Line
	2900 5250 2900 4000
Wire Bus Line
	2900 5250 3150 5250
Connection ~ 2900 5250
Wire Bus Line
	2800 3850 3000 3850
Wire Bus Line
	2800 5450 3000 5450
Wire Bus Line
	3000 5050 3000 5450
Connection ~ 3000 5050
Wire Bus Line
	3150 5050 3000 5050
Wire Bus Line
	3000 3850 3000 5050
Text Notes 12050 3800 0    50   ~ 0
HIGH
Text Notes 12050 3700 0    50   ~ 0
LOW
Text Notes 6850 6150 0    50   ~ 0
HIGH
Text Notes 4900 6150 0    50   ~ 0
LOW
Text Notes 3100 2250 0    50   ~ 0
LOW
$Sheet
S 5750 7300 1050 1150
U 640ADD2B
F0 "CN2" 50
F1 "CN2.sch" 50
F2 "GFX-OUT[0..15]" I R 6800 7700 50 
F3 "SECURITY[2..5]" B L 5750 7450 50 
F4 "CLK16M" O R 6800 8100 50 
F5 "CLK8M" O R 6800 8300 50 
F6 "CLK4M" O L 5750 8300 50 
F7 "CLK2M" O R 6800 8200 50 
F8 "~REFRESH" O L 5750 8200 50 
F9 "~RESETI" O R 6800 7450 50 
F10 "~VBLANK" O L 5750 8100 50 
F11 "GFX-ADDR[0..19]" O R 6800 7850 50 
$EndSheet
Wire Bus Line
	7500 6550 7500 7700
Wire Bus Line
	7600 7250 10400 7250
Wire Wire Line
	9900 6400 9900 5300
Wire Wire Line
	9900 5300 10400 5300
Wire Wire Line
	10100 7100 10100 5050
Wire Wire Line
	10100 5050 10400 5050
Connection ~ 10100 7100
Text Label 5100 3100 2    50   ~ 0
~REFRESH
Text Notes 7700 4450 0    50   ~ 0
BUS-CONTROL\n  1: ~BUS-UDSW~\n  2: ~BUS-W~\n  3: ~BUS-R~\n  4: ~BUS-AS~\n  5: ~BUS-E~\n  6: ~BUS-BGACK~\n  7: BUS-DIR
Text Notes 7700 3400 0    50   ~ 0
CPU-CONTROL to CPU\n  0: ~CPU-DTACK~\n  1: ~CPU-BR~\n  2: ~CPU-BGACK~\n  3: ~CPU-IPL0~\n  4: ~CPU-IPL1~\n  5: ~CPU-IPL2~\n  6: ~CPU-BGERR~\n  7: ~CPU-VPA~\n  8: ~CPU-HALT~
Text Notes 9900 3400 0    50   ~ 0
CPU-CONTROL from CPU\n  0: CPU-R~W~\n  1: ~CPU-AS~\n  2: ~CPU-UDSW~\n  3: ~CPU-LDSW~\n  4: ~CPU-BG~\n  5: CPU-FC0\n  6: CPU-FC1\n  7: CPU-FC2\n  
Text Notes 1300 7000 0    50   ~ 0
EEPROM\n  1: EEPROM-CS\n  2: EEPROM-CLK\n  3: EEPROM-DATA-IN\n  4: EEPROM-DATA-OUT\n  5: EEPROM-CS2
Text Notes 9900 2150 0    50   ~ 0
OBJRAM-CONTROL\n  0: ~OBJRAM-WL~\n  1: ~OBJRAM-WH~\n  2: ~OBJRAM-OE~\n  3: ~OBJRAM-E~
Text Notes 4100 8000 0    50   ~ 0
SECURITY\n  1: VCC\n  2: SECURITY-DATA\n  3: ~SECURITY-E1~\n  4: SECURITY-CLK\n  5: ~SECURITY-E2~\n  6: GND
Text Notes 1250 3400 0    50   ~ 0
CONTROL-OUT\n  1: ~BUS-UDSW~\n  2: ~BUS-W~\n  3: ~BUS-R~\n  4: ~BUS-AS~\n  5: NC\n  6: ~BUS-BGACK~
Wire Bus Line
	3400 1950 3400 2250
$EndSCHEMATC
