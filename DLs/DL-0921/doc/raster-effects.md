# DL-0921 (CPS-B-21) Raster Effects

This function is not handled by CPS1 board but is part of the CPS2. The idea behind raster effects is to provide an 
interrupts based on a desired column or line.

It consists in three decreasing counters initialized every frame (or line) with a value from the register.

When a timer reaches zero, a signal is sent to `/OBJUP`. On the CPS2 this signal is transmitted to IPL2 on the CPU. On
the CPS1, it is only transmitted to the A-01.

__NOTE__: This is still unknown and more work must be done in order to properly grasp how it works.

## Registers
The three registers exists, because those are not used by the CPS1, it is not known if those addresses were. But the
default addresses are `0x0e`, `0x10` and `0x12` (resp. `RASTER1`, `RASTER2`, `RASTER3`). 

All registers have a read mode to consult the current value of their counter.


### RASTER1
`RASTER1` counts on columns and is reset every line. Its first bit is actually ignored as it is the speed of the clock
Please consult the schematics for more details.

It sends an `/OBJUP` signal if it reaches ZERO.

### RASTER2 and RASTER3
`RASTER2` and `RASTER3` count lines and is reset every frame.

An unknown behaviour is performed by the MSB.

More work is necessary on that part to full understand how it works. 