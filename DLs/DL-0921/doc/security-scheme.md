# DL-0921 (CPS-B-21) Security Scheme

On top of a obfuscation of its usage, this chip manages a lot of the security scheme behind the more secured CPS-1 games.
The only additional measure is the Kabuki chip on a very specific set of games.

## Configuration Key

At the core of the security scheme, you have a 144 bits configuration key. This sets the address of each bus functions,
configures the layer enabling multiplexer and provides the parameters for other security checkers.

This key is stored inside the chip and is held alive actively by a battery (connected to VBAT and GND).

The advantage behind this as a constructor is two fold:
* Make it harder to create bootlegs
* You can use one chip to support the configuration of all other versions of 

The content of the security key is detailed in [this section](./configuration-key.md).

### Default mode
The chip has a default mode of operation in order to work if no battery is provided and the key is completely wiped.

This is the mode that the CPS2 runs on (no battery on the A-board). This mode overrides the key values to use a set of
default value. You can consult the default values in [this section](./configuration-key.md) and also most of the
schematics are annotated with the default values.

To enable this mode, put `SEC-MODE0` and `SEC-MODE1` to HIGH.

### Key injection
It is possible to program the configuration key with a security sequence. This sequence can be performed by an [Arduino
prototype provided by Eduardo Cruz](https://github.com/ArcadeHacker/ArcadeHacker_CPS1). It can also be performed by
the [InfiniKey-CPS1 by undamned](https://www.arcade-projects.com/forums/index.php?thread/8337-infinikey-cps1/).

#### Setup stage
Unlock the security by setting `SEC-/E1` to LOW and `SEC-E2` to HIGH.

This will activate the security injection module.

#### Stage 1
First you have to send 4 impulse on the `SEC-STROBE` pin, on the 4th falling edge, the stage 1 is complete.

This will force the main bus data to mute its output. Starting from now, you will use CD0 as DATA and CD1 as CLK.

#### Stage 2
Using DATA and CLK, send the following data 1 > 0 > 1.

#### Stage 3
Using CLK, send 15 impulses on CLK.

Now, CLK and DATA are connected to a 144 bits shift register. 

#### Stage 4
Inject each bit one by one using DATA and CLK. Please consult the [this section](./configuration-key.md) to know in
which order you should send your data.

#### Stage 5
When you're done, lock the security again with `SEC-/E1` and `SEC-E2` by setting both to LOW for instance.

Restart your CPS and it should work.

## CPS-ID check
The identifier check is a simple check that the program can perform to see if its hardware is the right one.

This piece of code has been extracted from 3wonders (offset 0x5DA36). In this game, the offset to CPS-ID is `0x32`
(on top of the `0x800140` base address).

```nasm
move.w  ($800172).l,d2  ; read offset 0x32 into d2 [here: CPS-ID]
andi.w  #$FC3F,d2       ; apply a FC3F bitmask on d2
cmpi.w  #$800,d2        ; compare against 0x0800
bne.w   infinite_loop   ; if not equal go to an infinite loop
```

The mask is technically not necessary but the bits `[6..9]` holds a counter of how many CPS-ID reads have been performed
and those are not checked here.

## Multiplication and the palette lock
Additionally, the chip contains a mechanism to lock itself if a wrong value is written on some of it's registers.

Here's a complete sequence of this mechanism, again extracted from 3wonders (offset 0x1F0)

```nasm
move.w  #$17,($80014E).l    ; write 0x0017 to offset 0x0e [here: MULT-X]
move.w  #$12C,($80014C).l   ; write 0x012C to offset 0x0c [here: MULT-Y]
move.w  ($80014A).l,d0      ; read offset 0x0a into d0 [here: MULT-LSB]
addi.w  #$49,d0             ; d0 += d0 + 0x49
move.w  d0,($800144).l      ; write d0 to offset 0x04 [here: CHECK1]
move.w  #$E6B,($800142).l   ; write 0xE6B to offset 0x02 [here: CHECK2]
```

In here, we basically ask the hardware to put:
* `0x17 * 0x12C + 0x49` (=`0x1B3D`) into `CHECK1`
* `0xE6B` into `CHECK2`

There are actually multiple instance of this code during the execution of the program with different values but they
always produce the same values in `CHECK1` and `CHECK2`. For instance, on another place (offset 0x25614) 
we have `CHECK1 = 0x1B3D` directly and `CHECK2 = 0xC * 0x12C + 0x5B` (`=E6B`).

Internally, whenever a write on `CHECK2` happens, the chip will then check the values that were written on `CHECKs` and
see if there are equal to what the configuration key provides.

__In case of failure__:
* A 64 frame timer is launched
* When the timer hits, the `LUT` bus, meaning the palette ram address bus becomes completely disabled
(as well as all other mechanisms related to `LUT`) leading to a solid color screen.
* The game will continue running (sound might still be running) but only way to have a picture back is the
total reboot of the system.

_Important note_: if you _never_ call this registry it will never launch the lockdown timer and the game runs without
issues. That is how Eduardo Cruz made his key injection work. The `CHECK2` address is always set to an unused 
value (`0x00` or `0x3e`). So, when the program writes into what it things is `CHECK2`, nothing happens.
