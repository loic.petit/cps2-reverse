# CPS-B Palette copy phase

As far as this chip is concerned, this phase is triggered by two signals WSTLUT and LUTPRO.
Those are mainly provided by the DL-0311 (A-01). In the case of the CPS2, it is also provided by the PAL BGSA5.

## Role
Because the palette ram is used actively to transmit the colour data to the video output, it's not possible to
write directly on the palette RAM.

So the phase goes basically like this:
* At some point during the blanking phase, the CPS-A asks the 68k CPU for a BUS Request (LUTPRO)
* Now it can use the BUS to transmit data from the graphics ram to the palette ram.
* The CPS-A will launch the copy (WSTLUT) and ask the CPS-B to do the same
* The CPS-A will iterate on the RAM addresses to get the data.
* The B-21 will iterate on the palette addresses and ask the palette ram to write the data it sees on the bus.

## The palette control register
Its function is to control which page of the palette RAM will be copied. Each page contains 0x200 addresses.
The register supports only the 6 LSB bits and each corresponds to a page.

Here are the bits description and the range of addresses that the B-21 uses for his graphics assembly.

| Bit | Name | Range |
| --- | --- | --- |
| 0 | Sprites | 0x000 - 0x1FF |
| 1 | Scroll1 | 0x200 - 0x3FF |
| 2 | Scroll2 | 0x400 - 0x5FF |
| 3 | Scroll3 | 0x600 - 0x7FF |
| 4 | Stars1  | 0x800 - 0x97F |
| 5 | Stars2  | 0xA00 - 0xA7F |

A page is copied only if the corresponding bit is set.

### The base address quirk
Surely in order to optimize the amount of bus downtime, the B-21 will start from the minimal range. Meaning,
if you put 0x08 in the register, only bit 3 is set, therefore the copy phase start at 0x600. __However__, the copy
phase always starts at the beginning of the palette section of the graphics ram.
