# DL-0411 - CPS-B-xx previous versions

Starting from Three Wonders in 1991, C-boards started to use the B-21 chip. The only exception is Varth in World & USA
versions in 1992. Previous versions of the C-boards used different revisions of the DL-0411 chip marked CPS-B-xx with xx
being a number between 01-05 and 11-18. The transition to the B-21 was surely to avoid creating custom chips of every game and
also the [added security](./security-scheme.md).

## Pinout

The pinout of the DL-0411 is similar to the DL-0921 with some minor alterations:

| DL-0921 | DL-0411 | Function |
| --- | --- | --- |
| 1 - 30 | 1 - 30 | Same function |
| . | 31 | ASD9 |
| . | 32 | ASD10 |
| . | 33 | ASD11 |
| . | 34 | ASD12 |
| 31 | 35 | BSD0 |
| 32 | 36 | BSD1 |
| 33 | 37 | BSD2 |
| 34 | 38 | BSD3 |
| 35 | 39 | BSD4 |
| 36 | 40 | BSD5 |
| 37 | 41 | BSD6 |
| 38 | 42 | BSD7 |
| 39 | 43 | BSD8 |
| 40 | . | VBAT |
| 41 | . | GND |
| 42 | . | SEC-STROBE |
| 43 | . | SEC-/E1 |
| 44 | . | SEC-E2 |
| 45 | . | SEC-MODE0 |
| 46 | . | SEC-MODE1 |
| 47 | . | GND |
| . | 44 | BSD9 |
| . | 45 | BSD10 |
| . | 46 | BSD11 |
| . | 47 | BSD12 |
| 48 - 160 | 48 - 160 | Same function |

ASD / BSD are memory data buses that provide sprite palette data. A 13 bits bus was allocated in early design,
however, the A-board never needed 13 bits, in consequence some memory slots were not populated.
On dash A-boards, those slots were completely removed from the design.

## Features
The DL-0411 variations don't support configuration key as clearly shown on the pinout. So, all revisions have hardcoded
values for a configuration key. Fortunately, they don't require a battery to operate.

Moreover, the DL-0411 ***don't*** have the following feature:
* Security: multiplier and palette lockdown
* Raster effects
