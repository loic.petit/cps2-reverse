# CPS-B Star Raster

For more details about the structure of the star fields, consult the [CPS-A Star Field](../../DL-0311/doc/stars.md)
documentation.

## ROM Data
Contrary to other layers, the star field uses only the `APD` bus as the ROM input.

It also takes the full `XS[0..4]` bus to shift the raster within the 32 pixels while the other layers use
the `XS[0..3]` maximum.

| Bits | Description |
| --- | --- |
| 5..7 | `PALETTE_ID[0..2]` |
| 0..4 | `POSITION` |

The star position indicates which pixel should be lit within the 32 pixel row.

**Important**: If the star position equals `0x0f` then the star will not be drawn.

## Palette Address

Assuming that a pixel is drawn, the final palette address will have the following shape.

| Bits | Description |
| --- | --- |
| 11 | 1 |
| 10 | 0 |
| 9 | 0 if `STAR1`<br>1 if `STAR2` |
| 7..8 | 0 |
| 4..6 | `PALETTE_ID[0..2]` |
| 0..3 | `CNT16[0..3]` if `~PALETTE_ID2`<br>`CNT15[0..3]` if `PALETTE_ID2` |

* CNT16 is a counter that increments every 16 frames and resets naturally every 16 increments.
* CNT15 is a counter that increments every 16 frames and resets every 15 increments.
