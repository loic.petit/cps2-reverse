EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 12 69
Title ""
Date "2020-11-26"
Rev ""
Comp ""
Comment1 "Licence: CC-BY"
Comment2 "Author: Loïc *WydD* Petit"
Comment3 ""
Comment4 ""
$EndDescr
Connection ~ 8950 4400
Connection ~ 8850 4300
Connection ~ 8950 4850
Connection ~ 9050 4750
Connection ~ 9150 4650
Connection ~ 10200 4950
Connection ~ 9050 3400
Connection ~ 9150 3300
Connection ~ 13200 2800
Connection ~ 9150 4000
Connection ~ 12500 3200
Connection ~ 12500 3950
Connection ~ 13200 3550
Connection ~ 13200 8050
Connection ~ 12500 7700
Connection ~ 13200 8800
Connection ~ 12500 8450
Connection ~ 13200 7300
Connection ~ 13200 6550
Connection ~ 8350 3450
Connection ~ 9050 3650
Connection ~ 8950 3500
Connection ~ 9150 2250
Connection ~ 9150 2900
Connection ~ 9050 2800
Connection ~ 8950 4200
Connection ~ 9050 4100
Connection ~ 13200 5050
Connection ~ 12500 5450
Connection ~ 12500 6200
Connection ~ 13200 4300
Connection ~ 13200 5800
Connection ~ 12500 4700
Connection ~ 12500 6950
Connection ~ 7650 6950
Connection ~ 8750 8150
Connection ~ 8650 8250
Connection ~ 8550 8350
Connection ~ 7650 6200
Connection ~ 8750 7450
Connection ~ 8350 7300
Connection ~ 8350 6550
Connection ~ 8850 8050
Connection ~ 8850 7350
Connection ~ 8650 7550
Connection ~ 7650 7700
Connection ~ 8550 7500
Connection ~ 7650 8450
Connection ~ 8450 8250
Connection ~ 7650 4600
Connection ~ 8350 4200
Connection ~ 7650 3850
Connection ~ 8850 5150
Connection ~ 8350 5800
Connection ~ 8750 6000
Connection ~ 8750 5050
Connection ~ 7650 5350
Connection ~ 8650 6100
Connection ~ 8650 6750
Connection ~ 8350 4950
Connection ~ 8850 4950
Connection ~ 8450 6300
Connection ~ 8550 6200
Connection ~ 8550 9000
Connection ~ 8850 8700
Connection ~ 8650 8900
Connection ~ 8750 8800
Connection ~ 8450 9100
Connection ~ 4150 5550
Connection ~ 5300 5600
Connection ~ 3500 8250
Connection ~ 11950 8800
Connection ~ 2950 9600
Connection ~ 3600 8350
Connection ~ 12600 2250
NoConn ~ 4750 6000
Entry Wire Line
	3050 4500 2950 4400
Entry Wire Line
	3050 4600 2950 4500
Entry Wire Line
	3050 4700 2950 4600
Entry Wire Line
	3050 4800 2950 4700
Entry Wire Line
	3050 4900 2950 4800
Entry Wire Line
	3050 7700 2950 7600
Entry Wire Line
	3050 7800 2950 7700
Entry Wire Line
	3050 7900 2950 7800
Entry Wire Line
	3050 8000 2950 7900
Wire Wire Line
	2400 9150 2950 9150
Wire Wire Line
	2400 9600 2950 9600
Wire Wire Line
	2400 10100 2950 10100
Wire Wire Line
	2950 9250 2950 9600
Wire Wire Line
	2950 9600 2950 10000
Wire Wire Line
	3050 4500 3700 4500
Wire Wire Line
	3050 4600 3700 4600
Wire Wire Line
	3050 4700 3700 4700
Wire Wire Line
	3050 4800 3700 4800
Wire Wire Line
	3050 4900 3700 4900
Wire Wire Line
	3050 7700 3700 7700
Wire Wire Line
	3050 7800 3700 7800
Wire Wire Line
	3050 7900 3700 7900
Wire Wire Line
	3050 8000 3700 8000
Wire Wire Line
	3250 8350 3600 8350
Wire Wire Line
	3500 5150 3500 8250
Wire Wire Line
	3500 5150 3700 5150
Wire Wire Line
	3500 8250 3500 9200
Wire Wire Line
	3500 8250 3700 8250
Wire Wire Line
	3500 10050 11400 10050
Wire Wire Line
	3600 5250 3600 8350
Wire Wire Line
	3700 5250 3600 5250
Wire Wire Line
	3700 8350 3600 8350
Wire Wire Line
	4000 6100 4250 6100
Wire Wire Line
	4050 5550 4150 5550
Wire Wire Line
	4150 5550 4750 5550
Wire Wire Line
	4150 5900 4150 5550
Wire Wire Line
	4250 5900 4150 5900
Wire Wire Line
	4300 4600 5400 4600
Wire Wire Line
	4300 4700 5400 4700
Wire Wire Line
	4300 4800 5400 4800
Wire Wire Line
	4300 4900 5400 4900
Wire Wire Line
	4300 7700 5400 7700
Wire Wire Line
	4300 7800 5400 7800
Wire Wire Line
	4300 7900 5400 7900
Wire Wire Line
	4300 8000 5400 8000
Wire Wire Line
	4750 5650 4750 5900
Wire Wire Line
	4800 800  4800 4500
Wire Wire Line
	4800 4500 4300 4500
Wire Wire Line
	4900 900  4900 5400
Wire Wire Line
	4900 900  11100 900 
Wire Wire Line
	4900 5400 5400 5400
Wire Wire Line
	4900 8200 4900 10450
Wire Wire Line
	4900 10450 11300 10450
Wire Wire Line
	5000 1000 5000 5300
Wire Wire Line
	5000 1000 11000 1000
Wire Wire Line
	5000 5300 5400 5300
Wire Wire Line
	5000 8300 5000 10350
Wire Wire Line
	5000 8300 5400 8300
Wire Wire Line
	5000 10350 11200 10350
Wire Wire Line
	5100 1100 5100 5200
Wire Wire Line
	5100 1100 10900 1100
Wire Wire Line
	5100 5200 5400 5200
Wire Wire Line
	5100 8400 5100 10250
Wire Wire Line
	5100 10250 11100 10250
Wire Wire Line
	5200 1200 5200 5100
Wire Wire Line
	5200 1200 10800 1200
Wire Wire Line
	5200 5100 5400 5100
Wire Wire Line
	5200 8500 5200 10150
Wire Wire Line
	5200 8500 5400 8500
Wire Wire Line
	5200 10150 11000 10150
Wire Wire Line
	5300 5600 5300 8700
Wire Wire Line
	5300 5600 5400 5600
Wire Wire Line
	5300 8700 5400 8700
Wire Wire Line
	5400 8200 4900 8200
Wire Wire Line
	5400 8400 5100 8400
Wire Wire Line
	6400 4950 7350 4950
Wire Wire Line
	6400 5150 7750 5150
Wire Wire Line
	6400 7950 7350 7950
Wire Wire Line
	6400 8050 7450 8050
Wire Wire Line
	6400 8150 7550 8150
Wire Wire Line
	6400 8250 7750 8250
Wire Wire Line
	7250 2900 7250 4850
Wire Wire Line
	7250 2900 7750 2900
Wire Wire Line
	7250 4850 6400 4850
Wire Wire Line
	7350 3650 7750 3650
Wire Wire Line
	7350 4950 7350 3650
Wire Wire Line
	7350 6000 7350 7950
Wire Wire Line
	7350 6000 7750 6000
Wire Wire Line
	7450 4400 7450 5050
Wire Wire Line
	7450 4400 7750 4400
Wire Wire Line
	7450 5050 6400 5050
Wire Wire Line
	7450 6750 7450 8050
Wire Wire Line
	7450 6750 7750 6750
Wire Wire Line
	7550 7500 7750 7500
Wire Wire Line
	7550 8150 7550 7500
Wire Wire Line
	7650 3100 7650 3850
Wire Wire Line
	7650 3850 7650 4600
Wire Wire Line
	7650 4600 7650 5350
Wire Wire Line
	7650 5350 7650 6200
Wire Wire Line
	7650 6200 7650 6950
Wire Wire Line
	7650 6950 7650 7700
Wire Wire Line
	7650 7700 7650 8450
Wire Wire Line
	7650 9150 7650 8450
Wire Wire Line
	7750 3100 7650 3100
Wire Wire Line
	7750 3850 7650 3850
Wire Wire Line
	7750 4600 7650 4600
Wire Wire Line
	7750 5350 7650 5350
Wire Wire Line
	7750 6200 7650 6200
Wire Wire Line
	7750 6950 7650 6950
Wire Wire Line
	7750 7700 7650 7700
Wire Wire Line
	7750 8450 7650 8450
Wire Wire Line
	8000 2700 8350 2700
Wire Wire Line
	8000 3450 8350 3450
Wire Wire Line
	8000 4950 8350 4950
Wire Wire Line
	8000 5800 8350 5800
Wire Wire Line
	8000 6550 8350 6550
Wire Wire Line
	8000 8050 8350 8050
Wire Wire Line
	8250 2900 9150 2900
Wire Wire Line
	8250 3650 9050 3650
Wire Wire Line
	8250 4400 8950 4400
Wire Wire Line
	8250 5150 8850 5150
Wire Wire Line
	8250 6000 8750 6000
Wire Wire Line
	8250 6750 8650 6750
Wire Wire Line
	8250 7500 8550 7500
Wire Wire Line
	8250 8250 8450 8250
Wire Wire Line
	8350 2700 8350 3450
Wire Wire Line
	8350 3450 8350 4200
Wire Wire Line
	8350 4200 8000 4200
Wire Wire Line
	8350 4200 8350 4950
Wire Wire Line
	8350 4950 8350 5800
Wire Wire Line
	8350 5800 8350 6550
Wire Wire Line
	8350 6550 8350 7300
Wire Wire Line
	8350 7300 8000 7300
Wire Wire Line
	8350 7300 8350 8050
Wire Wire Line
	8450 1600 8450 6300
Wire Wire Line
	8450 6300 8450 8250
Wire Wire Line
	8450 6300 11700 6300
Wire Wire Line
	8450 8250 8450 9100
Wire Wire Line
	8450 9100 10300 9100
Wire Wire Line
	8450 9950 8450 9100
Wire Wire Line
	8450 9950 9550 9950
Wire Wire Line
	8550 1600 8550 6200
Wire Wire Line
	8550 6200 8550 7500
Wire Wire Line
	8550 6200 11800 6200
Wire Wire Line
	8550 7500 8550 8350
Wire Wire Line
	8550 8350 8550 9000
Wire Wire Line
	8550 8350 10300 8350
Wire Wire Line
	8550 9000 9700 9000
Wire Wire Line
	8550 9850 8550 9000
Wire Wire Line
	8550 9850 9550 9850
Wire Wire Line
	8650 1600 8650 6100
Wire Wire Line
	8650 6100 8650 6750
Wire Wire Line
	8650 6100 11900 6100
Wire Wire Line
	8650 6750 8650 7550
Wire Wire Line
	8650 7550 8650 8250
Wire Wire Line
	8650 7550 10300 7550
Wire Wire Line
	8650 8250 8650 8900
Wire Wire Line
	8650 8250 9700 8250
Wire Wire Line
	8650 8900 9700 8900
Wire Wire Line
	8650 9750 8650 8900
Wire Wire Line
	8650 9750 9550 9750
Wire Wire Line
	8750 1600 8750 5050
Wire Wire Line
	8750 5050 8750 6000
Wire Wire Line
	8750 6000 8750 7450
Wire Wire Line
	8750 6000 12600 6000
Wire Wire Line
	8750 7450 8750 8150
Wire Wire Line
	8750 7450 9750 7450
Wire Wire Line
	8750 8150 8750 8800
Wire Wire Line
	8750 8150 9700 8150
Wire Wire Line
	8750 8800 9700 8800
Wire Wire Line
	8750 9650 8750 8800
Wire Wire Line
	8750 9650 9550 9650
Wire Wire Line
	8850 1600 8850 4300
Wire Wire Line
	8850 4300 8850 4950
Wire Wire Line
	8850 4950 8850 5150
Wire Wire Line
	8850 5150 8850 5900
Wire Wire Line
	8850 5900 11900 5900
Wire Wire Line
	8850 6500 8850 7350
Wire Wire Line
	8850 6500 10200 6500
Wire Wire Line
	8850 7350 8850 8050
Wire Wire Line
	8850 7350 9750 7350
Wire Wire Line
	8850 8050 8850 8700
Wire Wire Line
	8850 8050 9700 8050
Wire Wire Line
	8850 8700 9700 8700
Wire Wire Line
	8850 9550 8850 8700
Wire Wire Line
	8850 9550 9550 9550
Wire Wire Line
	8950 1600 8950 3500
Wire Wire Line
	8950 3500 8950 4200
Wire Wire Line
	8950 4200 8950 4400
Wire Wire Line
	8950 4400 8950 4850
Wire Wire Line
	8950 4850 8950 5800
Wire Wire Line
	8950 5800 11800 5800
Wire Wire Line
	9050 1600 9050 2800
Wire Wire Line
	9050 2800 9050 3400
Wire Wire Line
	9050 3400 9050 3650
Wire Wire Line
	9050 3650 9050 4100
Wire Wire Line
	9050 4100 9050 4750
Wire Wire Line
	9050 4750 9050 5700
Wire Wire Line
	9050 5700 11700 5700
Wire Wire Line
	9150 1600 9150 2250
Wire Wire Line
	9150 2250 9150 2900
Wire Wire Line
	9150 2250 10200 2250
Wire Wire Line
	9150 2900 9150 3300
Wire Wire Line
	9150 3300 9150 4000
Wire Wire Line
	9150 4000 9150 4650
Wire Wire Line
	9150 4650 9150 5600
Wire Wire Line
	9150 5600 11600 5600
Wire Wire Line
	9600 4000 9150 4000
Wire Wire Line
	9600 4100 9050 4100
Wire Wire Line
	9600 4200 8950 4200
Wire Wire Line
	9600 4650 9150 4650
Wire Wire Line
	9600 4750 9050 4750
Wire Wire Line
	9600 4850 8950 4850
Wire Wire Line
	9600 4950 8850 4950
Wire Wire Line
	9650 3300 9150 3300
Wire Wire Line
	9650 3400 9050 3400
Wire Wire Line
	10200 2800 9050 2800
Wire Wire Line
	10200 2900 9150 2900
Wire Wire Line
	10200 3400 10200 3350
Wire Wire Line
	10200 3500 8950 3500
Wire Wire Line
	10200 4200 10200 4100
Wire Wire Line
	10200 4300 8850 4300
Wire Wire Line
	10200 4950 10200 4800
Wire Wire Line
	10200 4950 10200 6500
Wire Wire Line
	10200 4950 10300 4950
Wire Wire Line
	10300 5050 8750 5050
Wire Wire Line
	10300 7450 10300 7400
Wire Wire Line
	10300 8250 10300 8150
Wire Wire Line
	10300 8850 10300 9000
Wire Wire Line
	10750 2850 10900 2850
Wire Wire Line
	10750 3450 11000 3450
Wire Wire Line
	10750 4250 11100 4250
Wire Wire Line
	10800 1200 10800 2250
Wire Wire Line
	10850 5000 11300 5000
Wire Wire Line
	10850 7500 11200 7500
Wire Wire Line
	10850 8300 11100 8300
Wire Wire Line
	10850 9050 11000 9050
Wire Wire Line
	10900 1100 10900 2850
Wire Wire Line
	10900 9750 14100 9750
Wire Wire Line
	11000 1000 11000 3450
Wire Wire Line
	11000 9050 11000 10150
Wire Wire Line
	11100 900  11100 4250
Wire Wire Line
	11100 8300 11100 10250
Wire Wire Line
	11200 800  4800 800 
Wire Wire Line
	11200 800  11200 2200
Wire Wire Line
	11200 2200 12050 2200
Wire Wire Line
	11200 7500 11200 10350
Wire Wire Line
	11300 5000 11300 10450
Wire Wire Line
	11400 8800 11400 10050
Wire Wire Line
	11600 3000 11600 5600
Wire Wire Line
	11700 3750 11700 5700
Wire Wire Line
	11700 8250 11700 6300
Wire Wire Line
	11700 8250 12600 8250
Wire Wire Line
	11800 4500 11800 5800
Wire Wire Line
	11800 7500 11800 6200
Wire Wire Line
	11800 7500 12600 7500
Wire Wire Line
	11900 5250 11900 5900
Wire Wire Line
	11900 6750 11900 6100
Wire Wire Line
	11900 6750 12600 6750
Wire Wire Line
	11950 8800 11400 8800
Wire Wire Line
	11950 8800 11950 8650
Wire Wire Line
	12500 2450 12500 3200
Wire Wire Line
	12500 3200 12500 3950
Wire Wire Line
	12500 3950 12500 4700
Wire Wire Line
	12500 4700 12500 5450
Wire Wire Line
	12500 5450 12500 6200
Wire Wire Line
	12500 6200 12500 6950
Wire Wire Line
	12500 6950 12500 7700
Wire Wire Line
	12500 7700 12500 8450
Wire Wire Line
	12500 8450 12600 8450
Wire Wire Line
	12500 8600 12500 8450
Wire Wire Line
	12600 1700 14100 1700
Wire Wire Line
	12600 2250 12600 1700
Wire Wire Line
	12600 2450 12500 2450
Wire Wire Line
	12600 3000 11600 3000
Wire Wire Line
	12600 3200 12500 3200
Wire Wire Line
	12600 3750 11700 3750
Wire Wire Line
	12600 3950 12500 3950
Wire Wire Line
	12600 4500 11800 4500
Wire Wire Line
	12600 4700 12500 4700
Wire Wire Line
	12600 5250 11900 5250
Wire Wire Line
	12600 5450 12500 5450
Wire Wire Line
	12600 6200 12500 6200
Wire Wire Line
	12600 6950 12500 6950
Wire Wire Line
	12600 7700 12500 7700
Wire Wire Line
	12850 2050 13200 2050
Wire Wire Line
	12850 2800 13200 2800
Wire Wire Line
	12850 4300 13200 4300
Wire Wire Line
	12850 5800 13200 5800
Wire Wire Line
	12850 6550 13200 6550
Wire Wire Line
	12850 8050 13200 8050
Wire Wire Line
	13200 2050 13200 2800
Wire Wire Line
	13200 2800 13200 3550
Wire Wire Line
	13200 3550 12850 3550
Wire Wire Line
	13200 3550 13200 4300
Wire Wire Line
	13200 4300 13200 5050
Wire Wire Line
	13200 5050 12850 5050
Wire Wire Line
	13200 5050 13200 5800
Wire Wire Line
	13200 5800 13200 6550
Wire Wire Line
	13200 6550 13200 7300
Wire Wire Line
	13200 7300 12850 7300
Wire Wire Line
	13200 7300 13200 8050
Wire Wire Line
	13200 8050 13200 8800
Wire Wire Line
	13200 8800 11950 8800
Wire Wire Line
	13200 8800 13200 9250
Wire Wire Line
	13200 9250 13300 9250
Wire Bus Line
	2650 4250 2950 4250
Text Label 3150 4500 0    50   ~ 0
~BUS0
Text Label 3150 4600 0    50   ~ 0
~BUS1
Text Label 3150 4700 0    50   ~ 0
~BUS2
Text Label 3150 4800 0    50   ~ 0
~BUS3
Text Label 3150 4900 0    50   ~ 0
~BUS4
Text Label 3150 7700 0    50   ~ 0
~BUS5
Text Label 3150 7800 0    50   ~ 0
~BUS6
Text Label 3150 7900 0    50   ~ 0
~BUS7
Text Label 3150 8000 0    50   ~ 0
~BUS8
Text Label 7050 9150 2    50   ~ 0
~CLK4M
Text Label 11950 8550 2    50   ~ 0
~CLK4M
Text Label 12050 2300 2    50   ~ 0
~CLK4M
Text HLabel 2400 9150 0    50   Input ~ 0
WR
Text HLabel 2400 9600 0    50   Input ~ 0
ENABLE
Text HLabel 2400 10100 0    50   Input ~ 0
RD
Text HLabel 2650 4250 0    50   Input ~ 0
~BUS[0..8]
Text HLabel 3250 8350 0    50   Input ~ 0
~RESET
Text HLabel 4000 6100 0    50   Input ~ 0
~CLK4M
Text HLabel 4050 5550 0    50   Input ~ 0
LI
Text HLabel 13300 9250 2    50   Output ~ 0
~OUT-ENABLE
Text HLabel 14100 1700 2    50   Output ~ 0
_OUT0
Text HLabel 14100 9750 2    50   Output ~ 0
~ZERO
$Comp
L A5C:NOR02 UBZ344
U 1 1 5F8C2148
P 3200 9200
AR Path="/5F8733D0/5F8C2148" Ref="UBZ344"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/5F8C2148" Ref="UBZ344"  Part="1" 
F 0 "UBZ344" H 3225 9466 50  0000 C CNN
F 1 "NOR02" H 3225 9375 50  0000 C CNN
F 2 "" H 3200 9200 50  0001 C CNN
F 3 "" H 3200 9200 50  0001 C CNN
	1    3200 9200
	1    0    0    -1  
$EndComp
$Comp
L A5C:OR02 UBW366
U 1 1 601E0AC1
P 3200 10050
AR Path="/5F8733D0/601E0AC1" Ref="UBW366"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0AC1" Ref="UBW366"  Part="1" 
F 0 "UBW366" H 3225 10317 50  0000 C CNN
F 1 "OR02" H 3225 10226 50  0000 C CNN
F 2 "" H 3250 10050 50  0001 C CNN
F 3 "" H 3250 10050 50  0001 C CNN
	1    3200 10050
	1    0    0    -1  
$EndComp
$Comp
L A5C:NOR02 UBZ428
U 1 1 5F8C1C96
P 5000 5600
AR Path="/5F8733D0/5F8C1C96" Ref="UBZ428"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/5F8C1C96" Ref="UBZ428"  Part="1" 
F 0 "UBZ428" H 5025 5866 50  0000 C CNN
F 1 "NOR02" H 5025 5775 50  0000 C CNN
F 2 "" H 5000 5600 50  0001 C CNN
F 3 "" H 5000 5600 50  0001 C CNN
	1    5000 5600
	1    0    0    1   
$EndComp
$Comp
L A5C:OR02 UBZ355
U 1 1 601E0ABF
P 9900 3350
AR Path="/5F8733D0/601E0ABF" Ref="UBZ355"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0ABF" Ref="UBZ355"  Part="1" 
F 0 "UBZ355" H 9925 3617 50  0000 C CNN
F 1 "OR02" H 9925 3526 50  0000 C CNN
F 2 "" H 9950 3350 50  0001 C CNN
F 3 "" H 9950 3350 50  0001 C CNN
	1    9900 3350
	1    0    0    -1  
$EndComp
$Comp
L A5C:OR02 UCC354
U 1 1 601E0ABE
P 10000 7400
AR Path="/5F8733D0/601E0ABE" Ref="UCC354"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0ABE" Ref="UCC354"  Part="1" 
F 0 "UCC354" H 10025 7667 50  0000 C CNN
F 1 "OR02" H 10025 7576 50  0000 C CNN
F 2 "" H 10050 7400 50  0001 C CNN
F 3 "" H 10050 7400 50  0001 C CNN
	1    10000 7400
	1    0    0    -1  
$EndComp
$Comp
L A5C:XNOR02 UCC388
U 1 1 601E0ABD
P 10450 2850
AR Path="/5F8733D0/601E0ABD" Ref="UCC388"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0ABD" Ref="UCC388"  Part="1" 
F 0 "UCC388" H 10450 3167 50  0000 C CNN
F 1 "XNOR02" H 10450 3076 50  0000 C CNN
F 2 "" H 10450 2850 50  0001 C CNN
F 3 "" H 10450 2850 50  0001 C CNN
	1    10450 2850
	1    0    0    -1  
$EndComp
$Comp
L A5C:XNOR02 UBZ357
U 1 1 601E0ABC
P 10450 3450
AR Path="/5F8733D0/601E0ABC" Ref="UBZ357"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0ABC" Ref="UBZ357"  Part="1" 
F 0 "UBZ357" H 10450 3767 50  0000 C CNN
F 1 "XNOR02" H 10450 3676 50  0000 C CNN
F 2 "" H 10450 3450 50  0001 C CNN
F 3 "" H 10450 3450 50  0001 C CNN
	1    10450 3450
	1    0    0    -1  
$EndComp
$Comp
L A5C:XNOR02 UBW414
U 1 1 601E0ABB
P 10450 4250
AR Path="/5F8733D0/601E0ABB" Ref="UBW414"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0ABB" Ref="UBW414"  Part="1" 
F 0 "UBW414" H 10450 4567 50  0000 C CNN
F 1 "XNOR02" H 10450 4476 50  0000 C CNN
F 2 "" H 10450 4250 50  0001 C CNN
F 3 "" H 10450 4250 50  0001 C CNN
	1    10450 4250
	1    0    0    -1  
$EndComp
$Comp
L A5C:XNOR02 UCC396
U 1 1 601E0ABA
P 10550 5000
AR Path="/5F8733D0/601E0ABA" Ref="UCC396"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0ABA" Ref="UCC396"  Part="1" 
F 0 "UCC396" H 10550 5317 50  0000 C CNN
F 1 "XNOR02" H 10550 5226 50  0000 C CNN
F 2 "" H 10550 5000 50  0001 C CNN
F 3 "" H 10550 5000 50  0001 C CNN
	1    10550 5000
	1    0    0    -1  
$EndComp
$Comp
L A5C:XNOR02 UBZ341
U 1 1 601E0AB9
P 10550 7500
AR Path="/5F8733D0/601E0AB9" Ref="UBZ341"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0AB9" Ref="UBZ341"  Part="1" 
F 0 "UBZ341" H 10550 7817 50  0000 C CNN
F 1 "XNOR02" H 10550 7726 50  0000 C CNN
F 2 "" H 10550 7500 50  0001 C CNN
F 3 "" H 10550 7500 50  0001 C CNN
	1    10550 7500
	1    0    0    -1  
$EndComp
$Comp
L A5C:XNOR02 UCC422
U 1 1 601E0A65
P 10550 8300
AR Path="/5F8733D0/601E0A65" Ref="UCC422"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0A65" Ref="UCC422"  Part="1" 
F 0 "UCC422" H 10550 8617 50  0000 C CNN
F 1 "XNOR02" H 10550 8526 50  0000 C CNN
F 2 "" H 10550 8300 50  0001 C CNN
F 3 "" H 10550 8300 50  0001 C CNN
	1    10550 8300
	1    0    0    -1  
$EndComp
$Comp
L A5C:XNOR02 UBZ338
U 1 1 7FFFFFFF
P 10550 9050
AR Path="/5F8733D0/7FFFFFFF" Ref="UBZ338"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/7FFFFFFF" Ref="UBZ338"  Part="1" 
F 0 "UBZ338" H 10550 9367 50  0000 C CNN
F 1 "XNOR02" H 10550 9276 50  0000 C CNN
F 2 "" H 10550 9050 50  0001 C CNN
F 3 "" H 10550 9050 50  0001 C CNN
	1    10550 9050
	1    0    0    -1  
$EndComp
$Comp
L A5C:NAND02 UBW440
U 1 1 601E0AE8
P 12200 8600
AR Path="/5F8733D0/601E0AE8" Ref="UBW440"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0AE8" Ref="UBW440"  Part="1" 
F 0 "UBW440" H 12225 8867 50  0000 C CNN
F 1 "NAND02" H 12225 8776 50  0000 C CNN
F 2 "" H 12200 8600 50  0001 C CNN
F 3 "" H 12200 8600 50  0001 C CNN
	1    12200 8600
	1    0    0    -1  
$EndComp
$Comp
L A5C:XNOR02 UBW384
U 1 1 601E0AE7
P 12300 2250
AR Path="/5F8733D0/601E0AE7" Ref="UBW384"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0AE7" Ref="UBW384"  Part="1" 
F 0 "UBW384" H 12300 2567 50  0000 C CNN
F 1 "XNOR02" H 12300 2476 50  0000 C CNN
F 2 "" H 12300 2250 50  0001 C CNN
F 3 "" H 12300 2250 50  0001 C CNN
	1    12300 2250
	1    0    0    -1  
$EndComp
$Comp
L A5C:INV01 UCC383
U 1 1 601E0AE6
P 7350 9150
AR Path="/5F8733D0/601E0AE6" Ref="UCC383"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0AE6" Ref="UCC383"  Part="1" 
F 0 "UCC383" H 7350 9467 50  0000 C CNN
F 1 "INV01" H 7350 9376 50  0000 C CNN
F 2 "" H 7350 8950 50  0001 C CNN
F 3 "" H 7350 8950 50  0001 C CNN
	1    7350 9150
	1    0    0    -1  
$EndComp
$Comp
L A5C:OR03 UBW419
U 1 1 601E0AE5
P 9900 4100
AR Path="/5F8733D0/601E0AE5" Ref="UBW419"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0AE5" Ref="UBW419"  Part="1" 
F 0 "UBW419" H 9900 4417 50  0000 C CNN
F 1 "OR03" H 9900 4326 50  0000 C CNN
F 2 "" H 9900 4100 50  0001 C CNN
F 3 "" H 9900 4100 50  0001 C CNN
	1    9900 4100
	1    0    0    -1  
$EndComp
$Comp
L A5C:OR03 UCC393
U 1 1 601E0AE4
P 10000 8150
AR Path="/5F8733D0/601E0AE4" Ref="UCC393"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0AE4" Ref="UCC393"  Part="1" 
F 0 "UCC393" H 10000 8467 50  0000 C CNN
F 1 "OR03" H 10000 8376 50  0000 C CNN
F 2 "" H 10000 8150 50  0001 C CNN
F 3 "" H 10000 8150 50  0001 C CNN
	1    10000 8150
	1    0    0    -1  
$EndComp
$Comp
L A5C:INV01 UBZ377
U 1 1 601E0AE3
P 10500 2250
AR Path="/5F8733D0/601E0AE3" Ref="UBZ377"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0AE3" Ref="UBZ377"  Part="1" 
F 0 "UBZ377" H 10500 2567 50  0000 C CNN
F 1 "INV01" H 10500 2476 50  0000 C CNN
F 2 "" H 10500 2050 50  0001 C CNN
F 3 "" H 10500 2050 50  0001 C CNN
	1    10500 2250
	1    0    0    -1  
$EndComp
$Comp
L A5C:INV01 UBZ337
U 1 1 601E0AE2
P 10600 9750
AR Path="/5F8733D0/601E0AE2" Ref="UBZ337"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0AE2" Ref="UBZ337"  Part="1" 
F 0 "UBZ337" H 10600 10067 50  0000 C CNN
F 1 "INV01" H 10600 9976 50  0000 C CNN
F 2 "" H 10600 9550 50  0001 C CNN
F 3 "" H 10600 9550 50  0001 C CNN
	1    10600 9750
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFFCOO UBU439
U 1 1 5F8D0C1D
P 4500 6000
AR Path="/5F8733D0/5F8D0C1D" Ref="UBU439"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/5F8D0C1D" Ref="UBU439"  Part="1" 
F 0 "UBU439" H 4500 6361 50  0000 C CNN
F 1 "DFFCOO" H 4500 6270 50  0000 C CNN
F 2 "" H 4500 6000 50  0001 C CNN
F 3 "" H 4500 6000 50  0001 C CNN
F 4 "UBU437" H 4500 6000 50  0001 C CNN "MiscRef"
	1    4500 6000
	1    0    0    -1  
$EndComp
$Comp
L A5C:OR04 UCC385
U 1 1 601E0AE0
P 9900 4800
AR Path="/5F8733D0/601E0AE0" Ref="UCC385"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0AE0" Ref="UCC385"  Part="1" 
F 0 "UCC385" H 9900 5142 50  0000 C CNN
F 1 "OR04" H 9900 5051 50  0000 C CNN
F 2 "" H 9900 4800 50  0001 C CNN
F 3 "" H 9900 4800 50  0001 C CNN
	1    9900 4800
	1    0    0    -1  
$EndComp
$Comp
L A5C:OR04 UBZ349
U 1 1 601E0ADF
P 10000 8850
AR Path="/5F8733D0/601E0ADF" Ref="UBZ349"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0ADF" Ref="UBZ349"  Part="1" 
F 0 "UBZ349" H 10000 9192 50  0000 C CNN
F 1 "OR04" H 10000 9101 50  0000 C CNN
F 2 "" H 10000 8850 50  0001 C CNN
F 3 "" H 10000 8850 50  0001 C CNN
	1    10000 8850
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFFCOS UCC359
U 1 1 601E0AD1
P 8000 3000
AR Path="/5F8733D0/601E0AD1" Ref="UCC359"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0AD1" Ref="UCC359"  Part="1" 
F 0 "UCC359" H 8000 2727 50  0000 C CNN
F 1 "DFFCOS" H 8000 2636 50  0000 C CNN
F 2 "" H 8000 3000 50  0001 C CNN
F 3 "" H 8000 3000 50  0001 C CNN
F 4 "UCC357;UCC377" H 8000 3000 50  0001 C CNN "MiscRef"
	1    8000 3000
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFFCOS UCC363
U 1 1 601E0ADD
P 8000 3750
AR Path="/5F8733D0/601E0ADD" Ref="UCC363"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0ADD" Ref="UCC363"  Part="1" 
F 0 "UCC363" H 8000 3477 50  0000 C CNN
F 1 "DFFCOS" H 8000 3386 50  0000 C CNN
F 2 "" H 8000 3750 50  0001 C CNN
F 3 "" H 8000 3750 50  0001 C CNN
	1    8000 3750
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFFCOS UCC368
U 1 1 601E0ADC
P 8000 4500
AR Path="/5F8733D0/601E0ADC" Ref="UCC368"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0ADC" Ref="UCC368"  Part="1" 
F 0 "UCC368" H 8000 4227 50  0000 C CNN
F 1 "DFFCOS" H 8000 4136 50  0000 C CNN
F 2 "" H 8000 4500 50  0001 C CNN
F 3 "" H 8000 4500 50  0001 C CNN
	1    8000 4500
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFFCOS UCC372
U 1 1 601E0ADB
P 8000 5250
AR Path="/5F8733D0/601E0ADB" Ref="UCC372"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0ADB" Ref="UCC372"  Part="1" 
F 0 "UCC372" H 8000 4977 50  0000 C CNN
F 1 "DFFCOS" H 8000 4886 50  0000 C CNN
F 2 "" H 8000 5250 50  0001 C CNN
F 3 "" H 8000 5250 50  0001 C CNN
	1    8000 5250
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFFCOS UCC400
U 1 1 601E0ADA
P 8000 6100
AR Path="/5F8733D0/601E0ADA" Ref="UCC400"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0ADA" Ref="UCC400"  Part="1" 
F 0 "UCC400" H 8000 5827 50  0000 C CNN
F 1 "DFFCOS" H 8000 5736 50  0000 C CNN
F 2 "" H 8000 6100 50  0001 C CNN
F 3 "" H 8000 6100 50  0001 C CNN
F 4 "UCC399;UCC418" H 8000 6100 50  0001 C CNN "MiscRef"
	1    8000 6100
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFFCOS UCC405
U 1 1 601E0AD9
P 8000 6850
AR Path="/5F8733D0/601E0AD9" Ref="UCC405"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0AD9" Ref="UCC405"  Part="1" 
F 0 "UCC405" H 8000 6577 50  0000 C CNN
F 1 "DFFCOS" H 8000 6486 50  0000 C CNN
F 2 "" H 8000 6850 50  0001 C CNN
F 3 "" H 8000 6850 50  0001 C CNN
	1    8000 6850
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFFCOS UCC409
U 1 1 601E0AD8
P 8000 7600
AR Path="/5F8733D0/601E0AD8" Ref="UCC409"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0AD8" Ref="UCC409"  Part="1" 
F 0 "UCC409" H 8000 7327 50  0000 C CNN
F 1 "DFFCOS" H 8000 7236 50  0000 C CNN
F 2 "" H 8000 7600 50  0001 C CNN
F 3 "" H 8000 7600 50  0001 C CNN
	1    8000 7600
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFFCOS UCC414
U 1 1 601E0AD7
P 8000 8350
AR Path="/5F8733D0/601E0AD7" Ref="UCC414"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0AD7" Ref="UCC414"  Part="1" 
F 0 "UCC414" H 8000 8077 50  0000 C CNN
F 1 "DFFCOS" H 8000 7986 50  0000 C CNN
F 2 "" H 8000 8350 50  0001 C CNN
F 3 "" H 8000 8350 50  0001 C CNN
	1    8000 8350
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFGOO UBR456
U 1 1 601E0AD6
P 12850 2350
AR Path="/5F8733D0/601E0AD6" Ref="UBR456"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0AD6" Ref="UBR456"  Part="1" 
F 0 "UBR456" H 12850 2077 50  0000 C CNN
F 1 "DFGOO" H 12850 1986 50  0000 C CNN
F 2 "" H 12850 2350 50  0001 C CNN
F 3 "" H 12850 2350 50  0001 C CNN
F 4 "UBR453;UBR486" H 12850 2350 50  0001 C CNN "MiscRef"
	1    12850 2350
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFGOO UBR462
U 1 1 601E0AD5
P 12850 3100
AR Path="/5F8733D0/601E0AD5" Ref="UBR462"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0AD5" Ref="UBR462"  Part="1" 
F 0 "UBR462" H 12850 2827 50  0000 C CNN
F 1 "DFGOO" H 12850 2736 50  0000 C CNN
F 2 "" H 12850 3100 50  0001 C CNN
F 3 "" H 12850 3100 50  0001 C CNN
	1    12850 3100
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFGOO UBR468
U 1 1 601E0AD4
P 12850 3850
AR Path="/5F8733D0/601E0AD4" Ref="UBR468"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0AD4" Ref="UBR468"  Part="1" 
F 0 "UBR468" H 12850 3577 50  0000 C CNN
F 1 "DFGOO" H 12850 3486 50  0000 C CNN
F 2 "" H 12850 3850 50  0001 C CNN
F 3 "" H 12850 3850 50  0001 C CNN
	1    12850 3850
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFGOO UBR474
U 1 1 601E0AD3
P 12850 4600
AR Path="/5F8733D0/601E0AD3" Ref="UBR474"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0AD3" Ref="UBR474"  Part="1" 
F 0 "UBR474" H 12850 4327 50  0000 C CNN
F 1 "DFGOO" H 12850 4236 50  0000 C CNN
F 2 "" H 12850 4600 50  0001 C CNN
F 3 "" H 12850 4600 50  0001 C CNN
	1    12850 4600
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFGOO UBR479
U 1 1 601E0AD2
P 12850 5350
AR Path="/5F8733D0/601E0AD2" Ref="UBR479"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0AD2" Ref="UBR479"  Part="1" 
F 0 "UBR479" H 12850 5077 50  0000 C CNN
F 1 "DFGOO" H 12850 4986 50  0000 C CNN
F 2 "" H 12850 5350 50  0001 C CNN
F 3 "" H 12850 5350 50  0001 C CNN
	1    12850 5350
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFGOO UBZ440
U 1 1 601E0AAC
P 12850 6100
AR Path="/5F8733D0/601E0AAC" Ref="UBZ440"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0AAC" Ref="UBZ440"  Part="1" 
F 0 "UBZ440" H 12850 5827 50  0000 C CNN
F 1 "DFGOO" H 12850 5736 50  0000 C CNN
F 2 "" H 12850 6100 50  0001 C CNN
F 3 "" H 12850 6100 50  0001 C CNN
F 4 "UBZ438" H 12850 6100 50  0001 C CNN "MiscRef"
	1    12850 6100
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFGOO UBZ446
U 1 1 601E0A9E
P 12850 6850
AR Path="/5F8733D0/601E0A9E" Ref="UBZ446"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0A9E" Ref="UBZ446"  Part="1" 
F 0 "UBZ446" H 12850 6577 50  0000 C CNN
F 1 "DFGOO" H 12850 6486 50  0000 C CNN
F 2 "" H 12850 6850 50  0001 C CNN
F 3 "" H 12850 6850 50  0001 C CNN
	1    12850 6850
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFGOO UBZ452
U 1 1 601E0A9D
P 12850 7600
AR Path="/5F8733D0/601E0A9D" Ref="UBZ452"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0A9D" Ref="UBZ452"  Part="1" 
F 0 "UBZ452" H 12850 7327 50  0000 C CNN
F 1 "DFGOO" H 12850 7236 50  0000 C CNN
F 2 "" H 12850 7600 50  0001 C CNN
F 3 "" H 12850 7600 50  0001 C CNN
	1    12850 7600
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFGOO UBZ458
U 1 1 601E0A9C
P 12850 8350
AR Path="/5F8733D0/601E0A9C" Ref="UBZ458"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0A9C" Ref="UBZ458"  Part="1" 
F 0 "UBZ458" H 12850 8077 50  0000 C CNN
F 1 "DFGOO" H 12850 7986 50  0000 C CNN
F 2 "" H 12850 8350 50  0001 C CNN
F 3 "" H 12850 8350 50  0001 C CNN
	1    12850 8350
	1    0    0    -1  
$EndComp
$Comp
L A5C:NOR05 UBZ346
U 1 1 601E0A9B
P 9850 9750
AR Path="/5F8733D0/601E0A9B" Ref="UBZ346"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0A9B" Ref="UBZ346"  Part="1" 
F 0 "UBZ346" H 9925 10165 50  0000 C CNN
F 1 "NOR05" H 9925 10074 50  0000 C CNN
F 2 "" H 9850 9800 50  0001 C CNN
F 3 "" H 9850 9800 50  0001 C CNN
	1    9850 9750
	1    0    0    -1  
$EndComp
$Comp
L A5C:R42 UBZ413
U 1 1 74F5DF2D
P 4000 8050
AR Path="/5F8733D0/74F5DF2D" Ref="UBZ413"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/74F5DF2D" Ref="UBZ413"  Part="1" 
F 0 "UBZ413" H 4000 8665 50  0000 C CNN
F 1 "R42" H 4000 8574 50  0000 C CNN
F 2 "" H 3950 8300 50  0001 C CNN
F 3 "" H 3950 8300 50  0001 C CNN
	1    4000 8050
	1    0    0    -1  
$EndComp
$Comp
L A5C:R52 UBZ360
U 1 1 7AD9EE91
P 4000 4900
AR Path="/5F8733D0/7AD9EE91" Ref="UBZ360"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/7AD9EE91" Ref="UBZ360"  Part="1" 
F 0 "UBZ360" H 4000 5565 50  0000 C CNN
F 1 "R52" H 4000 5474 50  0000 C CNN
F 2 "" H 3950 5200 50  0001 C CNN
F 3 "" H 3950 5200 50  0001 C CNN
	1    4000 4900
	1    0    0    -1  
$EndComp
$Comp
L A5C:MUX24H UBZ383
U 1 1 601E0A9A
P 5900 5200
AR Path="/5F8733D0/601E0A9A" Ref="UBZ383"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0A9A" Ref="UBZ383"  Part="1" 
F 0 "UBZ383" H 5900 6065 50  0000 C CNN
F 1 "MUX24H" H 5900 5974 50  0000 C CNN
F 2 "" H 5900 5200 50  0001 C CNN
F 3 "" H 5900 5200 50  0001 C CNN
F 4 "UBZ385;UBZ388;UBZ391;UBZ395" H 5900 5200 50  0001 C CNN "MiscRef"
	1    5900 5200
	1    0    0    -1  
$EndComp
$Comp
L A5C:MUX24H UBZ398
U 1 1 601E0A99
P 5900 8300
AR Path="/5F8733D0/601E0A99" Ref="UBZ398"  Part="1" 
AR Path="/5FBC4837/5FBC4DA0/601E0A99" Ref="UBZ398"  Part="1" 
F 0 "UBZ398" H 5900 9165 50  0000 C CNN
F 1 "MUX24H" H 5900 9074 50  0000 C CNN
F 2 "" H 5900 8300 50  0001 C CNN
F 3 "" H 5900 8300 50  0001 C CNN
F 4 "UBZ400;UBZ403;UBZ406;UBZ410" H 5900 8300 50  0001 C CNN "MiscRef"
	1    5900 8300
	1    0    0    -1  
$EndComp
Entry Wire Line
	14100 2250 14200 2350
Entry Wire Line
	14100 3000 14200 3100
Entry Wire Line
	14100 3750 14200 3850
Entry Wire Line
	14100 4500 14200 4600
Entry Wire Line
	14100 5250 14200 5350
Entry Wire Line
	14100 6000 14200 6100
Entry Wire Line
	14100 6750 14200 6850
Entry Wire Line
	14100 7500 14200 7600
Entry Wire Line
	14100 8250 14200 8350
Wire Wire Line
	13100 2250 14100 2250
Wire Wire Line
	13100 3000 14100 3000
Wire Wire Line
	13100 3750 14100 3750
Wire Wire Line
	13100 4500 14100 4500
Wire Wire Line
	13100 5250 14100 5250
Wire Wire Line
	13100 6000 14100 6000
Wire Wire Line
	13100 6750 14100 6750
Wire Wire Line
	13100 7500 14100 7500
Wire Wire Line
	13100 8250 14100 8250
Wire Bus Line
	14200 8650 14400 8650
Text HLabel 14400 8650 2    50   3State ~ 0
BUS-OUT[0..8]
Text Label 14000 2250 2    50   ~ 0
BUS-OUT0
Text Label 14000 3000 2    50   ~ 0
BUS-OUT1
Text Label 14000 3750 2    50   ~ 0
BUS-OUT2
Text Label 14000 4500 2    50   ~ 0
BUS-OUT3
Text Label 14000 5250 2    50   ~ 0
BUS-OUT4
Text Label 14000 6000 2    50   ~ 0
BUS-OUT5
Text Label 14000 6750 2    50   ~ 0
BUS-OUT6
Text Label 14000 7500 2    50   ~ 0
BUS-OUT7
Text Label 14000 8250 2    50   ~ 0
BUS-OUT8
Connection ~ 8000 2700
Wire Wire Line
	3600 5250 3600 2700
Wire Wire Line
	3600 2700 8000 2700
Connection ~ 3600 5250
Wire Bus Line
	2950 4250 2950 7900
Wire Bus Line
	14200 2350 14200 8650
$EndSCHEMATC
