# DL-0921 (CPS-B-21) Reverse Engineering

![DL-0921](doc/DL-0921.png)

## Render

[Click here to explore the schematics online](https://petitl.fr/cps2/DL-0921/).

[Click here to download a PDF export of the latest version](https://petitl.fr/cps2/DL-0921.pdf).

[![DL-0921](https://petitl.fr/cps2/DL-0921/DL-0921.svg)](https://petitl.fr/cps2/DL-0921/)

## Main Functions

The main functions of this chip are:
* Assemble the graphics data coming from ROM to the palette address
* Produce the video sync signals and clocks
* Handle security and configuration schemes for the different games
* Provide a fast 16 bits multiplier (used as part of the security scheme)
* Provide interrupts based on the raster scan
* Controls the palette ram during the palette copy phase

## Technical documentation

* [Security Scheme](doc/security-scheme.md)
* [Palette Copy Phase](doc/palette-copy.md)
* [Layer Assembly](doc/layer-assembly.md)
* [Video Signals and Graphics Timings](doc/video-signals.md)
* [Configuration Key Layout](doc/configuration-key.md)
* [Raster Effects](doc/raster-effects.md)
* [DL-0411 - B-xx versions](doc/versions.md)
* [Pinout](doc/pinout.md)

## Schematics notes

* The references of all the gates are all of the following format `UXXy`, `XX` points to the row identifier and `y` is the column on the die.
* The name of the gates are the name of the equivalent RICOH RSC-15 cell. Not all names are exactly as written in the databook but, the given names should be in the same naming scheme.

## Authors & Licence & Thanks
See project [README](../../README.md).

Very special thanks to:
* [John McMaster](https://twitter.com/johndmcmaster) For providing a *very high quality* and delayered die shot and additional tooling
* [Eduardo Cruz](http://arcadehacker.blogspot.com/) For being the pioneer on CPS1 reversing, he laid out the ground work for this project
* [Jose Tejada](https://twitter.com/topapate) For helping me understanding some low-tech knowledge about CPS1
* [ElectronAsh](https://twitter.com/ashevans81) For his notes on graphics processing on CPS tech
* [Darksoft](https://www.arcade-projects.com) For his support across the project
* The [MAME developers as a whole](https://www.mamedev.org/) For providing the emulation implementation of CP systems, it's been invaluable
 
