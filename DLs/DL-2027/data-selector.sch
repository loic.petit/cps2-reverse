EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 6
Title "DL-2027"
Date "2020-04-28"
Rev ""
Comp ""
Comment1 ""
Comment2 "Licence: CC-BY"
Comment3 "Author: Loic *WydD* Petit"
Comment4 "Project: CPS2 Reverse Engineering: DL-2027"
$EndDescr
$Comp
L CG24-cells:P24 U2R135
U 1 1 5EF28376
P 2200 2500
F 0 "U2R135" H 2200 3315 50  0000 C CNN
F 1 "P24" H 2200 3224 50  0000 C CNN
F 2 "" H 1900 2950 50  0001 C CNN
F 3 "" H 1900 2950 50  0001 C CNN
	1    2200 2500
	1    0    0    -1  
$EndComp
Text Label 1600 2450 0    50   ~ 0
A2
Text Label 1600 2700 0    50   ~ 0
A3
Text Label 1600 3800 0    50   ~ 0
A4
Text Label 1600 4050 0    50   ~ 0
A5
Text Label 1600 4300 0    50   ~ 0
A6
Text Label 1600 4550 0    50   ~ 0
A7
Text Label 4000 1950 0    50   ~ 0
A8
Text Label 4000 2200 0    50   ~ 0
A9
Text Label 4000 2450 0    50   ~ 0
A10
Text Label 4000 2700 0    50   ~ 0
A11
Text Label 4000 3800 0    50   ~ 0
A12
Text Label 4000 4050 0    50   ~ 0
A13
Text Label 4000 4300 0    50   ~ 0
A14
Text Label 4000 4550 0    50   ~ 0
A15
Text Label 6400 1900 0    50   ~ 0
A16
Text Label 6400 2150 0    50   ~ 0
A17
Text Label 6400 2400 0    50   ~ 0
A18
Text Label 6400 2650 0    50   ~ 0
A19
Text Label 6400 3750 0    50   ~ 0
A20
Text Label 6400 4000 0    50   ~ 0
A21
Text Label 6400 4250 0    50   ~ 0
A22
Text Label 6400 4500 0    50   ~ 0
A23
Text Label 8800 1900 0    50   ~ 0
A24
Text Label 8800 2150 0    50   ~ 0
A25
Text Label 8800 2400 0    50   ~ 0
A26
Text Label 8800 2650 0    50   ~ 0
A27
Text Label 8800 3750 0    50   ~ 0
A28
Text Label 8800 4000 0    50   ~ 0
A29
Text Label 8800 4250 0    50   ~ 0
A30
Text Label 8800 4500 0    50   ~ 0
A31
Text Label 1600 2050 0    50   ~ 0
B0
Text Label 1600 2300 0    50   ~ 0
B1
Text Label 1600 2550 0    50   ~ 0
B2
Text Label 1600 2800 0    50   ~ 0
B3
Text Label 1600 3900 0    50   ~ 0
B4
Text Label 1600 4150 0    50   ~ 0
B5
Text Label 1600 4400 0    50   ~ 0
B6
Text Label 1600 4650 0    50   ~ 0
B7
Text Label 4000 2050 0    50   ~ 0
B8
Text Label 4000 2300 0    50   ~ 0
B9
Text Label 4000 2550 0    50   ~ 0
B10
Text Label 4000 2800 0    50   ~ 0
B11
Text Label 4000 3900 0    50   ~ 0
B12
Text Label 4000 4150 0    50   ~ 0
B13
Text Label 4000 4400 0    50   ~ 0
B14
Text Label 4000 4650 0    50   ~ 0
B15
Text Label 6400 2000 0    50   ~ 0
B16
Text Label 6400 2250 0    50   ~ 0
B17
Text Label 6400 2500 0    50   ~ 0
B18
Text Label 6400 2750 0    50   ~ 0
B19
Text Label 6400 3850 0    50   ~ 0
B20
Text Label 6400 4100 0    50   ~ 0
B21
Text Label 6400 4350 0    50   ~ 0
B22
Text Label 6400 4600 0    50   ~ 0
B23
Text Label 8800 2000 0    50   ~ 0
B24
Text Label 8800 2250 0    50   ~ 0
B25
Text Label 8800 2500 0    50   ~ 0
B26
Text Label 8800 2750 0    50   ~ 0
B27
Text Label 8800 3850 0    50   ~ 0
B28
Text Label 8800 4100 0    50   ~ 0
B29
Text Label 8800 4350 0    50   ~ 0
B30
Text Label 8800 4600 0    50   ~ 0
B31
Wire Wire Line
	1550 2200 1850 2200
Wire Wire Line
	1550 2450 1850 2450
Wire Wire Line
	1550 2700 1850 2700
Wire Wire Line
	1550 1950 1850 1950
Text Label 1600 2200 0    50   ~ 0
A1
Text Label 1600 1950 0    50   ~ 0
A0
Text Label 2750 2250 2    50   ~ 0
Q1
Wire Wire Line
	2850 2250 2550 2250
Wire Wire Line
	2850 2000 2550 2000
Text Label 2750 2000 2    50   ~ 0
Q0
Text Label 2750 2750 2    50   ~ 0
Q3
Wire Wire Line
	2850 2750 2550 2750
Wire Wire Line
	2850 2500 2550 2500
Text Label 2750 2500 2    50   ~ 0
Q2
Entry Wire Line
	1450 1850 1550 1950
Entry Wire Line
	1300 1950 1400 2050
Entry Wire Line
	1300 2200 1400 2300
Entry Wire Line
	1300 2450 1400 2550
Entry Wire Line
	1300 2700 1400 2800
Wire Wire Line
	1400 2050 1850 2050
Wire Wire Line
	1400 2300 1850 2300
Wire Wire Line
	1400 2550 1850 2550
Wire Wire Line
	1400 2800 1850 2800
Entry Wire Line
	1450 2100 1550 2200
Entry Wire Line
	1450 2350 1550 2450
Entry Wire Line
	1450 2600 1550 2700
$Comp
L CG24-cells:P24 U4R135
U 1 1 5EF81D61
P 2200 4350
F 0 "U4R135" H 2200 5165 50  0000 C CNN
F 1 "P24" H 2200 5074 50  0000 C CNN
F 2 "" H 1900 4800 50  0001 C CNN
F 3 "" H 1900 4800 50  0001 C CNN
	1    2200 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 4050 1850 4050
Wire Wire Line
	1550 4300 1850 4300
Wire Wire Line
	1550 4550 1850 4550
Wire Wire Line
	1550 3800 1850 3800
Wire Wire Line
	2850 4100 2550 4100
Wire Wire Line
	2850 3850 2550 3850
Wire Wire Line
	2850 4600 2550 4600
Wire Wire Line
	2850 4350 2550 4350
Entry Wire Line
	1450 3700 1550 3800
Entry Wire Line
	1300 3800 1400 3900
Entry Wire Line
	1300 4050 1400 4150
Entry Wire Line
	1300 4300 1400 4400
Entry Wire Line
	1300 4550 1400 4650
Wire Wire Line
	1400 3900 1850 3900
Wire Wire Line
	1400 4150 1850 4150
Wire Wire Line
	1400 4400 1850 4400
Wire Wire Line
	1400 4650 1850 4650
Entry Wire Line
	1450 3950 1550 4050
Entry Wire Line
	1450 4200 1550 4300
Entry Wire Line
	1450 4450 1550 4550
Entry Wire Line
	2950 4700 2850 4600
Entry Wire Line
	2950 4450 2850 4350
Entry Wire Line
	2950 4200 2850 4100
Entry Wire Line
	2950 3950 2850 3850
Entry Wire Line
	2950 2850 2850 2750
Entry Wire Line
	2950 2600 2850 2500
Entry Wire Line
	2950 2350 2850 2250
Entry Wire Line
	2950 2100 2850 2000
Entry Wire Line
	5350 2100 5250 2000
Entry Wire Line
	5350 2350 5250 2250
Entry Wire Line
	5350 2600 5250 2500
Entry Wire Line
	5350 2850 5250 2750
Entry Wire Line
	3850 4450 3950 4550
Entry Wire Line
	3850 4200 3950 4300
Entry Wire Line
	3850 3950 3950 4050
Wire Wire Line
	3800 4650 4250 4650
Wire Wire Line
	3800 4400 4250 4400
Wire Wire Line
	3800 4150 4250 4150
Wire Wire Line
	3800 3900 4250 3900
Entry Wire Line
	3700 4550 3800 4650
Entry Wire Line
	3700 4300 3800 4400
Entry Wire Line
	3700 4050 3800 4150
Entry Wire Line
	3700 3800 3800 3900
Entry Wire Line
	3850 3700 3950 3800
Wire Wire Line
	5250 4350 4950 4350
Wire Wire Line
	5250 4600 4950 4600
Wire Wire Line
	5250 3850 4950 3850
Wire Wire Line
	5250 4100 4950 4100
Wire Wire Line
	3950 3800 4250 3800
Wire Wire Line
	3950 4550 4250 4550
Wire Wire Line
	3950 4300 4250 4300
Wire Wire Line
	3950 4050 4250 4050
$Comp
L CG24-cells:P24 U6R23
U 1 1 5EFA9B84
P 4600 4350
F 0 "U6R23" H 4600 5165 50  0000 C CNN
F 1 "P24" H 4600 5074 50  0000 C CNN
F 2 "" H 4300 4800 50  0001 C CNN
F 3 "" H 4300 4800 50  0001 C CNN
	1    4600 4350
	1    0    0    -1  
$EndComp
Entry Wire Line
	3850 2600 3950 2700
Entry Wire Line
	3850 2350 3950 2450
Entry Wire Line
	3850 2100 3950 2200
Wire Wire Line
	3800 2800 4250 2800
Wire Wire Line
	3800 2550 4250 2550
Wire Wire Line
	3800 2300 4250 2300
Wire Wire Line
	3800 2050 4250 2050
Entry Wire Line
	3700 2700 3800 2800
Entry Wire Line
	3700 2450 3800 2550
Entry Wire Line
	3700 2200 3800 2300
Entry Wire Line
	3700 1950 3800 2050
Entry Wire Line
	3850 1850 3950 1950
Wire Wire Line
	3950 1950 4250 1950
Wire Wire Line
	3950 2700 4250 2700
Wire Wire Line
	3950 2450 4250 2450
Wire Wire Line
	3950 2200 4250 2200
$Comp
L CG24-cells:P24 U12R23
U 1 1 5EFB2341
P 4600 2500
F 0 "U12R23" H 4600 3315 50  0000 C CNN
F 1 "P24" H 4600 3224 50  0000 C CNN
F 2 "" H 4300 2950 50  0001 C CNN
F 3 "" H 4300 2950 50  0001 C CNN
	1    4600 2500
	1    0    0    -1  
$EndComp
Entry Wire Line
	7650 4550 7750 4650
Entry Wire Line
	7650 4300 7750 4400
Entry Wire Line
	7650 4050 7750 4150
Entry Wire Line
	7650 3800 7750 3900
Entry Wire Line
	6250 4400 6350 4500
Entry Wire Line
	6250 4150 6350 4250
Entry Wire Line
	6250 3900 6350 4000
Wire Wire Line
	6200 4600 6650 4600
Wire Wire Line
	6200 4350 6650 4350
Wire Wire Line
	6200 4100 6650 4100
Wire Wire Line
	6200 3850 6650 3850
Entry Wire Line
	6100 4500 6200 4600
Entry Wire Line
	6100 4250 6200 4350
Entry Wire Line
	6100 4000 6200 4100
Entry Wire Line
	6100 3750 6200 3850
Entry Wire Line
	6250 3650 6350 3750
Wire Wire Line
	7650 4300 7350 4300
Wire Wire Line
	7650 4550 7350 4550
Wire Wire Line
	7650 3800 7350 3800
Wire Wire Line
	7650 4050 7350 4050
Wire Wire Line
	6350 3750 6650 3750
Wire Wire Line
	6350 4500 6650 4500
Wire Wire Line
	6350 4250 6650 4250
Wire Wire Line
	6350 4000 6650 4000
$Comp
L CG24-cells:P24 U6R120
U 1 1 5EFBA61D
P 7000 4300
F 0 "U6R120" H 7000 5115 50  0000 C CNN
F 1 "P24" H 7000 5024 50  0000 C CNN
F 2 "" H 6700 4750 50  0001 C CNN
F 3 "" H 6700 4750 50  0001 C CNN
	1    7000 4300
	1    0    0    -1  
$EndComp
Entry Wire Line
	7650 2700 7750 2800
Entry Wire Line
	7650 2450 7750 2550
Entry Wire Line
	7650 2200 7750 2300
Entry Wire Line
	7650 1950 7750 2050
Entry Wire Line
	6250 2550 6350 2650
Entry Wire Line
	6250 2300 6350 2400
Entry Wire Line
	6250 2050 6350 2150
Wire Wire Line
	6200 2750 6650 2750
Wire Wire Line
	6200 2500 6650 2500
Wire Wire Line
	6200 2250 6650 2250
Wire Wire Line
	6200 2000 6650 2000
Entry Wire Line
	6100 2650 6200 2750
Entry Wire Line
	6100 2400 6200 2500
Entry Wire Line
	6100 2150 6200 2250
Entry Wire Line
	6100 1900 6200 2000
Entry Wire Line
	6250 1800 6350 1900
Wire Wire Line
	7650 2450 7350 2450
Wire Wire Line
	7650 2700 7350 2700
Wire Wire Line
	7650 1950 7350 1950
Wire Wire Line
	7650 2200 7350 2200
Wire Wire Line
	6350 1900 6650 1900
Wire Wire Line
	6350 2650 6650 2650
Wire Wire Line
	6350 2400 6650 2400
Wire Wire Line
	6350 2150 6650 2150
$Comp
L CG24-cells:P24 U8R23
U 1 1 5EFBA63B
P 7000 2450
F 0 "U8R23" H 7000 3265 50  0000 C CNN
F 1 "P24" H 7000 3174 50  0000 C CNN
F 2 "" H 6700 2900 50  0001 C CNN
F 3 "" H 6700 2900 50  0001 C CNN
	1    7000 2450
	1    0    0    -1  
$EndComp
Entry Wire Line
	10050 4550 10150 4650
Entry Wire Line
	10050 4300 10150 4400
Entry Wire Line
	10050 4050 10150 4150
Entry Wire Line
	10050 3800 10150 3900
Entry Wire Line
	8650 4400 8750 4500
Entry Wire Line
	8650 4150 8750 4250
Entry Wire Line
	8650 3900 8750 4000
Wire Wire Line
	8600 4600 9050 4600
Wire Wire Line
	8600 4350 9050 4350
Wire Wire Line
	8600 4100 9050 4100
Wire Wire Line
	8600 3850 9050 3850
Entry Wire Line
	8500 4500 8600 4600
Entry Wire Line
	8500 4250 8600 4350
Entry Wire Line
	8500 4000 8600 4100
Entry Wire Line
	8500 3750 8600 3850
Entry Wire Line
	8650 3650 8750 3750
Wire Wire Line
	10050 4300 9750 4300
Wire Wire Line
	10050 4550 9750 4550
Wire Wire Line
	10050 3800 9750 3800
Wire Wire Line
	10050 4050 9750 4050
Wire Wire Line
	8750 3750 9050 3750
Wire Wire Line
	8750 4500 9050 4500
Wire Wire Line
	8750 4250 9050 4250
Wire Wire Line
	8750 4000 9050 4000
$Comp
L CG24-cells:P24 U28R21
U 1 1 5EFD6366
P 9400 4300
F 0 "U28R21" H 9400 5115 50  0000 C CNN
F 1 "P24" H 9400 5024 50  0000 C CNN
F 2 "" H 9100 4750 50  0001 C CNN
F 3 "" H 9100 4750 50  0001 C CNN
	1    9400 4300
	1    0    0    -1  
$EndComp
Entry Wire Line
	10050 2700 10150 2800
Entry Wire Line
	10050 2450 10150 2550
Entry Wire Line
	10050 2200 10150 2300
Entry Wire Line
	10050 1950 10150 2050
Entry Wire Line
	8650 2550 8750 2650
Entry Wire Line
	8650 2300 8750 2400
Entry Wire Line
	8650 2050 8750 2150
Wire Wire Line
	8600 2750 9050 2750
Wire Wire Line
	8600 2500 9050 2500
Wire Wire Line
	8600 2250 9050 2250
Wire Wire Line
	8600 2000 9050 2000
Entry Wire Line
	8500 2650 8600 2750
Entry Wire Line
	8500 2400 8600 2500
Entry Wire Line
	8500 2150 8600 2250
Entry Wire Line
	8500 1900 8600 2000
Entry Wire Line
	8650 1800 8750 1900
Wire Wire Line
	10050 2450 9750 2450
Wire Wire Line
	10050 2700 9750 2700
Wire Wire Line
	10050 1950 9750 1950
Wire Wire Line
	10050 2200 9750 2200
Wire Wire Line
	8750 1900 9050 1900
Wire Wire Line
	8750 2650 9050 2650
Wire Wire Line
	8750 2400 9050 2400
Wire Wire Line
	8750 2150 9050 2150
$Comp
L CG24-cells:P24 U26R120
U 1 1 5EFE80FC
P 9400 2450
F 0 "U26R120" H 9400 3265 50  0000 C CNN
F 1 "P24" H 9400 3174 50  0000 C CNN
F 2 "" H 9100 2900 50  0001 C CNN
F 3 "" H 9100 2900 50  0001 C CNN
	1    9400 2450
	1    0    0    -1  
$EndComp
Text Label 2650 3850 0    50   ~ 0
Q4
Text Label 2650 4100 0    50   ~ 0
Q5
Text Label 2650 4350 0    50   ~ 0
Q6
Text Label 2650 4600 0    50   ~ 0
Q7
Text Label 5050 2000 0    50   ~ 0
Q8
Text Label 5050 2250 0    50   ~ 0
Q9
Text Label 5050 3850 0    50   ~ 0
Q12
Text Label 5050 4100 0    50   ~ 0
Q13
Text Label 5050 4350 0    50   ~ 0
Q14
Text Label 5050 4600 0    50   ~ 0
Q15
Text Label 7450 1950 0    50   ~ 0
Q16
Text Label 7450 2200 0    50   ~ 0
Q17
Text Label 7450 2450 0    50   ~ 0
Q18
Text Label 7450 2700 0    50   ~ 0
Q19
Text Label 7450 3800 0    50   ~ 0
Q20
Text Label 7450 4050 0    50   ~ 0
Q21
Text Label 7450 4300 0    50   ~ 0
Q22
Text Label 7450 4550 0    50   ~ 0
Q23
Text Label 9850 1950 0    50   ~ 0
Q24
Text Label 9850 2200 0    50   ~ 0
Q25
Text Label 9850 2450 0    50   ~ 0
Q26
Text Label 9850 2700 0    50   ~ 0
Q27
Text Label 9850 3800 0    50   ~ 0
Q28
Text Label 9850 4050 0    50   ~ 0
Q29
Text Label 9850 4300 0    50   ~ 0
Q30
Text Label 9850 4550 0    50   ~ 0
Q31
Text Label 5050 2750 0    50   ~ 0
Q11
Text Label 5050 2500 0    50   ~ 0
Q10
Wire Wire Line
	5250 2250 4950 2250
Wire Wire Line
	5250 2000 4950 2000
Wire Wire Line
	5250 2750 4950 2750
Wire Wire Line
	5250 2500 4950 2500
Entry Wire Line
	5350 4700 5250 4600
Entry Wire Line
	5350 4450 5250 4350
Entry Wire Line
	5350 4200 5250 4100
Entry Wire Line
	5350 3950 5250 3850
Wire Bus Line
	2950 5550 5350 5550
Wire Bus Line
	7750 5550 10150 5550
Wire Bus Line
	3700 1400 1300 1400
Wire Bus Line
	3700 1400 6100 1400
Connection ~ 3700 1400
Wire Bus Line
	6100 1400 8500 1400
Connection ~ 6100 1400
Wire Bus Line
	1450 1500 3850 1500
Connection ~ 6250 1500
Wire Bus Line
	6250 1500 8650 1500
Connection ~ 3850 1500
Wire Bus Line
	3850 1500 6250 1500
Wire Bus Line
	10150 5550 10450 5550
Text HLabel 10450 5550 2    50   Output ~ 0
Q[0..31]
Text HLabel 1150 1500 0    50   Input ~ 0
A[0..31]
Wire Bus Line
	1150 1500 1450 1500
Connection ~ 1450 1500
Text HLabel 1150 1400 0    50   Input ~ 0
B[0..31]
Wire Bus Line
	1150 1400 1300 1400
Connection ~ 1300 1400
Wire Wire Line
	1850 3100 1200 3100
Wire Wire Line
	1200 3100 1200 4950
Wire Wire Line
	1200 4950 1850 4950
Wire Wire Line
	1100 4850 1100 3000
Wire Wire Line
	1100 3000 1850 3000
Wire Wire Line
	1100 4850 1850 4850
Wire Wire Line
	4250 3100 3600 3100
Wire Wire Line
	3600 3100 3600 4950
Wire Wire Line
	3600 4950 4250 4950
Wire Wire Line
	3500 4850 3500 3000
Wire Wire Line
	3500 3000 4250 3000
Wire Wire Line
	3500 4850 4250 4850
Wire Wire Line
	6650 3050 6000 3050
Wire Wire Line
	6000 3050 6000 4900
Wire Wire Line
	6000 4900 6650 4900
Wire Wire Line
	5900 4800 5900 2950
Wire Wire Line
	5900 2950 6650 2950
Wire Wire Line
	5900 4800 6650 4800
Wire Wire Line
	9050 3050 8400 3050
Wire Wire Line
	8400 3050 8400 4900
Wire Wire Line
	8400 4900 9050 4900
Wire Wire Line
	8300 4800 8300 2950
Wire Wire Line
	8300 2950 9050 2950
Wire Wire Line
	8300 4800 9050 4800
Wire Wire Line
	1200 4950 1200 5750
Wire Wire Line
	1200 5750 3600 5750
Wire Wire Line
	8400 5750 8400 4900
Connection ~ 1200 4950
Connection ~ 8400 4900
Wire Wire Line
	6000 4900 6000 5750
Connection ~ 6000 4900
Connection ~ 6000 5750
Wire Wire Line
	6000 5750 8400 5750
Wire Wire Line
	3600 4950 3600 5750
Connection ~ 3600 4950
Connection ~ 3600 5750
Wire Wire Line
	3600 5750 6000 5750
Wire Wire Line
	1100 4850 1100 5650
Wire Wire Line
	1100 5650 3500 5650
Wire Wire Line
	8300 5650 8300 4800
Wire Wire Line
	5900 4800 5900 5650
Connection ~ 5900 5650
Wire Wire Line
	5900 5650 8300 5650
Wire Wire Line
	3500 4850 3500 5650
Connection ~ 3500 5650
Wire Wire Line
	3500 5650 5900 5650
Connection ~ 5350 5550
Wire Bus Line
	7750 5550 5350 5550
Connection ~ 7750 5550
Connection ~ 10150 5550
Connection ~ 1100 4850
Connection ~ 3500 4850
Connection ~ 5900 4800
Connection ~ 8300 4800
Wire Wire Line
	1100 5650 1000 5650
Connection ~ 1100 5650
Wire Wire Line
	1200 5750 1000 5750
Connection ~ 1200 5750
Text HLabel 1000 5650 0    50   Input ~ 0
SA
Text HLabel 1000 5750 0    50   Input ~ 0
SB
Wire Bus Line
	8500 1400 8500 4500
Wire Bus Line
	8650 1500 8650 4400
Wire Bus Line
	10150 2050 10150 5550
Wire Bus Line
	6100 1400 6100 4500
Wire Bus Line
	6250 1500 6250 4400
Wire Bus Line
	7750 2050 7750 5550
Wire Bus Line
	3700 1400 3700 4550
Wire Bus Line
	3850 1500 3850 4450
Wire Bus Line
	5350 2100 5350 5550
Wire Bus Line
	2950 2100 2950 5550
Wire Bus Line
	1300 1400 1300 4550
Wire Bus Line
	1450 1500 1450 4450
$EndSCHEMATC
