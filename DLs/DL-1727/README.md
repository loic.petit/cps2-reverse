# DL-1727 "MIF" Reverse Engineering

![DL-1727](./DL-1727.jpg)

## PDF Render

[Click here to explore the schematics online](https://petitl.fr/cps2/DL-1727/).

[Click here to download a PDF export of the latest version](https://petitl.fr/cps2/DL-1727.pdf).

[![DL-1727](https://petitl.fr/cps2/DL-1727/DL-1727.svg)](https://petitl.fr/cps2/DL-1727/)

## Function

This chip is in charge of the following:
* Transmit the main CPU bus to the peripherals
* Handles five specific functions on the bus (see notes)
* Provides hook-up points for the developer

## 8040xx functions

### Registers definition

The MIF contains additional registers.

| Name | Size | Bus Position | Default | Pin |
| ----- | ----- | ----- | ----- | ----- |
| REG8 | 1 | `0` | LOW | 147 |
| REG9 | 5 | `0..4` | LOW | 32,31,107,108,109 |
|  | 1 | `15` | HIGH | 111 |
| REGA | 1 | `0` | HIGH | 137 |

### Bus operations
The DL-1727 functions can be accessed using the following bit mask `0b1000 0000 01xx xxxx xxxx xxxx` which corresponds to `0x804xxx`-`0x807xxx`.

| Bit mask | Operation | Description |
| ----- | ----- | ----- |
| `0bxxx0 1000 xxxx`<br>`0x08x` | Write Low | Write on REG8 |
| | Read | Contains the data available on the MISC-D bus (see additional notes) |
| `0bxxx0 1001 xxxx`<br>`0x09x` | Write Low | Write on REG9 (LSB) |
| | Write High | Write on REG9 (MSB) |
| | Read | Provides the REG9 data. This can be overriden using REGA (see additional notes) |
| `0bxxx0 1010 xxxx`<br>`0x0Ax` | Write Low | Write on REGA |
| `0bxxx0 1011 xxxx`<br>`0x0Bx` | Read | Transmit the read signal to pin 46. Provide whatever is on the peripherals bus. (Unused) |
| `0bxxx0 1110 xxxx`<br>`0x0Ex` | Write | Transmit the write signal to pin 136 (for the DL-1827 CIF to write the OBJRAM bank change) |

### REGA role
REGA disables the bus transceivers by muting `/CBUS-ADDR-E` in particular and also overrides the `REG9` output by transmitting the `REG-D` as input.

It is by *active* by default, therefore it MUST be deactivated to use the A-board properly. It is maybe used to inject some boot setting using REG9 I/O.

### The MISC bus and REG8
Usage is unknown.

This I/O is actually a 3bit decoder. For instance, if `011` is the input of `MISC-S[0..2]`, all outputs will be Hi-Z except pin 153 (= `MISC-D3`).

On the CPS2, all those those are just bound to a pull-up resistor array. You can actually measure on the CPS2 that all pins are HIGH except pin 147 (`MISC-D7`) because the selection pins forced to HIGH (= `111` = 7).

The state of this I/O can be read using the REG8 address. `DATA[8..10]` will contain the `S[0..2]` data.

## Schematics notes

* The references of all the gates are all of the following format `UxRy`. `x` and `y` point to the coordinates in the gate array.
* The name of the gate is the name of the equivalent Fujitsu CG10 cell. One exception is the `B12` which looks and behaves like a 1-bit bus driver (`B11`) without the input transistors (maybe it's a CG24 only cell?).
* All the available gates are shown in the schematic even the buffers. That is why there are files with only buffers that transform pins like `_CLK2M` to `CLK2M` for   
instance. The used notation being that, in case of conflicts, signals with `_` as a prefix are the unbuffered direct pin.
* Please read the additional notes written in the schematics. They include additional information than what is described here.

## Pinout

| Pin | Type | Name |
| ----- | ----- | ----- |
| 1 | GND |  |
| 2 | Bidirectional | CPU-DATA15 |
| 3 | Bidirectional | CPU-DATA14 |
| 4 | Bidirectional | CPU-DATA13 |
| 5 | Bidirectional | CPU-DATA12 |
| 6 | Bidirectional | CPU-DATA11 |
| 7 | Bidirectional | CPU-DATA10 |
| 8 | Bidirectional | CPU-DATA9 |
| 9 | Bidirectional | CPU-DATA8 |
| 10 | GND |  |
| 11 | Bidirectional | CPU-DATA7 |
| 12 | Bidirectional | CPU-DATA6 |
| 13 | Bidirectional | CPU-DATA5 |
| 14 | Bidirectional | CPU-DATA4 |
| 15 | Bidirectional | CPU-DATA3 |
| 16 | Bidirectional | CPU-DATA2 |
| 17 | Bidirectional | CPU-DATA1 |
| 18 | Bidirectional | CPU-DATA0 |
| 19 | NC |  |
| 20 | VCC |  |
| 21 | 3 State | BUS-ADDR23 |
| 22 | 3 State | BUS-ADDR22 |
| 23 | NC |  |
| 24 | Input | /ENABLE-DATA |
| 25 | Input | /A-BOARD-CONNECTED |
| 26 | NC |  |
| 27 | 3 State | BUS-ADDR21 |
| 28 | 3 State | BUS-ADDR20 |
| 29 | 3 State | BUS-ADDR19 |
| 30 | GND |  |
| 31 | Bidirectional | REG9-D1 |
| 32 | Bidirectional | REG9-D0 |
| 33 | NC |  |
| 34 | Input | ENABLE-ADDR |
| 35 | Input | DEBUG |
| 36 | NC |  |
| 37 | 3 State | BUS-ADDR18 |
| 38 | 3 State | BUS-ADDR17 |
| 39 | 3 State | BUS-ADDR16 |
| 40 | VCC |  |
| 41 | GND |  |
| 42 | 3 State | BUS-ADDR15 |
| 43 | 3 State | BUS-ADDR14 |
| 44 | 3 State | CBUS-ADDR-DIR |
| 45 | 3 State | /CBUS-ADDR-E |
| 46 | 3 State | /REGB-R |
| 47 | Input | CPU-ADDR23 |
| 48 | Input | CPU-ADDR22 |
| 49 | Input | CPU-ADDR21 |
| 50 | GND |  |
| 51 | Clock Input | CLK2M |
| 52 | Input | CPU-ADDR20 |
| 53 | Input | CPU-ADDR19 |
| 54 | Input | /RESETI |
| 55 | Input | CPU-ADDR18 |
| 56 | Input | VCC-OK |
| 57 | Input | CPU-ADDR17 |
| 58 | Clock Input | CLK16M |
| 59 | GND |  |
| 60 | VCC |  |
| 61 | Input | /RESET-REGISTERS |
| 62 | Input | CPU-ADDR16 |
| 63 | Input | CPU-ADDR15 |
| 64 | Input | CPU-ADDR14 |
| 65 | 3 State | CBUS-DATA-DIR |
| 66 | Output | /RESET |
| 67 | 3 State | /EXPANSION-E |
| 68 | 3 State | BUS-ADDR13 |
| 69 | 3 State | BUS-ADDR12 |
| 70 | GND |  |
| 71 | 3 State | BUS-ADDR11 |
| 72 | 3 State | BUS-ADDR10 |
| 73 | 3 State | BUS-ADDR9 |
| 74 | 3 State | DEBUG-Q7 |
| 75 | Input | MISC-S2 |
| 76 | Input | MISC-S1 |
| 77 | Input | MISC-S0 |
| 78 | 3 State | BUS-ADDR8 |
| 79 | 3 State | BUS-ADDR7 |
| 80 | VCC |  |
| 81 | GND |  |
| 82 | 3 State | BUS-ADDR6 |
| 83 | 3 State | BUS-ADDR5 |
| 84 | 3 State | BUS-ADDR4 |
| 85 | Input | DEBUG-S2 |
| 86 | Input | DEBUG-S1 |
| 87 | Input | DEBUG-S0 |
| 88 | 3 State | BUS-ADDR3 |
| 89 | 3 State | BUS-ADDR2 |
| 90 | GND |  |
| 91 | 3 State | BUS-ADDR1 |
| 92 | 3 State | DEBUG-Q6 |
| 93 | Input | CPU-ADDR13 |
| 94 | Input | CPU-ADDR12 |
| 95 | Input | CPU-ADDR11 |
| 96 | Input | CPU-ADDR10 |
| 97 | Input | CPU-ADDR9 |
| 98 | Input | CPU-ADDR8 |
| 99 | Input | CPU-ADDR7 |
| 100 | VCC |  |
| 101 | Input | CPU-ADDR6 |
| 102 | Input | CPU-ADDR5 |
| 103 | Input | CPU-ADDR4 |
| 104 | Input | CPU-ADDR3 |
| 105 | Input | CPU-ADDR2 |
| 106 | Input | CPU-ADDR1 |
| 107 | Bidirectional | REG9-D2 |
| 108 | Bidirectional | REG9-D3 |
| 109 | Bidirectional | REG9-D4 |
| 110 | GND |  |
| 111 | Bidirectional | REG9-D15 |
| 112 | Bidirectional | BUS-DATA15 |
| 113 | Bidirectional | BUS-DATA14 |
| 114 | 3 State | DEBUG-Q5 |
| 115 | 3 State | DEBUG-Q4 |
| 116 | Bidirectional | BUS-DATA13 |
| 117 | Bidirectional | BUS-DATA12 |
| 118 | Bidirectional | BUS-DATA11 |
| 119 | Bidirectional | BUS-DATA10 |
| 120 | VCC |  |
| 121 | GND |  |
| 122 | Bidirectional | BUS-DATA9 |
| 123 | Bidirectional | BUS-DATA8 |
| 124 | Bidirectional | BUS-DATA7 |
| 125 | 3 State | DEBUG-Q3 |
| 126 | Bidirectional | BUS-DATA6 |
| 127 | Bidirectional | BUS-DATA5 |
| 128 | Bidirectional | BUS-DATA4 |
| 129 | Bidirectional | BUS-DATA3 |
| 130 | GND |  |
| 131 | Bidirectional | BUS-DATA2 |
| 132 | Bidirectional | BUS-DATA1 |
| 133 | Bidirectional | BUS-DATA0 |
| 134 | 3 State | DEBUG-Q2 |
| 135 | Output | CLK2M |
| 136 | 3 State | /OBJRAM-BANK-W |
| 137 | 3 State | REGA |
| 138 | Output | CLK16M |
| 139 | GND |  |
| 140 | VCC |  |
| 141 | Input | BUS-DIR |
| 142 | Input | /BUS-E |
| 143 | Input | /BUS-AS |
| 144 | Input | /BUS-R |
| 145 | Input | /BUS-LDSW |
| 146 | Input | /BUS-UDSW |
| 147 | 3 State | REG8 |
| 148 | Bidirectional | MISC-D7 |
| 149 | Bidirectional | MISC-D6 |
| 150 | GND |  |
| 151 | Bidirectional | MISC-D5 |
| 152 | Bidirectional | MISC-D4 |
| 153 | Bidirectional | MISC-D3 |
| 154 | Output | /RESETI-OUT |
| 155 | 3 State | DEBUG-Q1 |
| 156 | 3 State | DEBUG-Q0 |
| 157 | Bidirectional | MISC-D2 |
| 158 | Bidirectional | MISC-D1 |
| 159 | Bidirectional | MISC-D0 |
| 160 | VCC |  |


## Authors & Licence & Thanks
See main [README](../../README.md) and the [DL README](../README.md).
