EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 61 83
Title "CPS Reverse Engineering: DL-0311"
Date "2021-09-12"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 "Licence: CC-BY"
Comment4 "Author: Loïc *WydD* Petit"
$EndDescr
Connection ~ 3550 4150
Connection ~ 3550 2350
Connection ~ 3550 1550
Connection ~ 3550 3250
Connection ~ 3550 5950
Connection ~ 3550 5050
Connection ~ 3550 6850
Entry Wire Line
	2850 700  2950 800 
Entry Wire Line
	2850 1250 2950 1350
Entry Wire Line
	2850 1800 2950 1900
Entry Wire Line
	2850 1900 2950 2000
Entry Wire Line
	2850 2000 2950 2100
Entry Wire Line
	2850 2100 2950 2200
Entry Wire Line
	2850 2700 2950 2800
Entry Wire Line
	2850 2800 2950 2900
Entry Wire Line
	2850 2900 2950 3000
Entry Wire Line
	2850 3000 2950 3100
Entry Wire Line
	2850 3600 2950 3700
Entry Wire Line
	2850 3700 2950 3800
Entry Wire Line
	2850 3800 2950 3900
Entry Wire Line
	2850 3900 2950 4000
Entry Wire Line
	2850 4500 2950 4600
Entry Wire Line
	2850 4600 2950 4700
Entry Wire Line
	2850 4700 2950 4800
Entry Wire Line
	2850 4800 2950 4900
Entry Wire Line
	2850 5400 2950 5500
Entry Wire Line
	2850 5500 2950 5600
Entry Wire Line
	2850 5600 2950 5700
Entry Wire Line
	2850 5700 2950 5800
Entry Wire Line
	2850 6300 2950 6400
Entry Wire Line
	2850 6400 2950 6500
Entry Wire Line
	2850 6500 2950 6600
Entry Wire Line
	2850 6600 2950 6700
Entry Wire Line
	4900 2200 5000 2300
Entry Wire Line
	4900 2800 5000 2900
Entry Wire Line
	4900 2900 5000 3000
Entry Wire Line
	4900 3000 5000 3100
Entry Wire Line
	4900 3100 5000 3200
Entry Wire Line
	5200 1250 5100 1350
Entry Wire Line
	5200 1800 5100 1900
Entry Wire Line
	5200 3700 5300 3800
Entry Wire Line
	5200 3800 5300 3900
Entry Wire Line
	5200 3900 5300 4000
Entry Wire Line
	5200 4000 5300 4100
Entry Wire Line
	5200 4600 5300 4700
Entry Wire Line
	5200 4700 5300 4800
Entry Wire Line
	5200 4800 5300 4900
Entry Wire Line
	5200 4900 5300 5000
Entry Wire Line
	5200 5500 5300 5600
Entry Wire Line
	5200 5600 5300 5700
Entry Wire Line
	5200 5700 5300 5800
Entry Wire Line
	5200 5800 5300 5900
Entry Wire Line
	5200 6400 5300 6500
Entry Wire Line
	5200 6500 5300 6600
Entry Wire Line
	5200 6600 5300 6700
Entry Wire Line
	5200 6700 5300 6800
Wire Wire Line
	2950 800  3950 800 
Wire Wire Line
	2950 1350 3950 1350
Wire Wire Line
	2950 1900 3650 1900
Wire Wire Line
	2950 2000 3650 2000
Wire Wire Line
	2950 2100 3650 2100
Wire Wire Line
	2950 2200 3650 2200
Wire Wire Line
	2950 2800 3650 2800
Wire Wire Line
	2950 2900 3650 2900
Wire Wire Line
	2950 3000 3650 3000
Wire Wire Line
	2950 3100 3650 3100
Wire Wire Line
	2950 3700 3650 3700
Wire Wire Line
	2950 3800 3650 3800
Wire Wire Line
	2950 3900 3650 3900
Wire Wire Line
	2950 4000 3650 4000
Wire Wire Line
	2950 4600 3650 4600
Wire Wire Line
	2950 4700 3650 4700
Wire Wire Line
	2950 4800 3650 4800
Wire Wire Line
	2950 4900 3650 4900
Wire Wire Line
	2950 5500 3650 5500
Wire Wire Line
	2950 5600 3650 5600
Wire Wire Line
	2950 5700 3650 5700
Wire Wire Line
	2950 5800 3650 5800
Wire Wire Line
	2950 6400 3650 6400
Wire Wire Line
	2950 6500 3650 6500
Wire Wire Line
	2950 6600 3650 6600
Wire Wire Line
	2950 6700 3650 6700
Wire Wire Line
	3450 6850 3550 6850
Wire Wire Line
	3550 1000 3550 1550
Wire Wire Line
	3550 1000 4350 1000
Wire Wire Line
	3550 1550 3550 2350
Wire Wire Line
	3550 2350 3550 3250
Wire Wire Line
	3550 3250 3550 4150
Wire Wire Line
	3550 4150 3650 4150
Wire Wire Line
	3550 5050 3550 4150
Wire Wire Line
	3550 5050 3650 5050
Wire Wire Line
	3550 5950 3550 5050
Wire Wire Line
	3550 5950 3650 5950
Wire Wire Line
	3550 6850 3550 5950
Wire Wire Line
	3550 6850 3650 6850
Wire Wire Line
	3650 2350 3550 2350
Wire Wire Line
	3650 3250 3550 3250
Wire Wire Line
	4250 900  4350 900 
Wire Wire Line
	4250 1450 4350 1450
Wire Wire Line
	4350 800  5100 800 
Wire Wire Line
	4350 1000 4350 900 
Wire Wire Line
	4350 1350 5100 1350
Wire Wire Line
	4350 1550 3550 1550
Wire Wire Line
	4350 1550 4350 1450
Wire Wire Line
	4650 2200 4900 2200
Wire Wire Line
	4650 2800 4900 2800
Wire Wire Line
	4650 2900 4900 2900
Wire Wire Line
	4650 3000 4900 3000
Wire Wire Line
	4650 3100 4900 3100
Wire Wire Line
	5100 1900 4650 1900
Wire Wire Line
	5100 2000 4650 2000
Wire Wire Line
	5100 2100 4650 2100
Wire Wire Line
	5200 3700 4650 3700
Wire Wire Line
	5200 3800 4650 3800
Wire Wire Line
	5200 3900 4650 3900
Wire Wire Line
	5200 4000 4650 4000
Wire Wire Line
	5200 4600 4650 4600
Wire Wire Line
	5200 4700 4650 4700
Wire Wire Line
	5200 4800 4650 4800
Wire Wire Line
	5200 4900 4650 4900
Wire Wire Line
	5200 5500 4650 5500
Wire Wire Line
	5200 5600 4650 5600
Wire Wire Line
	5200 5700 4650 5700
Wire Wire Line
	5200 5800 4650 5800
Wire Wire Line
	5200 6400 4650 6400
Wire Wire Line
	5200 6500 4650 6500
Wire Wire Line
	5200 6600 4650 6600
Wire Wire Line
	5200 6700 4650 6700
Wire Bus Line
	2850 600  2750 600 
Wire Bus Line
	2850 3450 2750 3450
Wire Bus Line
	5000 3300 5100 3300
Wire Bus Line
	5200 1150 5300 1150
Wire Bus Line
	5300 6900 5450 6900
Text Label 3000 800  0    50   ~ 0
~RAM-ATTR9
Text Label 3000 1350 0    50   ~ 0
~RAM-ATTR8
Text Label 3000 1900 0    50   ~ 0
~RAM-ATTR7
Text Label 3000 2000 0    50   ~ 0
~RAM-ATTR6
Text Label 3000 2100 0    50   ~ 0
~RAM-ATTR5
Text Label 3000 2200 0    50   ~ 0
~RAM-ATTR4
Text Label 3000 2800 0    50   ~ 0
~RAM-ATTR3
Text Label 3000 2900 0    50   ~ 0
~RAM-ATTR2
Text Label 3000 3000 0    50   ~ 0
~RAM-ATTR1
Text Label 3000 3100 0    50   ~ 0
~RAM-ATTR0
Text Label 3000 3700 0    50   ~ 0
~RAM-TILE15
Text Label 3000 3800 0    50   ~ 0
~RAM-TILE14
Text Label 3000 3900 0    50   ~ 0
~RAM-TILE13
Text Label 3000 4000 0    50   ~ 0
~RAM-TILE12
Text Label 3000 4600 0    50   ~ 0
~RAM-TILE11
Text Label 3000 4700 0    50   ~ 0
~RAM-TILE10
Text Label 3000 4800 0    50   ~ 0
~RAM-TILE9
Text Label 3000 4900 0    50   ~ 0
~RAM-TILE8
Text Label 3000 5500 0    50   ~ 0
~RAM-TILE7
Text Label 3000 5600 0    50   ~ 0
~RAM-TILE6
Text Label 3000 5700 0    50   ~ 0
~RAM-TILE5
Text Label 3000 5800 0    50   ~ 0
~RAM-TILE4
Text Label 3000 6400 0    50   ~ 0
~RAM-TILE3
Text Label 3000 6500 0    50   ~ 0
~RAM-TILE2
Text Label 3000 6600 0    50   ~ 0
~RAM-TILE1
Text Label 3000 6700 0    50   ~ 0
~RAM-TILE0
Text Label 4650 1350 0    50   ~ 0
_CPC1
Text Label 4650 1900 0    50   ~ 0
_CPC0
Text Label 4650 2200 0    50   ~ 0
_CB4
Text Label 4650 2800 0    50   ~ 0
_CB3
Text Label 4650 2900 0    50   ~ 0
_CB2
Text Label 4650 3000 0    50   ~ 0
_CB1
Text Label 4650 3100 0    50   ~ 0
_CB0
Text Label 5200 3700 2    50   ~ 0
GFX-ADDR19
Text Label 5200 3800 2    50   ~ 0
GFX-ADDR18
Text Label 5200 3900 2    50   ~ 0
GFX-ADDR17
Text Label 5200 4000 2    50   ~ 0
GFX-ADDR16
Text Label 5200 4600 2    50   ~ 0
GFX-ADDR15
Text Label 5200 4700 2    50   ~ 0
GFX-ADDR14
Text Label 5200 4800 2    50   ~ 0
GFX-ADDR13
Text Label 5200 4900 2    50   ~ 0
GFX-ADDR12
Text Label 5200 5500 2    50   ~ 0
GFX-ADDR11
Text Label 5200 5600 2    50   ~ 0
GFX-ADDR10
Text Label 5200 5700 2    50   ~ 0
GFX-ADDR9
Text Label 5200 5800 2    50   ~ 0
GFX-ADDR8
Text Label 5200 6400 2    50   ~ 0
GFX-ADDR7
Text Label 5200 6500 2    50   ~ 0
GFX-ADDR6
Text Label 5200 6600 2    50   ~ 0
GFX-ADDR5
Text Label 5200 6700 2    50   ~ 0
GFX-ADDR4
Text HLabel 2750 600  0    50   Input ~ 0
~RAM-ATTR[0..9]
Text HLabel 2750 3450 0    50   Input ~ 0
~RAM-TILE[0..15]
Text HLabel 3450 6850 0    50   Input ~ 0
~SCROLL-T
Text HLabel 5100 800  2    50   3State ~ 0
_CDEN
Text HLabel 5100 2000 2    50   3State ~ 0
_YFLIP
Text HLabel 5100 2100 2    50   3State ~ 0
_XFLIP
Text HLabel 5100 3300 2    50   3State ~ 0
_CB[0..4]
Text HLabel 5300 1150 2    50   3State ~ 0
_CPC[0..1]
Text HLabel 5450 6900 2    50   3State ~ 0
GFX-ADDR[4..19]
$Comp
L A5C:TINVBF UAD225
U 1 1 61D81DAF
P 4150 800
F 0 "UAD225" H 4150 1065 50  0000 C CNN
F 1 "TINVBF" H 4150 974 50  0000 C CNN
F 2 "" H 3650 750 50  0001 C CNN
F 3 "" H 3650 750 50  0001 C CNN
	1    4150 800 
	1    0    0    -1  
$EndComp
$Comp
L A5C:TINVBF UAD307
U 1 1 61D81DA9
P 4150 1350
F 0 "UAD307" H 4150 1615 50  0000 C CNN
F 1 "TINVBF" H 4150 1524 50  0000 C CNN
F 2 "" H 3650 1300 50  0001 C CNN
F 3 "" H 3650 1300 50  0001 C CNN
	1    4150 1350
	1    0    0    -1  
$EndComp
$Comp
L A5C:M368C UCC?
U 1 1 61D7A64E
P 4150 2150
AR Path="/604B5523/61D7A64E" Ref="UCC?"  Part="1" 
AR Path="/61D7A64E" Ref="UCC129"  Part="1" 
AR Path="/611C5306/61D7A64E" Ref="UCC129"  Part="1" 
AR Path="/612E497E/612E4E14/61D7A64E" Ref="UCC129"  Part="1" 
F 0 "UCC129" H 4150 2660 50  0000 C CNN
F 1 "M368C" H 4150 2570 50  0000 C CNN
F 2 "" H 4450 1950 50  0001 C CNN
F 3 "" H 4450 1950 50  0001 C CNN
	1    4150 2150
	1    0    0    -1  
$EndComp
$Comp
L A5C:M368C UCC?
U 1 1 61D79761
P 4150 3050
AR Path="/604B5523/61D79761" Ref="UCC?"  Part="1" 
AR Path="/61D79761" Ref="UCC78"  Part="1" 
AR Path="/611C5306/61D79761" Ref="UCC78"  Part="1" 
AR Path="/612E497E/612E4E14/61D79761" Ref="UCC78"  Part="1" 
F 0 "UCC78" H 4150 3560 50  0000 C CNN
F 1 "M368C" H 4150 3470 50  0000 C CNN
F 2 "" H 4450 2850 50  0001 C CNN
F 3 "" H 4450 2850 50  0001 C CNN
	1    4150 3050
	1    0    0    -1  
$EndComp
$Comp
L A5C:M368C UCC?
U 1 1 61D7B203
P 4150 3950
AR Path="/604B5523/61D7B203" Ref="UCC?"  Part="1" 
AR Path="/61D7B203" Ref="UCB129"  Part="1" 
AR Path="/611C5306/61D7B203" Ref="UCB129"  Part="1" 
AR Path="/612E497E/612E4E14/61D7B203" Ref="UCB129"  Part="1" 
F 0 "UCB129" H 4150 4460 50  0000 C CNN
F 1 "M368C" H 4150 4370 50  0000 C CNN
F 2 "" H 4450 3750 50  0001 C CNN
F 3 "" H 4450 3750 50  0001 C CNN
	1    4150 3950
	1    0    0    -1  
$EndComp
$Comp
L A5C:M368C UCC?
U 1 1 75EE9147
P 4150 4850
AR Path="/604B5523/75EE9147" Ref="UCC?"  Part="1" 
AR Path="/75EE9147" Ref="UCB392"  Part="1" 
AR Path="/611C5306/75EE9147" Ref="UCB392"  Part="1" 
AR Path="/612E497E/612E4E14/75EE9147" Ref="UCB392"  Part="1" 
F 0 "UCB392" H 4150 5360 50  0000 C CNN
F 1 "M368C" H 4150 5270 50  0000 C CNN
F 2 "" H 4450 4650 50  0001 C CNN
F 3 "" H 4450 4650 50  0001 C CNN
	1    4150 4850
	1    0    0    -1  
$EndComp
$Comp
L A5C:M368C UCC?
U 1 1 61D7BE79
P 4150 5750
AR Path="/604B5523/61D7BE79" Ref="UCC?"  Part="1" 
AR Path="/61D7BE79" Ref="UBJ8"  Part="1" 
AR Path="/611C5306/61D7BE79" Ref="UBJ8"  Part="1" 
AR Path="/612E497E/612E4E14/61D7BE79" Ref="UBJ8"  Part="1" 
F 0 "UBJ8" H 4150 6260 50  0000 C CNN
F 1 "M368C" H 4150 6170 50  0000 C CNN
F 2 "" H 4450 5550 50  0001 C CNN
F 3 "" H 4450 5550 50  0001 C CNN
	1    4150 5750
	1    0    0    -1  
$EndComp
$Comp
L A5C:M368C UCB?
U 1 1 61D7C9D0
P 4150 6650
AR Path="/604B5523/61D7C9D0" Ref="UCB?"  Part="1" 
AR Path="/61D7C9D0" Ref="UCC410"  Part="1" 
AR Path="/611C5306/61D7C9D0" Ref="UCC410"  Part="1" 
AR Path="/612E497E/612E4E14/61D7C9D0" Ref="UCC410"  Part="1" 
F 0 "UCC410" H 4150 7160 50  0000 C CNN
F 1 "M368C" H 4150 7070 50  0000 C CNN
F 2 "" H 4450 6450 50  0001 C CNN
F 3 "" H 4450 6450 50  0001 C CNN
	1    4150 6650
	1    0    0    -1  
$EndComp
Wire Bus Line
	5200 1150 5200 1800
Wire Bus Line
	5000 2300 5000 3300
Wire Bus Line
	2850 600  2850 3000
Wire Bus Line
	2850 3450 2850 6600
Wire Bus Line
	5300 3800 5300 6900
$EndSCHEMATC
