EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 47 83
Title "CPS Reverse Engineering: DL-0311"
Date "2021-09-12"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 "Licence: CC-BY"
Comment4 "Author: Loïc *WydD* Petit"
$EndDescr
Wire Wire Line
	4250 3250 4250 3500
Wire Wire Line
	4250 3500 4350 3500
$Sheet
S 6850 2900 1350 1250
U 612FA810
F0 "palette-addr" 50
F1 "palette-addr.sch" 50
F2 "~LUT-CNT[0..7]" I L 6850 3500 50 
F3 "LUT-CNT[8..11]" I L 6850 3600 50 
F4 "~PALETTE-BASE0" O L 6850 3750 50 
F5 "~BUS-WSTLUT" I L 6850 3250 50 
F6 "ADDR[1..23]" T R 8200 3050 50 
F7 "~PALETTE-BASE" I L 6850 3150 50 
F8 "~CLK4M" I L 6850 3900 50 
F9 "BUS-DATA[0..15]" I L 6850 3050 50 
$EndSheet
$Sheet
S 4350 3350 1400 800 
U 612FA816
F0 "wstlut-counter" 50
F1 "wstlut-counter.sch" 50
F2 "~PALETTE-BASE0" I R 5750 3750 50 
F3 "WSTLUT-END" O R 5750 4000 50 
F4 "~BUS-WSTLUT" I L 4350 3500 50 
F5 "~DEBUG5" I L 4350 3900 50 
F6 "LUTPRO" I L 4350 3600 50 
F7 "MAIN-RESET" I L 4350 4000 50 
F8 "LUT-CNT[8..11]" O R 5750 3600 50 
F9 "~LUT-CNT[0..7]" O R 5750 3500 50 
F10 "~CLK4M" I L 4350 3750 50 
$EndSheet
Text HLabel 4150 3250 0    50   Input ~ 0
~BUS-WSTLUT
Wire Wire Line
	4150 3250 4250 3250
Text HLabel 4150 3600 0    50   Input ~ 0
LUTPRO
Text HLabel 4150 3750 0    50   Input ~ 0
~CLK4M
Text HLabel 6750 3900 0    50   Input ~ 0
~CLK4M
Wire Wire Line
	6750 3900 6850 3900
Wire Wire Line
	4150 3750 4350 3750
Wire Wire Line
	4150 3600 4350 3600
Text HLabel 6750 3150 0    50   Input ~ 0
~PALETTE-BASE
Wire Wire Line
	6750 3150 6850 3150
Text HLabel 6750 3050 0    50   Input ~ 0
BUS-DATA[0..15]
Wire Bus Line
	6750 3050 6850 3050
Text HLabel 5850 4000 2    50   Output ~ 0
WSTLUT-END
Wire Wire Line
	5850 4000 5750 4000
Wire Wire Line
	5750 3750 6850 3750
Wire Bus Line
	5750 3500 6850 3500
Wire Bus Line
	5750 3600 6850 3600
Wire Wire Line
	4250 3250 6850 3250
Connection ~ 4250 3250
Wire Bus Line
	3550 3800 3700 3800
Text HLabel 3550 3800 0    50   Input ~ 0
~DEBUG[5..5]
Entry Wire Line
	3700 3800 3800 3900
Text Label 3850 3900 0    50   ~ 0
~DEBUG5
Wire Wire Line
	3800 3900 4350 3900
Text HLabel 4150 4000 0    50   Input ~ 0
MAIN-RESET
Wire Wire Line
	4150 4000 4350 4000
Text HLabel 8300 3050 2    50   3State ~ 0
ADDR[1..23]
Wire Bus Line
	8200 3050 8300 3050
$EndSCHEMATC
