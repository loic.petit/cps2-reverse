EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 58 83
Title "CPS Reverse Engineering: DL-0311"
Date "2021-09-12"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 "Licence: CC-BY"
Comment4 "Author: Loïc *WydD* Petit"
$EndDescr
Connection ~ 8150 4800
Connection ~ 5200 1850
Connection ~ 2200 6450
Connection ~ 8150 5250
Connection ~ 8150 5900
Connection ~ 7400 5450
Connection ~ 8150 2350
Connection ~ 8150 2800
Connection ~ 8150 3450
Connection ~ 7400 3000
Connection ~ 3850 4100
Connection ~ 3950 4200
Connection ~ 3850 6550
Connection ~ 3850 2900
Connection ~ 3950 3000
Connection ~ 3950 6650
Connection ~ 5100 3000
Connection ~ 3950 3200
NoConn ~ 2800 7150
NoConn ~ 10000 4300
NoConn ~ 10000 1850
Entry Wire Line
	3150 1250 3250 1350
Entry Wire Line
	3150 1350 3250 1450
Entry Wire Line
	3150 1450 3250 1550
Entry Wire Line
	3150 1550 3250 1650
Entry Wire Line
	3150 2300 3250 2400
Entry Wire Line
	3150 2400 3250 2500
Entry Wire Line
	3150 2500 3250 2600
Entry Wire Line
	3150 2600 3250 2700
Entry Wire Line
	3150 3500 3250 3600
Entry Wire Line
	3150 3600 3250 3700
Entry Wire Line
	3150 3700 3250 3800
Entry Wire Line
	3150 3800 3250 3900
Entry Wire Line
	3150 5950 3250 6050
Entry Wire Line
	3150 6050 3250 6150
Entry Wire Line
	3150 6150 3250 6250
Entry Wire Line
	3150 6250 3250 6350
Entry Wire Line
	6650 1350 6750 1250
Entry Wire Line
	6650 1450 6750 1350
Entry Wire Line
	6650 1550 6750 1450
Entry Wire Line
	6650 1650 6750 1550
Entry Wire Line
	6650 2400 6750 2300
Entry Wire Line
	6650 2500 6750 2400
Entry Wire Line
	6650 2600 6750 2500
Entry Wire Line
	6650 2700 6750 2600
Entry Wire Line
	7400 1900 7500 1800
Entry Wire Line
	7400 2350 7500 2250
Entry Wire Line
	7400 2800 7500 2700
Entry Wire Line
	7400 3250 7500 3350
Entry Wire Line
	7400 4350 7500 4250
Entry Wire Line
	7400 4800 7500 4700
Entry Wire Line
	7400 5250 7500 5150
Entry Wire Line
	7400 5700 7500 5800
Entry Wire Line
	10550 2000 10650 2100
Entry Wire Line
	10550 2100 10650 2200
Entry Wire Line
	10550 2850 10650 2950
Entry Wire Line
	10550 3500 10650 3600
Entry Wire Line
	10550 4450 10650 4550
Entry Wire Line
	10550 4550 10650 4650
Entry Wire Line
	10550 5300 10650 5400
Entry Wire Line
	10550 5950 10650 6050
Wire Wire Line
	1500 6650 2600 6650
Wire Wire Line
	2100 6450 2200 6450
Wire Wire Line
	2200 6450 2600 6450
Wire Wire Line
	2200 7050 2200 6450
Wire Wire Line
	2300 7050 2200 7050
Wire Wire Line
	3150 6550 3850 6550
Wire Wire Line
	3250 1350 4050 1350
Wire Wire Line
	3250 1450 4050 1450
Wire Wire Line
	3250 1550 4050 1550
Wire Wire Line
	3250 1650 4050 1650
Wire Wire Line
	3250 2400 4050 2400
Wire Wire Line
	3250 2500 4050 2500
Wire Wire Line
	3250 2600 4050 2600
Wire Wire Line
	3250 2700 4050 2700
Wire Wire Line
	3250 3600 4050 3600
Wire Wire Line
	3250 3700 4050 3700
Wire Wire Line
	3250 3800 4050 3800
Wire Wire Line
	3250 3900 4050 3900
Wire Wire Line
	3250 6050 4050 6050
Wire Wire Line
	3250 6150 4050 6150
Wire Wire Line
	3250 6250 4050 6250
Wire Wire Line
	3250 6350 4050 6350
Wire Wire Line
	3850 1850 3850 2900
Wire Wire Line
	3850 1850 4050 1850
Wire Wire Line
	3850 2900 3850 4100
Wire Wire Line
	3850 2900 4050 2900
Wire Wire Line
	3850 4100 3850 6550
Wire Wire Line
	3850 4100 4050 4100
Wire Wire Line
	3850 6550 4050 6550
Wire Wire Line
	3850 6650 3950 6650
Wire Wire Line
	3950 1950 3950 3000
Wire Wire Line
	3950 1950 4050 1950
Wire Wire Line
	3950 3000 3950 3200
Wire Wire Line
	3950 3200 3950 4200
Wire Wire Line
	3950 4200 3950 6650
Wire Wire Line
	4050 3000 3950 3000
Wire Wire Line
	4050 4200 3950 4200
Wire Wire Line
	4050 6650 3950 6650
Wire Wire Line
	4850 1350 5300 1350
Wire Wire Line
	4850 1450 5300 1450
Wire Wire Line
	4850 1550 5300 1550
Wire Wire Line
	4850 1650 5300 1650
Wire Wire Line
	4850 2400 5300 2400
Wire Wire Line
	4850 2500 5300 2500
Wire Wire Line
	4850 2600 5300 2600
Wire Wire Line
	4850 2700 5300 2700
Wire Wire Line
	4850 3600 8900 3600
Wire Wire Line
	4850 3700 9000 3700
Wire Wire Line
	4850 3800 9100 3800
Wire Wire Line
	4850 3900 9200 3900
Wire Wire Line
	4850 6050 8900 6050
Wire Wire Line
	4850 6150 9000 6150
Wire Wire Line
	4850 6250 9100 6250
Wire Wire Line
	4850 6350 9200 6350
Wire Wire Line
	5100 900  5200 900 
Wire Wire Line
	5100 1950 5100 3000
Wire Wire Line
	5100 1950 5300 1950
Wire Wire Line
	5100 3000 5100 3200
Wire Wire Line
	5100 3200 3950 3200
Wire Wire Line
	5200 900  5200 1850
Wire Wire Line
	5200 1850 5200 2900
Wire Wire Line
	5300 1850 5200 1850
Wire Wire Line
	5300 2900 5200 2900
Wire Wire Line
	5300 3000 5100 3000
Wire Wire Line
	6100 1350 6650 1350
Wire Wire Line
	6100 1450 6650 1450
Wire Wire Line
	6100 1550 6650 1550
Wire Wire Line
	6100 1650 6650 1650
Wire Wire Line
	6100 2400 6650 2400
Wire Wire Line
	6100 2500 6650 2500
Wire Wire Line
	6100 2600 6650 2600
Wire Wire Line
	6100 2700 6650 2700
Wire Wire Line
	7500 1800 8250 1800
Wire Wire Line
	7500 2250 8250 2250
Wire Wire Line
	7500 2700 8250 2700
Wire Wire Line
	7500 3350 8250 3350
Wire Wire Line
	7500 4250 8250 4250
Wire Wire Line
	7500 4700 8250 4700
Wire Wire Line
	7500 5150 8250 5150
Wire Wire Line
	7500 5800 8250 5800
Wire Wire Line
	8050 3450 8150 3450
Wire Wire Line
	8050 5900 8150 5900
Wire Wire Line
	8150 1900 8250 1900
Wire Wire Line
	8150 2350 8150 1900
Wire Wire Line
	8150 2800 8150 2350
Wire Wire Line
	8150 2800 8150 3450
Wire Wire Line
	8150 3450 8250 3450
Wire Wire Line
	8150 4350 8250 4350
Wire Wire Line
	8150 4800 8150 4350
Wire Wire Line
	8150 5250 8150 4800
Wire Wire Line
	8150 5250 8150 5900
Wire Wire Line
	8150 5900 8250 5900
Wire Wire Line
	8250 2350 8150 2350
Wire Wire Line
	8250 2800 8150 2800
Wire Wire Line
	8250 4800 8150 4800
Wire Wire Line
	8250 5250 8150 5250
Wire Wire Line
	8800 1950 8800 2300
Wire Wire Line
	8800 2750 9300 2750
Wire Wire Line
	8800 3400 9300 3400
Wire Wire Line
	8800 4400 8800 4750
Wire Wire Line
	8800 5200 9300 5200
Wire Wire Line
	8800 5850 9300 5850
Wire Wire Line
	8900 2150 8900 3600
Wire Wire Line
	8900 4600 8900 6050
Wire Wire Line
	9000 2250 9000 3700
Wire Wire Line
	9000 4700 9000 6150
Wire Wire Line
	9100 2950 9100 3800
Wire Wire Line
	9100 2950 9300 2950
Wire Wire Line
	9100 5400 9100 6250
Wire Wire Line
	9100 5400 9300 5400
Wire Wire Line
	9200 1850 8800 1850
Wire Wire Line
	9200 1950 8800 1950
Wire Wire Line
	9200 2150 8900 2150
Wire Wire Line
	9200 2250 9000 2250
Wire Wire Line
	9200 3600 9200 3900
Wire Wire Line
	9200 4300 8800 4300
Wire Wire Line
	9200 4400 8800 4400
Wire Wire Line
	9200 4600 8900 4600
Wire Wire Line
	9200 4700 9000 4700
Wire Wire Line
	9200 6050 9200 6350
Wire Wire Line
	9300 3600 9200 3600
Wire Wire Line
	9300 6050 9200 6050
Wire Wire Line
	9900 2750 10000 2750
Wire Wire Line
	9900 2850 10550 2850
Wire Wire Line
	9900 3400 9900 2950
Wire Wire Line
	9900 3500 10550 3500
Wire Wire Line
	9900 5200 10000 5200
Wire Wire Line
	9900 5300 10550 5300
Wire Wire Line
	9900 5850 9900 5400
Wire Wire Line
	9900 5950 10550 5950
Wire Wire Line
	10000 2000 10550 2000
Wire Wire Line
	10000 2100 10550 2100
Wire Wire Line
	10000 2250 10000 2750
Wire Wire Line
	10000 4450 10550 4450
Wire Wire Line
	10000 4550 10550 4550
Wire Wire Line
	10000 4700 10000 5200
Wire Bus Line
	3150 1150 3050 1150
Wire Bus Line
	6750 1100 6850 1100
Wire Bus Line
	7400 3000 7300 3000
Wire Bus Line
	7400 3250 7400 3000
Wire Bus Line
	7400 5450 7300 5450
Wire Bus Line
	7400 5700 7400 5450
Wire Bus Line
	10650 3800 10550 3800
Wire Bus Line
	10650 6250 10550 6250
Text Notes 10900 2800 1    50   ~ 0
SPRITE-HI
Text Notes 10950 5400 1    50   ~ 0
SPRITE-LO
Text Label 3300 1350 0    50   ~ 0
BUS-DATA15
Text Label 3300 1450 0    50   ~ 0
BUS-DATA14
Text Label 3300 1550 0    50   ~ 0
BUS-DATA13
Text Label 3300 1650 0    50   ~ 0
BUS-DATA12
Text Label 3300 2400 0    50   ~ 0
BUS-DATA11
Text Label 3300 2500 0    50   ~ 0
BUS-DATA10
Text Label 3300 2600 0    50   ~ 0
BUS-DATA9
Text Label 3300 2700 0    50   ~ 0
BUS-DATA8
Text Label 3300 3600 0    50   ~ 0
BUS-DATA7
Text Label 3300 3700 0    50   ~ 0
BUS-DATA6
Text Label 3300 3800 0    50   ~ 0
BUS-DATA5
Text Label 3300 3900 0    50   ~ 0
BUS-DATA4
Text Label 3300 6050 0    50   ~ 0
BUS-DATA3
Text Label 3300 6150 0    50   ~ 0
BUS-DATA2
Text Label 3300 6250 0    50   ~ 0
BUS-DATA1
Text Label 3300 6350 0    50   ~ 0
BUS-DATA0
Text Label 6150 1350 0    50   ~ 0
~TILE-BASE7
Text Label 6150 1450 0    50   ~ 0
~TILE-BASE6
Text Label 6150 1550 0    50   ~ 0
~TILE-BASE5
Text Label 6150 1650 0    50   ~ 0
~TILE-BASE4
Text Label 6150 2400 0    50   ~ 0
~TILE-BASE3
Text Label 6150 2500 0    50   ~ 0
~TILE-BASE2
Text Label 6150 2600 0    50   ~ 0
~TILE-BASE1
Text Label 6150 2700 0    50   ~ 0
~TILE-BASE0
Text Label 7500 1800 0    50   ~ 0
ATTR-YBLOCK3
Text Label 7500 2250 0    50   ~ 0
ATTR-YBLOCK2
Text Label 7500 2700 0    50   ~ 0
ATTR-YBLOCK1
Text Label 7500 3350 0    50   ~ 0
ATTR-YBLOCK0
Text Label 7500 4250 0    50   ~ 0
ATTR-XBLOCK3
Text Label 7500 4700 0    50   ~ 0
ATTR-XBLOCK2
Text Label 7500 5150 0    50   ~ 0
ATTR-XBLOCK1
Text Label 7500 5800 0    50   ~ 0
ATTR-XBLOCK0
Text Label 10500 2000 2    50   ~ 0
TILE-YOFF3
Text Label 10500 2100 2    50   ~ 0
TILE-YOFF2
Text Label 10500 2850 2    50   ~ 0
TILE-YOFF1
Text Label 10500 3500 2    50   ~ 0
TILE-YOFF0
Text Label 10500 4450 2    50   ~ 0
TILE-XOFF3
Text Label 10500 4550 2    50   ~ 0
TILE-XOFF2
Text Label 10500 5300 2    50   ~ 0
TILE-XOFF1
Text Label 10500 5950 2    50   ~ 0
TILE-XOFF0
Text HLabel 1500 6450 0    50   Input ~ 0
~BUS-SPRITE-TILE
Text HLabel 1500 6650 0    50   Input ~ 0
SPRITE-BLOCK-READY
Text HLabel 2300 7250 0    50   Input ~ 0
~CLK4M
Text HLabel 2600 6550 0    50   Input ~ 0
CLK4M
Text HLabel 2800 7050 2    50   Output ~ 0
BUS-SPRITE-TILE-SYNC
Text HLabel 3050 1150 0    50   Input ~ 0
BUS-DATA[0..15]
Text HLabel 3850 6650 0    50   Input ~ 0
~RESET-SCAN243
Text HLabel 4550 850  0    50   Input ~ 0
CLK4M
Text HLabel 4550 950  0    50   Input ~ 0
SPRITE-BLOCK-RESET
Text HLabel 6850 1100 2    50   Output ~ 0
~TILE-BASE[0..7]
Text HLabel 7300 3000 0    50   Input ~ 0
ATTR-YBLOCK[0..3]
Text HLabel 7300 5450 0    50   Input ~ 0
ATTR-XBLOCK[0..3]
Text HLabel 8050 3450 0    50   Input ~ 0
ATTR-YFLIP
Text HLabel 8050 5900 0    50   Input ~ 0
ATTR-XFLIP
Text HLabel 10550 3800 0    50   Output ~ 0
TILE-YOFF[0..3]
Text HLabel 10550 6250 0    50   Output ~ 0
TILE-XOFF[0..3]
$Comp
L A5C:NAND02 UCB117
U 1 1 61118D7B
P 4800 900
F 0 "UCB117" H 4825 1167 50  0000 C CNN
F 1 "NAND02" H 4825 1076 50  0000 C CNN
F 2 "" H 4800 900 50  0001 C CNN
F 3 "" H 4800 900 50  0001 C CNN
	1    4800 900 
	1    0    0    -1  
$EndComp
$Comp
L A5C:AND02 UBF21
U 1 1 74043BD1
P 8500 1850
F 0 "UBF21" H 8525 2117 50  0000 C CNN
F 1 "AND02" H 8525 2026 50  0000 C CNN
F 2 "" H 8500 1850 50  0001 C CNN
F 3 "" H 8500 1850 50  0001 C CNN
	1    8500 1850
	1    0    0    -1  
$EndComp
$Comp
L A5C:AND02 UBF49
U 1 1 74043509
P 8500 2300
F 0 "UBF49" H 8525 2567 50  0000 C CNN
F 1 "AND02" H 8525 2476 50  0000 C CNN
F 2 "" H 8500 2300 50  0001 C CNN
F 3 "" H 8500 2300 50  0001 C CNN
	1    8500 2300
	1    0    0    -1  
$EndComp
$Comp
L A5C:AND02 UBG44
U 1 1 74043094
P 8500 2750
F 0 "UBG44" H 8525 3017 50  0000 C CNN
F 1 "AND02" H 8525 2926 50  0000 C CNN
F 2 "" H 8500 2750 50  0001 C CNN
F 3 "" H 8500 2750 50  0001 C CNN
	1    8500 2750
	1    0    0    -1  
$EndComp
$Comp
L A5C:AND02 UBG21
U 1 1 6BA09DFD
P 8500 3400
F 0 "UBG21" H 8525 3667 50  0000 C CNN
F 1 "AND02" H 8525 3576 50  0000 C CNN
F 2 "" H 8500 3400 50  0001 C CNN
F 3 "" H 8500 3400 50  0001 C CNN
	1    8500 3400
	1    0    0    -1  
$EndComp
$Comp
L A5C:AND02 UCA344
U 1 1 7BCBFB05
P 8500 4300
F 0 "UCA344" H 8525 4567 50  0000 C CNN
F 1 "AND02" H 8525 4476 50  0000 C CNN
F 2 "" H 8500 4300 50  0001 C CNN
F 3 "" H 8500 4300 50  0001 C CNN
	1    8500 4300
	1    0    0    -1  
$EndComp
$Comp
L A5C:AND02 UCA334
U 1 1 7BCBFAFF
P 8500 4750
F 0 "UCA334" H 8525 5017 50  0000 C CNN
F 1 "AND02" H 8525 4926 50  0000 C CNN
F 2 "" H 8500 4750 50  0001 C CNN
F 3 "" H 8500 4750 50  0001 C CNN
	1    8500 4750
	1    0    0    -1  
$EndComp
$Comp
L A5C:AND02 UBI48
U 1 1 7BCBFAF9
P 8500 5200
F 0 "UBI48" H 8525 5467 50  0000 C CNN
F 1 "AND02" H 8525 5376 50  0000 C CNN
F 2 "" H 8500 5200 50  0001 C CNN
F 3 "" H 8500 5200 50  0001 C CNN
	1    8500 5200
	1    0    0    -1  
$EndComp
$Comp
L A5C:AND02 UCA377
U 1 1 7BCBFAEF
P 8500 5850
F 0 "UCA377" H 8525 6117 50  0000 C CNN
F 1 "AND02" H 8525 6026 50  0000 C CNN
F 2 "" H 8500 5850 50  0001 C CNN
F 3 "" H 8500 5850 50  0001 C CNN
	1    8500 5850
	1    0    0    -1  
$EndComp
$Comp
L A5C:NAND03 UCA101
U 1 1 61118D87
P 2850 6550
F 0 "UCA101" H 2875 6842 50  0000 C CNN
F 1 "NAND03" H 2875 6751 50  0000 C CNN
F 2 "" H 2850 6550 50  0001 C CNN
F 3 "" H 2850 6550 50  0001 C CNN
	1    2850 6550
	1    0    0    -1  
$EndComp
$Comp
L A5C:INV01 UCB36
U 1 1 61118D85
P 1800 6450
F 0 "UCB36" H 1800 6767 50  0000 C CNN
F 1 "INV01" H 1800 6676 50  0000 C CNN
F 2 "" H 1800 6250 50  0001 C CNN
F 3 "" H 1800 6250 50  0001 C CNN
	1    1800 6450
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFFCOO UAD90
U 1 1 61118D84
P 2550 7150
F 0 "UAD90" H 2550 7515 50  0000 C CNN
F 1 "DFFCOO" H 2550 7424 50  0000 C CNN
F 2 "" H 2550 7150 50  0001 C CNN
F 3 "" H 2550 7150 50  0001 C CNN
	1    2550 7150
	1    0    0    -1  
$EndComp
$Comp
L A5C:FA1 UDH?
U 1 1 6739DC96
P 9600 2900
AR Path="/613100A2/6739DC96" Ref="UDH?"  Part="1" 
AR Path="/6739DC96" Ref="UBG63"  Part="1" 
AR Path="/6116C2BE/6739DC96" Ref="UBG63"  Part="1" 
AR Path="/612CA5FA/612CAD3C/6739DC96" Ref="UBG63"  Part="1" 
F 0 "UBG63" H 9600 3315 50  0000 C CNN
F 1 "FA1" H 9600 3224 50  0000 C CNN
F 2 "" H 9550 2950 50  0001 C CNN
F 3 "" H 9550 2950 50  0001 C CNN
	1    9600 2900
	1    0    0    -1  
$EndComp
$Comp
L A5C:HA1 UDF?
U 1 1 6739DC90
P 9600 3500
AR Path="/613100A2/6739DC90" Ref="UDF?"  Part="1" 
AR Path="/6739DC90" Ref="UBG100"  Part="1" 
AR Path="/6116C2BE/6739DC90" Ref="UBG100"  Part="1" 
AR Path="/612CA5FA/612CAD3C/6739DC90" Ref="UBG100"  Part="1" 
F 0 "UBG100" H 9600 3865 50  0000 C CNN
F 1 "HA1" H 9600 3774 50  0000 C CNN
F 2 "" H 9550 3500 50  0001 C CNN
F 3 "" H 9550 3500 50  0001 C CNN
	1    9600 3500
	1    0    0    -1  
$EndComp
$Comp
L A5C:FA1 UBG?
U 1 1 6A888998
P 9600 5350
AR Path="/613100A2/6A888998" Ref="UBG?"  Part="1" 
AR Path="/6A888998" Ref="UBI155"  Part="1" 
AR Path="/6116C2BE/6A888998" Ref="UBI155"  Part="1" 
AR Path="/612CA5FA/612CAD3C/6A888998" Ref="UBI155"  Part="1" 
F 0 "UBI155" H 9600 5765 50  0000 C CNN
F 1 "FA1" H 9600 5674 50  0000 C CNN
F 2 "" H 9550 5400 50  0001 C CNN
F 3 "" H 9550 5400 50  0001 C CNN
	1    9600 5350
	1    0    0    -1  
$EndComp
$Comp
L A5C:HA1 UBG?
U 1 1 6A888992
P 9600 5950
AR Path="/613100A2/6A888992" Ref="UBG?"  Part="1" 
AR Path="/6A888992" Ref="UBI50"  Part="1" 
AR Path="/6116C2BE/6A888992" Ref="UBI50"  Part="1" 
AR Path="/612CA5FA/612CAD3C/6A888992" Ref="UBI50"  Part="1" 
F 0 "UBI50" H 9600 6315 50  0000 C CNN
F 1 "HA1" H 9600 6224 50  0000 C CNN
F 2 "" H 9550 5950 50  0001 C CNN
F 3 "" H 9550 5950 50  0001 C CNN
	1    9600 5950
	1    0    0    -1  
$EndComp
$Comp
L A5C:FA2 UDI?
U 1 1 6739DC9D
P 9600 2150
AR Path="/613100A2/6739DC9D" Ref="UDI?"  Part="1" 
AR Path="/6739DC9D" Ref="UBF52"  Part="1" 
AR Path="/6116C2BE/6739DC9D" Ref="UBF52"  Part="1" 
AR Path="/612CA5FA/612CAD3C/6739DC9D" Ref="UBF52"  Part="1" 
F 0 "UBF52" H 9600 2715 50  0000 C CNN
F 1 "FA2" H 9600 2624 50  0000 C CNN
F 2 "" H 9450 2350 50  0001 C CNN
F 3 "" H 9450 2350 50  0001 C CNN
	1    9600 2150
	1    0    0    -1  
$EndComp
$Comp
L A5C:FA2 UBF?
U 1 1 6A88899F
P 9600 4600
AR Path="/613100A2/6A88899F" Ref="UBF?"  Part="1" 
AR Path="/6A88899F" Ref="UCB333"  Part="1" 
AR Path="/6116C2BE/6A88899F" Ref="UCB333"  Part="1" 
AR Path="/612CA5FA/612CAD3C/6A88899F" Ref="UCB333"  Part="1" 
F 0 "UCB333" H 9600 5165 50  0000 C CNN
F 1 "FA2" H 9600 5074 50  0000 C CNN
F 2 "" H 9450 4800 50  0001 C CNN
F 3 "" H 9450 4800 50  0001 C CNN
	1    9600 4600
	1    0    0    -1  
$EndComp
$Comp
L A5C:M175C UCA81
U 1 1 61118D7F
P 4450 1650
F 0 "UCA81" H 4450 2215 50  0000 C CNN
F 1 "M175C" H 4450 2124 50  0000 C CNN
F 2 "" H 4450 1450 50  0001 C CNN
F 3 "" H 4450 1450 50  0001 C CNN
	1    4450 1650
	1    0    0    -1  
$EndComp
$Comp
L A5C:M175C UCB38
U 1 1 61118D80
P 4450 2700
F 0 "UCB38" H 4450 3265 50  0000 C CNN
F 1 "M175C" H 4450 3174 50  0000 C CNN
F 2 "" H 4450 2500 50  0001 C CNN
F 3 "" H 4450 2500 50  0001 C CNN
	1    4450 2700
	1    0    0    -1  
$EndComp
$Comp
L A5C:M175C UBG75
U 1 1 61118D88
P 4450 3900
F 0 "UBG75" H 4450 4465 50  0000 C CNN
F 1 "M175C" H 4450 4374 50  0000 C CNN
F 2 "" H 4450 3700 50  0001 C CNN
F 3 "" H 4450 3700 50  0001 C CNN
	1    4450 3900
	1    0    0    -1  
$EndComp
$Comp
L A5C:M175C UED254
U 1 1 61118D86
P 4450 6350
F 0 "UED254" H 4450 6915 50  0000 C CNN
F 1 "M175C" H 4450 6824 50  0000 C CNN
F 2 "" H 4450 6150 50  0001 C CNN
F 3 "" H 4450 6150 50  0001 C CNN
	1    4450 6350
	1    0    0    -1  
$EndComp
$Comp
L A5C:M175CL UCA109
U 1 1 61118D7D
P 5700 1650
F 0 "UCA109" H 5700 2215 50  0000 C CNN
F 1 "M175CL" H 5700 2124 50  0000 C CNN
F 2 "" H 5700 1450 50  0001 C CNN
F 3 "" H 5700 1450 50  0001 C CNN
	1    5700 1650
	1    0    0    -1  
$EndComp
$Comp
L A5C:M175CL UCC52
U 1 1 63F7B52B
P 5700 2700
F 0 "UCC52" H 5700 3265 50  0000 C CNN
F 1 "M175CL" H 5700 3174 50  0000 C CNN
F 2 "" H 5700 2500 50  0001 C CNN
F 3 "" H 5700 2500 50  0001 C CNN
	1    5700 2700
	1    0    0    -1  
$EndComp
Wire Bus Line
	7400 1900 7400 3000
Wire Bus Line
	7400 4350 7400 5450
Wire Bus Line
	10650 2100 10650 3800
Wire Bus Line
	10650 4550 10650 6250
Wire Bus Line
	6750 1100 6750 2600
Wire Bus Line
	3150 1150 3150 6250
$EndSCHEMATC
