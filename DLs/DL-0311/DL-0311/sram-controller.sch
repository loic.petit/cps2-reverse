EESchema Schematic File Version 5
EELAYER 34 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 37 52
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
Wire Wire Line
	3800 3800 3900 3800
Wire Wire Line
	3800 3900 3900 3900
Wire Wire Line
	3800 4850 3900 4850
Wire Wire Line
	3800 4950 3900 4950
Wire Wire Line
	5850 2900 5950 2900
Wire Wire Line
	7900 3600 8500 3600
Wire Wire Line
	8400 3250 8500 3250
Wire Wire Line
	8400 3350 8500 3350
Wire Wire Line
	8400 3450 8500 3450
Wire Wire Line
	8400 3700 8500 3700
Wire Wire Line
	8400 3800 8500 3800
Wire Wire Line
	8400 4000 8500 4000
Wire Wire Line
	8400 4100 8500 4100
Wire Bus Line
	3800 3250 3900 3250
Wire Bus Line
	3800 3350 3900 3350
Wire Bus Line
	3800 3600 3900 3600
Wire Bus Line
	3800 4450 3900 4450
Wire Bus Line
	3800 4600 3900 4600
Wire Bus Line
	3800 4750 3900 4750
Wire Bus Line
	3900 3450 3800 3450
Wire Bus Line
	5950 2800 5850 2800
Wire Bus Line
	8500 3050 8400 3050
Wire Bus Line
	9950 3800 9850 3800
Wire Bus Line
	9950 3900 9850 3900
Text HLabel 3800 3250 0    50   Input ~ 0
SCROLL1-X[0..5]
Text HLabel 3800 3350 0    50   Input ~ 0
SCROLL2-X[1..6]
Text HLabel 3800 3450 0    50   Input ~ 0
SCROLL3-X[2..5]
Text HLabel 3800 3600 0    50   Input ~ 0
Y-OUT[3..5]
Text HLabel 3800 3800 0    50   Input ~ 0
~SCROLL1-T
Text HLabel 3800 3900 0    50   Input ~ 0
~SCROLL3-T
Text HLabel 3800 4450 0    50   Input ~ 0
SCROLL-CNT[1..6]
Text HLabel 3800 4600 0    50   Input ~ 0
LINE[3..5]
Text HLabel 3800 4750 0    50   Input ~ 0
BUS-STATE[0..1]
Text HLabel 3800 4850 0    50   Input ~ 0
~BUS-SCROLL1
Text HLabel 3800 4950 0    50   Input ~ 0
~BUS-SCROLL3
Text HLabel 5850 2800 0    50   Input ~ 0
CA[10..17]
Text HLabel 5850 2900 0    50   Input ~ 0
~DEBUG3
Text HLabel 8400 3050 0    50   Input ~ 0
~BUS-DATA[0..15]
Text HLabel 8400 3250 0    50   Input ~ 0
~BUS-SCROLL1
Text HLabel 8400 3350 0    50   Input ~ 0
~BUS-SCROLL2
Text HLabel 8400 3450 0    50   Input ~ 0
~BUS-SCROLL3
Text HLabel 8400 3700 0    50   Input ~ 0
CLK4M
Text HLabel 8400 3800 0    50   Input ~ 0
CLK8M
Text HLabel 8400 4000 0    50   Input ~ 0
~WR
Text HLabel 8400 4100 0    50   Input ~ 0
~DEBUG3
Text HLabel 9950 3800 2    50   Output ~ 0
~RAM-TILE[0..15]
Text HLabel 9950 3900 2    50   Output ~ 0
~RAM-ATTR[0..9]
$EndSCHEMATC
