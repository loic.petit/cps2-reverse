EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 22 83
Title "CPS Reverse Engineering: DL-0311"
Date "2021-09-12"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 "Licence: CC-BY"
Comment4 "Author: Loïc *WydD* Petit"
$EndDescr
Connection ~ 3850 5100
Connection ~ 3750 5000
Connection ~ 3750 6150
Connection ~ 3850 4150
NoConn ~ 1950 6200
NoConn ~ 4600 3850
Entry Wire Line
	3050 3650 3150 3750
Entry Wire Line
	3050 4400 3150 4500
Entry Wire Line
	3050 4500 3150 4600
Entry Wire Line
	3050 4600 3150 4700
Entry Wire Line
	3050 4700 3150 4800
Entry Wire Line
	3050 5550 3150 5650
Entry Wire Line
	3050 5650 3150 5750
Entry Wire Line
	3050 5750 3150 5850
Entry Wire Line
	3050 5850 3150 5950
Entry Wire Line
	4900 6750 5000 6850
Entry Wire Line
	4900 6850 5000 6950
Entry Wire Line
	4900 6950 5000 7050
Entry Wire Line
	4900 7050 5000 7150
Entry Wire Line
	8250 4000 8350 3900
Entry Wire Line
	8250 4100 8350 4000
Entry Wire Line
	8250 4200 8350 4100
Entry Wire Line
	8250 4300 8350 4200
Entry Wire Line
	8250 4400 8350 4300
Entry Wire Line
	8250 4500 8350 4400
Entry Wire Line
	8250 4600 8350 4500
Entry Wire Line
	8250 4700 8350 4600
Entry Wire Line
	8250 4800 8350 4700
Entry Wire Line
	8250 4900 8350 4800
Wire Wire Line
	1950 6100 2350 6100
Wire Wire Line
	2900 4150 3850 4150
Wire Wire Line
	2900 6150 3750 6150
Wire Wire Line
	3150 3750 4100 3750
Wire Wire Line
	3150 4500 3950 4500
Wire Wire Line
	3150 4600 3950 4600
Wire Wire Line
	3150 4700 3950 4700
Wire Wire Line
	3150 4800 3950 4800
Wire Wire Line
	3150 5650 3950 5650
Wire Wire Line
	3150 5750 3950 5750
Wire Wire Line
	3150 5850 3950 5850
Wire Wire Line
	3150 5950 3950 5950
Wire Wire Line
	3750 3950 4100 3950
Wire Wire Line
	3750 5000 3750 3950
Wire Wire Line
	3750 5000 3750 6150
Wire Wire Line
	3750 5000 3950 5000
Wire Wire Line
	3750 6150 3950 6150
Wire Wire Line
	3850 4150 4350 4150
Wire Wire Line
	3850 5100 3850 4150
Wire Wire Line
	3850 5100 3850 6250
Wire Wire Line
	3950 5100 3850 5100
Wire Wire Line
	3950 6250 3850 6250
Wire Wire Line
	4750 1000 4750 3750
Wire Wire Line
	4750 1000 6500 1000
Wire Wire Line
	4750 3750 4600 3750
Wire Wire Line
	4750 4500 4850 4500
Wire Wire Line
	4750 4600 4950 4600
Wire Wire Line
	4750 4800 5150 4800
Wire Wire Line
	4750 5650 5250 5650
Wire Wire Line
	4750 5850 5450 5850
Wire Wire Line
	4750 5950 6500 5950
Wire Wire Line
	4850 1650 4850 4500
Wire Wire Line
	4850 1650 6500 1650
Wire Wire Line
	4950 2300 4950 4600
Wire Wire Line
	4950 2300 6500 2300
Wire Wire Line
	5000 6850 5650 6850
Wire Wire Line
	5000 6950 5750 6950
Wire Wire Line
	5000 7050 5850 7050
Wire Wire Line
	5000 7150 5950 7150
Wire Wire Line
	5050 3000 5050 4700
Wire Wire Line
	5050 3000 6500 3000
Wire Wire Line
	5050 4700 4750 4700
Wire Wire Line
	5150 3700 6500 3700
Wire Wire Line
	5150 4800 5150 3700
Wire Wire Line
	5250 4450 5250 5650
Wire Wire Line
	5350 4550 5350 5750
Wire Wire Line
	5350 4550 6500 4550
Wire Wire Line
	5350 5750 4750 5750
Wire Wire Line
	5450 5300 6500 5300
Wire Wire Line
	5450 5850 5450 5300
Wire Wire Line
	5650 4750 5650 6850
Wire Wire Line
	5650 4750 6500 4750
Wire Wire Line
	5750 4850 6500 4850
Wire Wire Line
	5750 6950 5750 4850
Wire Wire Line
	5850 7050 5850 5500
Wire Wire Line
	5950 6150 6500 6150
Wire Wire Line
	5950 7150 5950 6150
Wire Wire Line
	6500 2500 6500 2650
Wire Wire Line
	6500 2650 7100 2650
Wire Wire Line
	6500 3350 6500 3200
Wire Wire Line
	6500 3350 7100 3350
Wire Wire Line
	6500 4100 6500 3900
Wire Wire Line
	6500 4450 5250 4450
Wire Wire Line
	6500 5500 5850 5500
Wire Wire Line
	7100 1100 7800 1100
Wire Wire Line
	7100 1200 7100 1650
Wire Wire Line
	7100 1750 7700 1750
Wire Wire Line
	7100 1850 7100 2300
Wire Wire Line
	7100 2400 7600 2400
Wire Wire Line
	7100 2650 7100 3000
Wire Wire Line
	7100 3700 7100 3350
Wire Wire Line
	7100 5300 7300 5300
Wire Wire Line
	7100 5400 7450 5400
Wire Wire Line
	7100 5950 7100 5500
Wire Wire Line
	7100 6050 7550 6050
Wire Wire Line
	7300 4100 6500 4100
Wire Wire Line
	7300 4450 7300 4100
Wire Wire Line
	7300 4600 8250 4600
Wire Wire Line
	7300 4700 8250 4700
Wire Wire Line
	7300 5300 7300 4850
Wire Wire Line
	7400 3800 7100 3800
Wire Wire Line
	7400 4500 7400 3800
Wire Wire Line
	7400 4500 8250 4500
Wire Wire Line
	7450 4800 7450 5400
Wire Wire Line
	7450 4800 8250 4800
Wire Wire Line
	7500 3100 7100 3100
Wire Wire Line
	7500 4400 7500 3100
Wire Wire Line
	7500 4400 8250 4400
Wire Wire Line
	7550 4900 8250 4900
Wire Wire Line
	7550 6050 7550 4900
Wire Wire Line
	7600 2400 7600 4300
Wire Wire Line
	7600 4300 8250 4300
Wire Wire Line
	7700 1750 7700 4200
Wire Wire Line
	7700 4200 8250 4200
Wire Wire Line
	7800 1100 7800 4100
Wire Wire Line
	7800 4100 8250 4100
Wire Wire Line
	7900 1000 7100 1000
Wire Wire Line
	7900 4000 7900 1000
Wire Wire Line
	7900 4000 8250 4000
Wire Bus Line
	3050 3500 2950 3500
Wire Bus Line
	4800 6650 4900 6650
Wire Bus Line
	8350 3750 8500 3750
Text Label 3650 3750 2    50   ~ 0
BUS-DATA8
Text Label 3650 4500 2    50   ~ 0
BUS-DATA7
Text Label 3650 4600 2    50   ~ 0
BUS-DATA6
Text Label 3650 4700 2    50   ~ 0
BUS-DATA5
Text Label 3650 4800 2    50   ~ 0
BUS-DATA4
Text Label 3650 5650 2    50   ~ 0
BUS-DATA3
Text Label 3650 5750 2    50   ~ 0
BUS-DATA2
Text Label 3650 5850 2    50   ~ 0
BUS-DATA1
Text Label 3650 5950 2    50   ~ 0
BUS-DATA0
Text Label 5050 6850 0    50   ~ 0
BASE-X0
Text Label 5050 6950 0    50   ~ 0
BASE-X1
Text Label 5050 7050 0    50   ~ 0
BASE-X2
Text Label 5050 7150 0    50   ~ 0
BASE-X3
Text Label 8150 4000 2    50   ~ 0
X9
Text Label 8150 4100 2    50   ~ 0
X8
Text Label 8150 4200 2    50   ~ 0
X7
Text Label 8150 4300 2    50   ~ 0
X6
Text Label 8150 4400 2    50   ~ 0
X5
Text Label 8150 4500 2    50   ~ 0
X4
Text Label 8150 4600 2    50   ~ 0
X3
Text Label 8150 4700 2    50   ~ 0
X2
Text Label 8150 4800 2    50   ~ 0
X1
Text Label 8150 4900 2    50   ~ 0
X0
Text HLabel 1450 6100 0    50   Input ~ 0
~BUS-ROWSCROLL
Text HLabel 1450 6300 0    50   Input ~ 0
~CLK4M
Text HLabel 2350 4100 0    50   Input ~ 0
FI
Text HLabel 2350 4200 0    50   Input ~ 0
MAIN-RESET
Text HLabel 2350 6200 0    50   Input ~ 0
~CLK4M
Text HLabel 2950 3500 0    50   Input ~ 0
BUS-DATA[0..8]
Text HLabel 4800 6650 0    50   Input ~ 0
BASE-X[0..3]
Text HLabel 6500 1200 0    50   Input ~ 0
FLIP
Text HLabel 6500 1850 0    50   Input ~ 0
FLIP
Text HLabel 8500 3750 2    50   Output ~ 0
X[0..9]
$Comp
L A5C:NOR02 UEK120
U 1 1 602A94D0
P 2600 4150
AR Path="/60E86855/6112B94A/602A94D0" Ref="UEK120"  Part="1" 
AR Path="/612D3239/612D3C9E/6112B94A/602A94D0" Ref="UEK120"  Part="1" 
F 0 "UEK120" H 2625 4417 50  0000 C CNN
F 1 "NOR02" H 2625 4326 50  0000 C CNN
F 2 "" H 2600 4150 50  0001 C CNN
F 3 "" H 2600 4150 50  0001 C CNN
	1    2600 4150
	1    0    0    -1  
$EndComp
$Comp
L A5C:OR02 UEB173
U 1 1 61118E01
P 2600 6150
AR Path="/60E86855/6112B94A/61118E01" Ref="UEB173"  Part="1" 
AR Path="/612D3239/612D3C9E/6112B94A/61118E01" Ref="UEB173"  Part="1" 
F 0 "UEB173" H 2625 6417 50  0000 C CNN
F 1 "OR02" H 2625 6326 50  0000 C CNN
F 2 "" H 2650 6150 50  0001 C CNN
F 3 "" H 2650 6150 50  0001 C CNN
	1    2600 6150
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFFCOO UEB169
U 1 1 61118E00
P 1700 6200
AR Path="/60E86855/6112B94A/61118E00" Ref="UEB169"  Part="1" 
AR Path="/612D3239/612D3C9E/6112B94A/61118E00" Ref="UEB169"  Part="1" 
F 0 "UEB169" H 1700 6565 50  0000 C CNN
F 1 "DFFCOO" H 1700 6474 50  0000 C CNN
F 2 "" H 1700 6200 50  0001 C CNN
F 3 "" H 1700 6200 50  0001 C CNN
	1    1700 6200
	1    0    0    -1  
$EndComp
$Comp
L A5C:FA1 UED74
U 1 1 61118DFD
P 6800 1150
AR Path="/60E86855/6112B94A/61118DFD" Ref="UED74"  Part="1" 
AR Path="/612D3239/612D3C9E/6112B94A/61118DFD" Ref="UED74"  Part="1" 
F 0 "UED74" H 6800 1565 50  0000 C CNN
F 1 "FA1" H 6800 1474 50  0000 C CNN
F 2 "" H 6750 1200 50  0001 C CNN
F 3 "" H 6750 1200 50  0001 C CNN
	1    6800 1150
	1    0    0    -1  
$EndComp
$Comp
L A5C:FA1 UEH70
U 1 1 61118DFE
P 6800 1800
AR Path="/60E86855/6112B94A/61118DFE" Ref="UEH70"  Part="1" 
AR Path="/612D3239/612D3C9E/6112B94A/61118DFE" Ref="UEH70"  Part="1" 
F 0 "UEH70" H 6800 2215 50  0000 C CNN
F 1 "FA1" H 6800 2124 50  0000 C CNN
F 2 "" H 6750 1850 50  0001 C CNN
F 3 "" H 6750 1850 50  0001 C CNN
	1    6800 1800
	1    0    0    -1  
$EndComp
$Comp
L A5C:HA1 UEJ106
U 1 1 61118DFF
P 6800 2400
AR Path="/60E86855/6112B94A/61118DFF" Ref="UEJ106"  Part="1" 
AR Path="/612D3239/612D3C9E/6112B94A/61118DFF" Ref="UEJ106"  Part="1" 
F 0 "UEJ106" H 6800 2765 50  0000 C CNN
F 1 "HA1" H 6800 2674 50  0000 C CNN
F 2 "" H 6750 2400 50  0001 C CNN
F 3 "" H 6750 2400 50  0001 C CNN
	1    6800 2400
	1    0    0    -1  
$EndComp
$Comp
L A5C:HA1 UEH108
U 1 1 61118DFB
P 6800 3100
AR Path="/60E86855/6112B94A/61118DFB" Ref="UEH108"  Part="1" 
AR Path="/612D3239/612D3C9E/6112B94A/61118DFB" Ref="UEH108"  Part="1" 
F 0 "UEH108" H 6800 3465 50  0000 C CNN
F 1 "HA1" H 6800 3374 50  0000 C CNN
F 2 "" H 6750 3100 50  0001 C CNN
F 3 "" H 6750 3100 50  0001 C CNN
	1    6800 3100
	1    0    0    -1  
$EndComp
$Comp
L A5C:HA1 UEI117
U 1 1 61118DFC
P 6800 3800
AR Path="/60E86855/6112B94A/61118DFC" Ref="UEI117"  Part="1" 
AR Path="/612D3239/612D3C9E/6112B94A/61118DFC" Ref="UEI117"  Part="1" 
F 0 "UEI117" H 6800 4165 50  0000 C CNN
F 1 "HA1" H 6800 4074 50  0000 C CNN
F 2 "" H 6750 3800 50  0001 C CNN
F 3 "" H 6750 3800 50  0001 C CNN
	1    6800 3800
	1    0    0    -1  
$EndComp
$Comp
L A5C:FA1 UEJ63
U 1 1 7E3E3919
P 6800 5450
AR Path="/60E86855/6112B94A/7E3E3919" Ref="UEJ63"  Part="1" 
AR Path="/612D3239/612D3C9E/6112B94A/7E3E3919" Ref="UEJ63"  Part="1" 
F 0 "UEJ63" H 6800 5865 50  0000 C CNN
F 1 "FA1" H 6800 5774 50  0000 C CNN
F 2 "" H 6750 5500 50  0001 C CNN
F 3 "" H 6750 5500 50  0001 C CNN
	1    6800 5450
	1    0    0    -1  
$EndComp
$Comp
L A5C:HA1 UEK53
U 1 1 7ECBD3C6
P 6800 6050
AR Path="/60E86855/6112B94A/7ECBD3C6" Ref="UEK53"  Part="1" 
AR Path="/612D3239/612D3C9E/6112B94A/7ECBD3C6" Ref="UEK53"  Part="1" 
F 0 "UEK53" H 6800 6415 50  0000 C CNN
F 1 "HA1" H 6800 6324 50  0000 C CNN
F 2 "" H 6750 6050 50  0001 C CNN
F 3 "" H 6750 6050 50  0001 C CNN
	1    6800 6050
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFFCOR UEG87
U 1 1 61118DF3
P 4350 3850
AR Path="/60E86855/6112B94A/61118DF3" Ref="UEG87"  Part="1" 
AR Path="/612D3239/612D3C9E/6112B94A/61118DF3" Ref="UEG87"  Part="1" 
F 0 "UEG87" H 4350 4215 50  0000 C CNN
F 1 "DFFCOR" H 4350 4124 50  0000 C CNN
F 2 "" H 4350 3850 50  0001 C CNN
F 3 "" H 4350 3850 50  0001 C CNN
	1    4350 3850
	1    0    0    -1  
$EndComp
$Comp
L A5C:FA2 UEI26
U 1 1 75AAD6F5
P 6900 4750
AR Path="/60E86855/6112B94A/75AAD6F5" Ref="UEI26"  Part="1" 
AR Path="/612D3239/612D3C9E/6112B94A/75AAD6F5" Ref="UEI26"  Part="1" 
F 0 "UEI26" H 6900 5315 50  0000 C CNN
F 1 "FA2" H 6900 5224 50  0000 C CNN
F 2 "" H 6750 4950 50  0001 C CNN
F 3 "" H 6750 4950 50  0001 C CNN
	1    6900 4750
	1    0    0    -1  
$EndComp
$Comp
L A5C:M175C UEI98
U 1 1 61118DF5
P 4350 4800
AR Path="/60E86855/6112B94A/61118DF5" Ref="UEI98"  Part="1" 
AR Path="/612D3239/612D3C9E/6112B94A/61118DF5" Ref="UEI98"  Part="1" 
F 0 "UEI98" H 4350 5365 50  0000 C CNN
F 1 "M175C" H 4350 5274 50  0000 C CNN
F 2 "" H 4350 4600 50  0001 C CNN
F 3 "" H 4350 4600 50  0001 C CNN
	1    4350 4800
	1    0    0    -1  
$EndComp
$Comp
L A5C:M175C UEI45
U 1 1 61118DF4
P 4350 5950
AR Path="/60E86855/6112B94A/61118DF4" Ref="UEI45"  Part="1" 
AR Path="/612D3239/612D3C9E/6112B94A/61118DF4" Ref="UEI45"  Part="1" 
F 0 "UEI45" H 4350 6515 50  0000 C CNN
F 1 "M175C" H 4350 6424 50  0000 C CNN
F 2 "" H 4350 5750 50  0001 C CNN
F 3 "" H 4350 5750 50  0001 C CNN
	1    4350 5950
	1    0    0    -1  
$EndComp
Wire Bus Line
	4900 6650 4900 7050
Wire Bus Line
	3050 3500 3050 5850
Wire Bus Line
	8350 3750 8350 4800
$EndSCHEMATC
