EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 16 83
Title "CPS Reverse Engineering: DL-0311"
Date "2021-09-12"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 "Licence: CC-BY"
Comment4 "Author: Loïc *WydD* Petit"
$EndDescr
Connection ~ 3700 2100
Connection ~ 2700 2250
Connection ~ 2600 3200
Connection ~ 7350 5050
Connection ~ 7250 6000
Connection ~ 8350 4900
Connection ~ 8350 2100
Connection ~ 7350 2250
Connection ~ 7250 3200
Connection ~ 9950 950 
Connection ~ 1900 850 
NoConn ~ 4800 1650
NoConn ~ 4800 1750
NoConn ~ 9450 5700
NoConn ~ 9450 5800
NoConn ~ 9450 3000
NoConn ~ 9450 1650
Entry Wire Line
	1900 2600 2000 2700
Entry Wire Line
	1900 2700 2000 2800
Entry Wire Line
	1900 2800 2000 2900
Entry Wire Line
	1900 2900 2000 3000
Entry Wire Line
	5200 1850 5300 1750
Entry Wire Line
	5200 1950 5300 1850
Entry Wire Line
	5200 2700 5300 2600
Entry Wire Line
	5200 2800 5300 2700
Entry Wire Line
	5200 2900 5300 2800
Entry Wire Line
	5200 3000 5300 2900
Entry Wire Line
	6550 1550 6650 1650
Entry Wire Line
	6550 1650 6650 1750
Entry Wire Line
	6550 1750 6650 1850
Entry Wire Line
	6550 1850 6650 1950
Entry Wire Line
	6550 2600 6650 2700
Entry Wire Line
	6550 2700 6650 2800
Entry Wire Line
	6550 2800 6650 2900
Entry Wire Line
	6550 2900 6650 3000
Entry Wire Line
	6550 4350 6650 4450
Entry Wire Line
	6550 4450 6650 4550
Entry Wire Line
	6550 4550 6650 4650
Entry Wire Line
	6550 4650 6650 4750
Entry Wire Line
	6550 5400 6650 5500
Entry Wire Line
	6550 5500 6650 5600
Entry Wire Line
	6550 5600 6650 5700
Entry Wire Line
	6550 5700 6650 5800
Entry Wire Line
	9850 1750 9950 1650
Entry Wire Line
	9850 1850 9950 1750
Entry Wire Line
	9850 1950 9950 1850
Entry Wire Line
	9850 2700 9950 2600
Entry Wire Line
	9850 2800 9950 2700
Entry Wire Line
	9850 2900 9950 2800
Entry Wire Line
	9850 4450 9950 4350
Entry Wire Line
	9850 4550 9950 4450
Entry Wire Line
	9850 4650 9950 4550
Entry Wire Line
	9850 4750 9950 4650
Entry Wire Line
	9850 5500 9950 5400
Entry Wire Line
	9850 5600 9950 5500
Wire Wire Line
	1750 3200 2600 3200
Wire Wire Line
	2000 1650 2800 1650
Wire Wire Line
	2000 1750 2800 1750
Wire Wire Line
	2000 1850 2800 1850
Wire Wire Line
	2000 1950 2800 1950
Wire Wire Line
	2000 2700 2800 2700
Wire Wire Line
	2000 2800 2800 2800
Wire Wire Line
	2000 2900 2800 2900
Wire Wire Line
	2000 3000 2800 3000
Wire Wire Line
	2600 2150 2800 2150
Wire Wire Line
	2600 3200 2600 2150
Wire Wire Line
	2700 1550 2700 2250
Wire Wire Line
	2700 2250 2700 3300
Wire Wire Line
	2700 2250 2800 2250
Wire Wire Line
	2700 3300 2800 3300
Wire Wire Line
	2800 3200 2600 3200
Wire Wire Line
	3350 1250 3700 1250
Wire Wire Line
	3600 1650 3800 1650
Wire Wire Line
	3600 1750 3800 1750
Wire Wire Line
	3600 1850 3800 1850
Wire Wire Line
	3600 1950 3800 1950
Wire Wire Line
	3600 2700 3800 2700
Wire Wire Line
	3600 2800 3800 2800
Wire Wire Line
	3600 2900 3800 2900
Wire Wire Line
	3600 3000 3800 3000
Wire Wire Line
	3700 1250 3700 2100
Wire Wire Line
	3700 2100 3700 3150
Wire Wire Line
	3800 2100 3700 2100
Wire Wire Line
	3800 3150 3700 3150
Wire Wire Line
	4800 1850 5200 1850
Wire Wire Line
	4800 1950 5200 1950
Wire Wire Line
	4800 2700 5200 2700
Wire Wire Line
	4800 2800 5200 2800
Wire Wire Line
	4800 2900 5200 2900
Wire Wire Line
	4800 3000 5200 3000
Wire Wire Line
	6400 3200 7250 3200
Wire Wire Line
	6400 6000 7250 6000
Wire Wire Line
	6650 1650 7450 1650
Wire Wire Line
	6650 1750 7450 1750
Wire Wire Line
	6650 1850 7450 1850
Wire Wire Line
	6650 1950 7450 1950
Wire Wire Line
	6650 2700 7450 2700
Wire Wire Line
	6650 2800 7450 2800
Wire Wire Line
	6650 2900 7450 2900
Wire Wire Line
	6650 3000 7450 3000
Wire Wire Line
	6650 4450 7450 4450
Wire Wire Line
	6650 4550 7450 4550
Wire Wire Line
	6650 4650 7450 4650
Wire Wire Line
	6650 4750 7450 4750
Wire Wire Line
	6650 5500 7450 5500
Wire Wire Line
	6650 5600 7450 5600
Wire Wire Line
	6650 5700 7450 5700
Wire Wire Line
	6650 5800 7450 5800
Wire Wire Line
	7250 2150 7450 2150
Wire Wire Line
	7250 3200 7250 2150
Wire Wire Line
	7250 4950 7450 4950
Wire Wire Line
	7250 6000 7250 4950
Wire Wire Line
	7350 1550 7350 2250
Wire Wire Line
	7350 2250 7350 3300
Wire Wire Line
	7350 2250 7450 2250
Wire Wire Line
	7350 3300 7450 3300
Wire Wire Line
	7350 4350 7350 5050
Wire Wire Line
	7350 5050 7350 6100
Wire Wire Line
	7350 5050 7450 5050
Wire Wire Line
	7350 6100 7450 6100
Wire Wire Line
	7450 3200 7250 3200
Wire Wire Line
	7450 6000 7250 6000
Wire Wire Line
	8000 1250 8350 1250
Wire Wire Line
	8000 4050 8350 4050
Wire Wire Line
	8250 1650 8450 1650
Wire Wire Line
	8250 1750 8450 1750
Wire Wire Line
	8250 1850 8450 1850
Wire Wire Line
	8250 1950 8450 1950
Wire Wire Line
	8250 2700 8450 2700
Wire Wire Line
	8250 2800 8450 2800
Wire Wire Line
	8250 2900 8450 2900
Wire Wire Line
	8250 3000 8450 3000
Wire Wire Line
	8250 4450 8450 4450
Wire Wire Line
	8250 4550 8450 4550
Wire Wire Line
	8250 4650 8450 4650
Wire Wire Line
	8250 4750 8450 4750
Wire Wire Line
	8250 5500 8450 5500
Wire Wire Line
	8250 5600 8450 5600
Wire Wire Line
	8250 5700 8450 5700
Wire Wire Line
	8250 5800 8450 5800
Wire Wire Line
	8350 1250 8350 2100
Wire Wire Line
	8350 2100 8350 3150
Wire Wire Line
	8350 4050 8350 4900
Wire Wire Line
	8350 4900 8350 5950
Wire Wire Line
	8450 2100 8350 2100
Wire Wire Line
	8450 3150 8350 3150
Wire Wire Line
	8450 4900 8350 4900
Wire Wire Line
	8450 5950 8350 5950
Wire Wire Line
	9450 1750 9850 1750
Wire Wire Line
	9450 1850 9850 1850
Wire Wire Line
	9450 1950 9850 1950
Wire Wire Line
	9450 2700 9850 2700
Wire Wire Line
	9450 2800 9850 2800
Wire Wire Line
	9450 2900 9850 2900
Wire Wire Line
	9450 4450 9850 4450
Wire Wire Line
	9450 4550 9850 4550
Wire Wire Line
	9450 4650 9850 4650
Wire Wire Line
	9450 4750 9850 4750
Wire Wire Line
	9450 5500 9850 5500
Wire Wire Line
	9450 5600 9850 5600
Wire Bus Line
	1500 850  1900 850 
Wire Bus Line
	1900 850  6550 850 
Wire Bus Line
	5300 950  9950 950 
Wire Bus Line
	9950 950  10200 950 
Text Label 2500 1650 2    50   ~ 0
BUS-DATA10
Text Label 2500 1750 2    50   ~ 0
BUS-DATA9
Text Label 2500 1850 2    50   ~ 0
BUS-DATA8
Text Label 2500 1950 2    50   ~ 0
BUS-DATA7
Text Label 2500 2700 2    50   ~ 0
BUS-DATA6
Text Label 2500 2800 2    50   ~ 0
BUS-DATA5
Text Label 2500 2900 2    50   ~ 0
BUS-DATA4
Text Label 2500 3000 2    50   ~ 0
BUS-DATA3
Text Label 4800 1850 0    50   ~ 0
BASE-X5
Text Label 4800 1950 0    50   ~ 0
BASE-X4
Text Label 4800 2700 0    50   ~ 0
BASE-X3
Text Label 4800 2800 0    50   ~ 0
BASE-X2
Text Label 4800 2900 0    50   ~ 0
BASE-X1
Text Label 4800 3000 0    50   ~ 0
BASE-X0
Text Label 7150 1650 2    50   ~ 0
BUS-DATA10
Text Label 7150 1750 2    50   ~ 0
BUS-DATA9
Text Label 7150 1850 2    50   ~ 0
BUS-DATA8
Text Label 7150 1950 2    50   ~ 0
BUS-DATA7
Text Label 7150 2700 2    50   ~ 0
BUS-DATA6
Text Label 7150 2800 2    50   ~ 0
BUS-DATA5
Text Label 7150 2900 2    50   ~ 0
BUS-DATA4
Text Label 7150 3000 2    50   ~ 0
BUS-DATA3
Text Label 7150 4450 2    50   ~ 0
BUS-DATA10
Text Label 7150 4550 2    50   ~ 0
BUS-DATA9
Text Label 7150 4650 2    50   ~ 0
BUS-DATA8
Text Label 7150 4750 2    50   ~ 0
BUS-DATA7
Text Label 7150 5500 2    50   ~ 0
BUS-DATA6
Text Label 7150 5600 2    50   ~ 0
BUS-DATA5
Text Label 7150 5700 2    50   ~ 0
BUS-DATA4
Text Label 7150 5800 2    50   ~ 0
BUS-DATA3
Text Label 9450 1750 0    50   ~ 0
BASE-X5
Text Label 9450 1850 0    50   ~ 0
BASE-X4
Text Label 9450 1950 0    50   ~ 0
BASE-X3
Text Label 9450 2700 0    50   ~ 0
BASE-X2
Text Label 9450 2800 0    50   ~ 0
BASE-X1
Text Label 9450 2900 0    50   ~ 0
BASE-X0
Text Label 9450 4450 0    50   ~ 0
BASE-X5
Text Label 9450 4550 0    50   ~ 0
BASE-X4
Text Label 9450 4650 0    50   ~ 0
BASE-X3
Text Label 9450 4750 0    50   ~ 0
BASE-X2
Text Label 9450 5500 0    50   ~ 0
BASE-X1
Text Label 9450 5600 0    50   ~ 0
BASE-X0
Text HLabel 1200 3150 0    50   Input ~ 0
~CLK4M
Text HLabel 1200 3250 0    50   Input ~ 0
~SCROLL1-X
Text HLabel 1500 850  0    50   Input ~ 0
BUS-DATA[0..10]
Text HLabel 3350 1250 0    50   Input ~ 0
~BUS-ELSE
Text HLabel 5850 3150 0    50   Input ~ 0
~CLK4M
Text HLabel 5850 3250 0    50   Input ~ 0
~SCROLL2-X
Text HLabel 5850 5950 0    50   Input ~ 0
~CLK4M
Text HLabel 5850 6050 0    50   Input ~ 0
~SCROLL3-X
Text HLabel 8000 1250 0    50   Input ~ 0
~BUS-SCROLL2
Text HLabel 8000 4050 0    50   Input ~ 0
~BUS-SCROLL3
Text HLabel 10200 950  2    50   Output ~ 0
BASE-X[0..9]
$Comp
L power:VCC #PWR?
U 1 1 61741DBC
P 2700 1550
AR Path="/61111462/611CEDF2/61741DBC" Ref="#PWR?"  Part="1" 
AR Path="/61111462/61111C69/61741DBC" Ref="#PWR0187"  Part="1" 
F 0 "#PWR0187" H 2700 1400 50  0001 C CNN
F 1 "VCC" H 2715 1723 50  0000 C CNN
F 2 "" H 2700 1550 50  0001 C CNN
F 3 "" H 2700 1550 50  0001 C CNN
	1    2700 1550
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 61741DC8
P 7350 1550
AR Path="/61111462/611CEDF2/61741DC8" Ref="#PWR?"  Part="1" 
AR Path="/61111462/61111C69/61741DC8" Ref="#PWR0234"  Part="1" 
F 0 "#PWR0234" H 7350 1400 50  0001 C CNN
F 1 "VCC" H 7365 1723 50  0000 C CNN
F 2 "" H 7350 1550 50  0001 C CNN
F 3 "" H 7350 1550 50  0001 C CNN
	1    7350 1550
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 61741DCE
P 7350 4350
AR Path="/61111462/611CEDF2/61741DCE" Ref="#PWR?"  Part="1" 
AR Path="/61111462/61111C69/61741DCE" Ref="#PWR0247"  Part="1" 
F 0 "#PWR0247" H 7350 4200 50  0001 C CNN
F 1 "VCC" H 7365 4523 50  0000 C CNN
F 2 "" H 7350 4350 50  0001 C CNN
F 3 "" H 7350 4350 50  0001 C CNN
	1    7350 4350
	1    0    0    -1  
$EndComp
$Comp
L A5C:OR02 UCC301
U 1 1 64464DF2
P 1450 3200
F 0 "UCC301" H 1475 3467 50  0000 C CNN
F 1 "OR02" H 1475 3376 50  0000 C CNN
F 2 "" H 1500 3200 50  0001 C CNN
F 3 "" H 1500 3200 50  0001 C CNN
	1    1450 3200
	1    0    0    -1  
$EndComp
$Comp
L A5C:OR02 UCA310
U 1 1 6BD0A7F6
P 6100 3200
F 0 "UCA310" H 6125 3467 50  0000 C CNN
F 1 "OR02" H 6125 3376 50  0000 C CNN
F 2 "" H 6150 3200 50  0001 C CNN
F 3 "" H 6150 3200 50  0001 C CNN
	1    6100 3200
	1    0    0    -1  
$EndComp
$Comp
L A5C:OR02 UCA367
U 1 1 6C156D8E
P 6100 6000
F 0 "UCA367" H 6125 6267 50  0000 C CNN
F 1 "OR02" H 6125 6176 50  0000 C CNN
F 2 "" H 6150 6000 50  0001 C CNN
F 3 "" H 6150 6000 50  0001 C CNN
	1    6100 6000
	1    0    0    -1  
$EndComp
$Comp
L A5C:M175CL UCB309
U 1 1 61D32E83
P 3200 1950
F 0 "UCB309" H 3200 2515 50  0000 C CNN
F 1 "M175CL" H 3200 2424 50  0000 C CNN
F 2 "" H 3200 1750 50  0001 C CNN
F 3 "" H 3200 1750 50  0001 C CNN
	1    3200 1950
	1    0    0    -1  
$EndComp
$Comp
L A5C:M175CL UCA380
U 1 1 6118560D
P 3200 3000
F 0 "UCA380" H 3200 3565 50  0000 C CNN
F 1 "M175CL" H 3200 3474 50  0000 C CNN
F 2 "" H 3200 2800 50  0001 C CNN
F 3 "" H 3200 2800 50  0001 C CNN
	1    3200 3000
	1    0    0    -1  
$EndComp
$Comp
L A5C:M175CL UCA252
U 1 1 6BD0A7DE
P 7850 1950
F 0 "UCA252" H 7850 2515 50  0000 C CNN
F 1 "M175CL" H 7850 2424 50  0000 C CNN
F 2 "" H 7850 1750 50  0001 C CNN
F 3 "" H 7850 1750 50  0001 C CNN
	1    7850 1950
	1    0    0    -1  
$EndComp
$Comp
L A5C:M175CL UCA290
U 1 1 6BD0A7D8
P 7850 3000
F 0 "UCA290" H 7850 3565 50  0000 C CNN
F 1 "M175CL" H 7850 3474 50  0000 C CNN
F 2 "" H 7850 2800 50  0001 C CNN
F 3 "" H 7850 2800 50  0001 C CNN
	1    7850 3000
	1    0    0    -1  
$EndComp
$Comp
L A5C:M175CL UBJ135
U 1 1 6C156D76
P 7850 4750
F 0 "UBJ135" H 7850 5315 50  0000 C CNN
F 1 "M175CL" H 7850 5224 50  0000 C CNN
F 2 "" H 7850 4550 50  0001 C CNN
F 3 "" H 7850 4550 50  0001 C CNN
	1    7850 4750
	1    0    0    -1  
$EndComp
$Comp
L A5C:M175CL UCA347
U 1 1 6C156D70
P 7850 5800
F 0 "UCA347" H 7850 6365 50  0000 C CNN
F 1 "M175CL" H 7850 6274 50  0000 C CNN
F 2 "" H 7850 5600 50  0001 C CNN
F 3 "" H 7850 5600 50  0001 C CNN
	1    7850 5800
	1    0    0    -1  
$EndComp
$Comp
L A5C:M368C UCA?
U 1 1 63934DB6
P 4300 1900
AR Path="/604B5523/63934DB6" Ref="UCA?"  Part="1" 
AR Path="/63934DB6" Ref="UCB292"  Part="1" 
AR Path="/60EC0B61/63934DB6" Ref="UCB292"  Part="1" 
AR Path="/61111462/61111C69/63934DB6" Ref="UCB292"  Part="1" 
F 0 "UCB292" H 4300 2410 50  0000 C CNN
F 1 "M368C" H 4300 2320 50  0000 C CNN
F 2 "" H 4600 1700 50  0001 C CNN
F 3 "" H 4600 1700 50  0001 C CNN
	1    4300 1900
	1    0    0    -1  
$EndComp
$Comp
L A5C:M368C UEH?
U 1 1 63550A84
P 4300 2950
AR Path="/604B5523/63550A84" Ref="UEH?"  Part="1" 
AR Path="/63550A84" Ref="UCA370"  Part="1" 
AR Path="/60EC0B61/63550A84" Ref="UCA370"  Part="1" 
AR Path="/61111462/61111C69/63550A84" Ref="UCA370"  Part="1" 
F 0 "UCA370" H 4300 3460 50  0000 C CNN
F 1 "M368C" H 4300 3370 50  0000 C CNN
F 2 "" H 4600 2750 50  0001 C CNN
F 3 "" H 4600 2750 50  0001 C CNN
	1    4300 2950
	1    0    0    -1  
$EndComp
$Comp
L A5C:M368C UCA?
U 1 1 6BD0A7A3
P 8950 1900
AR Path="/604B5523/6BD0A7A3" Ref="UCA?"  Part="1" 
AR Path="/6BD0A7A3" Ref="UCB181"  Part="1" 
AR Path="/60E86855/6BD0A7A3" Ref="UCB181"  Part="1" 
AR Path="/61111462/61111C69/6BD0A7A3" Ref="UCB181"  Part="1" 
F 0 "UCB181" H 8950 2410 50  0000 C CNN
F 1 "M368C" H 8950 2320 50  0000 C CNN
F 2 "" H 9250 1700 50  0001 C CNN
F 3 "" H 9250 1700 50  0001 C CNN
	1    8950 1900
	1    0    0    -1  
$EndComp
$Comp
L A5C:M368C UEH?
U 1 1 6BD0A79D
P 8950 2950
AR Path="/604B5523/6BD0A79D" Ref="UEH?"  Part="1" 
AR Path="/6BD0A79D" Ref="UCA281"  Part="1" 
AR Path="/60E86855/6BD0A79D" Ref="UCA281"  Part="1" 
AR Path="/61111462/61111C69/6BD0A79D" Ref="UCA281"  Part="1" 
F 0 "UCA281" H 8950 3460 50  0000 C CNN
F 1 "M368C" H 8950 3370 50  0000 C CNN
F 2 "" H 9250 2750 50  0001 C CNN
F 3 "" H 9250 2750 50  0001 C CNN
	1    8950 2950
	1    0    0    -1  
$EndComp
$Comp
L A5C:M368C UCA?
U 1 1 6C156D3B
P 8950 4700
AR Path="/604B5523/6C156D3B" Ref="UCA?"  Part="1" 
AR Path="/6C156D3B" Ref="UBJ155"  Part="1" 
AR Path="/60E34936/6C156D3B" Ref="UBJ155"  Part="1" 
AR Path="/61111462/61111C69/6C156D3B" Ref="UBJ155"  Part="1" 
F 0 "UBJ155" H 8950 5210 50  0000 C CNN
F 1 "M368C" H 8950 5120 50  0000 C CNN
F 2 "" H 9250 4500 50  0001 C CNN
F 3 "" H 9250 4500 50  0001 C CNN
	1    8950 4700
	1    0    0    -1  
$EndComp
$Comp
L A5C:M368C UEH?
U 1 1 6C156D35
P 8950 5750
AR Path="/604B5523/6C156D35" Ref="UEH?"  Part="1" 
AR Path="/6C156D35" Ref="UCA336"  Part="1" 
AR Path="/60E34936/6C156D35" Ref="UCA336"  Part="1" 
AR Path="/61111462/61111C69/6C156D35" Ref="UCA336"  Part="1" 
F 0 "UCA336" H 8950 6260 50  0000 C CNN
F 1 "M368C" H 8950 6170 50  0000 C CNN
F 2 "" H 9250 5550 50  0001 C CNN
F 3 "" H 9250 5550 50  0001 C CNN
	1    8950 5750
	1    0    0    -1  
$EndComp
Wire Bus Line
	1900 850  1900 2900
Wire Bus Line
	5300 950  5300 2900
Wire Bus Line
	9950 950  9950 5500
Wire Bus Line
	6550 850  6550 5700
$EndSCHEMATC
