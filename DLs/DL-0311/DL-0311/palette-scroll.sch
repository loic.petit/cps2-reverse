EESchema Schematic File Version 5
EELAYER 34 0
EELAYER END
$Descr A1 33110 23386
encoding utf-8
Sheet 23 52
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
Text Notes 25850 10637 2    787  ~ 0
PALETTE SCROLL\nDURING LUTPRO
$Sheet
S 11400 14100 1500 2000
U 611C3AC2
F0 "palette-addr" 50
F1 "palette-addr.sch" 50
$EndSheet
$Sheet
S 1250 13050 1550 2150
U 6119608B
F0 "wstlut-counter" 50
F1 "wstlut-counter.sch" 50
$EndSheet
$EndSCHEMATC
