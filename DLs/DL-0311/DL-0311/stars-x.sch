EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 25 83
Title "CPS Reverse Engineering: DL-0311"
Date "2021-09-12"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 "Licence: CC-BY"
Comment4 "Author: Loïc *WydD* Petit"
$EndDescr
Connection ~ 4750 3800
Connection ~ 4750 3050
Connection ~ 4750 4400
Connection ~ 7000 2550
Connection ~ 7000 1750
Connection ~ 7000 2450
Connection ~ 7000 1550
Connection ~ 7000 1650
Connection ~ 7000 2650
Connection ~ 6900 4700
Connection ~ 6900 3800
Connection ~ 6900 2900
Entry Wire Line
	1950 2850 1850 2750
Entry Wire Line
	1950 2950 1850 2850
Entry Wire Line
	1950 3050 1850 2950
Entry Wire Line
	1950 3150 1850 3050
Entry Wire Line
	1950 3350 1850 3450
Entry Wire Line
	1950 3450 1850 3550
Entry Wire Line
	1950 3550 1850 3650
Entry Wire Line
	1950 3650 1850 3750
Entry Wire Line
	6250 4250 6150 4350
Entry Wire Line
	6250 4350 6150 4450
Entry Wire Line
	6250 4450 6150 4550
Entry Wire Line
	6250 4550 6150 4650
Entry Wire Line
	8650 1450 8550 1550
Entry Wire Line
	8650 1550 8550 1650
Entry Wire Line
	8650 1650 8550 1750
Entry Wire Line
	8650 1750 8550 1850
Entry Wire Line
	8650 2350 8550 2450
Entry Wire Line
	8650 2450 8550 2550
Entry Wire Line
	8650 2550 8550 2650
Entry Wire Line
	8650 2650 8550 2750
Entry Wire Line
	8650 3250 8550 3350
Entry Wire Line
	8650 3350 8550 3450
Entry Wire Line
	8650 3450 8550 3550
Entry Wire Line
	8650 3550 8550 3650
Entry Wire Line
	8650 4150 8550 4250
Entry Wire Line
	8650 4250 8550 4350
Entry Wire Line
	8650 4350 8550 4450
Entry Wire Line
	8650 4450 8550 4550
Wire Wire Line
	2500 2850 1950 2850
Wire Wire Line
	2500 2950 1950 2950
Wire Wire Line
	2500 3050 1950 3050
Wire Wire Line
	2500 3150 1950 3150
Wire Wire Line
	2500 3350 1950 3350
Wire Wire Line
	2500 3450 1950 3450
Wire Wire Line
	2500 3550 1950 3550
Wire Wire Line
	2500 3650 1950 3650
Wire Wire Line
	3500 3100 3600 3100
Wire Wire Line
	3500 3300 3700 3300
Wire Wire Line
	3600 2250 3600 3100
Wire Wire Line
	3600 2250 3900 2250
Wire Wire Line
	3600 3400 3500 3400
Wire Wire Line
	3600 3400 3600 4300
Wire Wire Line
	3600 4300 4900 4300
Wire Wire Line
	3700 3200 3500 3200
Wire Wire Line
	3700 3200 3700 2850
Wire Wire Line
	3700 3300 3700 3600
Wire Wire Line
	3900 2500 3900 2350
Wire Wire Line
	3900 2850 3700 2850
Wire Wire Line
	3900 3050 3900 3250
Wire Wire Line
	3900 3250 4500 3250
Wire Wire Line
	3900 3600 3700 3600
Wire Wire Line
	4500 2500 3900 2500
Wire Wire Line
	4500 2850 4500 2500
Wire Wire Line
	4500 3250 4500 3600
Wire Wire Line
	4600 4400 4750 4400
Wire Wire Line
	4750 2400 4750 3050
Wire Wire Line
	4750 3050 4750 3800
Wire Wire Line
	4750 3800 4750 4400
Wire Wire Line
	4750 4400 4900 4400
Wire Wire Line
	4850 2300 4450 2300
Wire Wire Line
	4850 2400 4750 2400
Wire Wire Line
	4900 2950 4500 2950
Wire Wire Line
	4900 3050 4750 3050
Wire Wire Line
	4900 3700 4500 3700
Wire Wire Line
	4900 3800 4750 3800
Wire Wire Line
	5400 2350 5700 2350
Wire Wire Line
	5450 3750 5600 3750
Wire Wire Line
	5600 3000 5450 3000
Wire Wire Line
	5600 3000 5600 3450
Wire Wire Line
	5600 3450 7000 3450
Wire Wire Line
	5600 3550 7000 3550
Wire Wire Line
	5600 3750 5600 3550
Wire Wire Line
	5700 2350 5700 3350
Wire Wire Line
	5700 3350 7000 3350
Wire Wire Line
	5700 3650 5700 4350
Wire Wire Line
	5700 3650 7000 3650
Wire Wire Line
	5700 4350 5450 4350
Wire Wire Line
	6250 4250 7000 4250
Wire Wire Line
	6250 4350 7000 4350
Wire Wire Line
	6250 4450 7000 4450
Wire Wire Line
	6250 4550 7000 4550
Wire Wire Line
	6900 2000 7000 2000
Wire Wire Line
	6900 2900 6900 2000
Wire Wire Line
	6900 2900 7000 2900
Wire Wire Line
	6900 3800 6900 2900
Wire Wire Line
	6900 3800 7000 3800
Wire Wire Line
	6900 4700 6700 4700
Wire Wire Line
	6900 4700 6900 3800
Wire Wire Line
	7000 1650 7000 1550
Wire Wire Line
	7000 1750 7000 1650
Wire Wire Line
	7000 1850 7000 1750
Wire Wire Line
	7000 2550 7000 2450
Wire Wire Line
	7000 2650 7000 2550
Wire Wire Line
	7000 2750 7000 2650
Wire Wire Line
	7000 4700 6900 4700
Wire Wire Line
	8550 1550 8000 1550
Wire Wire Line
	8550 1650 8000 1650
Wire Wire Line
	8550 1750 8000 1750
Wire Wire Line
	8550 1850 8000 1850
Wire Wire Line
	8550 2450 8000 2450
Wire Wire Line
	8550 2550 8000 2550
Wire Wire Line
	8550 2650 8000 2650
Wire Wire Line
	8550 2750 8000 2750
Wire Wire Line
	8550 4250 8000 4250
Wire Wire Line
	8550 3350 8000 3350
Wire Wire Line
	8550 3450 8000 3450
Wire Wire Line
	8550 3550 8000 3550
Wire Wire Line
	8550 3650 8000 3650
Wire Wire Line
	8550 4350 8000 4350
Wire Wire Line
	8550 4450 8000 4450
Wire Wire Line
	8550 4550 8000 4550
Wire Bus Line
	1850 2650 1750 2650
Wire Bus Line
	1850 3850 1750 3850
Wire Bus Line
	6150 4800 6050 4800
Wire Bus Line
	8650 1300 8750 1300
Text Label 6300 4250 0    50   ~ 0
Y-OUT8
Text Label 6300 4350 0    50   ~ 0
Y-OUT7
Text Label 6300 4450 0    50   ~ 0
Y-OUT6
Text Label 6300 4550 0    50   ~ 0
Y-OUT5
Text Label 8550 1550 2    50   ~ 0
GFX-ADDR19
Text Label 8550 1650 2    50   ~ 0
GFX-ADDR18
Text Label 8550 1750 2    50   ~ 0
GFX-ADDR17
Text Label 8550 1850 2    50   ~ 0
GFX-ADDR16
Text Label 8550 2450 2    50   ~ 0
GFX-ADDR15
Text Label 8550 2550 2    50   ~ 0
GFX-ADDR14
Text Label 8550 2650 2    50   ~ 0
GFX-ADDR13
Text Label 8550 2750 2    50   ~ 0
GFX-ADDR12
Text Label 8550 4250 2    50   ~ 0
GFX-ADDR11
Text Label 8550 3350 2    50   ~ 0
GFX-ADDR10
Text Label 8550 3450 2    50   ~ 0
GFX-ADDR9
Text Label 8550 3550 2    50   ~ 0
GFX-ADDR8
Text Label 8550 3650 2    50   ~ 0
GFX-ADDR7
Text Label 8550 4350 2    50   ~ 0
GFX-ADDR6
Text Label 8550 4450 2    50   ~ 0
GFX-ADDR5
Text Label 8550 4550 2    50   ~ 0
GFX-ADDR4
Text HLabel 1750 2650 0    50   Input ~ 0
STAR2-X[0..3]
Text HLabel 1750 3850 0    50   Input ~ 0
STAR1-X[0..3]
Text HLabel 2500 3850 0    50   Input ~ 0
~STAR2-T
Text HLabel 4600 4400 0    50   Input ~ 0
FLIP
Text HLabel 6050 4800 0    50   Input ~ 0
Y-OUT[5..8]
Text HLabel 6700 4700 0    50   Input ~ 0
~STARS-T
Text HLabel 8750 1300 2    50   3State ~ 0
GFX-ADDR[4..19]
$Comp
L power:VCC #PWR0208
U 1 1 7326C178
P 3900 3800
F 0 "#PWR0208" H 3900 3650 50  0001 C CNN
F 1 "VCC" H 3915 3973 50  0000 C CNN
F 2 "" H 3900 3800 50  0001 C CNN
F 3 "" H 3900 3800 50  0001 C CNN
	1    3900 3800
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0109
U 1 1 61118DBC
P 7000 1550
F 0 "#PWR0109" H 7000 1400 50  0001 C CNN
F 1 "VCC" H 7015 1723 50  0000 C CNN
F 2 "" H 7000 1550 50  0001 C CNN
F 3 "" H 7000 1550 50  0001 C CNN
	1    7000 1550
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0110
U 1 1 792597DB
P 7000 2450
F 0 "#PWR0110" H 7000 2300 50  0001 C CNN
F 1 "VCC" H 7015 2623 50  0000 C CNN
F 2 "" H 7000 2450 50  0001 C CNN
F 3 "" H 7000 2450 50  0001 C CNN
	1    7000 2450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0209
U 1 1 72739FF2
P 2500 4000
F 0 "#PWR0209" H 2500 3750 50  0001 C CNN
F 1 "GND" H 2505 3827 50  0000 C CNN
F 2 "" H 2500 4000 50  0001 C CNN
F 3 "" H 2500 4000 50  0001 C CNN
	1    2500 4000
	1    0    0    -1  
$EndComp
$Comp
L A5C:XOR02 UEJ211
U 1 1 72AF63CF
P 4150 2300
AR Path="/60DE1519/72AF63CF" Ref="UEJ211"  Part="1" 
AR Path="/612D3239/612D3C70/72AF63CF" Ref="UEJ211"  Part="1" 
F 0 "UEJ211" H 4175 2567 50  0000 C CNN
F 1 "XOR02" H 4175 2476 50  0000 C CNN
F 2 "" H 4150 2300 50  0001 C CNN
F 3 "" H 4150 2300 50  0001 C CNN
	1    4150 2300
	1    0    0    -1  
$EndComp
$Comp
L A5C:XNOR02 UEI67
U 1 1 61118E19
P 5100 2350
AR Path="/60DE1519/61118E19" Ref="UEI67"  Part="1" 
AR Path="/612D3239/612D3C70/61118E19" Ref="UEI67"  Part="1" 
F 0 "UEI67" H 5125 2617 50  0000 C CNN
F 1 "XNOR02" H 5125 2526 50  0000 C CNN
F 2 "" H 5125 2350 50  0001 C CNN
F 3 "" H 5125 2350 50  0001 C CNN
	1    5100 2350
	1    0    0    -1  
$EndComp
$Comp
L A5C:XNOR02 UEJ131
U 1 1 7BD26EE5
P 5150 3000
AR Path="/60DE1519/7BD26EE5" Ref="UEJ131"  Part="1" 
AR Path="/612D3239/612D3C70/7BD26EE5" Ref="UEJ131"  Part="1" 
F 0 "UEJ131" H 5175 3267 50  0000 C CNN
F 1 "XNOR02" H 5175 3176 50  0000 C CNN
F 2 "" H 5175 3000 50  0001 C CNN
F 3 "" H 5175 3000 50  0001 C CNN
	1    5150 3000
	1    0    0    -1  
$EndComp
$Comp
L A5C:XNOR02 UEI78
U 1 1 7F96AF20
P 5150 3750
AR Path="/60DE1519/7F96AF20" Ref="UEI78"  Part="1" 
AR Path="/612D3239/612D3C70/7F96AF20" Ref="UEI78"  Part="1" 
F 0 "UEI78" H 5175 4017 50  0000 C CNN
F 1 "XNOR02" H 5175 3926 50  0000 C CNN
F 2 "" H 5175 3750 50  0001 C CNN
F 3 "" H 5175 3750 50  0001 C CNN
	1    5150 3750
	1    0    0    -1  
$EndComp
$Comp
L A5C:XNOR02 UEI186
U 1 1 753E978E
P 5150 4350
AR Path="/60DE1519/753E978E" Ref="UEI186"  Part="1" 
AR Path="/612D3239/612D3C70/753E978E" Ref="UEI186"  Part="1" 
F 0 "UEI186" H 5175 4617 50  0000 C CNN
F 1 "XNOR02" H 5175 4526 50  0000 C CNN
F 2 "" H 5175 4350 50  0001 C CNN
F 3 "" H 5175 4350 50  0001 C CNN
	1    5150 4350
	1    0    0    -1  
$EndComp
$Comp
L A5C:HA1 UEJ207
U 1 1 72AFF510
P 4200 2950
AR Path="/60DE1519/72AFF510" Ref="UEJ207"  Part="1" 
AR Path="/612D3239/612D3C70/72AFF510" Ref="UEJ207"  Part="1" 
F 0 "UEJ207" H 4200 3315 50  0000 C CNN
F 1 "HA1" H 4200 3224 50  0000 C CNN
F 2 "" H 4150 2950 50  0001 C CNN
F 3 "" H 4150 2950 50  0001 C CNN
	1    4200 2950
	1    0    0    -1  
$EndComp
$Comp
L A5C:HA1 UEI201
U 1 1 72EB9C72
P 4200 3700
AR Path="/60DE1519/72EB9C72" Ref="UEI201"  Part="1" 
AR Path="/612D3239/612D3C70/72EB9C72" Ref="UEI201"  Part="1" 
F 0 "UEI201" H 4200 4065 50  0000 C CNN
F 1 "HA1" H 4200 3974 50  0000 C CNN
F 2 "" H 4150 3700 50  0001 C CNN
F 3 "" H 4150 3700 50  0001 C CNN
	1    4200 3700
	1    0    0    -1  
$EndComp
$Comp
L A5C:M368C UCC?
U 1 1 61118DBB
P 7500 1800
AR Path="/604B5523/61118DBB" Ref="UCC?"  Part="1" 
AR Path="/61118DBB" Ref="UEF171"  Part="1" 
AR Path="/60DE1519/61118DBB" Ref="UEF171"  Part="1" 
AR Path="/612D3239/612D3C70/61118DBB" Ref="UEF171"  Part="1" 
F 0 "UEF171" H 7500 2310 50  0000 C CNN
F 1 "M368C" H 7500 2220 50  0000 C CNN
F 2 "" H 7800 1600 50  0001 C CNN
F 3 "" H 7800 1600 50  0001 C CNN
	1    7500 1800
	1    0    0    -1  
$EndComp
$Comp
L A5C:M368C UCC?
U 1 1 768506A1
P 7500 2700
AR Path="/604B5523/768506A1" Ref="UCC?"  Part="1" 
AR Path="/768506A1" Ref="UEI171"  Part="1" 
AR Path="/60DE1519/768506A1" Ref="UEI171"  Part="1" 
AR Path="/612D3239/612D3C70/768506A1" Ref="UEI171"  Part="1" 
F 0 "UEI171" H 7500 3210 50  0000 C CNN
F 1 "M368C" H 7500 3120 50  0000 C CNN
F 2 "" H 7800 2500 50  0001 C CNN
F 3 "" H 7800 2500 50  0001 C CNN
	1    7500 2700
	1    0    0    -1  
$EndComp
$Comp
L A5C:M368C UCC?
U 1 1 77D0605D
P 7500 3600
AR Path="/604B5523/77D0605D" Ref="UCC?"  Part="1" 
AR Path="/77D0605D" Ref="UEI70"  Part="1" 
AR Path="/60DE1519/77D0605D" Ref="UEI70"  Part="1" 
AR Path="/612D3239/612D3C70/77D0605D" Ref="UEI70"  Part="1" 
F 0 "UEI70" H 7500 4110 50  0000 C CNN
F 1 "M368C" H 7500 4020 50  0000 C CNN
F 2 "" H 7800 3400 50  0001 C CNN
F 3 "" H 7800 3400 50  0001 C CNN
	1    7500 3600
	1    0    0    -1  
$EndComp
$Comp
L A5C:M367C UEE0
U 1 1 7E61FF4E
P 7500 4500
AR Path="/60DE1519/7E61FF4E" Ref="UEE0"  Part="1" 
AR Path="/612D3239/612D3C70/7E61FF4E" Ref="UEE0"  Part="1" 
F 0 "UEE0" H 7500 5015 50  0000 C CNN
F 1 "M367C" H 7500 4924 50  0000 C CNN
F 2 "" H 7800 4300 50  0001 C CNN
F 3 "" H 7800 4300 50  0001 C CNN
	1    7500 4500
	1    0    0    -1  
$EndComp
$Comp
L A5C:M157C UEI205
U 1 1 66CE1CBC
P 3000 3450
AR Path="/60DE1519/66CE1CBC" Ref="UEI205"  Part="1" 
AR Path="/612D3239/612D3C70/66CE1CBC" Ref="UEI205"  Part="1" 
F 0 "UEI205" H 3000 4315 50  0000 C CNN
F 1 "M157C" H 3000 4224 50  0000 C CNN
F 2 "" H 3000 3450 50  0001 C CNN
F 3 "" H 3000 3450 50  0001 C CNN
	1    3000 3450
	1    0    0    -1  
$EndComp
Text Label 2050 3350 0    50   ~ 0
STAR1-X3
Text Label 2050 3450 0    50   ~ 0
STAR1-X2
Text Label 2050 3550 0    50   ~ 0
STAR1-X1
Text Label 2050 3650 0    50   ~ 0
STAR1-X0
Text Label 2050 3150 0    50   ~ 0
STAR2-X0
Text Label 2050 3050 0    50   ~ 0
STAR2-X1
Text Label 2050 2950 0    50   ~ 0
STAR2-X2
Text Label 2050 2850 0    50   ~ 0
STAR2-X3
Wire Bus Line
	1850 2650 1850 3050
Wire Bus Line
	1850 3450 1850 3850
Wire Bus Line
	6150 4350 6150 4800
Wire Bus Line
	8650 1300 8650 4450
$EndSCHEMATC
