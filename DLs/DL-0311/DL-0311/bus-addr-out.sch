EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 12 83
Title "CPS Reverse Engineering: DL-0311"
Date "2021-09-12"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 "Licence: CC-BY"
Comment4 "Author: Loïc *WydD* Petit"
$EndDescr
Connection ~ 5050 2500
Connection ~ 5050 3600
Connection ~ 5050 4700
Connection ~ 5050 5800
Connection ~ 5050 1400
Connection ~ 4550 3850
Connection ~ 6550 3850
Entry Wire Line
	4550 1100 4650 1000
Entry Wire Line
	4550 1200 4650 1100
Entry Wire Line
	4550 1300 4650 1200
Entry Wire Line
	4550 2100 4650 2000
Entry Wire Line
	4550 2200 4650 2100
Entry Wire Line
	4550 2300 4650 2200
Entry Wire Line
	4550 2400 4650 2300
Entry Wire Line
	4550 3200 4650 3100
Entry Wire Line
	4550 3300 4650 3200
Entry Wire Line
	4550 3400 4650 3300
Entry Wire Line
	4550 3500 4650 3400
Entry Wire Line
	4550 4100 4650 4200
Entry Wire Line
	4550 4200 4650 4300
Entry Wire Line
	4550 4300 4650 4400
Entry Wire Line
	4550 4400 4650 4500
Entry Wire Line
	4550 5200 4650 5300
Entry Wire Line
	4550 5300 4650 5400
Entry Wire Line
	4550 5400 4650 5500
Entry Wire Line
	4550 5500 4650 5600
Entry Wire Line
	4550 6300 4650 6400
Entry Wire Line
	4550 6400 4650 6500
Entry Wire Line
	4550 6500 4650 6600
Entry Wire Line
	4550 6600 4650 6700
Entry Wire Line
	6450 1000 6550 1100
Entry Wire Line
	6450 1100 6550 1200
Entry Wire Line
	6450 1200 6550 1300
Entry Wire Line
	6450 2000 6550 2100
Entry Wire Line
	6450 2100 6550 2200
Entry Wire Line
	6450 2200 6550 2300
Entry Wire Line
	6450 2300 6550 2400
Entry Wire Line
	6450 3100 6550 3200
Entry Wire Line
	6450 3200 6550 3300
Entry Wire Line
	6450 3300 6550 3400
Entry Wire Line
	6450 3400 6550 3500
Entry Wire Line
	6450 4200 6550 4100
Entry Wire Line
	6450 4300 6550 4200
Entry Wire Line
	6450 4400 6550 4300
Entry Wire Line
	6450 4500 6550 4400
Entry Wire Line
	6450 5300 6550 5200
Entry Wire Line
	6450 5400 6550 5300
Entry Wire Line
	6450 5500 6550 5400
Entry Wire Line
	6450 5600 6550 5500
Entry Wire Line
	6450 6400 6550 6300
Entry Wire Line
	6450 6500 6550 6400
Entry Wire Line
	6450 6600 6550 6500
Entry Wire Line
	6450 6700 6550 6600
Wire Wire Line
	4450 900  5100 900 
Wire Wire Line
	4950 800  5050 800 
Wire Wire Line
	5050 1400 5050 800 
Wire Wire Line
	5050 1400 5050 2500
Wire Wire Line
	5050 2500 5050 3600
Wire Wire Line
	5050 3600 5050 4700
Wire Wire Line
	5050 4700 5050 5800
Wire Wire Line
	5050 5800 5050 6900
Wire Wire Line
	5050 6900 5100 6900
Wire Wire Line
	5100 1000 4650 1000
Wire Wire Line
	5100 1100 4650 1100
Wire Wire Line
	5100 1200 4650 1200
Wire Wire Line
	5100 1400 5050 1400
Wire Wire Line
	5100 1500 4950 1500
Wire Wire Line
	5100 2000 4650 2000
Wire Wire Line
	5100 2100 4650 2100
Wire Wire Line
	5100 2200 4650 2200
Wire Wire Line
	5100 2300 4650 2300
Wire Wire Line
	5100 2500 5050 2500
Wire Wire Line
	5100 2600 4950 2600
Wire Wire Line
	5100 3100 4650 3100
Wire Wire Line
	5100 3200 4650 3200
Wire Wire Line
	5100 3300 4650 3300
Wire Wire Line
	5100 3400 4650 3400
Wire Wire Line
	5100 3600 5050 3600
Wire Wire Line
	5100 3700 4950 3700
Wire Wire Line
	5100 4200 4650 4200
Wire Wire Line
	5100 4300 4650 4300
Wire Wire Line
	5100 4400 4650 4400
Wire Wire Line
	5100 4500 4650 4500
Wire Wire Line
	5100 4700 5050 4700
Wire Wire Line
	5100 4800 4950 4800
Wire Wire Line
	5100 5300 4650 5300
Wire Wire Line
	5100 5400 4650 5400
Wire Wire Line
	5100 5500 4650 5500
Wire Wire Line
	5100 5600 4650 5600
Wire Wire Line
	5100 5800 5050 5800
Wire Wire Line
	5100 5900 4950 5900
Wire Wire Line
	5100 6400 4650 6400
Wire Wire Line
	5100 6500 4650 6500
Wire Wire Line
	5100 6600 4650 6600
Wire Wire Line
	5100 6700 4650 6700
Wire Wire Line
	5100 7000 4950 7000
Wire Wire Line
	5900 1000 6450 1000
Wire Wire Line
	5900 1100 6450 1100
Wire Wire Line
	5900 1200 6450 1200
Wire Wire Line
	5900 2000 6450 2000
Wire Wire Line
	5900 2100 6450 2100
Wire Wire Line
	5900 2200 6450 2200
Wire Wire Line
	5900 2300 6450 2300
Wire Wire Line
	5900 3100 6450 3100
Wire Wire Line
	5900 3200 6450 3200
Wire Wire Line
	5900 3300 6450 3300
Wire Wire Line
	5900 3400 6450 3400
Wire Wire Line
	5900 4200 6450 4200
Wire Wire Line
	5900 4300 6450 4300
Wire Wire Line
	5900 4400 6450 4400
Wire Wire Line
	5900 4500 6450 4500
Wire Wire Line
	5900 5300 6450 5300
Wire Wire Line
	5900 5400 6450 5400
Wire Wire Line
	5900 5500 6450 5500
Wire Wire Line
	5900 5600 6450 5600
Wire Wire Line
	5900 6400 6450 6400
Wire Wire Line
	5900 6500 6450 6500
Wire Wire Line
	5900 6600 6450 6600
Wire Wire Line
	5900 6700 6450 6700
Wire Wire Line
	6550 900  5900 900 
Wire Bus Line
	4550 3850 4200 3850
Wire Bus Line
	6900 3850 6550 3850
Text Label 5000 1000 2    50   ~ 0
ADDR23
Text Label 5000 1100 2    50   ~ 0
ADDR22
Text Label 5000 1200 2    50   ~ 0
ADDR21
Text Label 5000 2000 2    50   ~ 0
ADDR20
Text Label 5000 2100 2    50   ~ 0
ADDR19
Text Label 5000 2200 2    50   ~ 0
ADDR18
Text Label 5000 2300 2    50   ~ 0
ADDR17
Text Label 5000 3100 2    50   ~ 0
ADDR16
Text Label 5000 3200 2    50   ~ 0
ADDR15
Text Label 5000 3300 2    50   ~ 0
ADDR14
Text Label 5000 3400 2    50   ~ 0
ADDR13
Text Label 5000 4200 2    50   ~ 0
ADDR12
Text Label 5000 4300 2    50   ~ 0
ADDR11
Text Label 5000 4400 2    50   ~ 0
ADDR10
Text Label 5000 4500 2    50   ~ 0
ADDR9
Text Label 5000 5300 2    50   ~ 0
ADDR8
Text Label 5000 5400 2    50   ~ 0
ADDR7
Text Label 5000 5500 2    50   ~ 0
ADDR6
Text Label 5000 5600 2    50   ~ 0
ADDR5
Text Label 5000 6400 2    50   ~ 0
ADDR4
Text Label 5000 6500 2    50   ~ 0
ADDR3
Text Label 5000 6600 2    50   ~ 0
ADDR2
Text Label 5000 6700 2    50   ~ 0
ADDR1
Text Label 5900 1000 0    50   ~ 0
ADDR-OUT23
Text Label 5900 1100 0    50   ~ 0
ADDR-OUT22
Text Label 5900 1200 0    50   ~ 0
ADDR-OUT21
Text Label 5900 2000 0    50   ~ 0
ADDR-OUT20
Text Label 5900 2100 0    50   ~ 0
ADDR-OUT19
Text Label 5900 2200 0    50   ~ 0
ADDR-OUT18
Text Label 5900 2300 0    50   ~ 0
ADDR-OUT17
Text Label 5900 3100 0    50   ~ 0
ADDR-OUT16
Text Label 5900 3200 0    50   ~ 0
ADDR-OUT15
Text Label 5900 3300 0    50   ~ 0
ADDR-OUT14
Text Label 5900 3400 0    50   ~ 0
ADDR-OUT13
Text Label 5900 4200 0    50   ~ 0
ADDR-OUT12
Text Label 5900 4300 0    50   ~ 0
ADDR-OUT11
Text Label 5900 4400 0    50   ~ 0
ADDR-OUT10
Text Label 5900 4500 0    50   ~ 0
ADDR-OUT9
Text Label 5900 5300 0    50   ~ 0
ADDR-OUT8
Text Label 5900 5400 0    50   ~ 0
ADDR-OUT7
Text Label 5900 5500 0    50   ~ 0
ADDR-OUT6
Text Label 5900 5600 0    50   ~ 0
ADDR-OUT5
Text Label 5900 6400 0    50   ~ 0
ADDR-OUT4
Text Label 5900 6500 0    50   ~ 0
ADDR-OUT3
Text Label 5900 6600 0    50   ~ 0
ADDR-OUT2
Text Label 5900 6700 0    50   ~ 0
ADDR-OUT1
Text HLabel 4200 3850 0    50   Input ~ 0
ADDR[1..23]
Text HLabel 4950 800  0    50   Input ~ 0
~CLK4M
Text HLabel 6550 900  2    50   Output ~ 0
~RD-OUT
Text HLabel 6900 3850 2    50   Output ~ 0
ADDR-OUT[1..23]
$Comp
L power:VCC #PWR0120
U 1 1 6578477D
P 4950 1500
F 0 "#PWR0120" H 4950 1350 50  0001 C CNN
F 1 "VCC" H 4965 1673 50  0000 C CNN
F 2 "" H 4950 1500 50  0001 C CNN
F 3 "" H 4950 1500 50  0001 C CNN
	1    4950 1500
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0121
U 1 1 66641813
P 4950 2600
F 0 "#PWR0121" H 4950 2450 50  0001 C CNN
F 1 "VCC" H 4965 2773 50  0000 C CNN
F 2 "" H 4950 2600 50  0001 C CNN
F 3 "" H 4950 2600 50  0001 C CNN
	1    4950 2600
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0122
U 1 1 667BC68F
P 4950 3700
F 0 "#PWR0122" H 4950 3550 50  0001 C CNN
F 1 "VCC" H 4965 3873 50  0000 C CNN
F 2 "" H 4950 3700 50  0001 C CNN
F 3 "" H 4950 3700 50  0001 C CNN
	1    4950 3700
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0123
U 1 1 66AB3375
P 4950 4800
F 0 "#PWR0123" H 4950 4650 50  0001 C CNN
F 1 "VCC" H 4965 4973 50  0000 C CNN
F 2 "" H 4950 4800 50  0001 C CNN
F 3 "" H 4950 4800 50  0001 C CNN
	1    4950 4800
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0124
U 1 1 66C2F5DD
P 4950 5900
F 0 "#PWR0124" H 4950 5750 50  0001 C CNN
F 1 "VCC" H 4965 6073 50  0000 C CNN
F 2 "" H 4950 5900 50  0001 C CNN
F 3 "" H 4950 5900 50  0001 C CNN
	1    4950 5900
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0125
U 1 1 66DAC55B
P 4950 7000
F 0 "#PWR0125" H 4950 6850 50  0001 C CNN
F 1 "VCC" H 4965 7173 50  0000 C CNN
F 2 "" H 4950 7000 50  0001 C CNN
F 3 "" H 4950 7000 50  0001 C CNN
	1    4950 7000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0147
U 1 1 64BD2027
P 4450 900
F 0 "#PWR0147" H 4450 650 50  0001 C CNN
F 1 "GND" H 4455 727 50  0000 C CNN
F 2 "" H 4450 900 50  0001 C CNN
F 3 "" H 4450 900 50  0001 C CNN
	1    4450 900 
	1    0    0    -1  
$EndComp
$Comp
L A5C:M175C UBG23
U 1 1 6477BA9C
P 5500 1200
F 0 "UBG23" H 5500 1765 50  0000 C CNN
F 1 "M175C" H 5500 1674 50  0000 C CNN
F 2 "" H 5500 1000 50  0001 C CNN
F 3 "" H 5500 1000 50  0001 C CNN
	1    5500 1200
	1    0    0    -1  
$EndComp
$Comp
L A5C:M175C UBJ35
U 1 1 641BF92C
P 5500 2300
F 0 "UBJ35" H 5500 2865 50  0000 C CNN
F 1 "M175C" H 5500 2774 50  0000 C CNN
F 2 "" H 5500 2100 50  0001 C CNN
F 3 "" H 5500 2100 50  0001 C CNN
	1    5500 2300
	1    0    0    -1  
$EndComp
$Comp
L A5C:M175C UCA143
U 1 1 63EE3522
P 5500 3400
F 0 "UCA143" H 5500 3965 50  0000 C CNN
F 1 "M175C" H 5500 3874 50  0000 C CNN
F 2 "" H 5500 3200 50  0001 C CNN
F 3 "" H 5500 3200 50  0001 C CNN
	1    5500 3400
	1    0    0    -1  
$EndComp
$Comp
L A5C:M175C UBI19
U 1 1 63AA27E5
P 5500 4500
F 0 "UBI19" H 5500 5065 50  0000 C CNN
F 1 "M175C" H 5500 4974 50  0000 C CNN
F 2 "" H 5500 4300 50  0001 C CNN
F 3 "" H 5500 4300 50  0001 C CNN
	1    5500 4500
	1    0    0    -1  
$EndComp
$Comp
L A5C:M175C UCB371
U 1 1 6323AFEE
P 5500 5600
F 0 "UCB371" H 5500 6165 50  0000 C CNN
F 1 "M175C" H 5500 6074 50  0000 C CNN
F 2 "" H 5500 5400 50  0001 C CNN
F 3 "" H 5500 5400 50  0001 C CNN
	1    5500 5600
	1    0    0    -1  
$EndComp
$Comp
L A5C:M175C UBE85
U 1 1 608E0BD1
P 5500 6700
F 0 "UBE85" H 5500 7265 50  0000 C CNN
F 1 "M175C" H 5500 7174 50  0000 C CNN
F 2 "" H 5500 6500 50  0001 C CNN
F 3 "" H 5500 6500 50  0001 C CNN
	1    5500 6700
	1    0    0    -1  
$EndComp
Wire Bus Line
	4550 1100 4550 3850
Wire Bus Line
	6550 1100 6550 3850
Wire Bus Line
	4550 3850 4550 6600
Wire Bus Line
	6550 3850 6550 6600
$EndSCHEMATC
