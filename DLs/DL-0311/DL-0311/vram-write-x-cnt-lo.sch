EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 37 83
Title "CPS Reverse Engineering: DL-0311"
Date "2021-09-12"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 "Licence: CC-BY"
Comment4 "Author: Loïc *WydD* Petit"
$EndDescr
Connection ~ 5900 4400
Connection ~ 5750 4300
Connection ~ 5650 4200
Connection ~ 4300 5200
Connection ~ 5450 4800
Connection ~ 5450 4500
Connection ~ 5550 4200
Connection ~ 4300 3600
Connection ~ 3550 4250
Connection ~ 3550 2650
Connection ~ 3550 3450
Connection ~ 5750 3000
Connection ~ 5900 3100
Connection ~ 5750 2600
Connection ~ 5750 3600
Connection ~ 5900 3700
Connection ~ 4300 4400
Connection ~ 5650 3500
Connection ~ 5550 3800
Connection ~ 5550 4100
Connection ~ 5650 3200
Connection ~ 5650 3400
Connection ~ 5300 900 
Connection ~ 5000 5900
Connection ~ 5100 5800
Connection ~ 5200 5700
NoConn ~ 6750 5000
Entry Wire Line
	5850 5600 5950 5500
Entry Wire Line
	5850 5700 5950 5600
Entry Wire Line
	5850 5800 5950 5700
Entry Wire Line
	5850 5900 5950 5800
Wire Wire Line
	3150 1800 3150 4950
Wire Wire Line
	3150 1800 7700 1800
Wire Wire Line
	3150 4950 3650 4950
Wire Wire Line
	3250 1900 3250 4150
Wire Wire Line
	3250 1900 7600 1900
Wire Wire Line
	3250 4150 3650 4150
Wire Wire Line
	3350 2000 3350 3350
Wire Wire Line
	3350 2000 7500 2000
Wire Wire Line
	3350 3350 3650 3350
Wire Wire Line
	3450 2100 7400 2100
Wire Wire Line
	3450 2550 3450 2100
Wire Wire Line
	3450 2550 3650 2550
Wire Wire Line
	3550 2650 3050 2650
Wire Wire Line
	3550 2650 3550 3450
Wire Wire Line
	3550 3450 3550 4250
Wire Wire Line
	3550 4250 3550 5050
Wire Wire Line
	3550 5050 3650 5050
Wire Wire Line
	3650 2650 3550 2650
Wire Wire Line
	3650 3450 3550 3450
Wire Wire Line
	3650 4250 3550 4250
Wire Wire Line
	4100 900  5300 900 
Wire Wire Line
	4200 2600 4400 2600
Wire Wire Line
	4200 3400 4400 3400
Wire Wire Line
	4200 4200 4400 4200
Wire Wire Line
	4200 5000 4400 5000
Wire Wire Line
	4300 3600 4300 2800
Wire Wire Line
	4300 4400 4300 3600
Wire Wire Line
	4300 4400 4400 4400
Wire Wire Line
	4300 5200 4300 4400
Wire Wire Line
	4300 5200 4300 5350
Wire Wire Line
	4300 5350 4150 5350
Wire Wire Line
	4400 2800 4300 2800
Wire Wire Line
	4400 3600 4300 3600
Wire Wire Line
	4400 5200 4300 5200
Wire Wire Line
	4750 5800 5100 5800
Wire Wire Line
	4750 5900 5000 5900
Wire Wire Line
	4900 2600 5750 2600
Wire Wire Line
	4900 2700 5300 2700
Wire Wire Line
	4900 3400 5650 3400
Wire Wire Line
	4900 3500 5200 3500
Wire Wire Line
	4900 4200 5550 4200
Wire Wire Line
	4900 5000 5450 5000
Wire Wire Line
	4900 5100 5000 5100
Wire Wire Line
	5000 5100 5000 5900
Wire Wire Line
	5000 5900 5850 5900
Wire Wire Line
	5100 4300 4900 4300
Wire Wire Line
	5100 5800 5100 4300
Wire Wire Line
	5200 3500 5200 5700
Wire Wire Line
	5200 5700 4750 5700
Wire Wire Line
	5200 5700 5850 5700
Wire Wire Line
	5300 900  5300 1300
Wire Wire Line
	5300 2700 5300 5600
Wire Wire Line
	5300 5600 5850 5600
Wire Wire Line
	5450 2350 5450 4500
Wire Wire Line
	5450 4500 5450 4800
Wire Wire Line
	5450 4500 6750 4500
Wire Wire Line
	5450 4800 5450 5000
Wire Wire Line
	5550 2350 5550 3800
Wire Wire Line
	5550 3800 5550 4100
Wire Wire Line
	5550 3800 6750 3800
Wire Wire Line
	5550 4100 5550 4200
Wire Wire Line
	5550 4200 5550 4900
Wire Wire Line
	5650 2350 5650 3200
Wire Wire Line
	5650 3200 5650 3400
Wire Wire Line
	5650 3200 6750 3200
Wire Wire Line
	5650 3400 5650 3500
Wire Wire Line
	5650 3500 5650 4200
Wire Wire Line
	5650 4200 5650 5000
Wire Wire Line
	5750 2350 5750 2600
Wire Wire Line
	5750 2600 5750 3000
Wire Wire Line
	5750 3000 5750 3600
Wire Wire Line
	5750 3600 5750 4300
Wire Wire Line
	5750 4300 5750 5100
Wire Wire Line
	5850 5800 5100 5800
Wire Wire Line
	5900 1300 5900 3100
Wire Wire Line
	5900 3100 5900 3700
Wire Wire Line
	5900 3100 6000 3100
Wire Wire Line
	5900 3700 5900 4400
Wire Wire Line
	5900 3700 6000 3700
Wire Wire Line
	5900 4400 5900 5200
Wire Wire Line
	5900 4400 6000 4400
Wire Wire Line
	5900 5200 6000 5200
Wire Wire Line
	6000 900  5300 900 
Wire Wire Line
	6000 900  6000 2500
Wire Wire Line
	6000 2500 6750 2500
Wire Wire Line
	6000 3000 5750 3000
Wire Wire Line
	6000 3500 5650 3500
Wire Wire Line
	6000 3600 5750 3600
Wire Wire Line
	6000 4100 5550 4100
Wire Wire Line
	6000 4200 5650 4200
Wire Wire Line
	6000 4300 5750 4300
Wire Wire Line
	6000 4800 5450 4800
Wire Wire Line
	6000 4900 5550 4900
Wire Wire Line
	6000 5000 5650 5000
Wire Wire Line
	6000 5100 5750 5100
Wire Wire Line
	6550 3050 6550 3100
Wire Wire Line
	6550 3100 6750 3100
Wire Wire Line
	6600 3700 6600 3600
Wire Wire Line
	6600 4400 6600 4250
Wire Wire Line
	6750 2600 5750 2600
Wire Wire Line
	6750 3700 6600 3700
Wire Wire Line
	6750 4400 6600 4400
Wire Wire Line
	7300 3150 7500 3150
Wire Wire Line
	7300 3750 7600 3750
Wire Wire Line
	7300 4450 7700 4450
Wire Wire Line
	7400 2100 7400 2550
Wire Wire Line
	7400 2550 7300 2550
Wire Wire Line
	7500 3150 7500 2000
Wire Wire Line
	7600 3750 7600 1900
Wire Wire Line
	7700 1800 7700 4450
Wire Bus Line
	5950 5400 6100 5400
Text Notes 2250 3400 0    50   ~ 0
RESET:\nCLK4M & LI + RESET\n\nINC:\nCOL4-SYNC
Text Label 5850 5600 2    50   ~ 0
WX-C0
Text Label 5850 5700 2    50   ~ 0
WX-C1
Text Label 5850 5800 2    50   ~ 0
WX-C2
Text Label 5850 5900 2    50   ~ 0
WX-C3
Text HLabel 2500 2600 0    50   Input ~ 0
~ZERO
Text HLabel 2500 2700 0    50   Input ~ 0
~RESET
Text HLabel 4100 900  0    50   Input ~ 0
INC
Text HLabel 4150 5350 0    50   Input ~ 0
~CLK8M
Text HLabel 4200 5800 0    50   Output ~ 0
C=14-15
Text HLabel 6100 5400 2    50   Output ~ 0
WX-C[0..3]
$Comp
L A5C:AND02 UDE142
U 1 1 61118E96
P 2750 2650
F 0 "UDE142" H 2775 2917 50  0000 C CNN
F 1 "AND02" H 2775 2826 50  0000 C CNN
F 2 "" H 2750 2650 50  0001 C CNN
F 3 "" H 2750 2650 50  0001 C CNN
	1    2750 2650
	1    0    0    -1  
$EndComp
$Comp
L A5C:NAND02 UDD?
U 1 1 61118E98
P 3900 2600
AR Path="/61118E98" Ref="UDD?"  Part="1" 
AR Path="/613100A2/61118E98" Ref="UDD45"  Part="1" 
AR Path="/613100A2/612BF9D1/61118E98" Ref="UDD45"  Part="1" 
F 0 "UDD45" H 3925 2867 50  0000 C CNN
F 1 "NAND02" H 3925 2776 50  0000 C CNN
F 2 "" H 3900 2600 50  0001 C CNN
F 3 "" H 3900 2600 50  0001 C CNN
	1    3900 2600
	1    0    0    -1  
$EndComp
$Comp
L A5C:NAND02 UDH?
U 1 1 61118E94
P 3900 3400
AR Path="/611F0159/61118E94" Ref="UDH?"  Part="1" 
AR Path="/61118E94" Ref="UDH?"  Part="1" 
AR Path="/613100A2/61118E94" Ref="UDD51"  Part="1" 
AR Path="/613100A2/612BF9D1/61118E94" Ref="UDD51"  Part="1" 
F 0 "UDD51" H 3925 3667 50  0000 C CNN
F 1 "NAND02" H 3925 3576 50  0000 C CNN
F 2 "" H 3900 3400 50  0001 C CNN
F 3 "" H 3900 3400 50  0001 C CNN
	1    3900 3400
	1    0    0    -1  
$EndComp
$Comp
L A5C:NAND02 UDE?
U 1 1 61118E93
P 3900 4200
AR Path="/611F0159/61118E93" Ref="UDE?"  Part="1" 
AR Path="/61118E93" Ref="UDE?"  Part="1" 
AR Path="/613100A2/61118E93" Ref="UDB46"  Part="1" 
AR Path="/613100A2/612BF9D1/61118E93" Ref="UDB46"  Part="1" 
F 0 "UDB46" H 3925 4467 50  0000 C CNN
F 1 "NAND02" H 3925 4376 50  0000 C CNN
F 2 "" H 3900 4200 50  0001 C CNN
F 3 "" H 3900 4200 50  0001 C CNN
	1    3900 4200
	1    0    0    -1  
$EndComp
$Comp
L A5C:NAND02 UDD?
U 1 1 61118E95
P 3900 5000
AR Path="/611F0159/61118E95" Ref="UDD?"  Part="1" 
AR Path="/61118E95" Ref="UDD?"  Part="1" 
AR Path="/613100A2/61118E95" Ref="UDC55"  Part="1" 
AR Path="/613100A2/612BF9D1/61118E95" Ref="UDC55"  Part="1" 
F 0 "UDC55" H 3925 5267 50  0000 C CNN
F 1 "NAND02" H 3925 5176 50  0000 C CNN
F 2 "" H 3900 5000 50  0001 C CNN
F 3 "" H 3900 5000 50  0001 C CNN
	1    3900 5000
	1    0    0    -1  
$EndComp
$Comp
L A5C:NOR02 UDE?
U 1 1 61118EC9
P 6250 3050
AR Path="/611F0159/61118EC9" Ref="UDE?"  Part="1" 
AR Path="/61118EC9" Ref="UDE?"  Part="1" 
AR Path="/613100A2/61118EC9" Ref="UDE49"  Part="1" 
AR Path="/613100A2/612BF9D1/61118EC9" Ref="UDE49"  Part="1" 
F 0 "UDE49" H 6275 3317 50  0000 C CNN
F 1 "NOR02" H 6275 3226 50  0000 C CNN
F 2 "" H 6250 3050 50  0001 C CNN
F 3 "" H 6250 3050 50  0001 C CNN
	1    6250 3050
	1    0    0    -1  
$EndComp
$Comp
L A5C:XNOR02 UDD?
U 1 1 61118ED2
P 7000 2550
AR Path="/61118ED2" Ref="UDD?"  Part="1" 
AR Path="/613100A2/61118ED2" Ref="UDD39"  Part="1" 
AR Path="/613100A2/612BF9D1/61118ED2" Ref="UDD39"  Part="1" 
F 0 "UDD39" H 7025 2817 50  0000 C CNN
F 1 "XNOR02" H 7025 2726 50  0000 C CNN
F 2 "" H 7025 2550 50  0001 C CNN
F 3 "" H 7025 2550 50  0001 C CNN
	1    7000 2550
	1    0    0    -1  
$EndComp
$Comp
L A5C:XNOR02 UDL?
U 1 1 61118ED1
P 7000 3150
AR Path="/611F0159/61118ED1" Ref="UDL?"  Part="1" 
AR Path="/61118ED1" Ref="UDL?"  Part="1" 
AR Path="/613100A2/61118ED1" Ref="UDE51"  Part="1" 
AR Path="/613100A2/612BF9D1/61118ED1" Ref="UDE51"  Part="1" 
F 0 "UDE51" H 7025 3417 50  0000 C CNN
F 1 "XNOR02" H 7025 3326 50  0000 C CNN
F 2 "" H 7025 3150 50  0001 C CNN
F 3 "" H 7025 3150 50  0001 C CNN
	1    7000 3150
	1    0    0    -1  
$EndComp
$Comp
L A5C:XNOR02 UDE?
U 1 1 61118ECE
P 7000 3750
AR Path="/611F0159/61118ECE" Ref="UDE?"  Part="1" 
AR Path="/61118ECE" Ref="UDE?"  Part="1" 
AR Path="/613100A2/61118ECE" Ref="UDB48"  Part="1" 
AR Path="/613100A2/612BF9D1/61118ECE" Ref="UDB48"  Part="1" 
F 0 "UDB48" H 7025 4017 50  0000 C CNN
F 1 "XNOR02" H 7025 3926 50  0000 C CNN
F 2 "" H 7025 3750 50  0001 C CNN
F 3 "" H 7025 3750 50  0001 C CNN
	1    7000 3750
	1    0    0    -1  
$EndComp
$Comp
L A5C:XNOR02 UDD?
U 1 1 61118ECF
P 7000 4450
AR Path="/611F0159/61118ECF" Ref="UDD?"  Part="1" 
AR Path="/61118ECF" Ref="UDD?"  Part="1" 
AR Path="/613100A2/61118ECF" Ref="UDC61"  Part="1" 
AR Path="/613100A2/612BF9D1/61118ECF" Ref="UDC61"  Part="1" 
F 0 "UDC61" H 7025 4717 50  0000 C CNN
F 1 "XNOR02" H 7025 4626 50  0000 C CNN
F 2 "" H 7025 4450 50  0001 C CNN
F 3 "" H 7025 4450 50  0001 C CNN
	1    7000 4450
	1    0    0    -1  
$EndComp
$Comp
L A5C:AND03 UDC52
U 1 1 61118EC1
P 4500 5800
F 0 "UDC52" H 4525 6092 50  0000 C CNN
F 1 "AND03" H 4525 6001 50  0000 C CNN
F 2 "" H 4500 5800 50  0001 C CNN
F 3 "" H 4500 5800 50  0001 C CNN
	1    4500 5800
	-1   0    0    -1  
$EndComp
$Comp
L A5C:INV01 UDD?
U 1 1 61118EC7
P 5600 1300
AR Path="/611F0159/61118EC7" Ref="UDD?"  Part="1" 
AR Path="/61118EC7" Ref="UDD?"  Part="1" 
AR Path="/613100A2/61118EC7" Ref="UDF52"  Part="1" 
AR Path="/613100A2/612BF9D1/61118EC7" Ref="UDF52"  Part="1" 
F 0 "UDF52" H 5600 1617 50  0000 C CNN
F 1 "INV01" H 5600 1526 50  0000 C CNN
F 2 "" H 5600 1100 50  0001 C CNN
F 3 "" H 5600 1100 50  0001 C CNN
	1    5600 1300
	1    0    0    -1  
$EndComp
$Comp
L A5C:NOR03 UDE?
U 1 1 61118ED0
P 6300 3600
AR Path="/611F0159/61118ED0" Ref="UDE?"  Part="1" 
AR Path="/61118ED0" Ref="UDE?"  Part="1" 
AR Path="/613100A2/61118ED0" Ref="UDD53"  Part="1" 
AR Path="/613100A2/612BF9D1/61118ED0" Ref="UDD53"  Part="1" 
F 0 "UDD53" H 6300 3917 50  0000 C CNN
F 1 "NOR03" H 6300 3826 50  0000 C CNN
F 2 "" H 6300 3600 50  0001 C CNN
F 3 "" H 6300 3600 50  0001 C CNN
	1    6300 3600
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFFCOO UDD?
U 1 1 61118EC8
P 4650 2700
AR Path="/61118EC8" Ref="UDD?"  Part="1" 
AR Path="/613100A2/61118EC8" Ref="UDD41"  Part="1" 
AR Path="/613100A2/612BF9D1/61118EC8" Ref="UDD41"  Part="1" 
F 0 "UDD41" H 4650 3065 50  0000 C CNN
F 1 "DFFCOO" H 4650 2974 50  0000 C CNN
F 2 "" H 4650 2700 50  0001 C CNN
F 3 "" H 4650 2700 50  0001 C CNN
	1    4650 2700
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFFCOO UDI?
U 1 1 61118ECA
P 4650 3500
AR Path="/611F0159/61118ECA" Ref="UDI?"  Part="1" 
AR Path="/61118ECA" Ref="UDI?"  Part="1" 
AR Path="/613100A2/61118ECA" Ref="UDD47"  Part="1" 
AR Path="/613100A2/612BF9D1/61118ECA" Ref="UDD47"  Part="1" 
F 0 "UDD47" H 4650 3865 50  0000 C CNN
F 1 "DFFCOO" H 4650 3774 50  0000 C CNN
F 2 "" H 4650 3500 50  0001 C CNN
F 3 "" H 4650 3500 50  0001 C CNN
	1    4650 3500
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFFCOO UDE?
U 1 1 61118ECB
P 4650 4300
AR Path="/61118ECB" Ref="UDE?"  Part="1" 
AR Path="/613100A2/61118ECB" Ref="UDB42"  Part="1" 
AR Path="/613100A2/612BF9D1/61118ECB" Ref="UDB42"  Part="1" 
F 0 "UDB42" H 4650 4665 50  0000 C CNN
F 1 "DFFCOO" H 4650 4574 50  0000 C CNN
F 2 "" H 4650 4300 50  0001 C CNN
F 3 "" H 4650 4300 50  0001 C CNN
	1    4650 4300
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFFCOO UDD?
U 1 1 61118EC3
P 4650 5100
AR Path="/611F0159/61118EC3" Ref="UDD?"  Part="1" 
AR Path="/61118EC3" Ref="UDD?"  Part="1" 
AR Path="/613100A2/61118EC3" Ref="UDC56"  Part="1" 
AR Path="/613100A2/612BF9D1/61118EC3" Ref="UDC56"  Part="1" 
F 0 "UDC56" H 4650 5465 50  0000 C CNN
F 1 "DFFCOO" H 4650 5374 50  0000 C CNN
F 2 "" H 4650 5100 50  0001 C CNN
F 3 "" H 4650 5100 50  0001 C CNN
	1    4650 5100
	1    0    0    -1  
$EndComp
$Comp
L A5C:NOR04 UDD?
U 1 1 61118ECC
P 6300 4250
AR Path="/611F0159/61118ECC" Ref="UDD?"  Part="1" 
AR Path="/61118ECC" Ref="UDD?"  Part="1" 
AR Path="/613100A2/61118ECC" Ref="UDD59"  Part="1" 
AR Path="/613100A2/612BF9D1/61118ECC" Ref="UDD59"  Part="1" 
F 0 "UDD59" H 6300 4592 50  0000 C CNN
F 1 "NOR04" H 6300 4501 50  0000 C CNN
F 2 "" H 6300 4250 50  0001 C CNN
F 3 "" H 6300 4250 50  0001 C CNN
	1    6300 4250
	1    0    0    -1  
$EndComp
$Comp
L A5C:NOR05 UDD?
U 1 1 61118ECD
P 6300 5000
AR Path="/611F0159/61118ECD" Ref="UDD?"  Part="1" 
AR Path="/61118ECD" Ref="UDD?"  Part="1" 
AR Path="/613100A2/61118ECD" Ref="UDD56"  Part="1" 
AR Path="/613100A2/612BF9D1/61118ECD" Ref="UDD56"  Part="1" 
F 0 "UDD56" H 6375 5415 50  0000 C CNN
F 1 "NOR05" H 6375 5324 50  0000 C CNN
F 2 "" H 6300 5050 50  0001 C CNN
F 3 "" H 6300 5050 50  0001 C CNN
	1    6300 5000
	1    0    0    -1  
$EndComp
Wire Bus Line
	5950 5400 5950 5800
$EndSCHEMATC
