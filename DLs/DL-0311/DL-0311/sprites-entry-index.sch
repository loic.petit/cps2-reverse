EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 8268 11693 portrait
encoding utf-8
Sheet 59 83
Title "CPS Reverse Engineering: DL-0311"
Date "2021-09-12"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 "Licence: CC-BY"
Comment4 "Author: Loïc *WydD* Petit"
$EndDescr
Connection ~ 2900 3950
Connection ~ 2900 3150
Connection ~ 2900 2350
Connection ~ 4900 3800
Connection ~ 4900 3900
Connection ~ 3650 4100
Connection ~ 4800 4200
Connection ~ 5250 3400
Connection ~ 5000 3200
Connection ~ 5100 3300
Connection ~ 3650 4900
Connection ~ 4800 4500
Connection ~ 5000 2900
Connection ~ 5000 3100
Connection ~ 4900 3500
Connection ~ 5000 3900
Connection ~ 3650 3300
Connection ~ 4650 900 
Connection ~ 5100 2700
Connection ~ 5250 2800
Connection ~ 5100 4000
Connection ~ 5250 4100
Connection ~ 5100 2300
Connection ~ 3650 8150
Connection ~ 5000 7750
Connection ~ 4900 8350
Connection ~ 5000 8050
Connection ~ 5000 7950
Connection ~ 5100 8150
Connection ~ 5250 8250
Connection ~ 5250 7650
Connection ~ 5100 7550
Connection ~ 5100 7150
Connection ~ 2900 8000
Connection ~ 2900 7200
Connection ~ 2900 8800
Connection ~ 3650 8950
Connection ~ 4800 9350
Connection ~ 5000 8750
Connection ~ 5100 8850
Connection ~ 4900 8750
Connection ~ 4800 9050
Connection ~ 4900 8650
Connection ~ 5250 8950
Connection ~ 6100 6150
Connection ~ 1750 2400
Connection ~ 3650 7350
Connection ~ 3650 5500
NoConn ~ 6100 9550
Entry Wire Line
	5200 5150 5300 5050
Entry Wire Line
	5200 5250 5300 5150
Entry Wire Line
	5200 5350 5300 5250
Entry Wire Line
	5200 5450 5300 5350
Entry Wire Line
	5200 5550 5300 5450
Entry Wire Line
	5200 5650 5300 5550
Entry Wire Line
	5200 5750 5300 5650
Entry Wire Line
	5200 5850 5300 5750
Wire Wire Line
	1750 2400 1550 2400
Wire Wire Line
	1750 2400 1750 7250
Wire Wire Line
	1850 2400 1750 2400
Wire Wire Line
	1850 7250 1750 7250
Wire Wire Line
	2500 1500 2500 4650
Wire Wire Line
	2500 1500 7050 1500
Wire Wire Line
	2500 4650 3000 4650
Wire Wire Line
	2500 6350 2500 9500
Wire Wire Line
	2500 6350 7050 6350
Wire Wire Line
	2500 9500 3000 9500
Wire Wire Line
	2600 1600 2600 3850
Wire Wire Line
	2600 1600 6950 1600
Wire Wire Line
	2600 3850 3000 3850
Wire Wire Line
	2600 6450 2600 8700
Wire Wire Line
	2600 6450 6950 6450
Wire Wire Line
	2600 8700 3000 8700
Wire Wire Line
	2700 1700 2700 3050
Wire Wire Line
	2700 1700 6850 1700
Wire Wire Line
	2700 3050 3000 3050
Wire Wire Line
	2700 6550 2700 7900
Wire Wire Line
	2700 6550 6850 6550
Wire Wire Line
	2700 7900 3000 7900
Wire Wire Line
	2800 1800 6750 1800
Wire Wire Line
	2800 2250 2800 1800
Wire Wire Line
	2800 2250 3000 2250
Wire Wire Line
	2800 6650 6750 6650
Wire Wire Line
	2800 7100 2800 6650
Wire Wire Line
	2800 7100 3000 7100
Wire Wire Line
	2900 2350 2400 2350
Wire Wire Line
	2900 2350 2900 3150
Wire Wire Line
	2900 3150 2900 3950
Wire Wire Line
	2900 3950 2900 4750
Wire Wire Line
	2900 4750 3000 4750
Wire Wire Line
	2900 7200 2400 7200
Wire Wire Line
	2900 7200 2900 8000
Wire Wire Line
	2900 8000 2900 8800
Wire Wire Line
	2900 8800 2900 9600
Wire Wire Line
	2900 9600 3000 9600
Wire Wire Line
	3000 2350 2900 2350
Wire Wire Line
	3000 3150 2900 3150
Wire Wire Line
	3000 3950 2900 3950
Wire Wire Line
	3000 7200 2900 7200
Wire Wire Line
	3000 8000 2900 8000
Wire Wire Line
	3000 8800 2900 8800
Wire Wire Line
	3450 900  4100 900 
Wire Wire Line
	3550 2300 3750 2300
Wire Wire Line
	3550 3100 3750 3100
Wire Wire Line
	3550 3900 3750 3900
Wire Wire Line
	3550 4700 3750 4700
Wire Wire Line
	3550 7150 3750 7150
Wire Wire Line
	3550 7950 3750 7950
Wire Wire Line
	3550 8750 3750 8750
Wire Wire Line
	3550 9550 3750 9550
Wire Wire Line
	3650 3300 3650 2500
Wire Wire Line
	3650 4100 3650 3300
Wire Wire Line
	3650 4100 3750 4100
Wire Wire Line
	3650 4900 3650 4100
Wire Wire Line
	3650 4900 3650 5500
Wire Wire Line
	3650 5500 3300 5500
Wire Wire Line
	3650 5500 3650 7350
Wire Wire Line
	3650 8150 3650 7350
Wire Wire Line
	3650 8950 3650 8150
Wire Wire Line
	3650 8950 3750 8950
Wire Wire Line
	3650 9750 3650 8950
Wire Wire Line
	3750 2500 3650 2500
Wire Wire Line
	3750 3300 3650 3300
Wire Wire Line
	3750 4900 3650 4900
Wire Wire Line
	3750 7350 3650 7350
Wire Wire Line
	3750 8150 3650 8150
Wire Wire Line
	3750 9750 3650 9750
Wire Wire Line
	4250 2300 5100 2300
Wire Wire Line
	4250 2400 4650 2400
Wire Wire Line
	4250 3100 5000 3100
Wire Wire Line
	4250 3200 4550 3200
Wire Wire Line
	4250 3900 4900 3900
Wire Wire Line
	4250 4700 4800 4700
Wire Wire Line
	4250 7150 5100 7150
Wire Wire Line
	4250 7250 4350 7250
Wire Wire Line
	4250 7950 5000 7950
Wire Wire Line
	4250 8050 4450 8050
Wire Wire Line
	4250 8750 4900 8750
Wire Wire Line
	4250 8850 4550 8850
Wire Wire Line
	4250 9550 4800 9550
Wire Wire Line
	4350 4800 4250 4800
Wire Wire Line
	4350 4800 4350 5450
Wire Wire Line
	4350 5450 5200 5450
Wire Wire Line
	4350 5550 4350 7250
Wire Wire Line
	4350 5550 5200 5550
Wire Wire Line
	4450 4000 4250 4000
Wire Wire Line
	4450 4000 4450 5350
Wire Wire Line
	4450 5350 5200 5350
Wire Wire Line
	4450 5650 4450 8050
Wire Wire Line
	4450 5650 5200 5650
Wire Wire Line
	4550 3200 4550 5250
Wire Wire Line
	4550 5250 5200 5250
Wire Wire Line
	4550 5750 4550 8850
Wire Wire Line
	4550 5750 5200 5750
Wire Wire Line
	4650 900  5350 900 
Wire Wire Line
	4650 1300 4650 900 
Wire Wire Line
	4650 2400 4650 5150
Wire Wire Line
	4650 5150 5200 5150
Wire Wire Line
	4650 5850 4650 9650
Wire Wire Line
	4650 5850 5200 5850
Wire Wire Line
	4650 9650 4250 9650
Wire Wire Line
	4800 2050 4800 4200
Wire Wire Line
	4800 4200 4800 4500
Wire Wire Line
	4800 4200 6100 4200
Wire Wire Line
	4800 4500 4800 4700
Wire Wire Line
	4800 6900 4800 9050
Wire Wire Line
	4800 9050 4800 9350
Wire Wire Line
	4800 9050 6100 9050
Wire Wire Line
	4800 9350 4800 9550
Wire Wire Line
	4900 2050 4900 3500
Wire Wire Line
	4900 3500 4900 3800
Wire Wire Line
	4900 3500 6100 3500
Wire Wire Line
	4900 3800 4900 3900
Wire Wire Line
	4900 3900 4900 4600
Wire Wire Line
	4900 6900 4900 8350
Wire Wire Line
	4900 8350 4900 8650
Wire Wire Line
	4900 8350 6100 8350
Wire Wire Line
	4900 8650 4900 8750
Wire Wire Line
	4900 8750 4900 9450
Wire Wire Line
	5000 2050 5000 2900
Wire Wire Line
	5000 2900 5000 3100
Wire Wire Line
	5000 2900 6100 2900
Wire Wire Line
	5000 3100 5000 3200
Wire Wire Line
	5000 3200 5000 3900
Wire Wire Line
	5000 3900 5000 4700
Wire Wire Line
	5000 6900 5000 7750
Wire Wire Line
	5000 7750 5000 7950
Wire Wire Line
	5000 7750 6100 7750
Wire Wire Line
	5000 7950 5000 8050
Wire Wire Line
	5000 8050 5000 8750
Wire Wire Line
	5000 8750 5000 9550
Wire Wire Line
	5100 2050 5100 2300
Wire Wire Line
	5100 2300 5100 2700
Wire Wire Line
	5100 2700 5100 3300
Wire Wire Line
	5100 3300 5100 4000
Wire Wire Line
	5100 4000 5100 4800
Wire Wire Line
	5100 6900 5100 7150
Wire Wire Line
	5100 7150 5100 7550
Wire Wire Line
	5100 7550 5100 8150
Wire Wire Line
	5100 8150 5100 8850
Wire Wire Line
	5100 8850 5100 9650
Wire Wire Line
	5250 1300 5250 2800
Wire Wire Line
	5250 2800 5250 3400
Wire Wire Line
	5250 2800 5350 2800
Wire Wire Line
	5250 3400 5250 4100
Wire Wire Line
	5250 3400 5350 3400
Wire Wire Line
	5250 4100 5250 4900
Wire Wire Line
	5250 4100 5350 4100
Wire Wire Line
	5250 4900 5350 4900
Wire Wire Line
	5250 6150 5250 7650
Wire Wire Line
	5250 6150 5500 6150
Wire Wire Line
	5250 7650 5250 8250
Wire Wire Line
	5250 7650 5350 7650
Wire Wire Line
	5250 8250 5250 8950
Wire Wire Line
	5250 8250 5350 8250
Wire Wire Line
	5250 8950 5250 9750
Wire Wire Line
	5250 8950 5350 8950
Wire Wire Line
	5250 9750 5350 9750
Wire Wire Line
	5350 900  5350 2200
Wire Wire Line
	5350 2200 6100 2200
Wire Wire Line
	5350 2700 5100 2700
Wire Wire Line
	5350 3200 5000 3200
Wire Wire Line
	5350 3300 5100 3300
Wire Wire Line
	5350 3800 4900 3800
Wire Wire Line
	5350 3900 5000 3900
Wire Wire Line
	5350 4000 5100 4000
Wire Wire Line
	5350 4500 4800 4500
Wire Wire Line
	5350 4600 4900 4600
Wire Wire Line
	5350 4700 5000 4700
Wire Wire Line
	5350 4800 5100 4800
Wire Wire Line
	5350 7550 5100 7550
Wire Wire Line
	5350 8050 5000 8050
Wire Wire Line
	5350 8150 5100 8150
Wire Wire Line
	5350 8650 4900 8650
Wire Wire Line
	5350 8750 5000 8750
Wire Wire Line
	5350 8850 5100 8850
Wire Wire Line
	5350 9350 4800 9350
Wire Wire Line
	5350 9450 4900 9450
Wire Wire Line
	5350 9550 5000 9550
Wire Wire Line
	5350 9650 5100 9650
Wire Wire Line
	5900 2750 5900 2800
Wire Wire Line
	5900 2800 6100 2800
Wire Wire Line
	5900 7600 5900 7650
Wire Wire Line
	5900 7650 6100 7650
Wire Wire Line
	5950 3400 5950 3300
Wire Wire Line
	5950 4100 5950 3950
Wire Wire Line
	5950 8250 5950 8150
Wire Wire Line
	5950 8950 5950 8800
Wire Wire Line
	6100 2300 5100 2300
Wire Wire Line
	6100 3400 5950 3400
Wire Wire Line
	6100 4100 5950 4100
Wire Wire Line
	6100 4700 6100 6150
Wire Wire Line
	6100 6150 6100 7050
Wire Wire Line
	6100 7150 5100 7150
Wire Wire Line
	6100 8250 5950 8250
Wire Wire Line
	6100 8950 5950 8950
Wire Wire Line
	6650 2850 6850 2850
Wire Wire Line
	6650 3450 6950 3450
Wire Wire Line
	6650 4150 7050 4150
Wire Wire Line
	6650 7700 6850 7700
Wire Wire Line
	6650 8300 6950 8300
Wire Wire Line
	6650 9000 7050 9000
Wire Wire Line
	6750 1800 6750 2250
Wire Wire Line
	6750 2250 6650 2250
Wire Wire Line
	6750 6650 6750 7100
Wire Wire Line
	6750 7100 6650 7100
Wire Wire Line
	6850 2850 6850 1700
Wire Wire Line
	6850 7700 6850 6550
Wire Wire Line
	6950 3450 6950 1600
Wire Wire Line
	6950 8300 6950 6450
Wire Wire Line
	7050 1500 7050 4150
Wire Wire Line
	7050 6350 7050 9000
Wire Bus Line
	5300 5050 5400 5050
Text Notes 800  3300 0    50   ~ 0
RESET WHEN SCAN-243
Text Notes 5600 1300 0    50   ~ 0
INC when\n- BLOCK-READY\n- LI-SYNC\n- Not in 240-245
Text Label 4700 5150 0    50   ~ 0
ENTRY-IDX0
Text Label 4700 5250 0    50   ~ 0
ENTRY-IDX1
Text Label 4700 5350 0    50   ~ 0
ENTRY-IDX2
Text Label 4700 5450 0    50   ~ 0
ENTRY-IDX3
Text Label 4700 5550 0    50   ~ 0
ENTRY-IDX4
Text Label 4700 5650 0    50   ~ 0
ENTRY-IDX5
Text Label 4700 5750 0    50   ~ 0
ENTRY-IDX6
Text Label 4700 5850 0    50   ~ 0
ENTRY-IDX7
Text HLabel 1550 2400 0    50   Input ~ 0
~RESET
Text HLabel 2850 900  0    50   Input ~ 0
SCAN-240-245
Text HLabel 3300 5500 0    50   Input ~ 0
~CLK4M
Text HLabel 4100 800  0    50   Input ~ 0
BLOCK-READY
Text HLabel 4100 1000 0    50   Input ~ 0
LI-SYNCED
Text HLabel 5400 5050 2    50   Output ~ 0
ENTRY-IDX[0..7]
$Comp
L power:VCC #PWR0115
U 1 1 744410B9
P 1850 2300
F 0 "#PWR0115" H 1850 2150 50  0001 C CNN
F 1 "VCC" H 1865 2473 50  0000 C CNN
F 2 "" H 1850 2300 50  0001 C CNN
F 3 "" H 1850 2300 50  0001 C CNN
	1    1850 2300
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0116
U 1 1 74AA3960
P 1850 7150
F 0 "#PWR0116" H 1850 7000 50  0001 C CNN
F 1 "VCC" H 1865 7323 50  0000 C CNN
F 2 "" H 1850 7150 50  0001 C CNN
F 3 "" H 1850 7150 50  0001 C CNN
	1    1850 7150
	1    0    0    -1  
$EndComp
$Comp
L A5C:AND02 UBA?
U 1 1 6B9084E3
P 2100 2350
AR Path="/611F0159/6B9084E3" Ref="UBA?"  Part="1" 
AR Path="/6B9084E3" Ref="UBA46"  Part="1" 
AR Path="/611E8E91/6B9084E3" Ref="UBA46"  Part="1" 
AR Path="/612CA5FA/612CADBB/6B9084E3" Ref="UBA46"  Part="1" 
F 0 "UBA46" H 2125 2617 50  0000 C CNN
F 1 "AND02" H 2125 2526 50  0000 C CNN
F 2 "" H 2100 2350 50  0001 C CNN
F 3 "" H 2100 2350 50  0001 C CNN
	1    2100 2350
	1    0    0    -1  
$EndComp
$Comp
L A5C:AND02 UBA?
U 1 1 74AA38D2
P 2100 7200
AR Path="/611F0159/74AA38D2" Ref="UBA?"  Part="1" 
AR Path="/74AA38D2" Ref="UBC46"  Part="1" 
AR Path="/611E8E91/74AA38D2" Ref="UBC46"  Part="1" 
AR Path="/612CA5FA/612CADBB/74AA38D2" Ref="UBC46"  Part="1" 
F 0 "UBC46" H 2125 7467 50  0000 C CNN
F 1 "AND02" H 2125 7376 50  0000 C CNN
F 2 "" H 2100 7200 50  0001 C CNN
F 3 "" H 2100 7200 50  0001 C CNN
	1    2100 7200
	1    0    0    -1  
$EndComp
$Comp
L A5C:NAND02 UBD?
U 1 1 6B9084D1
P 3250 2300
AR Path="/611F0159/6B9084D1" Ref="UBD?"  Part="1" 
AR Path="/6B9084D1" Ref="UBA138"  Part="1" 
AR Path="/611E8E91/6B9084D1" Ref="UBA138"  Part="1" 
AR Path="/612CA5FA/612CADBB/6B9084D1" Ref="UBA138"  Part="1" 
F 0 "UBA138" H 3275 2567 50  0000 C CNN
F 1 "NAND02" H 3275 2476 50  0000 C CNN
F 2 "" H 3250 2300 50  0001 C CNN
F 3 "" H 3250 2300 50  0001 C CNN
	1    3250 2300
	1    0    0    -1  
$EndComp
$Comp
L A5C:NAND02 UBA?
U 1 1 6B9084DD
P 3250 3100
AR Path="/611F0159/6B9084DD" Ref="UBA?"  Part="1" 
AR Path="/6B9084DD" Ref="UBA109"  Part="1" 
AR Path="/611E8E91/6B9084DD" Ref="UBA109"  Part="1" 
AR Path="/612CA5FA/612CADBB/6B9084DD" Ref="UBA109"  Part="1" 
F 0 "UBA109" H 3275 3367 50  0000 C CNN
F 1 "NAND02" H 3275 3276 50  0000 C CNN
F 2 "" H 3250 3100 50  0001 C CNN
F 3 "" H 3250 3100 50  0001 C CNN
	1    3250 3100
	1    0    0    -1  
$EndComp
$Comp
L A5C:NAND02 UBA?
U 1 1 6B9084D7
P 3250 3900
AR Path="/611F0159/6B9084D7" Ref="UBA?"  Part="1" 
AR Path="/6B9084D7" Ref="UBA128"  Part="1" 
AR Path="/611E8E91/6B9084D7" Ref="UBA128"  Part="1" 
AR Path="/612CA5FA/612CADBB/6B9084D7" Ref="UBA128"  Part="1" 
F 0 "UBA128" H 3275 4167 50  0000 C CNN
F 1 "NAND02" H 3275 4076 50  0000 C CNN
F 2 "" H 3250 3900 50  0001 C CNN
F 3 "" H 3250 3900 50  0001 C CNN
	1    3250 3900
	1    0    0    -1  
$EndComp
$Comp
L A5C:NAND02 UBA82
U 1 1 6B908489
P 3250 4700
F 0 "UBA82" H 3275 4967 50  0000 C CNN
F 1 "NAND02" H 3275 4876 50  0000 C CNN
F 2 "" H 3250 4700 50  0001 C CNN
F 3 "" H 3250 4700 50  0001 C CNN
	1    3250 4700
	1    0    0    -1  
$EndComp
$Comp
L A5C:NAND02 UBA?
U 1 1 74AA38C6
P 3250 7150
AR Path="/611F0159/74AA38C6" Ref="UBA?"  Part="1" 
AR Path="/74AA38C6" Ref="UBC130"  Part="1" 
AR Path="/611E8E91/74AA38C6" Ref="UBC130"  Part="1" 
AR Path="/612CA5FA/612CADBB/74AA38C6" Ref="UBC130"  Part="1" 
F 0 "UBC130" H 3275 7417 50  0000 C CNN
F 1 "NAND02" H 3275 7326 50  0000 C CNN
F 2 "" H 3250 7150 50  0001 C CNN
F 3 "" H 3250 7150 50  0001 C CNN
	1    3250 7150
	1    0    0    -1  
$EndComp
$Comp
L A5C:NAND02 UBA?
U 1 1 74AA38CC
P 3250 7950
AR Path="/611F0159/74AA38CC" Ref="UBA?"  Part="1" 
AR Path="/74AA38CC" Ref="UBC114"  Part="1" 
AR Path="/611E8E91/74AA38CC" Ref="UBC114"  Part="1" 
AR Path="/612CA5FA/612CADBB/74AA38CC" Ref="UBC114"  Part="1" 
F 0 "UBC114" H 3275 8217 50  0000 C CNN
F 1 "NAND02" H 3275 8126 50  0000 C CNN
F 2 "" H 3250 7950 50  0001 C CNN
F 3 "" H 3250 7950 50  0001 C CNN
	1    3250 7950
	1    0    0    -1  
$EndComp
$Comp
L A5C:NAND02 UBA?
U 1 1 74AA38C0
P 3250 8750
AR Path="/611F0159/74AA38C0" Ref="UBA?"  Part="1" 
AR Path="/74AA38C0" Ref="UBC87"  Part="1" 
AR Path="/611E8E91/74AA38C0" Ref="UBC87"  Part="1" 
AR Path="/612CA5FA/612CADBB/74AA38C0" Ref="UBC87"  Part="1" 
F 0 "UBC87" H 3275 9017 50  0000 C CNN
F 1 "NAND02" H 3275 8926 50  0000 C CNN
F 2 "" H 3250 8750 50  0001 C CNN
F 3 "" H 3250 8750 50  0001 C CNN
	1    3250 8750
	1    0    0    -1  
$EndComp
$Comp
L A5C:NAND02 UBD101
U 1 1 74AA3878
P 3250 9550
F 0 "UBD101" H 3275 9817 50  0000 C CNN
F 1 "NAND02" H 3275 9726 50  0000 C CNN
F 2 "" H 3250 9550 50  0001 C CNN
F 3 "" H 3250 9550 50  0001 C CNN
	1    3250 9550
	1    0    0    -1  
$EndComp
$Comp
L A5C:NOR02 UBA?
U 1 1 6B9084CB
P 5600 2750
AR Path="/611F0159/6B9084CB" Ref="UBA?"  Part="1" 
AR Path="/6B9084CB" Ref="UBA101"  Part="1" 
AR Path="/611E8E91/6B9084CB" Ref="UBA101"  Part="1" 
AR Path="/612CA5FA/612CADBB/6B9084CB" Ref="UBA101"  Part="1" 
F 0 "UBA101" H 5625 3017 50  0000 C CNN
F 1 "NOR02" H 5625 2926 50  0000 C CNN
F 2 "" H 5600 2750 50  0001 C CNN
F 3 "" H 5600 2750 50  0001 C CNN
	1    5600 2750
	1    0    0    -1  
$EndComp
$Comp
L A5C:NOR02 UBA?
U 1 1 74AA38BA
P 5600 7600
AR Path="/611F0159/74AA38BA" Ref="UBA?"  Part="1" 
AR Path="/74AA38BA" Ref="UBC98"  Part="1" 
AR Path="/611E8E91/74AA38BA" Ref="UBC98"  Part="1" 
AR Path="/612CA5FA/612CADBB/74AA38BA" Ref="UBC98"  Part="1" 
F 0 "UBC98" H 5625 7867 50  0000 C CNN
F 1 "NOR02" H 5625 7776 50  0000 C CNN
F 2 "" H 5600 7600 50  0001 C CNN
F 3 "" H 5600 7600 50  0001 C CNN
	1    5600 7600
	1    0    0    -1  
$EndComp
$Comp
L A5C:XNOR02 UBA76
U 1 1 6B908483
P 6350 2250
F 0 "UBA76" H 6375 2517 50  0000 C CNN
F 1 "XNOR02" H 6375 2426 50  0000 C CNN
F 2 "" H 6375 2250 50  0001 C CNN
F 3 "" H 6375 2250 50  0001 C CNN
	1    6350 2250
	1    0    0    -1  
$EndComp
$Comp
L A5C:XNOR02 UBA?
U 1 1 6B9084C5
P 6350 2850
AR Path="/611F0159/6B9084C5" Ref="UBA?"  Part="1" 
AR Path="/6B9084C5" Ref="UBA105"  Part="1" 
AR Path="/611E8E91/6B9084C5" Ref="UBA105"  Part="1" 
AR Path="/612CA5FA/612CADBB/6B9084C5" Ref="UBA105"  Part="1" 
F 0 "UBA105" H 6375 3117 50  0000 C CNN
F 1 "XNOR02" H 6375 3026 50  0000 C CNN
F 2 "" H 6375 2850 50  0001 C CNN
F 3 "" H 6375 2850 50  0001 C CNN
	1    6350 2850
	1    0    0    -1  
$EndComp
$Comp
L A5C:XNOR02 UBA?
U 1 1 6B9084BF
P 6350 3450
AR Path="/611F0159/6B9084BF" Ref="UBA?"  Part="1" 
AR Path="/6B9084BF" Ref="UBA125"  Part="1" 
AR Path="/611E8E91/6B9084BF" Ref="UBA125"  Part="1" 
AR Path="/612CA5FA/612CADBB/6B9084BF" Ref="UBA125"  Part="1" 
F 0 "UBA125" H 6375 3717 50  0000 C CNN
F 1 "XNOR02" H 6375 3626 50  0000 C CNN
F 2 "" H 6375 3450 50  0001 C CNN
F 3 "" H 6375 3450 50  0001 C CNN
	1    6350 3450
	1    0    0    -1  
$EndComp
$Comp
L A5C:XNOR02 UBD?
U 1 1 6B9084B9
P 6350 4150
AR Path="/611F0159/6B9084B9" Ref="UBD?"  Part="1" 
AR Path="/6B9084B9" Ref="UBA79"  Part="1" 
AR Path="/611E8E91/6B9084B9" Ref="UBA79"  Part="1" 
AR Path="/612CA5FA/612CADBB/6B9084B9" Ref="UBA79"  Part="1" 
F 0 "UBA79" H 6375 4417 50  0000 C CNN
F 1 "XNOR02" H 6375 4326 50  0000 C CNN
F 2 "" H 6375 4150 50  0001 C CNN
F 3 "" H 6375 4150 50  0001 C CNN
	1    6350 4150
	1    0    0    -1  
$EndComp
$Comp
L A5C:XNOR02 UBC105
U 1 1 74AA3872
P 6350 7100
F 0 "UBC105" H 6375 7367 50  0000 C CNN
F 1 "XNOR02" H 6375 7276 50  0000 C CNN
F 2 "" H 6375 7100 50  0001 C CNN
F 3 "" H 6375 7100 50  0001 C CNN
	1    6350 7100
	1    0    0    -1  
$EndComp
$Comp
L A5C:XNOR02 UBA?
U 1 1 74AA38B4
P 6350 7700
AR Path="/611F0159/74AA38B4" Ref="UBA?"  Part="1" 
AR Path="/74AA38B4" Ref="UBC111"  Part="1" 
AR Path="/611E8E91/74AA38B4" Ref="UBC111"  Part="1" 
AR Path="/612CA5FA/612CADBB/74AA38B4" Ref="UBC111"  Part="1" 
F 0 "UBC111" H 6375 7967 50  0000 C CNN
F 1 "XNOR02" H 6375 7876 50  0000 C CNN
F 2 "" H 6375 7700 50  0001 C CNN
F 3 "" H 6375 7700 50  0001 C CNN
	1    6350 7700
	1    0    0    -1  
$EndComp
$Comp
L A5C:XNOR02 UBA?
U 1 1 74AA38AE
P 6350 8300
AR Path="/611F0159/74AA38AE" Ref="UBA?"  Part="1" 
AR Path="/74AA38AE" Ref="UBC92"  Part="1" 
AR Path="/611E8E91/74AA38AE" Ref="UBC92"  Part="1" 
AR Path="/612CA5FA/612CADBB/74AA38AE" Ref="UBC92"  Part="1" 
F 0 "UBC92" H 6375 8567 50  0000 C CNN
F 1 "XNOR02" H 6375 8476 50  0000 C CNN
F 2 "" H 6375 8300 50  0001 C CNN
F 3 "" H 6375 8300 50  0001 C CNN
	1    6350 8300
	1    0    0    -1  
$EndComp
$Comp
L A5C:XNOR02 UBA?
U 1 1 74AA38A8
P 6350 9000
AR Path="/611F0159/74AA38A8" Ref="UBA?"  Part="1" 
AR Path="/74AA38A8" Ref="UBD106"  Part="1" 
AR Path="/611E8E91/74AA38A8" Ref="UBD106"  Part="1" 
AR Path="/612CA5FA/612CADBB/74AA38A8" Ref="UBD106"  Part="1" 
F 0 "UBD106" H 6375 9267 50  0000 C CNN
F 1 "XNOR02" H 6375 9176 50  0000 C CNN
F 2 "" H 6375 9000 50  0001 C CNN
F 3 "" H 6375 9000 50  0001 C CNN
	1    6350 9000
	1    0    0    -1  
$EndComp
$Comp
L A5C:AND03 UAD238
U 1 1 74120E93
P 4350 900
F 0 "UAD238" H 4375 1192 50  0000 C CNN
F 1 "AND03" H 4375 1101 50  0000 C CNN
F 2 "" H 4350 900 50  0001 C CNN
F 3 "" H 4350 900 50  0001 C CNN
	1    4350 900 
	1    0    0    -1  
$EndComp
$Comp
L A5C:INV01 UAD?
U 1 1 788B0EDC
P 3150 900
AR Path="/611F0159/788B0EDC" Ref="UAD?"  Part="1" 
AR Path="/788B0EDC" Ref="UAD241"  Part="1" 
AR Path="/611E8E91/788B0EDC" Ref="UAD241"  Part="1" 
AR Path="/612CA5FA/612CADBB/788B0EDC" Ref="UAD241"  Part="1" 
F 0 "UAD241" H 3150 1217 50  0000 C CNN
F 1 "INV01" H 3150 1126 50  0000 C CNN
F 2 "" H 3150 700 50  0001 C CNN
F 3 "" H 3150 700 50  0001 C CNN
	1    3150 900 
	1    0    0    -1  
$EndComp
$Comp
L A5C:INV01 UAD?
U 1 1 6B9084B3
P 4950 1300
AR Path="/611F0159/6B9084B3" Ref="UAD?"  Part="1" 
AR Path="/6B9084B3" Ref="UAD439"  Part="1" 
AR Path="/611E8E91/6B9084B3" Ref="UAD439"  Part="1" 
AR Path="/612CA5FA/612CADBB/6B9084B3" Ref="UAD439"  Part="1" 
F 0 "UAD439" H 4950 1617 50  0000 C CNN
F 1 "INV01" H 4950 1526 50  0000 C CNN
F 2 "" H 4950 1100 50  0001 C CNN
F 3 "" H 4950 1100 50  0001 C CNN
	1    4950 1300
	1    0    0    -1  
$EndComp
$Comp
L A5C:NOR03 UBA?
U 1 1 6B9084AD
P 5650 3300
AR Path="/611F0159/6B9084AD" Ref="UBA?"  Part="1" 
AR Path="/6B9084AD" Ref="UBA122"  Part="1" 
AR Path="/611E8E91/6B9084AD" Ref="UBA122"  Part="1" 
AR Path="/612CA5FA/612CADBB/6B9084AD" Ref="UBA122"  Part="1" 
F 0 "UBA122" H 5650 3617 50  0000 C CNN
F 1 "NOR03" H 5650 3526 50  0000 C CNN
F 2 "" H 5650 3300 50  0001 C CNN
F 3 "" H 5650 3300 50  0001 C CNN
	1    5650 3300
	1    0    0    -1  
$EndComp
$Comp
L A5C:NOR03 UBA?
U 1 1 74AA389C
P 5650 8150
AR Path="/611F0159/74AA389C" Ref="UBA?"  Part="1" 
AR Path="/74AA389C" Ref="UBC108"  Part="1" 
AR Path="/611E8E91/74AA389C" Ref="UBC108"  Part="1" 
AR Path="/612CA5FA/612CADBB/74AA389C" Ref="UBC108"  Part="1" 
F 0 "UBC108" H 5650 8467 50  0000 C CNN
F 1 "NOR03" H 5650 8376 50  0000 C CNN
F 2 "" H 5650 8150 50  0001 C CNN
F 3 "" H 5650 8150 50  0001 C CNN
	1    5650 8150
	1    0    0    -1  
$EndComp
$Comp
L A5C:INV01 UAD?
U 1 1 74AA38A2
P 5800 6150
AR Path="/611F0159/74AA38A2" Ref="UAD?"  Part="1" 
AR Path="/74AA38A2" Ref="UBB92"  Part="1" 
AR Path="/611E8E91/74AA38A2" Ref="UBB92"  Part="1" 
AR Path="/612CA5FA/612CADBB/74AA38A2" Ref="UBB92"  Part="1" 
F 0 "UBB92" H 5800 6467 50  0000 C CNN
F 1 "INV01" H 5800 6376 50  0000 C CNN
F 2 "" H 5800 5950 50  0001 C CNN
F 3 "" H 5800 5950 50  0001 C CNN
	1    5800 6150
	-1   0    0    1   
$EndComp
$Comp
L A5C:DFFCOO UBC140
U 1 1 6B90848F
P 4000 2400
F 0 "UBC140" H 4000 2765 50  0000 C CNN
F 1 "DFFCOO" H 4000 2674 50  0000 C CNN
F 2 "" H 4000 2400 50  0001 C CNN
F 3 "" H 4000 2400 50  0001 C CNN
	1    4000 2400
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFFCOO UBE?
U 1 1 6B9084A7
P 4000 3200
AR Path="/611F0159/6B9084A7" Ref="UBE?"  Part="1" 
AR Path="/6B9084A7" Ref="UBB87"  Part="1" 
AR Path="/611E8E91/6B9084A7" Ref="UBB87"  Part="1" 
AR Path="/612CA5FA/612CADBB/6B9084A7" Ref="UBB87"  Part="1" 
F 0 "UBB87" H 4000 3565 50  0000 C CNN
F 1 "DFFCOO" H 4000 3474 50  0000 C CNN
F 2 "" H 4000 3200 50  0001 C CNN
F 3 "" H 4000 3200 50  0001 C CNN
	1    4000 3200
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFFCOO UBA114
U 1 1 6B908561
P 4000 4000
F 0 "UBA114" H 4000 4365 50  0000 C CNN
F 1 "DFFCOO" H 4000 4274 50  0000 C CNN
F 2 "" H 4000 4000 50  0001 C CNN
F 3 "" H 4000 4000 50  0001 C CNN
	1    4000 4000
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFFCOO UBF?
U 1 1 6B9084A1
P 4000 4800
AR Path="/611F0159/6B9084A1" Ref="UBF?"  Part="1" 
AR Path="/6B9084A1" Ref="UBA85"  Part="1" 
AR Path="/611E8E91/6B9084A1" Ref="UBA85"  Part="1" 
AR Path="/612CA5FA/612CADBB/6B9084A1" Ref="UBA85"  Part="1" 
F 0 "UBA85" H 4000 5165 50  0000 C CNN
F 1 "DFFCOO" H 4000 5074 50  0000 C CNN
F 2 "" H 4000 4800 50  0001 C CNN
F 3 "" H 4000 4800 50  0001 C CNN
	1    4000 4800
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFFCOO UBD116
U 1 1 74AA387E
P 4000 7250
F 0 "UBD116" H 4000 7615 50  0000 C CNN
F 1 "DFFCOO" H 4000 7524 50  0000 C CNN
F 2 "" H 4000 7250 50  0001 C CNN
F 3 "" H 4000 7250 50  0001 C CNN
	1    4000 7250
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFFCOO UBB?
U 1 1 74AA3896
P 4000 8050
AR Path="/611F0159/74AA3896" Ref="UBB?"  Part="1" 
AR Path="/74AA3896" Ref="UBD95"  Part="1" 
AR Path="/611E8E91/74AA3896" Ref="UBD95"  Part="1" 
AR Path="/612CA5FA/612CADBB/74AA3896" Ref="UBD95"  Part="1" 
F 0 "UBD95" H 4000 8415 50  0000 C CNN
F 1 "DFFCOO" H 4000 8324 50  0000 C CNN
F 2 "" H 4000 8050 50  0001 C CNN
F 3 "" H 4000 8050 50  0001 C CNN
	1    4000 8050
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFFCOO UBD63
U 1 1 74AA394E
P 4000 8850
F 0 "UBD63" H 4000 9215 50  0000 C CNN
F 1 "DFFCOO" H 4000 9124 50  0000 C CNN
F 2 "" H 4000 8850 50  0001 C CNN
F 3 "" H 4000 8850 50  0001 C CNN
	1    4000 8850
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFFCOO UBA?
U 1 1 74AA3890
P 4000 9650
AR Path="/611F0159/74AA3890" Ref="UBA?"  Part="1" 
AR Path="/74AA3890" Ref="UBF98"  Part="1" 
AR Path="/611E8E91/74AA3890" Ref="UBF98"  Part="1" 
AR Path="/612CA5FA/612CADBB/74AA3890" Ref="UBF98"  Part="1" 
F 0 "UBF98" H 4000 10015 50  0000 C CNN
F 1 "DFFCOO" H 4000 9924 50  0000 C CNN
F 2 "" H 4000 9650 50  0001 C CNN
F 3 "" H 4000 9650 50  0001 C CNN
	1    4000 9650
	1    0    0    -1  
$EndComp
$Comp
L A5C:NOR04 UBD?
U 1 1 6B90849B
P 5650 3950
AR Path="/611F0159/6B90849B" Ref="UBD?"  Part="1" 
AR Path="/6B90849B" Ref="UBA97"  Part="1" 
AR Path="/611E8E91/6B90849B" Ref="UBA97"  Part="1" 
AR Path="/612CA5FA/612CADBB/6B90849B" Ref="UBA97"  Part="1" 
F 0 "UBA97" H 5650 4292 50  0000 C CNN
F 1 "NOR04" H 5650 4201 50  0000 C CNN
F 2 "" H 5650 3950 50  0001 C CNN
F 3 "" H 5650 3950 50  0001 C CNN
	1    5650 3950
	1    0    0    -1  
$EndComp
$Comp
L A5C:NOR04 UBA?
U 1 1 74AA388A
P 5650 8800
AR Path="/611F0159/74AA388A" Ref="UBA?"  Part="1" 
AR Path="/74AA388A" Ref="UBD103"  Part="1" 
AR Path="/611E8E91/74AA388A" Ref="UBD103"  Part="1" 
AR Path="/612CA5FA/612CADBB/74AA388A" Ref="UBD103"  Part="1" 
F 0 "UBD103" H 5650 9142 50  0000 C CNN
F 1 "NOR04" H 5650 9051 50  0000 C CNN
F 2 "" H 5650 8800 50  0001 C CNN
F 3 "" H 5650 8800 50  0001 C CNN
	1    5650 8800
	1    0    0    -1  
$EndComp
$Comp
L A5C:NOR05 UBD?
U 1 1 6B908495
P 5650 4700
AR Path="/611F0159/6B908495" Ref="UBD?"  Part="1" 
AR Path="/6B908495" Ref="UBA94"  Part="1" 
AR Path="/611E8E91/6B908495" Ref="UBA94"  Part="1" 
AR Path="/612CA5FA/612CADBB/6B908495" Ref="UBA94"  Part="1" 
F 0 "UBA94" H 5725 5115 50  0000 C CNN
F 1 "NOR05" H 5725 5024 50  0000 C CNN
F 2 "" H 5650 4750 50  0001 C CNN
F 3 "" H 5650 4750 50  0001 C CNN
	1    5650 4700
	1    0    0    -1  
$EndComp
$Comp
L A5C:NOR05 UBA?
U 1 1 74AA3884
P 5650 9550
AR Path="/611F0159/74AA3884" Ref="UBA?"  Part="1" 
AR Path="/74AA3884" Ref="UBD109"  Part="1" 
AR Path="/611E8E91/74AA3884" Ref="UBD109"  Part="1" 
AR Path="/612CA5FA/612CADBB/74AA3884" Ref="UBD109"  Part="1" 
F 0 "UBD109" H 5725 9965 50  0000 C CNN
F 1 "NOR05" H 5725 9874 50  0000 C CNN
F 2 "" H 5650 9600 50  0001 C CNN
F 3 "" H 5650 9600 50  0001 C CNN
	1    5650 9550
	1    0    0    -1  
$EndComp
Wire Bus Line
	5300 5050 5300 5750
$EndSCHEMATC
