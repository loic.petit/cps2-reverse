EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 46 83
Title "CPS Reverse Engineering: DL-0311"
Date "2021-09-12"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 "Licence: CC-BY"
Comment4 "Author: Loïc *WydD* Petit"
$EndDescr
Connection ~ 3500 7500
Connection ~ 4600 2600
Connection ~ 4600 5450
Connection ~ 4600 3650
Connection ~ 4600 4700
Connection ~ 4600 1550
Connection ~ 4150 6300
NoConn ~ 4000 7400
NoConn ~ 4750 7300
NoConn ~ 4750 6650
NoConn ~ 6050 5350
NoConn ~ 6050 6000
NoConn ~ 6350 2400
NoConn ~ 6350 4500
Entry Wire Line
	4850 950  4950 1050
Entry Wire Line
	4850 1050 4950 1150
Entry Wire Line
	4850 1150 4950 1250
Entry Wire Line
	4850 1250 4950 1350
Entry Wire Line
	4850 2000 4950 2100
Entry Wire Line
	4850 2100 4950 2200
Entry Wire Line
	4850 2200 4950 2300
Entry Wire Line
	4850 3050 4950 3150
Entry Wire Line
	4850 3150 4950 3250
Entry Wire Line
	4850 3250 4950 3350
Entry Wire Line
	4850 3350 4950 3450
Entry Wire Line
	4850 4100 4950 4200
Entry Wire Line
	4850 4200 4950 4300
Entry Wire Line
	4850 4300 4950 4400
Entry Wire Line
	4850 5150 4950 5250
Entry Wire Line
	4850 5800 4950 5900
Entry Wire Line
	7200 950  7100 1050
Entry Wire Line
	7200 1050 7100 1150
Entry Wire Line
	7200 1150 7100 1250
Entry Wire Line
	7200 1250 7100 1350
Entry Wire Line
	7200 2000 7100 2100
Entry Wire Line
	7200 2100 7100 2200
Entry Wire Line
	7200 2200 7100 2300
Entry Wire Line
	7200 3050 7100 3150
Entry Wire Line
	7200 3150 7100 3250
Entry Wire Line
	7200 3250 7100 3350
Entry Wire Line
	7200 3350 7100 3450
Entry Wire Line
	7200 4100 7100 4200
Entry Wire Line
	7200 4200 7100 4300
Entry Wire Line
	7200 4300 7100 4400
Wire Wire Line
	3250 7500 3500 7500
Wire Wire Line
	3500 6300 4150 6300
Wire Wire Line
	3500 7700 3500 7500
Wire Wire Line
	3800 1500 4050 1500
Wire Wire Line
	4100 6850 4250 6850
Wire Wire Line
	4150 6300 5500 6300
Wire Wire Line
	4150 6650 4150 6300
Wire Wire Line
	4250 6650 4150 6650
Wire Wire Line
	4250 7300 4000 7300
Wire Wire Line
	4250 7500 4250 7700
Wire Wire Line
	4250 7700 3500 7700
Wire Wire Line
	4600 1550 4600 2600
Wire Wire Line
	4600 2600 4600 3650
Wire Wire Line
	4600 3650 4600 4700
Wire Wire Line
	4600 4700 4600 5450
Wire Wire Line
	4600 5450 4600 6100
Wire Wire Line
	4600 5450 5550 5450
Wire Wire Line
	4600 6100 5550 6100
Wire Wire Line
	4750 6750 4850 6750
Wire Wire Line
	4750 7400 4950 7400
Wire Wire Line
	4850 6500 4850 6750
Wire Wire Line
	4850 6500 5500 6500
Wire Wire Line
	4950 1050 5550 1050
Wire Wire Line
	4950 1150 5550 1150
Wire Wire Line
	4950 1250 5550 1250
Wire Wire Line
	4950 1350 5550 1350
Wire Wire Line
	4950 2100 5550 2100
Wire Wire Line
	4950 2200 5550 2200
Wire Wire Line
	4950 2300 5550 2300
Wire Wire Line
	4950 3150 5550 3150
Wire Wire Line
	4950 3250 5550 3250
Wire Wire Line
	4950 3350 5550 3350
Wire Wire Line
	4950 3450 5550 3450
Wire Wire Line
	4950 4200 5550 4200
Wire Wire Line
	4950 4300 5550 4300
Wire Wire Line
	4950 4400 5550 4400
Wire Wire Line
	4950 5250 5550 5250
Wire Wire Line
	4950 5900 5550 5900
Wire Wire Line
	4950 6400 4950 7400
Wire Wire Line
	5350 1850 5550 1850
Wire Wire Line
	5350 2900 5550 2900
Wire Wire Line
	5350 3950 5550 3950
Wire Wire Line
	5350 5000 5550 5000
Wire Wire Line
	5450 2400 5550 2400
Wire Wire Line
	5450 4500 5550 4500
Wire Wire Line
	5500 6400 4950 6400
Wire Wire Line
	5550 1550 4600 1550
Wire Wire Line
	5550 1850 5550 1650
Wire Wire Line
	5550 2600 4600 2600
Wire Wire Line
	5550 2900 5550 2700
Wire Wire Line
	5550 3650 4600 3650
Wire Wire Line
	5550 3950 5550 3750
Wire Wire Line
	5550 4700 4600 4700
Wire Wire Line
	5550 5000 5550 4800
Wire Wire Line
	6150 5250 6050 5250
Wire Wire Line
	6150 5250 6150 5800
Wire Wire Line
	6150 6100 6150 6400
Wire Wire Line
	6150 6100 6250 6100
Wire Wire Line
	6150 6400 6050 6400
Wire Wire Line
	6250 5800 6150 5800
Wire Wire Line
	6250 5900 6050 5900
Wire Wire Line
	7100 1050 6350 1050
Wire Wire Line
	7100 1150 6350 1150
Wire Wire Line
	7100 1250 6350 1250
Wire Wire Line
	7100 1350 6350 1350
Wire Wire Line
	7100 2100 6350 2100
Wire Wire Line
	7100 2200 6350 2200
Wire Wire Line
	7100 2300 6350 2300
Wire Wire Line
	7100 3150 6350 3150
Wire Wire Line
	7100 3250 6350 3250
Wire Wire Line
	7100 3350 6350 3350
Wire Wire Line
	7100 3450 6350 3450
Wire Wire Line
	7100 4200 6350 4200
Wire Wire Line
	7100 4300 6350 4300
Wire Wire Line
	7100 4400 6350 4400
Wire Wire Line
	7100 5800 7450 5800
Wire Wire Line
	7100 6000 7650 6000
Wire Wire Line
	7100 6100 7700 6100
Wire Wire Line
	7450 4600 7700 4600
Wire Wire Line
	7450 5800 7450 4600
Wire Wire Line
	7550 5100 7550 5900
Wire Wire Line
	7550 5100 7700 5100
Wire Wire Line
	7550 5900 7100 5900
Wire Wire Line
	7650 6000 7650 5600
Wire Wire Line
	7700 5600 7650 5600
Wire Bus Line
	4850 800  4750 800 
Wire Bus Line
	7200 850  7300 850 
Wire Bus Line
	7200 2950 7300 2950
Text Label 5000 1050 0    50   ~ 0
BUS-DATA15
Text Label 5000 1150 0    50   ~ 0
BUS-DATA14
Text Label 5000 1250 0    50   ~ 0
BUS-DATA13
Text Label 5000 1350 0    50   ~ 0
BUS-DATA12
Text Label 5000 2100 0    50   ~ 0
BUS-DATA11
Text Label 5000 2200 0    50   ~ 0
BUS-DATA10
Text Label 5000 2300 0    50   ~ 0
BUS-DATA9
Text Label 5000 3150 0    50   ~ 0
BUS-DATA8
Text Label 5000 3250 0    50   ~ 0
BUS-DATA7
Text Label 5000 3350 0    50   ~ 0
BUS-DATA6
Text Label 5000 3450 0    50   ~ 0
BUS-DATA5
Text Label 5000 4200 0    50   ~ 0
BUS-DATA4
Text Label 5000 4300 0    50   ~ 0
BUS-DATA3
Text Label 5000 4400 0    50   ~ 0
BUS-DATA2
Text Label 5000 5250 0    50   ~ 0
BUS-DATA1
Text Label 5000 5900 0    50   ~ 0
BUS-DATA0
Text Label 7100 1050 2    50   ~ 0
~RAM-BASE6
Text Label 7100 1150 2    50   ~ 0
~RAM-BASE5
Text Label 7100 1250 2    50   ~ 0
~RAM-BASE4
Text Label 7100 1350 2    50   ~ 0
~RAM-BASE3
Text Label 7100 2100 2    50   ~ 0
~RAM-BASE2
Text Label 7100 2200 2    50   ~ 0
~RAM-BASE1
Text Label 7100 2300 2    50   ~ 0
~RAM-BASE0
Text Label 7100 3150 2    50   ~ 0
~ROMA-BASE6
Text Label 7100 3250 2    50   ~ 0
~ROMA-BASE5
Text Label 7100 3350 2    50   ~ 0
~ROMA-BASE4
Text Label 7100 3450 2    50   ~ 0
~ROMA-BASE3
Text Label 7100 4200 2    50   ~ 0
~ROMA-BASE2
Text Label 7100 4300 2    50   ~ 0
~ROMA-BASE1
Text Label 7100 4400 2    50   ~ 0
~ROMA-BASE0
Text HLabel 2650 7500 0    50   Input ~ 0
CLK4M
Text HLabel 3200 1500 0    50   Input ~ 0
~WSTROM-COPY
Text HLabel 3500 6300 0    50   Input ~ 0
COL1
Text HLabel 3500 6850 0    50   Input ~ 0
CLK8M
Text HLabel 3500 7300 0    50   Input ~ 0
~WSTROM
Text HLabel 4050 1600 0    50   Input ~ 0
CLK4M
Text HLabel 4750 800  0    50   Input ~ 0
BUS-DATA[0..15]
Text HLabel 7300 850  2    50   Output ~ 0
~RAM-BASE[0..6]
Text HLabel 7300 2950 2    50   Output ~ 0
~ROMA-BASE[0..6]
Text HLabel 8300 6100 2    50   Output ~ 0
PL3WED
Text HLabel 8300 5600 2    50   Output ~ 0
PL2WED
Text HLabel 8300 5100 2    50   Output ~ 0
PL1WED
Text HLabel 8300 4600 2    50   Output ~ 0
PL0WED
$Comp
L power:VCC #PWR0105
U 1 1 61118D93
P 5350 1850
F 0 "#PWR0105" H 5350 1700 50  0001 C CNN
F 1 "VCC" H 5365 2023 50  0000 C CNN
F 2 "" H 5350 1850 50  0001 C CNN
F 3 "" H 5350 1850 50  0001 C CNN
	1    5350 1850
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0106
U 1 1 61118D95
P 5350 2900
F 0 "#PWR0106" H 5350 2750 50  0001 C CNN
F 1 "VCC" H 5365 3073 50  0000 C CNN
F 2 "" H 5350 2900 50  0001 C CNN
F 3 "" H 5350 2900 50  0001 C CNN
	1    5350 2900
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0107
U 1 1 61118D90
P 5350 3950
F 0 "#PWR0107" H 5350 3800 50  0001 C CNN
F 1 "VCC" H 5365 4123 50  0000 C CNN
F 2 "" H 5350 3950 50  0001 C CNN
F 3 "" H 5350 3950 50  0001 C CNN
	1    5350 3950
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0108
U 1 1 61118D8F
P 5350 5000
F 0 "#PWR0108" H 5350 4850 50  0001 C CNN
F 1 "VCC" H 5365 5173 50  0000 C CNN
F 2 "" H 5350 5000 50  0001 C CNN
F 3 "" H 5350 5000 50  0001 C CNN
	1    5350 5000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0145
U 1 1 61118D94
P 5450 2400
F 0 "#PWR0145" H 5450 2150 50  0001 C CNN
F 1 "GND" H 5455 2227 50  0001 C CNN
F 2 "" H 5450 2400 50  0001 C CNN
F 3 "" H 5450 2400 50  0001 C CNN
	1    5450 2400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0146
U 1 1 61118D8E
P 5450 4500
F 0 "#PWR0146" H 5450 4250 50  0001 C CNN
F 1 "GND" H 5455 4327 50  0001 C CNN
F 2 "" H 5450 4500 50  0001 C CNN
F 3 "" H 5450 4500 50  0001 C CNN
	1    5450 4500
	1    0    0    -1  
$EndComp
$Comp
L A5C:NAND02 UBF119
U 1 1 61118D92
P 4300 1550
AR Path="/611A2627/61118D92" Ref="UBF119"  Part="1" 
AR Path="/612E58BE/612EE471/61118D92" Ref="UBF119"  Part="1" 
F 0 "UBF119" H 4325 1817 50  0000 C CNN
F 1 "NAND02" H 4325 1726 50  0000 C CNN
F 2 "" H 4300 1550 50  0001 C CNN
F 3 "" H 4300 1550 50  0001 C CNN
	1    4300 1550
	1    0    0    -1  
$EndComp
$Comp
L A5C:NAND03 UBE59
U 1 1 61118DA0
P 5750 6400
AR Path="/611A2627/61118DA0" Ref="UBE59"  Part="1" 
AR Path="/612E58BE/612EE471/61118DA0" Ref="UBE59"  Part="1" 
F 0 "UBE59" H 5775 6692 50  0000 C CNN
F 1 "NAND03" H 5775 6601 50  0000 C CNN
F 2 "" H 5750 6400 50  0001 C CNN
F 3 "" H 5750 6400 50  0001 C CNN
	1    5750 6400
	1    0    0    1   
$EndComp
$Comp
L A5C:INV01 UBA43
U 1 1 61118D9A
P 2950 7500
AR Path="/611A2627/61118D9A" Ref="UBA43"  Part="1" 
AR Path="/612E58BE/612EE471/61118D9A" Ref="UBA43"  Part="1" 
F 0 "UBA43" H 2950 7817 50  0000 C CNN
F 1 "INV01" H 2950 7726 50  0000 C CNN
F 2 "" H 2950 7300 50  0001 C CNN
F 3 "" H 2950 7300 50  0001 C CNN
	1    2950 7500
	1    0    0    -1  
$EndComp
$Comp
L A5C:INV01 UBF94
U 1 1 61118D91
P 3500 1500
AR Path="/611A2627/61118D91" Ref="UBF94"  Part="1" 
AR Path="/612E58BE/612EE471/61118D91" Ref="UBF94"  Part="1" 
F 0 "UBF94" H 3500 1817 50  0000 C CNN
F 1 "INV01" H 3500 1726 50  0000 C CNN
F 2 "" H 3500 1300 50  0001 C CNN
F 3 "" H 3500 1300 50  0001 C CNN
	1    3500 1500
	1    0    0    -1  
$EndComp
$Comp
L A5C:INV01 UBD54
U 1 1 61118D98
P 3800 6850
AR Path="/611A2627/61118D98" Ref="UBD54"  Part="1" 
AR Path="/612E58BE/612EE471/61118D98" Ref="UBD54"  Part="1" 
F 0 "UBD54" H 3800 7167 50  0000 C CNN
F 1 "INV01" H 3800 7076 50  0000 C CNN
F 2 "" H 3800 6650 50  0001 C CNN
F 3 "" H 3800 6650 50  0001 C CNN
	1    3800 6850
	1    0    0    -1  
$EndComp
$Comp
L A5C:BUF12 UBG98
U 1 1 61118DA5
P 8000 4600
AR Path="/611A2627/61118DA5" Ref="UBG98"  Part="1" 
AR Path="/612E58BE/612EE471/61118DA5" Ref="UBG98"  Part="1" 
F 0 "UBG98" H 8000 4917 50  0000 C CNN
F 1 "BUF12" H 8000 4826 50  0000 C CNN
F 2 "" H 8000 4600 50  0001 C CNN
F 3 "" H 8000 4600 50  0001 C CNN
	1    8000 4600
	1    0    0    -1  
$EndComp
$Comp
L A5C:BUF12 UEI274
U 1 1 61118DA2
P 8000 5100
AR Path="/611A2627/61118DA2" Ref="UEI274"  Part="1" 
AR Path="/612E58BE/612EE471/61118DA2" Ref="UEI274"  Part="1" 
F 0 "UEI274" H 8000 5417 50  0000 C CNN
F 1 "BUF12" H 8000 5326 50  0000 C CNN
F 2 "" H 8000 5100 50  0001 C CNN
F 3 "" H 8000 5100 50  0001 C CNN
	1    8000 5100
	1    0    0    -1  
$EndComp
$Comp
L A5C:BUF12 UEI257
U 1 1 61118DA3
P 8000 5600
AR Path="/611A2627/61118DA3" Ref="UEI257"  Part="1" 
AR Path="/612E58BE/612EE471/61118DA3" Ref="UEI257"  Part="1" 
F 0 "UEI257" H 8000 5917 50  0000 C CNN
F 1 "BUF12" H 8000 5826 50  0000 C CNN
F 2 "" H 8000 5600 50  0001 C CNN
F 3 "" H 8000 5600 50  0001 C CNN
	1    8000 5600
	1    0    0    -1  
$EndComp
$Comp
L A5C:BUF12 UEG273
U 1 1 61118DA1
P 8000 6100
AR Path="/611A2627/61118DA1" Ref="UEG273"  Part="1" 
AR Path="/612E58BE/612EE471/61118DA1" Ref="UEG273"  Part="1" 
F 0 "UEG273" H 8000 6417 50  0000 C CNN
F 1 "BUF12" H 8000 6326 50  0000 C CNN
F 2 "" H 8000 6100 50  0001 C CNN
F 3 "" H 8000 6100 50  0001 C CNN
	1    8000 6100
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFFCOO UBB55
U 1 1 61118D99
P 3750 7400
AR Path="/611A2627/61118D99" Ref="UBB55"  Part="1" 
AR Path="/612E58BE/612EE471/61118D99" Ref="UBB55"  Part="1" 
F 0 "UBB55" H 3750 7765 50  0000 C CNN
F 1 "DFFCOO" H 3750 7674 50  0000 C CNN
F 2 "" H 3750 7400 50  0001 C CNN
F 3 "" H 3750 7400 50  0001 C CNN
	1    3750 7400
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFFCOO UBD58
U 1 1 61118D96
P 4500 6750
AR Path="/611A2627/61118D96" Ref="UBD58"  Part="1" 
AR Path="/612E58BE/612EE471/61118D96" Ref="UBD58"  Part="1" 
F 0 "UBD58" H 4500 7115 50  0000 C CNN
F 1 "DFFCOO" H 4500 7024 50  0000 C CNN
F 2 "" H 4500 6750 50  0001 C CNN
F 3 "" H 4500 6750 50  0001 C CNN
	1    4500 6750
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFFCOO UBC55
U 1 1 61118D97
P 4500 7400
AR Path="/611A2627/61118D97" Ref="UBC55"  Part="1" 
AR Path="/612E58BE/612EE471/61118D97" Ref="UBC55"  Part="1" 
F 0 "UBC55" H 4500 7765 50  0000 C CNN
F 1 "DFFCOO" H 4500 7674 50  0000 C CNN
F 2 "" H 4500 7400 50  0001 C CNN
F 3 "" H 4500 7400 50  0001 C CNN
	1    4500 7400
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFFCOO UBH124
U 1 1 61118D8D
P 5800 5350
AR Path="/611A2627/61118D8D" Ref="UBH124"  Part="1" 
AR Path="/612E58BE/612EE471/61118D8D" Ref="UBH124"  Part="1" 
F 0 "UBH124" H 5800 5715 50  0000 C CNN
F 1 "DFFCOO" H 5800 5624 50  0000 C CNN
F 2 "" H 5800 5350 50  0001 C CNN
F 3 "" H 5800 5350 50  0001 C CNN
	1    5800 5350
	1    0    0    -1  
$EndComp
$Comp
L A5C:DFFCOO UBI77
U 1 1 61118D8B
P 5800 6000
AR Path="/611A2627/61118D8B" Ref="UBI77"  Part="1" 
AR Path="/612E58BE/612EE471/61118D8B" Ref="UBI77"  Part="1" 
F 0 "UBI77" H 5800 6365 50  0000 C CNN
F 1 "DFFCOO" H 5800 6274 50  0000 C CNN
F 2 "" H 5800 6000 50  0001 C CNN
F 3 "" H 5800 6000 50  0001 C CNN
	1    5800 6000
	1    0    0    -1  
$EndComp
$Comp
L A5C:D24GL UBG109
U 1 1 61118D9F
P 6650 5950
AR Path="/611A2627/61118D9F" Ref="UBG109"  Part="1" 
AR Path="/612E58BE/612EE471/61118D9F" Ref="UBG109"  Part="1" 
F 0 "UBG109" H 6675 6340 50  0000 C CNN
F 1 "D24GL" H 6675 6249 50  0000 C CNN
F 2 "" H 6650 6225 50  0001 C CNN
F 3 "" H 6650 6225 50  0001 C CNN
	1    6650 5950
	1    0    0    -1  
$EndComp
$Comp
L A5C:M175CL UBG130
U 1 1 61118DB7
P 5950 1350
AR Path="/611A2627/61118DB7" Ref="UBG130"  Part="1" 
AR Path="/612E58BE/612EE471/61118DB7" Ref="UBG130"  Part="1" 
F 0 "UBG130" H 5950 1915 50  0000 C CNN
F 1 "M175CL" H 5950 1824 50  0000 C CNN
F 2 "" H 5950 1150 50  0001 C CNN
F 3 "" H 5950 1150 50  0001 C CNN
	1    5950 1350
	1    0    0    -1  
$EndComp
$Comp
L A5C:M175CL UEB292
U 1 1 61118D9D
P 5950 2400
AR Path="/611A2627/61118D9D" Ref="UEB292"  Part="1" 
AR Path="/612E58BE/612EE471/61118D9D" Ref="UEB292"  Part="1" 
F 0 "UEB292" H 5950 2965 50  0000 C CNN
F 1 "M175CL" H 5950 2874 50  0000 C CNN
F 2 "" H 5950 2200 50  0001 C CNN
F 3 "" H 5950 2200 50  0001 C CNN
	1    5950 2400
	1    0    0    -1  
$EndComp
$Comp
L A5C:M175CL UCC233
U 1 1 61118D9C
P 5950 3450
AR Path="/611A2627/61118D9C" Ref="UCC233"  Part="1" 
AR Path="/612E58BE/612EE471/61118D9C" Ref="UCC233"  Part="1" 
F 0 "UCC233" H 5950 4015 50  0000 C CNN
F 1 "M175CL" H 5950 3924 50  0000 C CNN
F 2 "" H 5950 3250 50  0001 C CNN
F 3 "" H 5950 3250 50  0001 C CNN
	1    5950 3450
	1    0    0    -1  
$EndComp
$Comp
L A5C:M175CL UCC257
U 1 1 61118D8C
P 5950 4500
AR Path="/611A2627/61118D8C" Ref="UCC257"  Part="1" 
AR Path="/612E58BE/612EE471/61118D8C" Ref="UCC257"  Part="1" 
F 0 "UCC257" H 5950 5065 50  0000 C CNN
F 1 "M175CL" H 5950 4974 50  0000 C CNN
F 2 "" H 5950 4300 50  0001 C CNN
F 3 "" H 5950 4300 50  0001 C CNN
	1    5950 4500
	1    0    0    -1  
$EndComp
Wire Bus Line
	7200 850  7200 2200
Wire Bus Line
	7200 2950 7200 4300
Wire Bus Line
	4850 800  4850 5800
$EndSCHEMATC
