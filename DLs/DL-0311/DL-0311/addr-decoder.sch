EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 83
Title "CPS Reverse Engineering: DL-0311"
Date "2021-09-12"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 "Licence: CC-BY"
Comment4 "Author: Loïc *WydD* Petit"
$EndDescr
Connection ~ 6100 2900
Connection ~ 6200 3950
Connection ~ 6300 3850
Connection ~ 6100 2550
Connection ~ 6200 2750
Connection ~ 6000 3200
Connection ~ 6100 4050
Connection ~ 6300 3500
Connection ~ 6300 2700
Connection ~ 6500 2450
Connection ~ 6200 2800
Connection ~ 6700 3300
Connection ~ 6700 2150
Connection ~ 7000 3400
Connection ~ 7000 4550
Connection ~ 4250 3050
NoConn ~ 5450 2650
NoConn ~ 5450 2850
NoConn ~ 8250 4350
NoConn ~ 8250 4150
NoConn ~ 8250 4450
NoConn ~ 8250 4550
NoConn ~ 2450 2600
NoConn ~ 2450 3100
NoConn ~ 2450 3600
Entry Wire Line
	4500 2050 4600 2150
Entry Wire Line
	4500 2250 4600 2350
Entry Wire Line
	4500 2450 4600 2550
Entry Wire Line
	4500 2650 4600 2750
Entry Wire Line
	4500 3400 4600 3500
Wire Wire Line
	4050 3050 4250 3050
Wire Wire Line
	4250 3050 4850 3050
Wire Wire Line
	4250 3700 4250 3050
Wire Wire Line
	4250 3700 4950 3700
Wire Wire Line
	4350 4550 7000 4550
Wire Wire Line
	4600 3500 4950 3500
Wire Wire Line
	4850 2150 4600 2150
Wire Wire Line
	4850 2350 4600 2350
Wire Wire Line
	4850 2550 4600 2550
Wire Wire Line
	4850 2750 4600 2750
Wire Wire Line
	5350 3950 5350 5500
Wire Wire Line
	5350 5500 5450 5500
Wire Wire Line
	5450 2150 6700 2150
Wire Wire Line
	5450 2350 6000 2350
Wire Wire Line
	5450 2450 6500 2450
Wire Wire Line
	5450 2550 6100 2550
Wire Wire Line
	5450 2750 6200 2750
Wire Wire Line
	5450 3500 6300 3500
Wire Wire Line
	5450 4050 6100 4050
Wire Wire Line
	5450 5000 5450 4050
Wire Wire Line
	6000 2350 6000 3200
Wire Wire Line
	6000 3200 6000 3950
Wire Wire Line
	6000 3200 7250 3200
Wire Wire Line
	6000 3950 5350 3950
Wire Wire Line
	6050 5000 6050 5250
Wire Wire Line
	6050 5250 7650 5250
Wire Wire Line
	6050 5350 7650 5350
Wire Wire Line
	6050 5500 6050 5350
Wire Wire Line
	6100 1750 6100 2550
Wire Wire Line
	6100 1750 7250 1750
Wire Wire Line
	6100 2550 6100 2900
Wire Wire Line
	6100 2900 6100 4050
Wire Wire Line
	6100 4050 7250 4050
Wire Wire Line
	6200 1650 6200 2750
Wire Wire Line
	6200 1650 7250 1650
Wire Wire Line
	6200 2750 6200 2800
Wire Wire Line
	6200 3950 6200 2800
Wire Wire Line
	6200 3950 6200 5150
Wire Wire Line
	6200 5150 7650 5150
Wire Wire Line
	6300 1550 7250 1550
Wire Wire Line
	6300 2700 6300 1550
Wire Wire Line
	6300 2700 6300 3500
Wire Wire Line
	6300 3500 6300 3850
Wire Wire Line
	6300 3850 7250 3850
Wire Wire Line
	6300 5050 6300 3850
Wire Wire Line
	6300 5050 7650 5050
Wire Wire Line
	6500 2050 6500 2450
Wire Wire Line
	6500 2050 7250 2050
Wire Wire Line
	6500 2450 6500 4350
Wire Wire Line
	6500 4350 7250 4350
Wire Wire Line
	6600 2250 5450 2250
Wire Wire Line
	6600 2250 6600 4450
Wire Wire Line
	6700 2150 7250 2150
Wire Wire Line
	6700 3300 6700 2150
Wire Wire Line
	6700 5450 6700 3300
Wire Wire Line
	6700 5450 7650 5450
Wire Wire Line
	7000 2250 7250 2250
Wire Wire Line
	7000 3400 7000 2250
Wire Wire Line
	7000 4550 7000 3400
Wire Wire Line
	7250 2700 6300 2700
Wire Wire Line
	7250 2800 6200 2800
Wire Wire Line
	7250 2900 6100 2900
Wire Wire Line
	7250 3300 6700 3300
Wire Wire Line
	7250 3400 7000 3400
Wire Wire Line
	7250 3950 6200 3950
Wire Wire Line
	7250 4450 6600 4450
Wire Wire Line
	7250 4550 7000 4550
Wire Wire Line
	7450 4950 7650 4950
Wire Bus Line
	4500 1900 4400 1900
Text Notes 9050 1550 0    50   ~ 0
0x00
Text Notes 9050 1650 0    50   ~ 0
0x02
Text Notes 9050 1750 0    50   ~ 0
0x04
Text Notes 9050 1850 0    50   ~ 0
0x06
Text Notes 9050 1950 0    50   ~ 0
0x08
Text Notes 9050 2050 0    50   ~ 0
0x0A
Text Notes 9050 2150 0    50   ~ 0
0x0C
Text Notes 9050 2250 0    50   ~ 0
0x0E
Text Notes 9150 2700 0    50   ~ 0
0x10
Text Notes 9150 2800 0    50   ~ 0
0x12
Text Notes 9150 2900 0    50   ~ 0
0x14
Text Notes 9150 3000 0    50   ~ 0
0x16
Text Notes 9150 3100 0    50   ~ 0
0x18
Text Notes 9150 3200 0    50   ~ 0
0x1A
Text Notes 9150 3300 0    50   ~ 0
0x1C
Text Notes 9150 3400 0    50   ~ 0
0x1E
Text Notes 9150 3850 0    50   ~ 0
0x20
Text Notes 9150 3950 0    50   ~ 0
0x22
Text Notes 9150 4050 0    50   ~ 0
0x24
Text Notes 9150 4250 0    50   ~ 0
0x28
Text Notes 9150 5200 0    50   ~ 0
0x26
Text Label 1850 2600 2    50   ~ 0
A5
Text Label 1850 3100 2    50   ~ 0
A2
Text Label 1850 3600 2    50   ~ 0
A1
Text Label 4600 2150 0    50   ~ 0
CA5
Text Label 4600 2350 0    50   ~ 0
CA4
Text Label 4600 2550 0    50   ~ 0
CA3
Text Label 4600 2750 0    50   ~ 0
CA2
Text Label 4600 3500 0    50   ~ 0
CA1
Text Label 5450 2150 0    50   ~ 0
A5
Text Label 5450 2250 0    50   ~ 0
~A5
Text Label 5450 2350 0    50   ~ 0
A4
Text Label 5450 2450 0    50   ~ 0
~A4
Text Label 5450 2550 0    50   ~ 0
A3
Text Label 5450 2750 0    50   ~ 0
A2
Text Label 5450 3500 0    50   ~ 0
A1
Text Label 5450 5000 2    50   ~ 0
A3
Text Label 5450 5500 2    50   ~ 0
A4
Text Label 6050 5000 0    50   ~ 0
~AA3
Text Label 6050 5500 0    50   ~ 0
~AA4
Text Label 7250 1550 2    50   ~ 0
A1
Text Label 7250 1650 2    50   ~ 0
A2
Text Label 7250 1750 2    50   ~ 0
A3
Text Label 7250 2050 2    50   ~ 0
~A4
Text Label 7250 2150 2    50   ~ 0
A5
Text Label 7250 2700 2    50   ~ 0
A1
Text Label 7250 2800 2    50   ~ 0
A2
Text Label 7250 2900 2    50   ~ 0
A3
Text Label 7250 3200 2    50   ~ 0
A4
Text Label 7250 3300 2    50   ~ 0
A5
Text Label 7250 3850 2    50   ~ 0
A1
Text Label 7250 3950 2    50   ~ 0
A2
Text Label 7250 4050 2    50   ~ 0
A3
Text Label 7250 4350 2    50   ~ 0
~A4
Text Label 7250 4450 2    50   ~ 0
~A5
Text Label 7600 5050 2    50   ~ 0
A1
Text Label 7600 5150 2    50   ~ 0
A2
Text Label 7600 5250 2    50   ~ 0
~AA3
Text Label 7600 5350 2    50   ~ 0
~AA4
Text Label 7600 5450 2    50   ~ 0
A5
Text HLabel 4050 3050 0    50   Input ~ 0
WRITE
Text HLabel 4350 4550 0    50   Input ~ 0
~DECODE
Text HLabel 4400 1900 0    50   Input ~ 0
CA[1..5]
Text HLabel 7450 4950 0    50   Input ~ 0
READ
Text HLabel 8250 1550 2    50   Output ~ 0
~OBJRAM-BASE
Text HLabel 8250 1650 2    50   Output ~ 0
~SCROLL1-BASE
Text HLabel 8250 1750 2    50   Output ~ 0
~SCROLL2-BASE
Text HLabel 8250 1850 2    50   Output ~ 0
~SCROLL3-BASE
Text HLabel 8250 1950 2    50   Output ~ 0
~ROWSCROLL-BASE
Text HLabel 8250 2050 2    50   Output ~ 0
~PALETTE-BASE
Text HLabel 8250 2150 2    50   Output ~ 0
~SCROLL1-X
Text HLabel 8250 2250 2    50   Output ~ 0
~SCROLL1-Y
Text HLabel 8250 2700 2    50   Output ~ 0
~SCROLL2-X
Text HLabel 8250 2800 2    50   Output ~ 0
~SCROLL2-Y
Text HLabel 8250 2900 2    50   Output ~ 0
~SCROLL3-X
Text HLabel 8250 3000 2    50   Output ~ 0
~SCROLL3-Y
Text HLabel 8250 3100 2    50   Output ~ 0
~STAR1-X
Text HLabel 8250 3200 2    50   Output ~ 0
~STAR1-Y
Text HLabel 8250 3300 2    50   Output ~ 0
~STAR2-X
Text HLabel 8250 3400 2    50   Output ~ 0
~STAR2-Y
Text HLabel 8250 3850 2    50   Output ~ 0
~ROWSCROLL-OFFSET
Text HLabel 8250 3950 2    50   Output ~ 0
~VIDEO-CONTROL
Text HLabel 8250 4050 2    50   Output ~ 0
~WSTROM-COPY
Text HLabel 8250 4250 2    50   Output ~ 0
~SCANLINE
Text HLabel 8250 5200 2    50   Output ~ 0
~CPS-ID
$Comp
L A5C:INV01 UCA241
U 1 1 602E4A5D
P 2150 2600
F 0 "UCA241" H 2150 2917 50  0000 C CNN
F 1 "INV01" H 2150 2826 50  0000 C CNN
F 2 "" H 2150 2400 50  0001 C CNN
F 3 "" H 2150 2400 50  0001 C CNN
	1    2150 2600
	1    0    0    -1  
$EndComp
$Comp
L A5C:INV01 UCA238
U 1 1 602E9170
P 2150 3100
F 0 "UCA238" H 2150 3417 50  0000 C CNN
F 1 "INV01" H 2150 3326 50  0000 C CNN
F 2 "" H 2150 2900 50  0001 C CNN
F 3 "" H 2150 2900 50  0001 C CNN
	1    2150 3100
	1    0    0    -1  
$EndComp
$Comp
L A5C:INV01 UCA237
U 1 1 602E9964
P 2150 3600
F 0 "UCA237" H 2150 3917 50  0000 C CNN
F 1 "INV01" H 2150 3826 50  0000 C CNN
F 2 "" H 2150 3400 50  0001 C CNN
F 3 "" H 2150 3400 50  0001 C CNN
	1    2150 3600
	1    0    0    -1  
$EndComp
$Comp
L A5C:INV01 UCA239
U 1 1 602E6274
P 5750 5000
F 0 "UCA239" H 5750 5317 50  0000 C CNN
F 1 "INV01" H 5750 5226 50  0000 C CNN
F 2 "" H 5750 4800 50  0001 C CNN
F 3 "" H 5750 4800 50  0001 C CNN
	1    5750 5000
	1    0    0    -1  
$EndComp
$Comp
L A5C:INV01 UCA240
U 1 1 602E570E
P 5750 5500
F 0 "UCA240" H 5750 5817 50  0000 C CNN
F 1 "INV01" H 5750 5726 50  0000 C CNN
F 2 "" H 5750 5300 50  0001 C CNN
F 3 "" H 5750 5300 50  0001 C CNN
	1    5750 5500
	1    0    0    -1  
$EndComp
$Comp
L A5C:L UAA247
U 1 1 604147F7
P 5200 3600
F 0 "UAA247" H 5200 3965 50  0000 C CNN
F 1 "L" H 5200 3874 50  0000 C CNN
F 2 "" H 5200 3600 50  0001 C CNN
F 3 "" H 5200 3600 50  0001 C CNN
	1    5200 3600
	1    0    0    -1  
$EndComp
$Comp
L A5C:NAND06 UCA243
U 1 1 604BBFAE
P 7950 5200
F 0 "UCA243" H 7950 5640 50  0000 C CNN
F 1 "NAND06" H 7950 5549 50  0000 C CNN
F 2 "" H 7900 5300 50  0001 C CNN
F 3 "" H 7900 5300 50  0001 C CNN
	1    7950 5200
	1    0    0    -1  
$EndComp
$Comp
L A5C:L4LH UAB292
U 1 1 603E1010
P 5150 2450
F 0 "UAB292" H 5150 3015 50  0000 C CNN
F 1 "L4LH" H 5150 2924 50  0000 C CNN
F 2 "" H 5100 2550 50  0001 C CNN
F 3 "" H 5100 2550 50  0001 C CNN
	1    5150 2450
	1    0    0    -1  
$EndComp
$Comp
L A5C:D38GL UCB190
U 1 1 60305156
P 7750 1900
F 0 "UCB190" H 7750 2517 50  0000 C CNN
F 1 "D38GL" H 7750 2426 50  0000 C CNN
F 2 "" H 7750 2425 50  0001 C CNN
F 3 "" H 7750 2425 50  0001 C CNN
	1    7750 1900
	1    0    0    -1  
$EndComp
$Comp
L A5C:D38GL UCB222
U 1 1 603046AC
P 7750 3050
F 0 "UCB222" H 7750 3667 50  0000 C CNN
F 1 "D38GL" H 7750 3576 50  0000 C CNN
F 2 "" H 7750 3575 50  0001 C CNN
F 3 "" H 7750 3575 50  0001 C CNN
	1    7750 3050
	1    0    0    -1  
$EndComp
$Comp
L A5C:D38GL UCA211
U 1 1 60303E77
P 7750 4200
F 0 "UCA211" H 7750 4817 50  0000 C CNN
F 1 "D38GL" H 7750 4726 50  0000 C CNN
F 2 "" H 7750 4725 50  0001 C CNN
F 3 "" H 7750 4725 50  0001 C CNN
	1    7750 4200
	1    0    0    -1  
$EndComp
Wire Bus Line
	4500 1900 4500 3400
$EndSCHEMATC
