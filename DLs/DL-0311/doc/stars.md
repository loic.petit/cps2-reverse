# CPS-A Star Fields

The CPS-A & CPS-B provides two star-fields `STAR1` and `STAR2`. For both fields, one pixel will be drawn
every 32 pixels. The total star field size is 512 (16 stars * 32 pixels) x 512.

This page only presents the CPS-A part of this management. To look at how stars are drawn, look at the 
[CPS-B Star Raster](../../DL-0921/doc/star-raster.md) documentation.

## Registers
There are 4 registers able to control the star field in the CPS-A.

| Register | Address |
| --- | --- |
| `STAR1-X` | 0x18 |
| `STAR1-Y` | 0x1A |
| `STAR2-X` | 0x1C |
| `STAR2-Y` | 0x1E |

All registers are 9 bits. Similar to SCROLL offset registers, the CPS-A will deal with separating inter-block shifts
using the XS bus and stars blocks for the CPS-B raster.

## Graphics Address
There are no section in the RAM to deal with the star fields, hence the absence of `STAR{n}-BASE` registers. Aside the
[ROM layout consideration](graphics-rom-layout.md), the `ROMA[0..19]` star selection address is hardwired using the
following pattern: `0b0000 000y xxxx yyyy yyyy`.

* `x` represents the horizontal star to display (0-15, one every 32 pixels).
* `y` represents the vertical coordinate (0-511). Please note that the most significant bit is separated from the rest.

## Related Topics
* [CPS-B Star Raster](../../DL-0921/doc/star-raster.md)
* [Graphics ROM Layout](graphics-rom-layout.md)