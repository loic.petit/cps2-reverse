# CPS-A Palette Copy

The purpose of the palette copy phase has been described in [this CPS-B documentation](../../DL-0921/doc/palette-copy.md).
This page will mainly focus on the CPS-A specifics about the palette management.

## RAM Layout
The developer can transmit data into the palette RAM using a section in the work RAM.<br>
The offset of the section is controlled using the `PALETTE-BASE` register at address `0x0A`.<br>
The offset is masked by `0xFFFC` and multiplied by 256 to get the final address.

Bit 0 of this register is used to mark the flag `ONE-SECTION`.<br>
This indicates that the copy of the palette should only take one section instead of the whole palette size.

The structure of the RAM is pretty straightforward. The RAM size cannot extend more than 0x1800 (because that's the
maximum number of palettes possible in CPS-1 and CPS-2). At every address of this section there is a 16 bit word
represents the colour.

## Palette word
In the case of the CPS-1 and CPS-2, a palette word looks like this:

| Bit | Sprite | 
| --- | --- |
| 12..15 | `F[0..3]` |
| 8..11 | `R[0..3]` |
| 4..7 | `G[0..3]` |
| 0..3 | `B[0..3]` |

`F` is the brightness factor which is applied to every colour. Obviously the final colour value will depend on the DAC.
In the case of the CPS-1 and CPS-2, all colours are balanced and the final value is _roughly_ the value of 
`C * (1 + 2.2 * F/15)`. To have a 255-normalized value multiply by `255/48`. (_Reminder: this is __not__ 100% accurate as it is
in practice not 100% linear_).

## WSTLUT Phase
When, _and only when_, a write operation on `PALETTE-BASE` happens, the `LUTPRO` becomes `HIGH` immediately which
indicates that a copy between RAM and palette is desired. This will eventually trigger a [bus request](bus-requests.md)
during the next `VBLANK`. When the bus is granted, the CPS-A enters in `WSTLUT` phase.

During this phase, the CPS-A will iterate over the RAM addresses of the palette section on the bus regardless of the
CPS-B configuration (with the section copy). Therefore, the CPS-A will iterate over 0x1800 addresses unless the flag
`ONE-SECTION` is active, if that's the case only `0x0400` will be browsed. The CPS-B will decide if it's worthwhile to
write to the palette depending on its configuration.

When the iteration is done, `LUTPRO` becomes low instantly and `WSTLUT` changes shortly after.

Note: because `0x1800` addresses can be copied with an address-base masked by `0xFFFC000`, there is a bit of
overlap but, an adder is there to handle such cases.

Note that if the RAM is modified, no changes will be applied on the screen unless the developer writes on `PALETTE-BASE`.

### Timing considerations
Because the palette ram needs exclusive access to the main bus for the copy, and because the palette RAM is busy during
rasterisation, the copy can ONLY happen during blanking. However, the copy takes `0x1800` 4Mhz strobes, and the VBLANK
contains a total of `0x2600` strobes. Moreover, there are other operations (scroll & sprites) to perform during this
period.

Therefore, if the developer wants to ensure that the copy operation is fully performed before the new raster starts.
Fortunately, the 68k processor has IPL1 wired to VBLANK, so it can be done with an interrupt easily. It is known that
the flag `ONE-SECTION` is rarely used however it can gain a lot of cpu cycles in some cases (a full palette copy takes
roughly 9% of the CPU performance).

If the copy is interrupted (by a new LI strobe for instance, or by the rasterization starting), all operations are 
paused until the next time the bus becomes available. In that case, it is resumed from the latest state. During
interruptions `~WSTLUT` becomes HIGH again but `LUTPRO` stays HIGH.
