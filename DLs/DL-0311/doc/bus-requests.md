# CPS-A Bus Requests

Contrary to the CPS-B, the CPS-A can request a total control on the main 68k bus. It uses it to consult the RAM and
transmit data to other components (palette, graphics ROM).

## Bus Request Primer
_(This section is just a reminder about how 68k bus requests behave and how CPS-1/2 handles them)_

When any module wants to control the address bus of a 68000 CPU.
* This module requests a bus request by setting the `~BR` to LOW.
* Shortly after, the CPU will bring `~BG` to LOW (_bus grant_).
* The module must bring `~BGACK` to LOW to acknowledge that it has received the bus rights.

On the CPS-1, the CPS-A is the only one which can access the bus. Therefore, the management of this bus is just a matter
of correctly syncing the signals (easier said than done because of clock cycles).

On the CPS-2, the CPS-A is still the only one to request, however the timing is a bit more tricky to handle. In this
version, it is possible to read and write directly on the sound system RAM. So, accessing this data requires to do
a bus request to the sound CPU (the Z80) to free the access to the RAM. Therefore, while the Z80 has freed the access
to the RAM, it becomes impossible to do a bus requests. You can see the exact timings in
[this part of the DL-1827](https://petitl.fr/cps2/DL-1827/#control-states).

## Request Intentions
The CPS-A will request the bus only when `LI` becomes HIGH, meaning, when a new scanline starts. At this point,
it will look at which module needs the bus and then grants it access. To do so, each module will declare its intention.

There are 7 possible intentions:
* [Scroll layer operations](scroll.md)
  * ROWSCROLL
    * Every line in range `[15, 240)`
  * SCROLL1
    * If `line = 1 mod 8` and in range `[8, 248)`
  * SCROLL2
    * If `line = 0 mod 16` and `line < 240`
  * SCROLL3
    * If `line = 0 mod 32` and `line < 240`, or `line == -2`
* [Sprites (WSTOBJ)](sprites.md)
  * Every line except 239-244
* [Palette Copy (WSTLUT)](wstlut.md)
  * If `PALETTE-BASE` has been written to and during VBLANK
* [Graphics ROM Copy (WSTROM)](wstrom.md)
  * If `WSTROM-COPY` has been written to

Note: similar to [CPS-B](../../DL-0921/doc/video-signals.md), the line counter goes from -3 to 258.

Naturally, multiple requests can happen and the bus request module can only grant one at a time.

## Bus State
The bus has 8 possible states: one for each module, and one for the `idle` operation.

Let's assume that the bus is `idle` as a starting point. When `LI` goes high, the state machine will look at the
intentions that are declared. It will pick the first in this priority order (top is first to be picked):
1. WSTROM
2. WSTOBJ
3. ROWSCROLL
4. SCROLL1
5. SCROLL2
6. SCROLL3
7. WSTLUT

Assuming that one gets picked, let's say `WSTOBJ`. Because the bus is not `idle` anymore, a bus request will be
performed on the 68000. Once the bus has been granted and acked, the `~BUS-WSTOBJ` becomes LOW.

When the module indicates that it has finished its tasks (using a dedicated signal here `WSTOBJ-END`), the state
machine looks at the priority list and the intentions to look what is next one the list, let's say `ROWSCROLL`.
The bus request has already been requested, so the `~BUS-ROWSCROLL` becomes LOW immediately.

When no other intentions are available, the bus transitions to `idle` again and `~BR` becomes HIGH instantly.

Here are some implementation notes to consider:
* It is not possible to go back in the priority list. For instance, if `WSTROM-COPY` gets written during `SCROLL2`,
the `WSTROM` bus request will not happen until the next `LI` pulse.
* It is possible that a bus request takes longer than one scanline. However, `LI` acts as a universal "end-of-task"
signal and the state machine is reset to the highest task in the priority list.
  * Example 1: `WSTROM` takes several frames, but it is the highest on the priority list, so it continues and other
operations are discarded.
  * Example 2: `WSTLUT` takes several lines, but on line 250, `WSTOBJ` has a higher priority, so it will switch to 
this operation first and then resumes `WSTOBJ`.

_Reverse engineering note: The state machine implementation is pretty complex and most operations were understood by
simulating the bus operations using the schematics and net list._

## Performance Notes
A bus request is a heavy operation because it halts the CPU operations. Therefore, the less bus request you can do,
the faster the program will run. To understand how much the bus request takes, here is a table that represents
the percentage of a frame time taken to perform the request.

| Request | Time | Control |
| --- | --- | --- |
| WSTROM | 391% | Write on `WSTROM-COPY` |
| WSTOBJ | 1.9% | None |
| ROWSCROLL | 0.3% | `VIDEO-CONTROL` register |
| SCROLL1 | 4.2% | `VIDEO-CONTROL` register |
| SCROLL2 | 2.1% | `VIDEO-CONTROL` register |
| SCROLL3 | 0.3% | `VIDEO-CONTROL` register |
| WSTLUT | 9.2% | Write on `PALETTE-BASE` |
| WSTLUT | 1.5% | (using the `ONE-SECTION` flag) |

This is a rough approximation which doesn't take into account the time it takes to perform the bus requests and other
sync issues.