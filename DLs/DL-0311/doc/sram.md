# CPS-A SRAM Technical Details

The CPS-A contains a SRAM to be able to raster scroll tile maps.
This documentation assumes that you know [SCROLL](scroll.md) operations.

## Physical Layout

This SRAM takes a large amount of space inside the die area as can be seen in this image.

![SRAM](sram.png)

It is split in two big blocks. Both blocks contain 256 addresses. The left one is able to store 10 bits
for scroll attributes and the one on the right stores 16 bits for the tile code.

They both share the same data input lines and the same address lines.

## Usage
During the raster, the CPS-A is not able to look in realtime at the RAM section where the scroll entries are.
To solve this issue, during HBLANK, it will ask for a bus request and copy the necessary data into its SRAM.

The invariant is the following: at any point in time of the raster, the SRAM contains current necessary entries.

It has to store two rows because Y shifts makes rows appear on a shifted grid while the bus requests happen at
a fixed timing. The bus requests timing management takes care of doing the requests at the right time (and prepares
the first row during VBLANK).

## Address Layout

The following table shows the address usage of the SRAM.

<table>
    <thead>
        <tr>
            <th>Address Bit</th>
            <th>SCROLL1</th>
            <th>SCROLL2</th>
            <th>SCROLL3</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>7</td><td>/LINE3</td><td>/LINE4</td><td>LINE5</td>
        </tr>
        <tr>
            <td>6</td><td>0</td><td>1</td><td>1</td>
        </tr>
        <tr>
            <td>5</td><td colspan=2>X5</td><td>1</td>
        </tr>
        <tr>
            <td>4</td><td colspan=2>X4</td><td>1</td>
        </tr>
        <tr>
            <td>3</td><td colspan=3>X3</td>
        </tr>
        <tr>
            <td>2</td><td colspan=3>X2</td>
        </tr>
        <tr>
            <td>1</td><td colspan=3>X1</td>
        </tr>
        <tr>
            <td>0</td><td colspan=3>X0</td>
        </tr>
    </tbody>
</table>

It is important to note that the `X[0..5]` can be subtle. In general this is the index of the drawn tile on the fixed
raster grid. For instance, on SCROLL1, there are 48 tiles to be drawn + 1 for shifts, hence the 6 bits of addresses,
0 to 48 (0b110000).

However, ROWSCROLL makes the matter more complex for SCROLL2 as it can not predict what the shift will be.
To solve this issue, the SRAM contains all the data for all possible values of the current ROWSCROLL. A ROWSCROLL is
on 9 bits. Because we draw 16 pixels blocks, this makes 23 possible values for block shifts due to ROWSCROLL. If we add
the 24 tiles to be drawn and the additional one to take pixel shifts into account. We get a total of 48 possibles values.
The address will span from 0 to 0b101111.

This is why, 0b11xxxx can be safely used by SCROLL3 without risking a collision. However, as mentioned in the
[SCROLL](scroll.md), if the ROWSCROLL returns a value higher than 384, it will tap into SCROLL3 data.
