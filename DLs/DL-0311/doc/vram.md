# CPS-1 Sprites Video RAM

It is impossible to talk about the CPS-A and its relation with the CPS-B without mentioning the big array
of RAMs that stands in between.

## Purpose
On the CPS-1 A-Board, there are 12 HM53461 chips. Those chips are CMOS Video DRAM 64k x 4 bits in SAM.
The role of this RAM is to store whole frame buffers of the sprites.

Because the sprites can appear anywhere on the screen, it is hard for the video render to synchronize a
tilemap rendering with random positioning of sprites (which can overlap etc...). Therefore, the idea of
this layer is to create a double-buffer of the pre-calculated frame. Therefore, all sprites on the CPS-1
(and CPS-2 for that matter) are by design rendered 1 frame latter.

When a frame is displayed, during the raster in the background, the CPS-A and B will coordinate to:
* Read the frame in the buffer A
* Write the next frame in the buffer B using random access to write.

## RAM Layout
The entire VRAM is split is two banks A and B. Each bank has its own signals and buses. Nothing is shared
between the two. The bank will alternate between read and write every frame. Within one bank, the chips
are split in two. Here is a screenshot of one of the memory bank from the leaked CPS-1 schematics.

![CPS-1 Sprites RAM](vram.png)

In gray are parts that are never used in any versions of the A-board, so they can be safely ignored.
The layout shows that the structure of the memory is split in two (top and bottom) with all buses shared.

Let's look at how the data is spread now. The HM53461 is a RAM which is addressed using a RAS/CAS mechanism.
Meaning, instead of giving the address of the data we want to access, we give its coordinates in a grid.
The grid of this RAM is organized in 256 columns x 256 rows x 4 bits.

Assuming the video counters `(X, Y)`, here is how the data is organized:
* The `ROW[0..7]` corresponds to the scanline
* The `COLUMN[0..7]` corresponds to `X[1..8]` on:
  * Top memory if `X[0]`
  * Bottom memory if `X[0]`
* The `DATA` corresponds to:
  * `DATA[0..3]` is the color within the palette (provided by CPS-B using `FXD[0..3]`)
  * `DATA[4..8]` is the palette ID.
  * (unused) `DATA[9..12]` would contain a counter incremented every 16 sprites copied.

These are roughly the idea and still need a bit of confirmation, it can be way more complex to handle 
flips and overflow managements.

## Timings
This is the hard part because CPS-A and CPS-B need to agree on the same timings. Plus, we have a bunch
of data to organise on every frame, and we don't have a lot of free clock cycles to spare. Therefore,
the timings are pretty hard to reverse. Hence the following warning:

:warning:  __This part is still VERY work-in-progress and could contain dummy theories.__

### Writing
As we can see on the [Sprites](sprites.md) documentation, on every scanline we copy the instructions to
display one 16x16 part of a sprite. Similar to the sprite management, it contains a bunch of counters to
handle which block is displayed, which row, and which pixel. Then, we can use a set of adders to bring
the adder to the target value for the ROW / COLUMN of the address.

Every 32 pixels, the controller will point to the correct address to write the row of 16 pixels one by
one, by sending strobe to CAS on the right row of chips. With 512 points in the pixel counter, it makes
possible the writing of 16 rows but synchronization is key.

### Reading
The reading part is a bit more straightforward. The data is read a pumped using the video counter addressing
(more or less) and the output is sent to CPS-B using the `XSD[0..8]` bus for raster.

## CPS-2
Why is this section only relevant to CPS-1? On the CPS-2, the designers decided that they needed another sprite engine,
but they kept the same tilemap layers. Hence, all the VRAM port are completely unplugged on the CPS-2 for the CPS-A
and the CPS-B.

The VRAM still exists but it's been simplified to only two HM514260 which are just bigger. They hold 512 x 512 x 16 bits
each. Note that the last 4 bits are completely ignored.

Access to it is guarded by the DL-1625 and DL-2227. Exact purpose of each is still unknown, but they act as a
controller and also an additional composition on top of the CPS-B.
