# CPS-A Graphics ROM Layout

The CPS-A main job is to drive the graphics ROM address bus. To do so, it assumes the ROM layout to a set of fixed
address ranges for the different layers using the following table.

| Layer | `ROMA[22..20]` |
| --- | --- |
| Sprites | `0b000` |
| Scroll 1 | `0b001` |
| Scroll 2 | `0b010` |
| Scroll 3 | `0b011` |
| Stars | `0b100` |

Moreover, as mentioned in the [CPS-B video signals](../../DL-0921/doc/video-signals.md) documentation, the graphics
ROM are split in two and the same address after 1 clock cycle of 2MHz will not use the same graphics ROM bank (STAR1
and STAR2 are on separated banks for instance, hence the share of the same address).

## CPS 1
On the CPS1, the ranges are fully managed by a unique-per-game PAL chip that will convert the given range to the actual
graphics ROM layout. This is most annoying to deal with when doing converts as it is required to reproduce the same
ranges.

The list of known roms and the mapping of the graphics ROM has been referenced by
[MAME](https://github.com/mamedev/mame/blob/master/src/mame/video/cps1.cpp#L1679) (look for `cps1_config_table`). One
good example is strider which has all layers available.

## CPS 2
On the CPS2, the CPS-A only takes care of the SCROLL & STARS and restricts it to a shorter range. `ROMA[20..22]` is
therefore _completely_ ignored. The rest of the bus is transmitted to the 
[DL-1927](https://petitl.fr/cps2/93646B/#gfx-addr-bus) using address mangling for ROM obfuscation.

The address of the CPS-A is converted by adding `0b01` on top and by shifting bit 17 to bit 0. All the rest of the
addresse range can used by sprites.

The layout of the ROMs is on the other hand more-or-less standardized (some games have expansion boards). The base
layout is: ROMS 13 & 15 for CHANNEL A and ROMS 17 & 19 for CHANNEL B. If needed 14, 16, 18 and 20 can be used as 
expansions.