# CPS-A Scroll Management

## Layers definition
There are three available layers in the CPS.

* `SCROLL1` is a 8x8 pixels tile map.
* `SCROLL2` is a 16x16 pixels tile map.
* `SCROLL3` is a 32x32 pixels tile map.

All layers are 64 x 64 grids.

## RAM Layout
The developer can control the content of the tiling with a RAM section.

The offset of the section is controlled by the following registers

| Register | Address |
| --- | --- |
| `SCROLL1-BASE` | 0x02 |
| `SCROLL2-BASE` | 0x04 |
| `SCROLL3-BASE` | 0x06 |

All values are masked by `0xFFC0` and multiplied by 256 to get the final address.

Example: `0x92C0` will point to the address `0x92C000` (and so will `0x92C4`).

### Tile Entry
A tile entry consists of two 16 bits words.

* The first one is the tile code (see the Graphics ROM Addresses section)
* The second one is the attribute data:

| Bits | Content | Description |
| --- | --- | --- |
| 9 | `CDEN` | Unused |
| 7..8 | `CDC[0..1]` | Priority group |
| 6 | Y Flip | Mirrored vertically |
| 5 | X Flip | Mirrored horizontally |
| 0..4 | `CB[0..4]` | Palette ID used to render the tile |

### Tile Addresses

Inside the range covered by the offset of the base provided by the register, the 64 x 64 grid entries
are addressed as such.

<table>
    <thead>
        <tr>
            <th>Address Bit</th>
            <th>SCROLL1</th>
            <th>SCROLL2</th>
            <th>SCROLL3</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>13</td><td colspan=3>Y5</td>
        </tr>
        <tr>
            <td>12</td><td>X5</td><td colspan=2>Y4</td>
        </tr>
        <tr>
            <td>11</td><td>X4</td><td>X5</td><td>Y3</td>
        </tr>
        <tr>
            <td>10</td><td>X3</td><td>X4</td><td>X5</td>
        </tr>
        <tr>
            <td>9</td><td>X2</td><td>X3</td><td>X4</td>
        </tr>
        <tr>
            <td>8</td><td>X1</td><td>X2</td><td>X3</td>
        </tr>
        <tr>
            <td>7</td><td>X0</td><td>X1</td><td>X2</td>
        </tr>
        <tr>
            <td>6</td><td>Y4</td><td>X0</td><td>X1</td>
        </tr>
        <tr>
            <td>5</td><td colspan=2>Y3</td><td>X0</td>
        </tr>
        <tr>
            <td>4</td><td colspan=3>Y2</td>
        </tr>
        <tr>
            <td>3</td><td colspan=3>Y1</td>
        </tr>
        <tr>
            <td>2</td><td colspan=3>Y0</td>
        </tr>
        <tr>
            <td>1</td><td colspan=3>(tile/attr)</td>
        </tr>
    </tbody>
</table>

In case of overflow during the scrolling, the coordinate space naturally wraps around.

Example: if it tries to display tile (60, 40) with an offset of (6, 2), the tile (2, 42) will be displayed.

## Offsets
All tile layers can be shifted during the render using the following registers.

| Register | Address |
| --- | --- |
| `SCROLL1-X` | 0x0C |
| `SCROLL1-Y` | 0x0E |
| `SCROLL2-X` | 0x10 |
| `SCROLL2-Y` | 0x12 |
| `SCROLL3-X` | 0x14 |
| `SCROLL3-Y` | 0x16 |

All registers are 10 bits only. The CPS-A chip will then automatically deal with either offsetting
rows/columns in the tile entry address or shifting the raster parameters.

## ROWSCROLL
The ROWSCROLL mechanism is a feature of the SCROLL2 tile map.

On every row, the X offset will be shifted by an amount that is found on the RAM.

| Register | Address | Description |
| --- | --- | --- |
| `ROWSCROLL-BASE` | 0x08 | Base of the RAM for the ROWSCROLL |
| `ROWSCROLL-OFFSET` | 0x20 | Offset of the `ROWSCROLL` index |

The `ROWSCROLL-BASE` is masked by `0xFFF8` and multiplied by 256 to get the final address.  The `X` offset becomes now for each `ROW`:

```SCROLL2_X + RAM[BASE << 8 | OFFSET + ROW] & 0x1FF```

The `OFFSET+ROW` operation is done in 10 bits (1024 total values).

Even thought the value is 9 bits long, the value **must** be between 0 and 383. Going above that might induce glitches 
with the SCROLL2 using the SCROLL3 data with overflow (see the [SRAM](sram.md) section).

## Feature Switch

It is possible to enable/disable the different layers. On the CPS-B the feature flag will choose
what to raster. In the CPS-A case, this will disable the bus requests related to each layer 
(and therefore the RAM read).

This is controlled by the `VIDEO-CONTROL` register on address `0x22`.

| Bit | Address |
| --- | --- |
| 15 | Screen Flip |
| 3 | Enable SCROLL3 |
| 2 | Enable SCROLL2 |
| 1 | Enable SCROLL1 |
| 0 | Enable ROWSCROLL |

Understandably, most games set this to `0xe` and don't touch anything.
Note that if the feature is set and then disabled, it will leave the internal state the same
(registers and [SRAM](sram.md). Furthermore, disabling `SCROLL{n}` will allow for less bus requests and 
more cycles on the CPU.

## Raster Technical Details
As detailed in the [tile map viewer sample implementation](https://gitlab.com/loic.petit/cps2-reverse/-/snippets/2040959),
the raster is mainly done by drawing 16 pixels lines (8x8 is done by feeding the same data on both channels, 32x32 is
done by drawing two 16 pixel lines).

The [graphics ROM timings](graphics-rom-timings.md) shows that the data is read at a fixed pace. This shows that the tilemap is actually drawn in
a 16 pixel grid. X-shift are handled by the CPS-B using the XS bus and Y-shifts are handled by the CPS-A which selects
the right address on the 16 pixel grid. What this also means is that the loading of the first tile is done before
/HBLANK to allow to load the first tile if shifted.

## Graphics ROM Addresses
Aside the [ROM layout consideration](graphics-rom-layout.md), the `ROMA[0..19]` has a different shape across the
different tile maps.

| Bit | SCROLL1 | SCROLL2 | SCROLL3 | 
| --- | --- | --- | --- |
| 19 | 0 | `TILE15` | `TILE13` |
| 6..18 | `TILE[3..15]` | `TILE[2..14]` | `TILE[0..13]` |
| 5 | `TILE2` | `TILE1` | `YFLIP(Y4)` |
| 4 | `TILE1` | `TILE0` | `YFLIP(Y3)` |
| 3 | `TILE0` | `YFLIP(Y3)` | `YFLIP(Y2)` |
| 2 | `YFLIP(Y2)` | `YFLIP(Y2)` | `YFLIP(Y1)` |
| 1 | `YFLIP(Y1)` | `YFLIP(Y1)` | `YFLIP(Y0)` |
| 0 | `YFLIP(Y0)` | `YFLIP(Y0)` | `XFLIP(SB)` |

* The FLIP operation is `FLIP(A) = ~FLIP xnor A`.
* `XFLIP` and `YFLIP` are provided by the tile attributes.
* `SB` is the _second block_ in the 32x32 layer (because 32 pixels are drawn in two passes of 16 pixels).
* `Y` is the line number within one tile

## Related Topics
* [Bus Requests](bus-requests.md)
* [SRAM](sram.md)
* [Graphics ROM Layout](graphics-rom-layout.md)
