# CPS-A Sprites Management

The Sprite layer is a layer which draws 16x16 tiles at any place on the screen. In the rendering, it has a specific
priority to be on top of the rest of the layers.

**Important**: because of hardware limitation of the CPS-A, it cannot display more than 256 tiles per screen.

## RAM Layout
The developer can control the list of sprites using a section in RAM.<br>
The offset of the section is controlled using the `OBJRAM-BASE` register at address `0x00`.<br>
The offset is masked by `0xFFC0` and multiplied by 256 to get the final address.

Example: `0x92C0` will point to the address `0x92C000` (and so will `0x92C4`).

Each entry in this section will be addressed using 

`BASE << 8 + IDX << 3`.

Moreover, the bit 0 of this register control another mode of addressing (unused by known games). If this bit is set each entry will
be addressed using 

`BASE << 8 + IDX << 6`.

### Sprite Entry
A sprite entry consists of four 16 bits words:
* X coordinate
* Y coordinate
* Tile code
* Attributes composed as such:

| Bits | Content | Description |
| --- | --- | --- |
| 0..4 | `CB[0..4]` | Palette ID used to render the tile |
| 5 | X Flip | Mirrored horizontally |
| 6 | Y Flip | Mirrored vertically |
| 7 | `LOOKUP` | Looks up the `CB` value into the RAM for each tile (see later section) |
| 8..11 | `XB[0..3]` | Horizontal size in tiles |
| 12..15 | `YB[0..3]` | Vertical size in tiles |

In case of multi-tile sprites, the tile code becomes as such: for a tile coordinate `(x,y)`: `TILE + x + y << 4`.

**Important**: The additions are all performed in 4 bits groups and there is no carry across the groups. For instance,
if you have the tile code `0x4e`, and you want to create a multi-tile with a tile at `(0,3)`, the result will be tile
`0x41` and not `0x51`.

### Colour Lookup
There is a mode that is to my knowledge not used: the lookup. If the flag is activated, then the CPS-A will look up
in the RAM to find the `CB` value to set for this specific tile.

The lookup address is the following for a BASE, a CB value and a (x,y) tile coordinate within the sprite.

`(BASE ^ 0x40) << 8 + CB << 9 + y << 5 + x << 1`

At this address, the five LSB will override the current CB value for this tile.

Example: say you write `0x2385` in attribute. You define a 3x2 sprite, with lookup in entry 5. Assuming you have `0x92C0`
in the base.
* The tile at (0,0) will use the palette ID at the address `0x928A00` (`0x928000` is the base of the lookup + `5 << 9`)
* The tile at (1,0) will use the palette ID at the address `0x928A02`
* The tile at (2,0) will use the palette ID at the address `0x928A04`
* The tile at (0,1) will use the palette ID at the address `0x928A20`
* The tile at (1,1) will use the palette ID at the address `0x928A22`

**Important**: the lookup assumes that you have a tile code as a multiple of 256. If your starting tile is
`0x4e`, then your lookup coordinate will start at `(x=14,y=4)`. Remember that coordinates in multiblocks are wired in
4 bits so, it will loop back to 0 if needed.

## Bus Operations
As seen on the [bus requests](bus-requests.md) documentation, on almost every scanline, a bus request is sent (`WSTOBJ`) 
to copy some data from RAM. It is actually a 5 cycle operation which is executed in this order.
1. Fetch attributes
2. Fetch the tile
3. Fetch the Y offset
4. Fetch the X offset
5. Perform the lookup (if necessary)

Every scanline corresponds to one tile. Therefore, in case of multi-tile sprites, only the colour part of the attributes
and the lookup part are effectively executed.

## Graphics ROM Addresses
The CPS-A keeps track of each tile coordinates within the sprite to create the graphics ROM address. Aside the
[ROM layout consideration](graphics-rom-layout.md), the `ROMA[0..19]` bus is layed out as such: 

| Bit | Sprite | 
| --- | --- |
| 12..19 | `BASE[0..7]` |
| 8..11 | `TILE-Y[0..3]` |
| 4..7 | `TILE-X[0..3]` |
| 0..3 | `YFLIP(Y[0..3])` |

* The FLIP operation is `FLIP(A) = ~FLIP xnor A`.
* `YFLIP` is provided by the sprite attribute.
* (`TILE-X`, `TILE-Y`) is the coordinate of the tile inside the sprite
* `Y` is the line number within one tile
