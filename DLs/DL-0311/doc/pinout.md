# CPS-A-01 Pinout

| Pin | Name | Type |
| --- | --- | --- |
| 1 | ~RD | Input |
| 2 | ~CS | Input |
| 3 | ~TEST3 | Input |
| 4 | CLK | Input |
| 5 | ~TEST2 | Input |
| 6 | ~TEST1 | Input |
| 7 | AA0 | Input |
| 8 | AA1 | Input |
| 9 | AA2 | Input |
| 10 | AA3 | Input |
| 11 | AA4 | Input |
| 12 | AA5 | Input |
| 13 | AA6 | Input |
| 14 | AA7 | Input |
| 15 | ~ACAS0 | Input |
| 16 | ~ACAS1 | Input |
| 17 | ~ASOE0 | Input |
| 18 | ~ASOE1 | Input |
| 19 | GND | Power input |
| 20 | ~ASCK | Input |
| 21 | ~ADTOE | Input |
| 22 | AD4 | Input |
| 23 | AD5 | Input |
| 24 | AD6 | Input |
| 25 | AD7 | Input |
| 26 | AD8 | Input |
| 27 | AD9 | Input |
| 28 | AD10 | Input |
| 29 | AD11 | Input |
| 30 | AD12 | Input |
| 31 | ~FWE | Input |
| 32 | ~FRAS | Input |
| 33 | BA0 | Input |
| 34 | BA1 | Input |
| 35 | BA2 | Input |
| 36 | BA3 | Input |
| 37 | BA4 | Input |
| 38 | BA5 | Input |
| 39 | BA6 | Input |
| 40 | BA7 | Input |
| 41 | ~BCAS0 | Input |
| 42 | ~BCAS1 | Input |
| 43 | ~BSOE0 | Input |
| 44 | ~BSOE1 | Input |
| 45 | VCC | Power input |
| 46 | ~BSCK | Input |
| 47 | ~BDTOE | Input |
| 48 | BD4 | Input |
| 49 | BD5 | Input |
| 50 | BD6 | Input |
| 51 | BD7 | Input |
| 52 | BD8 | Input |
| 53 | BD9 | Input |
| 54 | BD10 | Input |
| 55 | BD11 | Input |
| 56 | BD12 | Input |
| 57 | ~REWEN | Input |
| 58 | OBJEO | Output |
| 59 | ~OBJUP | Input |
| 60 | FRAME | Output |
| 61 | XS0 | Output |
| 62 | XS1 | Output |
| 63 | XS2 | Output |
| 64 | XS3 | Output |
| 65 | XS4 | Output |
| 66 | CPC0 | Output |
| 67 | CPC1 | Output |
| 68 | CDEN | Output |
| 69 | XSEL | Output |
| 70 | GND | Power input |
| 71 | PL0WED | Input |
| 72 | PL1WED | Input |
| 73 | PL2WED | Input |
| 74 | PL3WED | Input |
| 75 | ROMA0 | Input |
| 76 | ROMA1 | Input |
| 77 | ROMA2 | Input |
| 78 | ROMA3 | Input |
| 79 | ROMA4 | Input |
| 80 | ROMA5 | Input |
| 81 | VCC | Power input |
| 82 | ROMA6 | Input |
| 83 | ROMA7 | Input |
| 84 | ROMA8 | Input |
| 85 | ROMA9 | Input |
| 86 | ROMA10 | Input |
| 87 | ROMA11 | Input |
| 88 | ROMA12 | Input |
| 89 | ROMA13 | Input |
| 90 | ROMA14 | Input |
| 91 | ROMA15 | Input |
| 92 | ROMA16 | Input |
| 93 | ROMA17 | Input |
| 94 | ROMA18 | Input |
| 95 | ROMA19 | Input |
| 96 | ROMA20 | Input |
| 97 | ROMA21 | Input |
| 98 | ROMA22 | Input |
| 99 | CB0 | Output |
| 100 | CB1 | Output |
| 101 | CB2 | Output |
| 102 | CB3 | Output |
| 103 | CB4 | Output |
| 104 | FLIP | Output |
| 105 | ~RESETI | Input |
| 106 | ~RESET | Input |
| 107 | CK250 | Input |
| 108 | HSYNC | Input |
| 109 | CK125 | Input |
| 110 | VSYNC | Input |
| 111 | FI | Output |
| 112 | LI | Output |
| 113 | ~WSTROM | Input |
| 114 | WSTLUT | Output |
| 115 | ~WSTOBJ | Output |
| 116 | LUTPRO | Output |
| 117 | ~BR | Output |
| 118 | ~BGACK | Input |
| 119 | GND | Power input |
| 120 | CD0 | 3 State |
| 121 | CD1 | 3 State |
| 122 | CD2 | 3 State |
| 123 | CD3 | 3 State |
| 124 | CD4 | 3 State |
| 125 | CD5 | 3 State |
| 126 | CD6 | 3 State |
| 127 | CD7 | 3 State |
| 128 | CD8 | 3 State |
| 129 | CD9 | 3 State |
| 130 | CD10 | 3 State |
| 131 | CD11 | 3 State |
| 132 | CD12 | 3 State |
| 133 | CD13 | 3 State |
| 134 | CD14 | 3 State |
| 135 | CD15 | 3 State |
| 136 | VCC | Power input |
| 137 | CA1 | Bidirectional |
| 138 | CA2 | Bidirectional |
| 139 | CA3 | Bidirectional |
| 140 | CA4 | Bidirectional |
| 141 | CA5 | Bidirectional |
| 142 | CA6 | Bidirectional |
| 143 | CA7 | Bidirectional |
| 144 | CA8 | Bidirectional |
| 145 | CA9 | Bidirectional |
| 146 | CA10 | Bidirectional |
| 147 | CA11 | Bidirectional |
| 148 | CA12 | Bidirectional |
| 149 | CA13 | Bidirectional |
| 150 | CA14 | Bidirectional |
| 151 | CA15 | Bidirectional |
| 152 | CA16 | Bidirectional |
| 153 | CA17 | Bidirectional |
| 154 | CA18 | Bidirectional |
| 155 | CA19 | Bidirectional |
| 156 | CA20 | Bidirectional |
| 157 | CA21 | Bidirectional |
| 158 | CA22 | Bidirectional |
| 159 | CA23 | Bidirectional |
| 160 | ~WR | Input |
