# CPS-A Graphics ROM Write

This feature is __NOT__ possible on production CPS boards. This allows to copy a big blob of RAM into the Graphics "ROM".
This is only useful for development purposes.

:warning: __DO NOT USE THIS FEATURE ON A RETAIL CPS BOARD THIS COULD DO DAMAGE YOUR HARDWARE__ :warning:

## Operation description
The CPS-A defines the `WSTROM-COPY` register at address `0x24`. It has the following shape:

| Bit | Sprite | 
| --- | --- |
| 9..15 | `RAM-BASE[0..6]` |
| 2..8 | `ROMA-BASE[0..6]` |
| 0..1 | `PL[0..1]` |

When a write operation is performed on this register, a new [bus request](bus-requests.md) will be performed at the next
scanline. When the bus is granted, the copy starts shortly after and the `~WSTROM` signal becomes LOW.

The `PL` selects on bank of ROMs you want to write. It is likely to be wired to select {A,B,C,D} banks of the XPD bus.

The copy will iterate over `0xFFFF` addresses using the base in the register.
* The selected signal `PL{x}WED` will pulse write requests at 2Mhz.
* The addresses will increment at a pace of 1MHz.
* The CPS-B will transmit the data from the data bus to the XPD graphics buses by sending the same 8-bit data.
  * First it sends the MSB then the LSB (hence the faster write speed).
  * The `~CK10` pin on CPS-B can be used to differentiate between LSB and MSB.

This copy will take a while, at least 4 whole frames during which _nothing_ can be done, including raster.
