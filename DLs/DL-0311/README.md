# DL-0311 (CPS-A-01) Reverse Engineering

![DL-0311](doc/DL-0311.png)
![DL-0311 die shot](doc/die.png)

## Render

[Click here to explore the schematics online](https://petitl.fr/cps2/DL-0311/).

[Click here to download a PDF export of the latest version](https://petitl.fr/cps2/DL-0311.pdf).

[![DL-0311](https://petitl.fr/cps2/DL-0311/DL-0311.svg)](https://petitl.fr/cps2/DL-0311/)

## Main Functions

The main functions of this chip are:
* Produces the main video signals: LI and FI.
* Reads the RAM and prepare the layer graphics data (scrolls, sprites, stars).
* Controls the graphics ROM address bus.
* Controls the sprites video RAM with a double-buffer for sprites.
* Provides the CPS-B rasterization parameters (palette id, priority, x-shift, flips).
* Transmits data from RAM -> Graphics ROM (WSTROM), only useful on dev boards.
* Transmits data from RAM -> Palette RAM (WSTLUT)

## Technical documentation

* [Scroll Layers](doc/scroll.md)
* [Scroll SRAM Technical Details](doc/sram.md)
* [Star Fields](doc/stars.md)
* [Sprite Layer](doc/sprites.md)
* [Sprites Video RAM](doc/vram.md)
* [Bus Requests](doc/bus-requests.md)
* [Palette Copy (WSTLUT)](doc/wstlut.md)
* [Graphics ROM Write (WSTROM)](doc/wstrom.md)
* [Graphics ROM Layout](doc/graphics-rom-layout.md)
* [Pinout](doc/pinout.md)

## Schematics notes

* The references of all the gates are all of the following format `UXXy`, `XX` points to the row identifier and `y` is the column on the die.
* The name of the gates are the name of the equivalent RICOH RSC-15 cell. Not all names are exactly as written in the databook but, the given names should be in the same naming scheme.

## Authors & Licence & Thanks
See project [README](../../README.md).

Very special thanks to:
* [John McMaster](https://twitter.com/johndmcmaster) For providing a *very high quality* and delayered die shot and additional tooling
* [Jose Tejada](https://twitter.com/topapate) For helping me understanding some low-tech knowledge about CPS1
* The [MAME developers as a whole](https://www.mamedev.org/) For providing the emulation implementation of CP systems, it's been invaluable
