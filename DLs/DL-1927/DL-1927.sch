EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 8
Title "DL-1927"
Date "2020-05-05"
Rev ""
Comp ""
Comment1 ""
Comment2 "Licence: CC-BY"
Comment3 "Author: Loic *WydD* Petit"
Comment4 "Project: CPS2 Reverse Engineering: DL-1927"
$EndDescr
Wire Wire Line
	2050 950  2150 950 
Wire Wire Line
	2050 1050 2150 1050
Wire Wire Line
	2050 1150 2150 1150
Wire Wire Line
	2050 1250 2150 1250
Wire Wire Line
	2050 1350 2150 1350
Wire Wire Line
	2050 1450 2150 1450
Wire Wire Line
	2050 1550 2150 1550
Wire Wire Line
	2050 1650 2150 1650
Wire Wire Line
	2050 1750 2150 1750
Wire Wire Line
	2050 1850 2150 1850
Wire Wire Line
	2050 1950 2150 1950
Wire Wire Line
	2050 2050 2150 2050
Wire Wire Line
	2050 2150 2150 2150
Wire Wire Line
	2050 2250 2150 2250
Wire Wire Line
	2050 2350 2150 2350
Wire Wire Line
	2050 2450 2150 2450
Wire Wire Line
	2050 2550 2150 2550
Wire Wire Line
	2050 2650 2150 2650
Wire Wire Line
	2050 2750 2150 2750
Wire Wire Line
	2050 2850 2150 2850
Wire Wire Line
	2050 2950 2150 2950
Wire Wire Line
	2050 3050 2150 3050
Wire Wire Line
	2050 5200 2150 5200
Wire Wire Line
	2050 5300 2150 5300
Wire Wire Line
	2050 5400 2150 5400
Wire Wire Line
	2050 5500 2150 5500
Wire Wire Line
	2050 5600 2150 5600
Wire Wire Line
	2050 5700 2150 5700
Wire Wire Line
	2050 5800 2150 5800
Wire Wire Line
	2050 5900 2150 5900
Wire Wire Line
	2050 6000 2150 6000
Wire Wire Line
	2050 6100 2150 6100
Wire Wire Line
	2050 6200 2150 6200
Wire Wire Line
	2050 6300 2150 6300
Wire Wire Line
	2050 6400 2150 6400
Wire Wire Line
	2050 6500 2150 6500
Wire Wire Line
	2050 6600 2150 6600
Wire Wire Line
	2050 6700 2150 6700
Wire Wire Line
	2050 6800 2150 6800
Wire Wire Line
	2050 6900 2150 6900
Wire Wire Line
	2050 7000 2150 7000
Wire Wire Line
	2050 7100 2150 7100
Wire Wire Line
	2050 7200 2150 7200
Wire Wire Line
	2050 7300 2150 7300
Wire Wire Line
	2050 4450 2150 4450
Wire Wire Line
	2050 3800 2150 3800
Wire Wire Line
	2050 3650 2150 3650
Wire Wire Line
	2050 3900 2150 3900
Wire Wire Line
	2050 4250 2150 4250
Wire Wire Line
	2050 4050 2150 4050
Wire Wire Line
	3450 4150 3550 4150
Wire Wire Line
	3450 4250 3550 4250
Wire Wire Line
	3450 4600 3550 4600
Wire Wire Line
	9750 950  9850 950 
Wire Wire Line
	9750 1050 9850 1050
Wire Wire Line
	9750 1150 9850 1150
Wire Wire Line
	9750 1250 9850 1250
Wire Wire Line
	9750 1350 9850 1350
Wire Wire Line
	9750 1450 9850 1450
Wire Wire Line
	9750 1550 9850 1550
Wire Wire Line
	9750 1650 9850 1650
Wire Wire Line
	9750 1750 9850 1750
Wire Wire Line
	9750 1850 9850 1850
Wire Wire Line
	9750 1950 9850 1950
Wire Wire Line
	9750 2050 9850 2050
Wire Wire Line
	9750 2150 9850 2150
Wire Wire Line
	9750 2250 9850 2250
Wire Wire Line
	9750 2350 9850 2350
Wire Wire Line
	9750 2450 9850 2450
Wire Wire Line
	9750 2550 9850 2550
Wire Wire Line
	9750 2650 9850 2650
Wire Wire Line
	9750 2750 9850 2750
Wire Wire Line
	9750 2850 9850 2850
Wire Wire Line
	9750 2950 9850 2950
Wire Wire Line
	9750 3050 9850 3050
Wire Wire Line
	9800 3850 9900 3850
Wire Wire Line
	9800 3950 9900 3950
Wire Wire Line
	9800 4050 9900 4050
Wire Wire Line
	9800 4150 9900 4150
Wire Wire Line
	9800 4250 9900 4250
Wire Wire Line
	9800 4350 9900 4350
Wire Wire Line
	9800 4450 9900 4450
Wire Wire Line
	9800 4550 9900 4550
Wire Wire Line
	9800 4650 9900 4650
Wire Wire Line
	9800 4750 9900 4750
Wire Wire Line
	9800 4850 9900 4850
Wire Wire Line
	9800 4950 9900 4950
Wire Wire Line
	9800 5050 9900 5050
Wire Wire Line
	9800 5150 9900 5150
Wire Wire Line
	9800 5250 9900 5250
Wire Wire Line
	9800 5350 9900 5350
Wire Wire Line
	9800 5450 9900 5450
Wire Wire Line
	9800 5550 9900 5550
Wire Wire Line
	9800 5650 9900 5650
Wire Wire Line
	9800 5750 9900 5750
Wire Wire Line
	9800 5850 9900 5850
Wire Wire Line
	9800 5950 9900 5950
Text Notes 1850 7250 2    50   ~ 0
VCC
Text Notes 1850 7350 2    50   ~ 0
GND
Text GLabel 2050 950  0    50   Input ~ 0
63
Text GLabel 2050 1050 0    50   Input ~ 0
57
Text GLabel 2050 1150 0    50   Input ~ 0
56
Text GLabel 2050 1250 0    50   Input ~ 0
48
Text GLabel 2050 1350 0    50   Input ~ 0
47
Text GLabel 2050 1450 0    50   Input ~ 0
45
Text GLabel 2050 1550 0    50   Input ~ 0
44
Text GLabel 2050 1650 0    50   Input ~ 0
38
Text GLabel 2050 1750 0    50   Input ~ 0
37
Text GLabel 2050 1850 0    50   Input ~ 0
36
Text GLabel 2050 1950 0    50   Input ~ 0
28
Text GLabel 2050 2050 0    50   Input ~ 0
27
Text GLabel 2050 2150 0    50   Input ~ 0
26
Text GLabel 2050 2250 0    50   Input ~ 0
25
Text GLabel 2050 2350 0    50   Input ~ 0
24
Text GLabel 2050 2450 0    50   Input ~ 0
18
Text GLabel 2050 2550 0    50   Input ~ 0
17
Text GLabel 2050 2650 0    50   Input ~ 0
8
Text GLabel 2050 2750 0    50   Input ~ 0
7
Text GLabel 2050 2850 0    50   Input ~ 0
6
Text GLabel 2050 2950 0    50   Input ~ 0
5
Text GLabel 2050 3050 0    50   Input ~ 0
4
Text GLabel 2050 5200 0    50   Input ~ 0
117
Text GLabel 2050 5300 0    50   Input ~ 0
116
Text GLabel 2050 5400 0    50   Input ~ 0
108
Text GLabel 2050 5500 0    50   Input ~ 0
107
Text GLabel 2050 5600 0    50   Input ~ 0
105
Text GLabel 2050 5700 0    50   Input ~ 0
104
Text GLabel 2050 5800 0    50   Input ~ 0
98
Text GLabel 2050 5900 0    50   Input ~ 0
97
Text GLabel 2050 6000 0    50   Input ~ 0
96
Text GLabel 2050 6100 0    50   Input ~ 0
95
Text GLabel 2050 6200 0    50   Input ~ 0
94
Text GLabel 2050 6300 0    50   Input ~ 0
88
Text GLabel 2050 6400 0    50   Input ~ 0
87
Text GLabel 2050 6500 0    50   Input ~ 0
86
Text GLabel 2050 6600 0    50   Input ~ 0
85
Text GLabel 2050 6700 0    50   Input ~ 0
84
Text GLabel 2050 6800 0    50   Input ~ 0
78
Text GLabel 2050 6900 0    50   Input ~ 0
77
Text GLabel 2050 7000 0    50   Input ~ 0
68
Text GLabel 2050 7100 0    50   Input ~ 0
67
Text GLabel 2050 7200 0    50   Input ~ 0
66
Text GLabel 2050 7300 0    50   Input ~ 0
65
Text GLabel 2050 4450 0    50   Input ~ 0
109
Text GLabel 2050 3800 0    50   Input ~ 0
115
Text GLabel 2050 3650 0    50   Input ~ 0
119
Text GLabel 2050 3900 0    50   Input ~ 0
114
Text GLabel 2050 4250 0    50   Input ~ 0
110
Text GLabel 2050 4050 0    50   Input ~ 0
112
Text GLabel 3550 4150 2    50   3State ~ 0
103
Text GLabel 3550 4250 2    50   3State ~ 0
102
Text GLabel 9850 950  2    50   3State ~ 0
100
Text GLabel 9850 1050 2    50   3State ~ 0
99
Text GLabel 9850 1150 2    50   3State ~ 0
93
Text GLabel 9850 1250 2    50   3State ~ 0
92
Text GLabel 9850 1350 2    50   3State ~ 0
90
Text GLabel 9850 1450 2    50   3State ~ 0
89
Text GLabel 9850 1550 2    50   3State ~ 0
83
Text GLabel 9850 1650 2    50   3State ~ 0
82
Text GLabel 9850 1750 2    50   3State ~ 0
80
Text GLabel 9850 1850 2    50   3State ~ 0
79
Text GLabel 9850 1950 2    50   3State ~ 0
74
Text GLabel 9850 2050 2    50   3State ~ 0
73
Text GLabel 9850 2150 2    50   3State ~ 0
72
Text GLabel 9850 2250 2    50   3State ~ 0
70
Text GLabel 9850 2350 2    50   3State ~ 0
69
Text GLabel 9850 2450 2    50   3State ~ 0
64
Text GLabel 9850 2550 2    50   3State ~ 0
62
Text GLabel 9850 2650 2    50   3State ~ 0
59
Text GLabel 9850 2750 2    50   3State ~ 0
58
Text GLabel 9850 2850 2    50   3State ~ 0
54
Text GLabel 9850 2950 2    50   3State ~ 0
53
Text GLabel 9850 3050 2    50   3State ~ 0
52
Text GLabel 9900 3850 2    50   3State ~ 0
50
Text GLabel 9900 3950 2    50   3State ~ 0
49
Text GLabel 9900 4050 2    50   3State ~ 0
43
Text GLabel 9900 4150 2    50   3State ~ 0
42
Text GLabel 9900 4250 2    50   3State ~ 0
40
Text GLabel 9900 4350 2    50   3State ~ 0
39
Text GLabel 9900 4450 2    50   3State ~ 0
34
Text GLabel 9900 4550 2    50   3State ~ 0
33
Text GLabel 9900 4650 2    50   3State ~ 0
32
Text GLabel 9900 4750 2    50   3State ~ 0
30
Text GLabel 9900 4850 2    50   3State ~ 0
29
Text GLabel 9900 4950 2    50   3State ~ 0
23
Text GLabel 9900 5050 2    50   3State ~ 0
22
Text GLabel 9900 5150 2    50   3State ~ 0
20
Text GLabel 9900 5250 2    50   3State ~ 0
19
Text GLabel 9900 5350 2    50   3State ~ 0
14
Text GLabel 9900 5450 2    50   3State ~ 0
13
Text GLabel 9900 5550 2    50   3State ~ 0
12
Text GLabel 9900 5650 2    50   3State ~ 0
10
Text GLabel 9900 5750 2    50   3State ~ 0
9
Text GLabel 9900 5850 2    50   3State ~ 0
3
Text GLabel 9900 5950 2    50   3State ~ 0
2
$Sheet
S 8750 800  1000 2400
U 5EC133CE
F0 "a-out" 50
F1 "a-out.sch" 50
F2 "OUT6" O R 9750 1350 50 
F3 "OUT5" O R 9750 1250 50 
F4 "OUT4" O R 9750 1150 50 
F5 "DATA[2..23]" I L 8750 2000 50 
F6 "OUT7" O R 9750 1450 50 
F7 "OUT15" O R 9750 2250 50 
F8 "OUT12" O R 9750 1950 50 
F9 "OUT13" O R 9750 2050 50 
F10 "OUT14" O R 9750 2150 50 
F11 "OUT20" O R 9750 2750 50 
F12 "OUT21" O R 9750 2850 50 
F13 "OUT23" O R 9750 3050 50 
F14 "OUT22" O R 9750 2950 50 
F15 "OUT2" O R 9750 950 50 
F16 "OUT3" O R 9750 1050 50 
F17 "OUT10" O R 9750 1750 50 
F18 "OUT9" O R 9750 1650 50 
F19 "OUT11" O R 9750 1850 50 
F20 "OUT8" O R 9750 1550 50 
F21 "OUT17" O R 9750 2450 50 
F22 "OUT16" O R 9750 2350 50 
F23 "OUT19" O R 9750 2650 50 
F24 "OUT18" O R 9750 2550 50 
F25 "~ENABLE" I L 8750 3050 50 
$EndSheet
$Sheet
S 2150 5050 1050 2400
U 5EC5326F
F0 "a01-input" 50
F1 "a01-input.sch" 50
F2 "IN18" I L 2150 6800 50 
F3 "IN10" I L 2150 6000 50 
F4 "IN8" I L 2150 5800 50 
F5 "IN9" I L 2150 5900 50 
F6 "IN6" I L 2150 5600 50 
F7 "IN5" I L 2150 5500 50 
F8 "IN7" I L 2150 5700 50 
F9 "IN19" I L 2150 6900 50 
F10 "IN21" I L 2150 7100 50 
F11 "IN20" I L 2150 7000 50 
F12 "~ENABLE" I R 3200 5250 50 
F13 "IN17" I L 2150 6700 50 
F14 "IN16" I L 2150 6600 50 
F15 "IN15" I L 2150 6500 50 
F16 "IN14" I L 2150 6400 50 
F17 "IN13" I L 2150 6300 50 
F18 "IN22" I L 2150 7200 50 
F19 "DATA[2..23]" O R 3200 6250 50 
F20 "IN23" I L 2150 7300 50 
F21 "IN2" I L 2150 5200 50 
F22 "IN4" I L 2150 5400 50 
F23 "IN3" I L 2150 5300 50 
F24 "IN12" I L 2150 6200 50 
F25 "IN11" I L 2150 6100 50 
$EndSheet
$Sheet
S 8750 3700 1050 2400
U 5EB464DD
F0 "b-out" 50
F1 "b-out.sch" 50
F2 "OUT9" O R 9800 4550 50 
F3 "OUT10" O R 9800 4650 50 
F4 "OUT11" O R 9800 4750 50 
F5 "OUT16" O R 9800 5250 50 
F6 "OUT18" O R 9800 5450 50 
F7 "OUT17" O R 9800 5350 50 
F8 "OUT8" O R 9800 4450 50 
F9 "OUT4" O R 9800 4050 50 
F10 "OUT5" O R 9800 4150 50 
F11 "DATA[2..23]" I L 8750 4900 50 
F12 "OUT7" O R 9800 4350 50 
F13 "OUT6" O R 9800 4250 50 
F14 "OUT2" O R 9800 3850 50 
F15 "OUT12" O R 9800 4850 50 
F16 "OUT14" O R 9800 5050 50 
F17 "OUT19" O R 9800 5550 50 
F18 "OUT22" O R 9800 5850 50 
F19 "OUT20" O R 9800 5650 50 
F20 "OUT23" O R 9800 5950 50 
F21 "OUT21" O R 9800 5750 50 
F22 "OUT13" O R 9800 4950 50 
F23 "OUT3" O R 9800 3950 50 
F24 "OUT15" O R 9800 5150 50 
F25 "~ENABLE" I L 8750 4500 50 
$EndSheet
$Sheet
S 2150 800  1050 2400
U 5EB0C9D4
F0 "bus-input" 50
F1 "bus-input.sch" 50
F2 "BUS8" I L 2150 1550 50 
F3 "BUS16" I L 2150 2350 50 
F4 "~ENABLE" I R 3200 3050 50 
F5 "BUS10" I L 2150 1750 50 
F6 "BUS9" I L 2150 1650 50 
F7 "BUS18" I L 2150 2550 50 
F8 "BUS17" I L 2150 2450 50 
F9 "BUS11" I L 2150 1850 50 
F10 "BUS19" I L 2150 2650 50 
F11 "BUS20" I L 2150 2750 50 
F12 "BUS23" I L 2150 3050 50 
F13 "BUS21" I L 2150 2850 50 
F14 "BUS22" I L 2150 2950 50 
F15 "BUS5" I L 2150 1250 50 
F16 "BUS4" I L 2150 1150 50 
F17 "BUS15" I L 2150 2250 50 
F18 "BUS14" I L 2150 2150 50 
F19 "BUS7" I L 2150 1450 50 
F20 "BUS6" I L 2150 1350 50 
F21 "BUS12" I L 2150 1950 50 
F22 "BUS13" I L 2150 2050 50 
F23 "BUS2" I L 2150 950 50 
F24 "BUS3" I L 2150 1050 50 
F25 "DATA[2..23]" O R 3200 2000 50 
$EndSheet
$Sheet
S 4100 1850 850  500 
U 5ECA133F
F0 "bus-memory" 50
F1 "bus-memory.sch" 50
F2 "D[2..23]" I L 4100 2000 50 
F3 "CLK" I L 4100 2200 50 
F4 "Q[2..23]" O R 4950 2000 50 
$EndSheet
$Sheet
S 6100 3150 1000 700 
U 5ECEF190
F0 "data-selector" 50
F1 "data-selector.sch" 50
F2 "A[2..23]" I L 6100 3300 50 
F3 "B[2..23]" I L 6100 3700 50 
F4 "AX~BX" I L 6100 3450 50 
F5 "X[2..23]" O R 7100 3300 50 
F6 "Y[2..23]" O R 7100 3700 50 
F7 "~AY~BY" I L 6100 3550 50 
$EndSheet
$Sheet
S 2150 3500 1300 1250
U 5EE15478
F0 "enable" 50
F1 "enable.sch" 50
F2 "~ENABLE-A01-IN" O R 3450 4600 50 
F3 "~ENABLE-BUS-IN" O R 3450 3650 50 
F4 "_~ENABLE-OUT" I L 2150 4450 50 
F5 "ENABLE" I L 2150 3650 50 
F6 "AX~BX" O R 3450 3950 50 
F7 "BUS-W" O R 3450 3800 50 
F8 "_BUS-W" I L 2150 4050 50 
F9 "SELECT" I L 2150 4250 50 
F10 "ENABLE-SELECT" I L 2150 3900 50 
F11 "~ENABLE-B-OUT" O R 3450 4500 50 
F12 "~ENABLE-A-OUT" O R 3450 4400 50 
F13 "~AY~BY" O R 3450 4050 50 
F14 "~ENABLE-EXT" I L 2150 3800 50 
F15 "EXT-BUS" T R 3450 4150 50 
F16 "EXT-A01" T R 3450 4250 50 
$EndSheet
Wire Wire Line
	3850 2200 4100 2200
Wire Wire Line
	5150 3450 6100 3450
Wire Wire Line
	3450 3950 5150 3950
Wire Wire Line
	6100 3550 5250 3550
Wire Wire Line
	3450 4050 5250 4050
Wire Bus Line
	3200 6250 5400 6250
Wire Wire Line
	3700 3050 3700 3650
Wire Wire Line
	3200 5250 3550 5250
Wire Wire Line
	3550 4600 3550 5250
Wire Wire Line
	8650 3050 8750 3050
Wire Wire Line
	3850 2200 3850 3800
Wire Bus Line
	3200 2000 4100 2000
Text Notes 5850 2950 0    50   ~ 0
The data selector ensures that A and B will be sent to X or Y.\nIn practice, the signals AX~BX~ and ~AY~BY are always\n   the same so it has two positions:\n- A->X and B->Y\n- A->Y and B->X
Text Notes 4000 1650 0    50   ~ 0
The bus is shared with CGD\nso you need some kind of memory\nto hold the address in place
Text Notes 650  1800 0    50   ~ 0
Part of the 32 bit GFX bus\nto the B-Board SPA.
Text Notes 550  6500 0    50   ~ 0
Points to the ROMA address bus\non the CPS-A-01 on the A-Board\n\n\n!!!!!\nOn CPS2, the two MSB\nare hardwired to 0b01
Text Notes 1800 3700 2    50   ~ 0
Points to the main ENABLE signal\ncoming from MIF so... HIGH
Text Notes 3500 3650 0    50   ~ 0
LOW
Text Notes 3500 4500 0    50   ~ 0
LOW
Text Notes 1800 4500 2    50   ~ 0
GND
Text Notes 1800 3850 2    50   ~ 0
GND
Text Notes 1800 3950 2    50   ~ 0
HIGH
Text Notes 1800 4300 2    50   ~ 0
CPS2: Connected to CLK2M
Text Notes 1800 4100 2    50   ~ 0
CPS2: Coming from the SPA
Wire Wire Line
	3450 4500 8750 4500
Wire Wire Line
	3450 4400 8650 4400
Wire Wire Line
	8650 4400 8650 3050
Wire Wire Line
	3850 3800 3450 3800
Text Notes 3500 4600 0    50   ~ 0
LOW
Text Notes 3500 4400 0    50   ~ 0
LOW
Text Notes 3850 3950 0    50   ~ 0
SELECT
Text Notes 3850 4050 0    50   ~ 0
SELECT
Text Notes 3500 3800 0    50   ~ 0
BUS-W
Wire Wire Line
	3450 3650 3700 3650
Wire Wire Line
	3200 3050 3700 3050
Text Notes 10100 1900 0    50   ~ 0
GFXROM address bus\nA-BANK (13-14-15-16)
Text Notes 10150 4650 0    50   ~ 0
GFXROM address bus\nB-BANK (17-18-19-20)
Wire Wire Line
	5250 3550 5250 4050
Wire Bus Line
	5400 3700 5400 6250
Wire Bus Line
	8550 3700 8550 4900
Wire Bus Line
	7100 3300 8500 3300
Wire Bus Line
	8500 3300 8500 2000
Wire Bus Line
	8500 2000 8750 2000
Wire Bus Line
	8550 4900 8750 4900
Wire Bus Line
	7100 3700 8550 3700
Wire Bus Line
	4950 2000 5400 2000
Wire Bus Line
	5400 2000 5400 3300
Wire Bus Line
	5400 3300 6100 3300
Wire Bus Line
	5400 3700 6100 3700
Text Notes 3600 5450 0    50   ~ 0
EXT is there to have an external mux\nIt works only if ENABLE-SELECT is LOW\n  and ~ENABLE-EXT~ is LOW\nIf these conditions are met:\n- EXT-BUS is ~SELECT~\n- EXT-A01 is SELECT\n\nOn CPS2, it is not enabled because\nENABLE-SELECT is HIGH if properly plugged
Wire Wire Line
	5150 3450 5150 3950
$EndSCHEMATC
