EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 8
Title ""
Date "2020-05-05"
Rev ""
Comp ""
Comment1 ""
Comment2 "Licence: CC-BY"
Comment3 "Author: Loic *WydD* Petit"
Comment4 "Project: CPS2 Reverse Engineering: DL-1927"
$EndDescr
Connection ~ 3750 750 
Connection ~ 6150 750 
Connection ~ 3900 900 
Connection ~ 6300 900 
Connection ~ 5400 6300
Connection ~ 10000 6300
Connection ~ 2100 1500
Connection ~ 6700 1500
Connection ~ 1550 750 
Connection ~ 1700 900 
Connection ~ 2100 1650
Connection ~ 6700 1650
NoConn ~ 2800 1450
NoConn ~ 2800 1700
Entry Wire Line
	1550 1900 1650 2000
Entry Wire Line
	1550 2150 1650 2250
Entry Wire Line
	1550 3050 1650 3150
Entry Wire Line
	1550 3300 1650 3400
Entry Wire Line
	1550 3550 1650 3650
Entry Wire Line
	1550 3800 1650 3900
Entry Wire Line
	1550 4700 1650 4800
Entry Wire Line
	1550 4950 1650 5050
Entry Wire Line
	1550 5200 1650 5300
Entry Wire Line
	1550 5450 1650 5550
Entry Wire Line
	1700 1800 1800 1900
Entry Wire Line
	1700 2050 1800 2150
Entry Wire Line
	1700 2950 1800 3050
Entry Wire Line
	1700 3200 1800 3300
Entry Wire Line
	1700 3450 1800 3550
Entry Wire Line
	1700 3700 1800 3800
Entry Wire Line
	1700 4600 1800 4700
Entry Wire Line
	1700 4850 1800 4950
Entry Wire Line
	1700 5100 1800 5200
Entry Wire Line
	1700 5350 1800 5450
Entry Wire Line
	3200 2050 3100 1950
Entry Wire Line
	3200 2300 3100 2200
Entry Wire Line
	3200 3200 3100 3100
Entry Wire Line
	3200 3450 3100 3350
Entry Wire Line
	3200 3700 3100 3600
Entry Wire Line
	3200 3950 3100 3850
Entry Wire Line
	3200 4850 3100 4750
Entry Wire Line
	3200 5100 3100 5000
Entry Wire Line
	3200 5350 3100 5250
Entry Wire Line
	3200 5600 3100 5500
Entry Wire Line
	3750 1400 3850 1500
Entry Wire Line
	3750 1650 3850 1750
Entry Wire Line
	3750 1900 3850 2000
Entry Wire Line
	3750 2150 3850 2250
Entry Wire Line
	3750 3050 3850 3150
Entry Wire Line
	3750 3300 3850 3400
Entry Wire Line
	3750 3550 3850 3650
Entry Wire Line
	3750 3800 3850 3900
Entry Wire Line
	3750 4700 3850 4800
Entry Wire Line
	3750 4950 3850 5050
Entry Wire Line
	3750 5200 3850 5300
Entry Wire Line
	3750 5450 3850 5550
Entry Wire Line
	3900 1300 4000 1400
Entry Wire Line
	3900 1550 4000 1650
Entry Wire Line
	3900 1800 4000 1900
Entry Wire Line
	3900 2050 4000 2150
Entry Wire Line
	3900 2950 4000 3050
Entry Wire Line
	3900 3200 4000 3300
Entry Wire Line
	3900 3450 4000 3550
Entry Wire Line
	3900 3700 4000 3800
Entry Wire Line
	3900 4600 4000 4700
Entry Wire Line
	3900 4850 4000 4950
Entry Wire Line
	3900 5100 4000 5200
Entry Wire Line
	3900 5350 4000 5450
Entry Wire Line
	5300 1450 5400 1550
Entry Wire Line
	5300 1700 5400 1800
Entry Wire Line
	5300 1950 5400 2050
Entry Wire Line
	5300 2200 5400 2300
Entry Wire Line
	5300 3100 5400 3200
Entry Wire Line
	5300 3350 5400 3450
Entry Wire Line
	5300 3600 5400 3700
Entry Wire Line
	5300 3850 5400 3950
Entry Wire Line
	5300 4750 5400 4850
Entry Wire Line
	5300 5000 5400 5100
Entry Wire Line
	5300 5250 5400 5350
Entry Wire Line
	5300 5500 5400 5600
Entry Wire Line
	6150 1900 6250 2000
Entry Wire Line
	6150 2150 6250 2250
Entry Wire Line
	6150 3050 6250 3150
Entry Wire Line
	6150 3300 6250 3400
Entry Wire Line
	6150 3550 6250 3650
Entry Wire Line
	6150 3800 6250 3900
Entry Wire Line
	6150 4700 6250 4800
Entry Wire Line
	6150 4950 6250 5050
Entry Wire Line
	6150 5200 6250 5300
Entry Wire Line
	6150 5450 6250 5550
Entry Wire Line
	6300 1800 6400 1900
Entry Wire Line
	6300 2050 6400 2150
Entry Wire Line
	6300 2950 6400 3050
Entry Wire Line
	6300 3200 6400 3300
Entry Wire Line
	6300 3450 6400 3550
Entry Wire Line
	6300 3700 6400 3800
Entry Wire Line
	6300 4600 6400 4700
Entry Wire Line
	6300 4850 6400 4950
Entry Wire Line
	6300 5100 6400 5200
Entry Wire Line
	6300 5350 6400 5450
Entry Wire Line
	7800 1550 7700 1450
Entry Wire Line
	7800 1800 7700 1700
Entry Wire Line
	7800 2050 7700 1950
Entry Wire Line
	7800 2300 7700 2200
Entry Wire Line
	7800 3200 7700 3100
Entry Wire Line
	7800 3450 7700 3350
Entry Wire Line
	7800 3700 7700 3600
Entry Wire Line
	7800 3950 7700 3850
Entry Wire Line
	7800 4850 7700 4750
Entry Wire Line
	7800 5100 7700 5000
Entry Wire Line
	7800 5350 7700 5250
Entry Wire Line
	7800 5600 7700 5500
Entry Wire Line
	8350 1400 8450 1500
Entry Wire Line
	8350 1650 8450 1750
Entry Wire Line
	8350 1900 8450 2000
Entry Wire Line
	8350 2150 8450 2250
Entry Wire Line
	8350 3050 8450 3150
Entry Wire Line
	8350 3300 8450 3400
Entry Wire Line
	8350 3550 8450 3650
Entry Wire Line
	8350 3800 8450 3900
Entry Wire Line
	8350 4700 8450 4800
Entry Wire Line
	8350 4950 8450 5050
Entry Wire Line
	8350 5200 8450 5300
Entry Wire Line
	8350 5450 8450 5550
Entry Wire Line
	8500 1300 8600 1400
Entry Wire Line
	8500 1550 8600 1650
Entry Wire Line
	8500 1800 8600 1900
Entry Wire Line
	8500 2050 8600 2150
Entry Wire Line
	8500 2950 8600 3050
Entry Wire Line
	8500 3200 8600 3300
Entry Wire Line
	8500 3450 8600 3550
Entry Wire Line
	8500 3700 8600 3800
Entry Wire Line
	8500 4600 8600 4700
Entry Wire Line
	8500 4850 8600 4950
Entry Wire Line
	8500 5100 8600 5200
Entry Wire Line
	8500 5350 8600 5450
Entry Wire Line
	9900 1450 10000 1550
Entry Wire Line
	9900 1700 10000 1800
Entry Wire Line
	9900 1950 10000 2050
Entry Wire Line
	9900 2200 10000 2300
Entry Wire Line
	9900 3100 10000 3200
Entry Wire Line
	9900 3350 10000 3450
Entry Wire Line
	9900 3600 10000 3700
Entry Wire Line
	9900 3850 10000 3950
Entry Wire Line
	9900 4750 10000 4850
Entry Wire Line
	9900 5000 10000 5100
Entry Wire Line
	9900 5250 10000 5350
Entry Wire Line
	9900 5500 10000 5600
Wire Wire Line
	1650 2000 2100 2000
Wire Wire Line
	1650 2250 2100 2250
Wire Wire Line
	1650 3150 2100 3150
Wire Wire Line
	1650 3400 2100 3400
Wire Wire Line
	1650 3650 2100 3650
Wire Wire Line
	1650 3900 2100 3900
Wire Wire Line
	1650 4800 2100 4800
Wire Wire Line
	1650 5050 2100 5050
Wire Wire Line
	1650 5300 2100 5300
Wire Wire Line
	1650 5550 2100 5550
Wire Wire Line
	1800 1900 2100 1900
Wire Wire Line
	1800 2150 2100 2150
Wire Wire Line
	1800 3050 2100 3050
Wire Wire Line
	1800 3300 2100 3300
Wire Wire Line
	1800 3550 2100 3550
Wire Wire Line
	1800 3800 2100 3800
Wire Wire Line
	1800 4700 2100 4700
Wire Wire Line
	1800 4950 2100 4950
Wire Wire Line
	1800 5200 2100 5200
Wire Wire Line
	1800 5450 2100 5450
Wire Wire Line
	1900 2450 2100 2450
Wire Wire Line
	1900 2550 2100 2550
Wire Wire Line
	1900 4100 2100 4100
Wire Wire Line
	1900 4200 2100 4200
Wire Wire Line
	1900 5750 2100 5750
Wire Wire Line
	1900 5850 2100 5850
Wire Wire Line
	1900 6750 2100 6750
Wire Wire Line
	1900 7300 2100 7300
Wire Wire Line
	1950 1500 2100 1500
Wire Wire Line
	2100 1400 2100 1500
Wire Wire Line
	2100 1500 2100 1650
Wire Wire Line
	2100 1650 2100 1750
Wire Wire Line
	2700 6750 2900 6750
Wire Wire Line
	2700 7300 2900 7300
Wire Wire Line
	2800 1950 3100 1950
Wire Wire Line
	2800 2200 3100 2200
Wire Wire Line
	2800 3100 3100 3100
Wire Wire Line
	2800 3350 3100 3350
Wire Wire Line
	2800 3600 3100 3600
Wire Wire Line
	2800 3850 3100 3850
Wire Wire Line
	2800 4750 3100 4750
Wire Wire Line
	2800 5000 3100 5000
Wire Wire Line
	2800 5250 3100 5250
Wire Wire Line
	2800 5500 3100 5500
Wire Wire Line
	3850 1500 4300 1500
Wire Wire Line
	3850 1750 4300 1750
Wire Wire Line
	3850 2000 4300 2000
Wire Wire Line
	3850 2250 4300 2250
Wire Wire Line
	3850 3150 4300 3150
Wire Wire Line
	3850 3400 4300 3400
Wire Wire Line
	3850 3650 4300 3650
Wire Wire Line
	3850 3900 4300 3900
Wire Wire Line
	3850 4800 4300 4800
Wire Wire Line
	3850 5050 4300 5050
Wire Wire Line
	3850 5300 4300 5300
Wire Wire Line
	3850 5550 4300 5550
Wire Wire Line
	4000 1400 4300 1400
Wire Wire Line
	4000 1650 4300 1650
Wire Wire Line
	4000 1900 4300 1900
Wire Wire Line
	4000 2150 4300 2150
Wire Wire Line
	4000 3050 4300 3050
Wire Wire Line
	4000 3300 4300 3300
Wire Wire Line
	4000 3550 4300 3550
Wire Wire Line
	4000 3800 4300 3800
Wire Wire Line
	4000 4700 4300 4700
Wire Wire Line
	4000 4950 4300 4950
Wire Wire Line
	4000 5200 4300 5200
Wire Wire Line
	4000 5450 4300 5450
Wire Wire Line
	4100 2450 4300 2450
Wire Wire Line
	4100 2550 4300 2550
Wire Wire Line
	4100 4100 4300 4100
Wire Wire Line
	4100 4200 4300 4200
Wire Wire Line
	4100 5750 4300 5750
Wire Wire Line
	4100 5850 4300 5850
Wire Wire Line
	5000 1450 5300 1450
Wire Wire Line
	5000 1700 5300 1700
Wire Wire Line
	5000 1950 5300 1950
Wire Wire Line
	5000 2200 5300 2200
Wire Wire Line
	5000 3100 5300 3100
Wire Wire Line
	5000 3350 5300 3350
Wire Wire Line
	5000 3600 5300 3600
Wire Wire Line
	5000 3850 5300 3850
Wire Wire Line
	5000 4750 5300 4750
Wire Wire Line
	5000 5000 5300 5000
Wire Wire Line
	5000 5250 5300 5250
Wire Wire Line
	5000 5500 5300 5500
Wire Wire Line
	6250 2000 6700 2000
Wire Wire Line
	6250 2250 6700 2250
Wire Wire Line
	6250 3150 6700 3150
Wire Wire Line
	6250 3400 6700 3400
Wire Wire Line
	6250 3650 6700 3650
Wire Wire Line
	6250 3900 6700 3900
Wire Wire Line
	6250 4800 6700 4800
Wire Wire Line
	6250 5050 6700 5050
Wire Wire Line
	6250 5300 6700 5300
Wire Wire Line
	6250 5550 6700 5550
Wire Wire Line
	6400 1900 6700 1900
Wire Wire Line
	6400 2150 6700 2150
Wire Wire Line
	6400 3050 6700 3050
Wire Wire Line
	6400 3300 6700 3300
Wire Wire Line
	6400 3550 6700 3550
Wire Wire Line
	6400 3800 6700 3800
Wire Wire Line
	6400 4700 6700 4700
Wire Wire Line
	6400 4950 6700 4950
Wire Wire Line
	6400 5200 6700 5200
Wire Wire Line
	6400 5450 6700 5450
Wire Wire Line
	6500 2550 6700 2550
Wire Wire Line
	6500 2450 6700 2450
Wire Wire Line
	6500 4200 6700 4200
Wire Wire Line
	6500 4100 6700 4100
Wire Wire Line
	6500 5850 6700 5850
Wire Wire Line
	6500 5750 6700 5750
Wire Wire Line
	6550 1500 6700 1500
Wire Wire Line
	6700 1400 6700 1500
Wire Wire Line
	6700 1500 6700 1650
Wire Wire Line
	6700 1650 6700 1750
Wire Wire Line
	7400 1450 7700 1450
Wire Wire Line
	7400 1700 7700 1700
Wire Wire Line
	7400 1950 7700 1950
Wire Wire Line
	7400 2200 7700 2200
Wire Wire Line
	7400 3100 7700 3100
Wire Wire Line
	7400 3350 7700 3350
Wire Wire Line
	7400 3600 7700 3600
Wire Wire Line
	7400 3850 7700 3850
Wire Wire Line
	7400 4750 7700 4750
Wire Wire Line
	7400 5000 7700 5000
Wire Wire Line
	7400 5250 7700 5250
Wire Wire Line
	7400 5500 7700 5500
Wire Wire Line
	8450 1500 8900 1500
Wire Wire Line
	8450 1750 8900 1750
Wire Wire Line
	8450 2000 8900 2000
Wire Wire Line
	8450 2250 8900 2250
Wire Wire Line
	8450 3150 8900 3150
Wire Wire Line
	8450 3400 8900 3400
Wire Wire Line
	8450 3650 8900 3650
Wire Wire Line
	8450 3900 8900 3900
Wire Wire Line
	8450 4800 8900 4800
Wire Wire Line
	8450 5050 8900 5050
Wire Wire Line
	8450 5300 8900 5300
Wire Wire Line
	8450 5550 8900 5550
Wire Wire Line
	8600 1400 8900 1400
Wire Wire Line
	8600 1650 8900 1650
Wire Wire Line
	8600 1900 8900 1900
Wire Wire Line
	8600 2150 8900 2150
Wire Wire Line
	8600 3050 8900 3050
Wire Wire Line
	8600 3300 8900 3300
Wire Wire Line
	8600 3550 8900 3550
Wire Wire Line
	8600 3800 8900 3800
Wire Wire Line
	8600 4700 8900 4700
Wire Wire Line
	8600 4950 8900 4950
Wire Wire Line
	8600 5200 8900 5200
Wire Wire Line
	8600 5450 8900 5450
Wire Wire Line
	8700 2550 8900 2550
Wire Wire Line
	8700 2450 8900 2450
Wire Wire Line
	8700 4200 8900 4200
Wire Wire Line
	8700 4100 8900 4100
Wire Wire Line
	9600 1450 9900 1450
Wire Wire Line
	9600 1700 9900 1700
Wire Wire Line
	9600 1950 9900 1950
Wire Wire Line
	9600 2200 9900 2200
Wire Wire Line
	9600 3100 9900 3100
Wire Wire Line
	9600 3350 9900 3350
Wire Wire Line
	9600 3600 9900 3600
Wire Wire Line
	9600 3850 9900 3850
Wire Wire Line
	9600 4750 9900 4750
Wire Wire Line
	9600 5000 9900 5000
Wire Wire Line
	9600 5250 9900 5250
Wire Wire Line
	9600 5500 9900 5500
Wire Bus Line
	1250 750  1550 750 
Wire Bus Line
	1250 900  1700 900 
Wire Bus Line
	1700 900  3900 900 
Wire Bus Line
	3200 6300 5400 6300
Wire Bus Line
	3750 750  1550 750 
Wire Bus Line
	3750 750  6150 750 
Wire Bus Line
	3900 900  6300 900 
Wire Bus Line
	5400 6300 5650 6300
Wire Bus Line
	6300 900  8500 900 
Wire Bus Line
	7800 6300 10000 6300
Wire Bus Line
	8350 750  6150 750 
Wire Bus Line
	10000 6300 10250 6300
Text Label 2000 1900 2    50   ~ 0
A2
Text Label 2000 2000 2    50   ~ 0
B2
Text Label 2000 2150 2    50   ~ 0
A3
Text Label 2000 2250 2    50   ~ 0
B3
Text Label 2000 3050 2    50   ~ 0
A4
Text Label 2000 3150 2    50   ~ 0
B4
Text Label 2000 3300 2    50   ~ 0
A5
Text Label 2000 3400 2    50   ~ 0
B5
Text Label 2000 3550 2    50   ~ 0
A6
Text Label 2000 3650 2    50   ~ 0
B6
Text Label 2000 3800 2    50   ~ 0
A7
Text Label 2000 3900 2    50   ~ 0
B7
Text Label 2000 4700 2    50   ~ 0
A8
Text Label 2000 4800 2    50   ~ 0
B8
Text Label 2000 4950 2    50   ~ 0
A9
Text Label 2000 5050 2    50   ~ 0
B9
Text Label 2000 5200 2    50   ~ 0
A10
Text Label 2000 5300 2    50   ~ 0
B10
Text Label 2000 5450 2    50   ~ 0
A11
Text Label 2000 5550 2    50   ~ 0
B11
Text Label 2050 2450 2    50   ~ 0
AX
Text Label 2050 2550 2    50   ~ 0
BX
Text Label 2050 4100 2    50   ~ 0
AX
Text Label 2050 4200 2    50   ~ 0
BX
Text Label 2050 5750 2    50   ~ 0
AX
Text Label 2050 5850 2    50   ~ 0
BX
Text Label 2050 6750 2    50   ~ 0
AX
Text Label 2850 7300 2    50   ~ 0
AY
Text Label 2850 1950 0    50   ~ 0
X2
Text Label 2850 2200 0    50   ~ 0
X3
Text Label 2850 3100 0    50   ~ 0
X4
Text Label 2850 3350 0    50   ~ 0
X5
Text Label 2850 3600 0    50   ~ 0
X6
Text Label 2850 3850 0    50   ~ 0
X7
Text Label 2850 4750 0    50   ~ 0
X8
Text Label 2850 5000 0    50   ~ 0
X9
Text Label 2850 5250 0    50   ~ 0
X10
Text Label 2850 5500 0    50   ~ 0
X11
Text Label 2850 6750 2    50   ~ 0
BX
Text Label 2050 7300 2    50   ~ 0
BY
Text Label 4200 1400 2    50   ~ 0
A12
Text Label 4200 1500 2    50   ~ 0
B12
Text Label 4200 1650 2    50   ~ 0
A13
Text Label 4200 1750 2    50   ~ 0
B13
Text Label 4200 1900 2    50   ~ 0
A14
Text Label 4200 2000 2    50   ~ 0
B14
Text Label 4200 2150 2    50   ~ 0
A15
Text Label 4200 2250 2    50   ~ 0
B15
Text Label 4200 3050 2    50   ~ 0
A16
Text Label 4200 3150 2    50   ~ 0
B16
Text Label 4200 3300 2    50   ~ 0
A17
Text Label 4200 3400 2    50   ~ 0
B17
Text Label 4200 3550 2    50   ~ 0
A18
Text Label 4200 3650 2    50   ~ 0
B18
Text Label 4200 3800 2    50   ~ 0
A19
Text Label 4200 3900 2    50   ~ 0
B19
Text Label 4200 4700 2    50   ~ 0
A20
Text Label 4200 4800 2    50   ~ 0
B20
Text Label 4200 4950 2    50   ~ 0
A21
Text Label 4200 5050 2    50   ~ 0
B21
Text Label 4200 5200 2    50   ~ 0
A22
Text Label 4200 5300 2    50   ~ 0
B22
Text Label 4200 5450 2    50   ~ 0
A23
Text Label 4200 5550 2    50   ~ 0
B23
Text Label 4250 2450 2    50   ~ 0
AX
Text Label 4250 2550 2    50   ~ 0
BX
Text Label 4250 4100 2    50   ~ 0
AX
Text Label 4250 4200 2    50   ~ 0
BX
Text Label 4250 5750 2    50   ~ 0
AX
Text Label 4250 5850 2    50   ~ 0
BX
Text Label 5200 1450 2    50   ~ 0
X12
Text Label 5200 1700 2    50   ~ 0
X13
Text Label 5200 1950 2    50   ~ 0
X14
Text Label 5200 2200 2    50   ~ 0
X15
Text Label 5200 3100 2    50   ~ 0
X16
Text Label 5200 3350 2    50   ~ 0
X17
Text Label 5200 3600 2    50   ~ 0
X18
Text Label 5200 3850 2    50   ~ 0
X19
Text Label 5200 4750 2    50   ~ 0
X20
Text Label 5200 5000 2    50   ~ 0
X21
Text Label 5200 5250 2    50   ~ 0
X22
Text Label 5200 5500 2    50   ~ 0
X23
Text Label 6600 2250 2    50   ~ 0
B3
Text Label 6600 3150 2    50   ~ 0
A4
Text Label 6600 3050 2    50   ~ 0
B4
Text Label 6600 3400 2    50   ~ 0
A5
Text Label 6600 3300 2    50   ~ 0
B5
Text Label 6600 3650 2    50   ~ 0
A6
Text Label 6600 3550 2    50   ~ 0
B6
Text Label 6600 3900 2    50   ~ 0
A7
Text Label 6600 3800 2    50   ~ 0
B7
Text Label 6600 4800 2    50   ~ 0
A8
Text Label 6600 4700 2    50   ~ 0
B8
Text Label 6600 5050 2    50   ~ 0
A9
Text Label 6600 4950 2    50   ~ 0
B9
Text Label 6600 5300 2    50   ~ 0
A10
Text Label 6600 5200 2    50   ~ 0
B10
Text Label 6600 5550 2    50   ~ 0
A11
Text Label 6600 5450 2    50   ~ 0
B11
Text Label 6650 2550 2    50   ~ 0
AY
Text Label 6650 2450 2    50   ~ 0
BY
Text Label 6650 4200 2    50   ~ 0
AY
Text Label 6650 4100 2    50   ~ 0
BY
Text Label 6650 5850 2    50   ~ 0
AY
Text Label 6650 5750 2    50   ~ 0
BY
Text Label 7450 1450 0    50   ~ 0
Y0
Text Label 7450 1700 0    50   ~ 0
Y1
Text Label 7450 1950 0    50   ~ 0
Y2
Text Label 7450 2200 0    50   ~ 0
Y3
Text Label 7450 3100 0    50   ~ 0
Y4
Text Label 7450 3350 0    50   ~ 0
Y5
Text Label 7450 3600 0    50   ~ 0
Y6
Text Label 7450 3850 0    50   ~ 0
Y7
Text Label 7450 4750 0    50   ~ 0
Y8
Text Label 7450 5000 0    50   ~ 0
Y9
Text Label 7450 5250 0    50   ~ 0
Y10
Text Label 7450 5500 0    50   ~ 0
Y11
Text Label 8800 1500 2    50   ~ 0
A12
Text Label 8800 1400 2    50   ~ 0
B12
Text Label 8800 1750 2    50   ~ 0
A13
Text Label 8800 1650 2    50   ~ 0
B13
Text Label 8800 2000 2    50   ~ 0
A14
Text Label 8800 1900 2    50   ~ 0
B14
Text Label 8800 2250 2    50   ~ 0
A15
Text Label 8800 2150 2    50   ~ 0
B15
Text Label 8800 3150 2    50   ~ 0
A16
Text Label 8800 3050 2    50   ~ 0
B16
Text Label 8800 3400 2    50   ~ 0
A17
Text Label 8800 3300 2    50   ~ 0
B17
Text Label 8800 3650 2    50   ~ 0
A18
Text Label 8800 3550 2    50   ~ 0
B18
Text Label 8800 3900 2    50   ~ 0
A19
Text Label 8800 3800 2    50   ~ 0
B19
Text Label 8800 4800 2    50   ~ 0
A20
Text Label 8800 4700 2    50   ~ 0
B20
Text Label 8800 5050 2    50   ~ 0
A21
Text Label 8800 4950 2    50   ~ 0
B21
Text Label 8800 5300 2    50   ~ 0
A22
Text Label 8800 5200 2    50   ~ 0
B22
Text Label 8800 5550 2    50   ~ 0
A23
Text Label 8800 5450 2    50   ~ 0
B23
Text Label 8850 2550 2    50   ~ 0
AY
Text Label 8850 2450 2    50   ~ 0
BY
Text Label 8850 4200 2    50   ~ 0
AY
Text Label 8850 4100 2    50   ~ 0
BY
Text Label 9800 1450 2    50   ~ 0
Y12
Text Label 9800 1700 2    50   ~ 0
Y13
Text Label 9800 1950 2    50   ~ 0
Y14
Text Label 9800 2200 2    50   ~ 0
Y15
Text Label 9800 3100 2    50   ~ 0
Y16
Text Label 9800 3350 2    50   ~ 0
Y17
Text Label 9800 3600 2    50   ~ 0
Y18
Text Label 9800 3850 2    50   ~ 0
Y19
Text Label 9800 4750 2    50   ~ 0
Y20
Text Label 9800 5000 2    50   ~ 0
Y21
Text Label 9800 5250 2    50   ~ 0
Y22
Text Label 9800 5500 2    50   ~ 0
Y23
Text HLabel 1250 750  0    50   Input ~ 0
A[2..23]
Text HLabel 1250 900  0    50   Input ~ 0
B[2..23]
Text HLabel 1900 6750 0    50   Input ~ 0
AX~BX
Text HLabel 1900 7300 0    50   Input ~ 0
~AY~BY
Text HLabel 5650 6300 2    50   Output ~ 0
X[2..23]
Text HLabel 10250 6300 2    50   Output ~ 0
Y[2..23]
$Comp
L power:GND #PWR?
U 1 1 5EDBC4D9
P 1950 1500
F 0 "#PWR?" H 1950 1250 50  0001 C CNN
F 1 "GND" H 1955 1327 50  0000 C CNN
F 2 "" H 1950 1500 50  0001 C CNN
F 3 "" H 1950 1500 50  0001 C CNN
	1    1950 1500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EDBD9DA
P 6550 1500
F 0 "#PWR?" H 6550 1250 50  0001 C CNN
F 1 "GND" H 6555 1327 50  0000 C CNN
F 2 "" H 6550 1500 50  0001 C CNN
F 3 "" H 6550 1500 50  0001 C CNN
	1    6550 1500
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:V2B U20R76
U 1 1 5F8FD5CD
P 2400 6750
F 0 "U20R76" H 2400 7067 50  0000 C CNN
F 1 "V2B" H 2400 6976 50  0000 C CNN
F 2 "" H 2400 6550 50  0001 C CNN
F 3 "" H 2400 6550 50  0001 C CNN
	1    2400 6750
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:V2B U20R86
U 1 1 5EB8FA69
P 2400 7300
F 0 "U20R86" H 2400 7617 50  0000 C CNN
F 1 "V2B" H 2400 7526 50  0000 C CNN
F 2 "" H 2400 7100 50  0001 C CNN
F 3 "" H 2400 7100 50  0001 C CNN
	1    2400 7300
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:P24 U30R24
U 1 1 5EB6964C
P 2450 1950
F 0 "U30R24" H 2450 2765 50  0000 C CNN
F 1 "P24" H 2450 2674 50  0000 C CNN
F 2 "" H 2150 2400 50  0001 C CNN
F 3 "" H 2150 2400 50  0001 C CNN
	1    2450 1950
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:P24 U16R33
U 1 1 5EBA4FFB
P 2450 3600
F 0 "U16R33" H 2450 4415 50  0000 C CNN
F 1 "P24" H 2450 4324 50  0000 C CNN
F 2 "" H 2150 4050 50  0001 C CNN
F 3 "" H 2150 4050 50  0001 C CNN
	1    2450 3600
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:P24 U8R43
U 1 1 5ED62530
P 2450 5250
F 0 "U8R43" H 2450 6065 50  0000 C CNN
F 1 "P24" H 2450 5974 50  0000 C CNN
F 2 "" H 2150 5700 50  0001 C CNN
F 3 "" H 2150 5700 50  0001 C CNN
	1    2450 5250
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:P24 U20R100
U 1 1 5EDC78CD
P 4650 1950
F 0 "U20R100" H 4650 2765 50  0000 C CNN
F 1 "P24" H 4650 2674 50  0000 C CNN
F 2 "" H 4350 2400 50  0001 C CNN
F 3 "" H 4350 2400 50  0001 C CNN
	1    4650 1950
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:P24 U30R71
U 1 1 5EE67E8D
P 4650 3600
F 0 "U30R71" H 4650 4415 50  0000 C CNN
F 1 "P24" H 4650 4324 50  0000 C CNN
F 2 "" H 4350 4050 50  0001 C CNN
F 3 "" H 4350 4050 50  0001 C CNN
	1    4650 3600
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:P24 U30R81
U 1 1 5F2C1553
P 4650 5250
F 0 "U30R81" H 4650 6065 50  0000 C CNN
F 1 "P24" H 4650 5974 50  0000 C CNN
F 2 "" H 4350 5700 50  0001 C CNN
F 3 "" H 4350 5700 50  0001 C CNN
	1    4650 5250
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:P24 U16R62
U 1 1 5EB6E98E
P 7050 1950
F 0 "U16R62" H 7050 2765 50  0000 C CNN
F 1 "P24" H 7050 2674 50  0000 C CNN
F 2 "" H 6750 2400 50  0001 C CNN
F 3 "" H 6750 2400 50  0001 C CNN
	1    7050 1950
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:P24 U16R43
U 1 1 5EBA4103
P 7050 3600
F 0 "U16R43" H 7050 4415 50  0000 C CNN
F 1 "P24" H 7050 4324 50  0000 C CNN
F 2 "" H 6750 4050 50  0001 C CNN
F 3 "" H 6750 4050 50  0001 C CNN
	1    7050 3600
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:P24 U8R81
U 1 1 5ED62536
P 7050 5250
F 0 "U8R81" H 7050 6065 50  0000 C CNN
F 1 "P24" H 7050 5974 50  0000 C CNN
F 2 "" H 6750 5700 50  0001 C CNN
F 3 "" H 6750 5700 50  0001 C CNN
	1    7050 5250
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:P24 U16R100
U 1 1 5EDC78D3
P 9250 1950
F 0 "U16R100" H 9250 2765 50  0000 C CNN
F 1 "P24" H 9250 2674 50  0000 C CNN
F 2 "" H 8950 2400 50  0001 C CNN
F 3 "" H 8950 2400 50  0001 C CNN
	1    9250 1950
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:P24 U30R43
U 1 1 5EE67E93
P 9250 3600
F 0 "U30R43" H 9250 4415 50  0000 C CNN
F 1 "P24" H 9250 4324 50  0000 C CNN
F 2 "" H 8950 4050 50  0001 C CNN
F 3 "" H 8950 4050 50  0001 C CNN
	1    9250 3600
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:P24 U16R81
U 1 1 5F2C1559
P 9250 5250
F 0 "U16R81" H 9250 6065 50  0000 C CNN
F 1 "P24" H 9250 5974 50  0000 C CNN
F 2 "" H 8950 5700 50  0001 C CNN
F 3 "" H 8950 5700 50  0001 C CNN
	1    9250 5250
	1    0    0    -1  
$EndComp
Text Label 6600 2150 2    50   ~ 0
A3
Text Label 6600 1900 2    50   ~ 0
A2
Text Label 6600 2000 2    50   ~ 0
B2
Text Label 8850 5750 2    50   ~ 0
BY
Text Label 8850 5850 2    50   ~ 0
AY
Wire Wire Line
	8700 5750 8900 5750
Wire Wire Line
	8700 5850 8900 5850
Wire Bus Line
	6300 900  6300 5350
Wire Bus Line
	6150 750  6150 5450
Wire Bus Line
	3200 2050 3200 6300
Wire Bus Line
	1700 900  1700 5350
Wire Bus Line
	1550 750  1550 5450
Wire Bus Line
	3750 750  3750 5450
Wire Bus Line
	3900 900  3900 5350
Wire Bus Line
	5400 1550 5400 6300
Wire Bus Line
	7800 1550 7800 6300
Wire Bus Line
	8350 750  8350 5450
Wire Bus Line
	8500 900  8500 5350
Wire Bus Line
	10000 1550 10000 6300
$EndSCHEMATC
