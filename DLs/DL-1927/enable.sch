EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 8
Title ""
Date "2020-05-05"
Rev ""
Comp ""
Comment1 ""
Comment2 "Licence: CC-BY"
Comment3 "Author: Loic *WydD* Petit"
Comment4 "Project: CPS2 Reverse Engineering: DL-1927"
$EndDescr
Connection ~ 4950 3600
Connection ~ 5900 5200
Connection ~ 5900 5750
Connection ~ 6000 1700
Connection ~ 4950 1150
Connection ~ 5050 5650
Connection ~ 6800 3750
Wire Wire Line
	4550 3600 4950 3600
Wire Wire Line
	4950 1150 4950 1700
Wire Wire Line
	4950 3600 4950 5750
Wire Wire Line
	4950 3600 5050 3600
Wire Wire Line
	5050 5650 4550 5650
Wire Wire Line
	5050 5650 5050 5100
Wire Wire Line
	5050 5650 6000 5650
Wire Wire Line
	5550 1150 6100 1150
Wire Wire Line
	5550 1700 6000 1700
Wire Wire Line
	5650 5100 6000 5100
Wire Wire Line
	5900 4350 5650 4350
Wire Wire Line
	5900 4450 5900 5200
Wire Wire Line
	5900 5200 5900 5750
Wire Wire Line
	5900 5750 4950 5750
Wire Wire Line
	5900 5750 6000 5750
Wire Wire Line
	6000 1500 6000 1700
Wire Wire Line
	6000 1700 7000 1700
Wire Wire Line
	6000 3600 5650 3600
Wire Wire Line
	6000 3700 5900 3700
Wire Wire Line
	6000 4350 5900 4350
Wire Wire Line
	6000 4450 5900 4450
Wire Wire Line
	6000 5200 5900 5200
Wire Wire Line
	6100 1500 6000 1500
Wire Wire Line
	6550 5150 7150 5150
Wire Wire Line
	6550 5700 7150 5700
Wire Wire Line
	6800 2000 7000 2000
Wire Wire Line
	6800 2850 6550 2850
Wire Wire Line
	6800 3750 6800 2850
Wire Wire Line
	6800 3750 6900 3750
Wire Wire Line
	6900 3650 6550 3650
Wire Wire Line
	6900 4300 6800 4300
Wire Wire Line
	6900 4400 6550 4400
Wire Wire Line
	7000 1800 7000 2000
Wire Wire Line
	7550 1750 7650 1750
Text Label 6600 5150 0    50   ~ 0
~AY~BY
Text Label 6600 5700 0    50   ~ 0
AX~BX
Text HLabel 1850 3600 0    50   Input ~ 0
_BUS-W
Text HLabel 2450 3600 2    50   Output ~ 0
BUS-W
Text HLabel 3950 3600 0    50   Input ~ 0
SELECT
Text HLabel 3950 5650 0    50   Input ~ 0
ENABLE-SELECT
Text HLabel 4350 1150 0    50   Input ~ 0
ENABLE
Text HLabel 5600 2000 0    50   Input ~ 0
_~ENABLE-OUT
Text HLabel 5950 2850 0    50   Input ~ 0
~ENABLE-EXT
Text HLabel 6100 1150 2    50   Output ~ 0
~ENABLE-A01-IN
Text HLabel 6100 1500 2    50   Output ~ 0
~ENABLE-BUS-IN
Text HLabel 7150 5150 2    50   Output ~ 0
~AY~BY
Text HLabel 7150 5700 2    50   Output ~ 0
AX~BX
Text HLabel 8050 3700 2    50   3State ~ 0
EXT-BUS
Text HLabel 8050 4350 2    50   3State ~ 0
EXT-A01
$Comp
L CG24-cells:N2P U13R85
U 1 1 5F676367
P 6250 3650
F 0 "U13R85" H 6275 3917 50  0000 C CNN
F 1 "N2P" H 6275 3826 50  0000 C CNN
F 2 "" H 6250 3650 50  0001 C CNN
F 3 "" H 6250 3650 50  0001 C CNN
	1    6250 3650
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:N2P U12R85
U 1 1 5F79ED51
P 6250 4400
F 0 "U12R85" H 6275 4667 50  0000 C CNN
F 1 "N2P" H 6275 4576 50  0000 C CNN
F 2 "" H 6250 4400 50  0001 C CNN
F 3 "" H 6250 4400 50  0001 C CNN
	1    6250 4400
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:R2P U14R94
U 1 1 5EB90C2F
P 6250 5150
F 0 "U14R94" H 6275 5417 50  0000 C CNN
F 1 "R2P" H 6275 5326 50  0000 C CNN
F 2 "" H 6300 5150 50  0001 C CNN
F 3 "" H 6300 5150 50  0001 C CNN
	1    6250 5150
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:N2P U12R94
U 1 1 5F5FFC56
P 6250 5700
F 0 "U12R94" H 6275 5967 50  0000 C CNN
F 1 "N2P" H 6275 5876 50  0000 C CNN
F 2 "" H 6250 5700 50  0001 C CNN
F 3 "" H 6250 5700 50  0001 C CNN
	1    6250 5700
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:R2P U12R18
U 1 1 5FDAEF15
P 7150 3700
F 0 "U12R18" H 7175 3967 50  0000 C CNN
F 1 "R2P" H 7175 3876 50  0000 C CNN
F 2 "" H 7200 3700 50  0001 C CNN
F 3 "" H 7200 3700 50  0001 C CNN
	1    7150 3700
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:R2P U12R37
U 1 1 5FDEA435
P 7150 4350
F 0 "U12R37" H 7175 4617 50  0000 C CNN
F 1 "R2P" H 7175 4526 50  0000 C CNN
F 2 "" H 7200 4350 50  0001 C CNN
F 3 "" H 7200 4350 50  0001 C CNN
	1    7150 4350
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:N2N U6R29
U 1 1 60107975
P 7250 1750
F 0 "U6R29" H 7275 2017 50  0000 C CNN
F 1 "N2N" H 7275 1926 50  0000 C CNN
F 2 "" H 7250 1750 50  0001 C CNN
F 3 "" H 7250 1750 50  0001 C CNN
	1    7250 1750
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:I2S U2R0
U 1 1 5EAF1C7D
P 4650 1150
F 0 "U2R0" H 4650 1467 50  0000 C CNN
F 1 "I2S" H 4650 1376 50  0000 C CNN
F 2 "" H 4650 1150 50  0001 C CNN
F 3 "" H 4650 1150 50  0001 C CNN
	1    4650 1150
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:V2B U4R9
U 1 1 600A8C18
P 5250 1150
F 0 "U4R9" H 5250 1467 50  0000 C CNN
F 1 "V2B" H 5250 1376 50  0000 C CNN
F 2 "" H 5250 950 50  0001 C CNN
F 3 "" H 5250 950 50  0001 C CNN
	1    5250 1150
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:V2B U4R19
U 1 1 6010704E
P 5250 1700
F 0 "U4R19" H 5250 2017 50  0000 C CNN
F 1 "V2B" H 5250 1926 50  0000 C CNN
F 2 "" H 5250 1500 50  0001 C CNN
F 3 "" H 5250 1500 50  0001 C CNN
	1    5250 1700
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:V1N U12R84
U 1 1 5F73B0AD
P 5350 3600
F 0 "U12R84" H 5350 3917 50  0000 C CNN
F 1 "V1N" H 5350 3826 50  0000 C CNN
F 2 "" H 5350 3400 50  0001 C CNN
F 3 "" H 5350 3400 50  0001 C CNN
	1    5350 3600
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:V1N U14R86
U 1 1 5F65E4A7
P 5350 4350
F 0 "U14R86" H 5350 4667 50  0000 C CNN
F 1 "V1N" H 5350 4576 50  0000 C CNN
F 2 "" H 5350 4150 50  0001 C CNN
F 3 "" H 5350 4150 50  0001 C CNN
	1    5350 4350
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:V1N U15R95
U 1 1 5EB96255
P 5350 5100
F 0 "U15R95" H 5350 5417 50  0000 C CNN
F 1 "V1N" H 5350 5326 50  0000 C CNN
F 2 "" H 5350 4900 50  0001 C CNN
F 3 "" H 5350 4900 50  0001 C CNN
	1    5350 5100
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:I2R U14R0
U 1 1 5EAF1C7C
P 5900 2000
F 0 "U14R0" H 5900 2317 50  0000 C CNN
F 1 "I2R" H 5900 2226 50  0000 C CNN
F 2 "" H 5900 2000 50  0001 C CNN
F 3 "" H 5900 2000 50  0001 C CNN
	1    5900 2000
	1    0    0    1   
$EndComp
$Comp
L CG24-cells:V1N U8R19
U 1 1 60187745
P 6500 2000
F 0 "U8R19" H 6500 1683 50  0000 C CNN
F 1 "V1N" H 6500 1774 50  0000 C CNN
F 2 "" H 6500 1800 50  0001 C CNN
F 3 "" H 6500 1800 50  0001 C CNN
	1    6500 2000
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:B11 U10R0
U 1 1 5EAF1C45
P 2150 3600
F 0 "U10R0" H 2150 3917 50  0000 C CNN
F 1 "B11" H 2150 3826 50  0000 C CNN
F 2 "" H 2150 3600 50  0001 C CNN
F 3 "" H 2150 3600 50  0001 C CNN
	1    2150 3600
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:B12 U12R0
U 1 1 5EAF1C65
P 4250 3600
F 0 "U12R0" H 4250 3917 50  0000 C CNN
F 1 "B12" H 4250 3826 50  0000 C CNN
F 2 "" H 4250 3600 50  0001 C CNN
F 3 "" H 4250 3600 50  0001 C CNN
	1    4250 3600
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:B12 U8R0
U 1 1 5EAF1C68
P 4250 5650
F 0 "U8R0" H 4250 5967 50  0000 C CNN
F 1 "B12" H 4250 5876 50  0000 C CNN
F 2 "" H 4250 5650 50  0001 C CNN
F 3 "" H 4250 5650 50  0001 C CNN
	1    4250 5650
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:B12 U4R2
U 1 1 5EAF1C5E
P 6250 2850
F 0 "U4R2" H 6250 3167 50  0000 C CNN
F 1 "B12" H 6250 3076 50  0000 C CNN
F 2 "" H 6250 2850 50  0001 C CNN
F 3 "" H 6250 2850 50  0001 C CNN
	1    6250 2850
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:O4R U18R2
U 1 1 5EAF1CAF
P 7750 3700
F 0 "U18R2" H 7750 4017 50  0000 C CNN
F 1 "O4R" H 7750 3926 50  0000 C CNN
F 2 "" H 7750 3700 50  0001 C CNN
F 3 "" H 7750 3700 50  0001 C CNN
	1    7750 3700
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:O4R U22R0
U 1 1 5EAF1CAB
P 7750 4350
F 0 "U22R0" H 7750 4667 50  0000 C CNN
F 1 "O4R" H 7750 4576 50  0000 C CNN
F 2 "" H 7750 4350 50  0001 C CNN
F 3 "" H 7750 4350 50  0001 C CNN
	1    7750 4350
	1    0    0    -1  
$EndComp
Connection ~ 5050 5100
Wire Wire Line
	5050 4350 5050 5100
Connection ~ 5900 4350
Wire Wire Line
	5900 3700 5900 4350
Wire Wire Line
	6800 3750 6800 4300
Text Label 5600 1150 0    50   ~ 0
~E1
Text Label 5600 1700 0    50   ~ 0
~E2
Text Label 6400 3100 0    50   ~ 0
~E1
Wire Wire Line
	6400 3100 6250 3100
Text Label 4400 5900 0    50   ~ 0
~E1
Wire Wire Line
	4400 5900 4250 5900
Text Label 4400 3850 0    50   ~ 0
~E2
Wire Wire Line
	4400 3850 4250 3850
Text Label 2300 3850 0    50   ~ 0
~E2
Wire Wire Line
	2300 3850 2150 3850
$Comp
L CG24-cells:V2B U6R?
U 1 1 5EBBDE35
P 7950 1750
AR Path="/5EB464DD/5EBBDE35" Ref="U6R?"  Part="1" 
AR Path="/5EE15478/5EBBDE35" Ref="U6R38"  Part="1" 
F 0 "U6R38" H 7950 2067 50  0000 C CNN
F 1 "V2B" H 7950 1976 50  0000 C CNN
F 2 "" H 7950 1550 50  0001 C CNN
F 3 "" H 7950 1550 50  0001 C CNN
	1    7950 1750
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:V2B U8R?
U 1 1 5EBC3420
P 7950 1150
AR Path="/5EC133CE/5EBC3420" Ref="U8R?"  Part="1" 
AR Path="/5EE15478/5EBC3420" Ref="U8R38"  Part="1" 
F 0 "U8R38" H 7950 1467 50  0000 C CNN
F 1 "V2B" H 7950 1376 50  0000 C CNN
F 2 "" H 7950 950 50  0001 C CNN
F 3 "" H 7950 950 50  0001 C CNN
	1    7950 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 1150 7550 1150
Wire Wire Line
	7550 1150 7550 1750
Connection ~ 7550 1750
Wire Wire Line
	8250 1750 8450 1750
Text HLabel 8450 1150 2    50   Output ~ 0
~ENABLE-A-OUT
Wire Wire Line
	8250 1150 8450 1150
Text HLabel 8450 1750 2    50   Output ~ 0
~ENABLE-B-OUT
Text Label 8300 1750 0    50   ~ 0
~O2
Text Label 7950 4600 0    50   ~ 0
~O2
Wire Wire Line
	7950 4600 7750 4600
Text Label 8300 1150 0    50   ~ 0
~O1
Text Label 7950 3950 0    50   ~ 0
~O1
Wire Wire Line
	7950 3950 7750 3950
$EndSCHEMATC
